package com.regal.utility.awesome_navigation;

import com.runemate.game.api.hybrid.player_sense.PlayerSense;
import com.runemate.game.api.hybrid.util.calculations.Random;

import java.util.function.Supplier;

//Code provided by RuneMate member Savior
//https://www.runemate.com/community/members/savior.777/
//https://www.runemate.com/community/threads/use-custom-playersense-keys-in-your-bots-to-simulate-different-playstyles-across-your-userbase.11527/
public class PathingPlayerSense {
    public static void initializeKeys() {
        for (Key key : Key.values()) {
            if (PlayerSense.get(key.name) == null) {
                PlayerSense.put(key.name, key.supplier.get());
            }
        }
    }


    public enum Key {
        RUN_REENABLE_THRESHOLD_HIGH("regal_low_run_reenable_HIGH", ()-> Random.nextInt(35, 45)),
        REGAL_DISTANCE_TO_WALK("regal_distance_to_walk", ()->Random.nextInt(5, 8)),
        REGAL_ACCEPTABLE_VISIBILITY("regal_acceptable_visibility", ()->Random.nextDouble(.45, .7)),
        AWESOME_WEB_DISTANCE_WALK("awesome_web_distance_walk", () -> Random.nextInt(4, 8)),
        AWESOME_STAMINA_ENERGY_TRIGGER("awesome_stamina_energy_trigger", () -> Random.nextInt(50, 70)),
        ;

        private final String name;
        private final Supplier supplier;

        Key(String name, Supplier supplier) {
            this.name = name;
            this.supplier = supplier;
        }

        public String getKey() {
            return name;
        }

        public Integer getAsInteger() {
            return PlayerSense.getAsInteger(name);
        }

        public Double getAsDouble() {
            return PlayerSense.getAsDouble(name);
        }

        public Long getAsLong() {
            return PlayerSense.getAsLong(name);
        }

        public Boolean getAsBoolean() {
            return PlayerSense.getAsBoolean(name);
        }
    }
}