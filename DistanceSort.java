package com.regal.utility.awesome_navigation;

import com.regal.regal_bot.RegalBot;
import com.regal.utility.awesome_navigation.vertices.Vertex;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.LocatableEntity;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.entities.details.Locatable;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.results.LocatableEntityQueryResults;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.region.Region;
import com.runemate.game.api.hybrid.util.calculations.Distance;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DistanceSort {

    public static LocatableEntityQueryResults SortByActualDistance(LocatableEntityQueryResults locatableEntityQueryResults) {
        WeightedGraph weightedGraph = new WeightedGraph(true);
        AwesomePathGenerator generator = new AwesomePathGenerator();
        Player player = Players.getLocal();
            if(player != null) {
                locatableEntityQueryResults.sort(Comparator.comparingInt(value -> {
                    if(value == null) {
                        return Integer.MAX_VALUE;
                    }
                    List<Vertex> path;
                    if((path = generator.generatePath(weightedGraph, player.getPosition(), ((LocatableEntity)value).getPosition())) != null) {
                        return path.size();
                    }
                    return Integer.MAX_VALUE;
                }));
            }
        return locatableEntityQueryResults;
    }

    public static int getDistanceTo(Locatable monster) {
        //WeightedGraph weightedGraph = new WeightedGraph(true);
        //AwesomePathGenerator generator = new AwesomePathGenerator();
        RegalBot bot = (RegalBot) Environment.getBot();
        Player player = Players.getLocal();
        if (player != null) {
            if (monster == null) {
                return Integer.MAX_VALUE;
            }
            if (bot != null) {
                int cost = bot.getPathing().getDistanceBetween(player.getPosition(), monster.getPosition());
                System.out.println("DISTANCE TO: " + monster + " is: " + cost);
                return cost;
            }
        }
        return Integer.MAX_VALUE;
    }

    public static List<Coordinate> getCoordinatesWithinDistance(int distance) {
        WeightedGraph graph = new WeightedGraph(true);
        AwesomePathGenerator generator = new AwesomePathGenerator();
        List<Coordinate> coors = Region.getArea().getCoordinates();
        Player player = Players.getLocal();
        if (player != null) {
            Coordinate pos = player.getPosition();
            if (pos != null) {
                LinkedHashMap<Integer, Vertex> reachable = generator.getReachableVertices(graph.getGraph(), pos, false);
                coors.removeIf(coordinateIntegerEntry -> !reachable.containsKey(coordinateIntegerEntry.hashCode()) || coordinateIntegerEntry.distanceTo(pos, Distance.Algorithm.CHEBYSHEV) <= distance);
                coors.sort(Comparator.comparingDouble(coordinate -> coordinate.distanceTo(pos)));
            }
        }
        return coors;
    }

    public static List<Coordinate> getCoordinatesWithinDistance(int distance, Area from) {
        WeightedGraph graph = new WeightedGraph(true);
        AwesomePathGenerator generator = new AwesomePathGenerator();
        List<Coordinate> coors = Region.getArea().getCoordinates();
        LinkedHashMap<Integer, Vertex> reachable = generator.getReachableVertices(graph.getGraph(), from.getCenter(), false);
        coors.removeIf(coordinateIntegerEntry -> !reachable.containsKey(coordinateIntegerEntry.hashCode()) || from.getCoordinates().stream().mapToDouble(coor -> coordinateIntegerEntry.distanceTo(coor, Distance.Algorithm.CHEBYSHEV)).min().orElse(1) <= distance);
        Player player = Players.getLocal();
        if (player != null) {
            coors.sort(Comparator.comparingDouble(coordinate -> {
                double dist = coordinate.distanceTo(player);
                try {
                    return (dist > 0) ? dist : Double.MAX_VALUE;
                } catch (Exception ex) {
                    System.out.println(dist);
                    ex.printStackTrace();
                    return Double.MAX_VALUE;
                }
            }));
        }
        return coors;
    }

    public static List<Coordinate> getCoordinatesMinDistanceFrom(LocatableEntityQueryResults<Npc> npcs, int minDistance) {
        List<Coordinate> coors = Region.getArea().getCoordinates();
        Player player = Players.getLocal();
        if (player != null) {
            coors.removeIf(coordinateIntegerEntry -> npcs.stream().allMatch(npc -> coordinateIntegerEntry.distanceTo(npc) > minDistance));
            coors = coors.stream().sorted(Comparator.comparingDouble(coordinate -> coordinate.distanceTo(player.getPosition(), Distance.Algorithm.MANHATTAN))).collect(Collectors.toList());
        }
        return coors;
    }

    public static List<Coordinate> getSortedCoordinates(Area area, Locatable locatable) {
        List<Coordinate> coors = area.getCoordinates();
        coors = coors.stream().sorted(Comparator.comparingDouble(coordinate -> coordinate.distanceTo(locatable, Distance.Algorithm.CHEBYSHEV))).collect(Collectors.toList());

        return coors;
    }

    public static int getMinDistanceTo(LocatableEntityQueryResults<Npc> spawns) {
        Player player = Players.getLocal();
        if(player != null) {
            return spawns.stream().mapToInt(spawn -> (int) spawn.distanceTo(player, Distance.Algorithm.CHEBYSHEV)).min().orElse(Integer.MAX_VALUE);
        }
        return Integer.MAX_VALUE;
    }

    public static Coordinate getSafespotAwayFrom(LocatableEntityQueryResults<LocatableEntity> results, Coordinate[] safespots) {
        List<Coordinate> allCoors = results.stream().flatMap(npc -> {
            if (npc != null && npc.getArea() != null) {
                return npc.getArea().getCoordinates().stream();
            }
            return Stream.empty();
        }).collect(Collectors.toList());
        return getSafespotAwayFrom(allCoors, safespots, null);
    }

    public static Coordinate getSafespotAwayFrom(List<Coordinate> results, Coordinate[] safespots, Area.Rectangular shamanArea) {
        if(shamanArea != null) {
            return Arrays.stream(safespots).filter(coordinate -> shamanArea.getCoordinates().stream().mapToDouble(shamanCoor -> coordinate.distanceTo(shamanCoor, Distance.Algorithm.CHEBYSHEV)).min().orElse(Integer.MAX_VALUE) <= 5).filter(coordinate -> !results.contains(coordinate))
                    .findAny().orElse(null);
        }
        return Arrays.stream(safespots).filter(coordinate -> !results.contains(coordinate))
                .findFirst().orElse(null);
    }

    public static List<Coordinate> getSafespotsAwayFrom(List<Area> monsterAreas, Coordinate[] safespots, Area.Rectangular shamanArea) {
        if(shamanArea != null) {
            return Arrays.stream(safespots).filter(safespot -> shamanArea.getCoordinates().stream().mapToDouble(shamanCoor -> safespot.distanceTo(shamanCoor, Distance.Algorithm.CHEBYSHEV)).min().orElse(Integer.MAX_VALUE) <= 5)
                    .filter(safespot -> monsterAreas.stream().noneMatch(area -> area.getCoordinates().stream().anyMatch(coordinate -> safespot.distanceTo(coordinate, Distance.Algorithm.CHEBYSHEV) < 2))).collect(Collectors.toList());
        }
        return Arrays.stream(safespots).filter(safespot -> monsterAreas.stream().noneMatch(area -> area.getCoordinates().stream().anyMatch(coordinate -> safespot.distanceTo(coordinate, Distance.Algorithm.CHEBYSHEV) < 2))).collect(Collectors.toList());
    }
}
