package com.regal.utility.awesome_navigation;

import com.regal.utility.awesome_navigation.edges.Edge;
import com.regal.utility.awesome_navigation.edges.RequirementEdges;
import com.regal.utility.awesome_navigation.requirements.Requirement;
import com.regal.utility.awesome_navigation.vertices.CoordinateVertex;
import com.regal.utility.awesome_navigation.vertices.GameObjectVertices;
import com.regal.utility.awesome_navigation.vertices.NpcVertices;
import com.regal.utility.awesome_navigation.vertices.Vertex;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.region.Region;
import com.runemate.game.api.hybrid.util.Resources;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.List;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static com.runemate.game.api.hybrid.Environment.getLogger;

public class WeightedGraph implements Externalizable {

    private LinkedHashMap<Integer, Vertex> g1;
    private transient Graphics2D graphics2D;
    private Coordinate prevPosition;

    public WeightedGraph() {
        g1 = new LinkedHashMap<>();
        addRegion();
        getLogger().debug("Finished Graph Creation...");
    }

    public WeightedGraph(boolean addLocalRegion) {
        getLogger().debug("Building Navigation Graph...");
        g1 = new LinkedHashMap<>();
        if(addLocalRegion) {
            addRegion();
            Vertex[] vals = g1.values().toArray(new Vertex[0]);
            for (int i = 0; i < g1.size(); i++) {
                handleEdgeAdding(vals, i);
                if (vals[i].getEdges() == null) {
                    vals[i].createEdgeList();
                }
            }
        }
        getLogger().debug("Finished Graph Creation...");
    }

    public WeightedGraph(LinkedHashMap<Integer, Vertex> coordinatePathVertices) {
        this.g1 = coordinatePathVertices;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(g1);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        g1 = (LinkedHashMap<Integer, Vertex>) in.readObject();
    }

    /**
     * Inner class used in case serialization is different than that which was original.
     * Use this only to load old graph and save as new... Not every time.
     */
    private class ObjectInputStreamReader extends ObjectInputStream {

        public ObjectInputStreamReader(InputStream in) throws IOException {
            super(in);
        }

        @Override
        protected java.io.ObjectStreamClass readClassDescriptor()
                throws IOException, ClassNotFoundException {
            ObjectStreamClass desc = super.readClassDescriptor();
            //Check if the original class matches old... If so, load the new object. (For both)
            if (desc.getName().equals("com.regal.utility.regal_path.CoordinateVertex")) {
                ObjectStreamClass localClassDescriptor = ObjectStreamClass.lookup(CoordinateVertex.class);
                if (localClassDescriptor != null) { // only if class implements serializable
                    final long localSUID = localClassDescriptor.getSerialVersionUID();
                    final long streamSUID = desc.getSerialVersionUID();
                    if (streamSUID != localSUID) { // check for serialVersionUID mismatch.
                        final StringBuffer s = new StringBuffer("Overriding serialized class version mismatch: ");
                        s.append("local serialVersionUID = ").append(localSUID);
                        s.append(" stream serialVersionUID = ").append(streamSUID);
                        desc = localClassDescriptor; // Use local class descriptor for deserialization
                    }
                }
                return desc;
            } else if (desc.getName().equals("com.runemate.game.api.hybrid.location.Coordinate")) {
                ObjectStreamClass localClassDescriptor = ObjectStreamClass.lookup(Coordinate.class);
                if (localClassDescriptor != null) { // only if class implements serializable
                    final long localSUID = localClassDescriptor.getSerialVersionUID();
                    final long streamSUID = desc.getSerialVersionUID();
                    if (streamSUID != localSUID) { // check for serialVersionUID mismatch.
                        final StringBuffer s = new StringBuffer("Overriding serialized class version mismatch: ");
                        s.append("local serialVersionUID = ").append(localSUID);
                        s.append(" stream serialVersionUID = ").append(streamSUID);
                        desc = localClassDescriptor; // Use local class descriptor for deserialization
                    }
                }
            }
            return desc;
        }
    }

    public void merge() {
        if(g1 == null) {
            g1 = new LinkedHashMap<>();
        }
        getLogger().debug("Graph Size Before Merge: " + g1.size());
        getLogger().info("Merging in .nav file...");
        try {
            loadNavigationGraph("com/regal/utility/awesome_navigation/awesome_graph.nav");
        } catch (Exception e) {
            e.printStackTrace();
        }
        getLogger().debug("Graph Size After Merge: " + g1.size());
        NpcVertices.addAll(this);
        GameObjectVertices.addAll(this);
        RequirementEdges.addEdgesToGraph(this);
        getLogger().info("Completed adding the edges to the graph!");
    }

    public void merge(WeightedGraph graph) {
        if(g1 == null) {
            g1 = new LinkedHashMap<>();
        }
        getLogger().debug("Graph Size Before Merge: " + g1.size());
        getLogger().info("Merging in .nav file...");
        graph.getGraph().forEach((index, vertex) -> {
            if (g1.containsKey(index)) {
                g1.get(index).setFlag(g1.get(index).getFlag() & vertex.getFlag());
                //g1.get(index).setFlag(vertex.getFlag());
            } else {
                g1.put(index, vertex);
            }
        });

        getLogger().debug("Graph Size After Merge: " + g1.size());
        //NpcVertices.addAll(this);
        //GameObjectVertices.addAll(this);
        //RequirementEdges.addEdgesToGraph(this);
        getLogger().info("Completed adding the edges to the graph!");
    }

    public void merge(GraphSection section) {
        if(g1 == null) {
            g1 = new LinkedHashMap<>();
        }
        section.loadGraph(false);
        System.out.println("Loaded: " + section);

        getLogger().debug("Graph Size Before Merge: " + g1.size());
        getLogger().info("Merging in .nav file...");
        try {
            section.getSpecificMap().values().forEach(vertex -> g1.put(vertex.getCoordinate().hashCode(), vertex));
            //loadNavigationGraph();
        } catch (Exception e) {
            e.printStackTrace();
        }
        getLogger().debug("Graph Size After Merge: " + g1.size());
        //NpcVertices.addAll(this);
        //GameObjectVertices.addAll(this);
        //RequirementEdges.addEdgesToGraph(this);
        getLogger().info("Completed adding the edges to the graph!");
    }

    public void addSpecialVertices() {
        NpcVertices.addAll(this);
        GameObjectVertices.addAll(this);
        RequirementEdges.addEdgesToGraph(this);
    }



    public void mergeLocal(boolean b) {
        if(g1 == null) {
            g1 = new LinkedHashMap<>();
        }
        getLogger().debug("Graph Size Before Merge: " + g1.size());
        try {
            loadNavigationGraphLocal("com/regal/utility/awesome_navigation/awesome_graph.out");
        } catch (Exception e) {
            e.printStackTrace();
        }
        getLogger().debug("Graph Size After Merge: " + g1.size());
        if(b) {
            NpcVertices.addAll(this);
            GameObjectVertices.addAll(this);
            RequirementEdges.addEdgesToGraph(this);
        }
    }

    public void mergeTemp() {
        if(g1 == null) {
            g1 = new LinkedHashMap<>();
        }
        getLogger().debug("Graph Size Before Merge: " + g1.size());
        try {
            loadNavigationGraphLocal("com/regal/utility/awesome_navigation/awesome_graph.out");
        } catch (Exception e) {
            e.printStackTrace();
        }
        getLogger().debug("Graph Size After Merge: " + g1.size());
    }

    public void loadNavigationGraphLocal(String path) throws IOException {
        InputStream inputStream = new FileInputStream(path);
        ObjectInputStream ois = new ObjectInputStreamReader(inputStream);
        getLogger().debug("Graph Size Before Merge: " + g1.size());
        while (true) {
            try {
                int vertexID = ois.readInt();
                int index = (int) ois.readObject();
                Vertex vertex = new CoordinateVertex();
                    vertex.readExternal(ois);

                if (g1.containsKey(index)) {
                    g1.get(index).setFlag(g1.get(index).getFlag() & vertex.getFlag());
                    //g1.get(index).setFlag(vertex.getFlag());
                } else {
                    g1.put(index, vertex);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Environment.getLogger().debug("End of file reached...");
                break;
            }
        }
        getLogger().debug("Graph Size After Merge: " + g1.size());
        ois.close();
        Vertex[] vals = g1.values().toArray(new Vertex[0]);
        for (int i = 0; i < g1.size(); i++) {
            handleEdgeAdding(vals, i);
            if (vals[i].getEdges() == null) {
                vals[i].createEdgeList();
            }
        }
    }

    public void loadNavigationGraphLocal(String path, boolean addEdges) throws IOException {
        InputStream inputStream = new FileInputStream(path);
        ObjectInputStream ois = new ObjectInputStreamReader(inputStream);
        getLogger().debug("Graph Size Before Merge: " + g1.size());
        while (true) {
            try {
                int vertexID = ois.readInt();
                int index = (int) ois.readObject();
                Vertex vertex = null;
                if(vertexID == CoordinateVertex.VERTEX_ID) {
                    vertex = new CoordinateVertex();
                    vertex.readExternal(ois);
                }
                if (g1.containsKey(index)) {
                    g1.get(index).setFlag(g1.get(index).getFlag() & vertex.getFlag());
                    //g1.get(index).setFlag(vertex.getFlag());
                } else {
                    g1.put(index, vertex);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Environment.getLogger().debug("End of file reached...");
                break;
            }
        }
        getLogger().debug("Graph Size After Merge: " + g1.size());
        ois.close();
        if(addEdges) {
            Vertex[] vals = g1.values().toArray(new Vertex[0]);
            for (int i = 0; i < g1.size(); i++) {
                handleEdgeAdding(vals, i);
                if (vals[i].getEdges() == null) {
                    vals[i].createEdgeList();
                }
            }
        }
    }

    public void loadNavigationGraph(String path) throws IOException {
        readFromZip(path);
        Vertex[] vals = g1.values().toArray(new Vertex[0]);
        for (int i = 0; i < g1.size(); i++) {
            handleEdgeAdding(vals, i);
            if (vals[i].getEdges() == null) {
                vals[i].createEdgeList();
            }
        }
        //handleGameObjectVertices();
    }

    void readFromZip(String url) throws IOException {
        /*final String FILE_NAME = "awesome_graph.nav";
        if (!Files.exists(Paths.get(Environment.getStorageDirectory().getPath() + "/" + FILE_NAME))) {
            final String FILE_URL = "https://github.com/awesome123man/Runemate/raw/master/awesome_graph.nav";
            InputStream in = new URL(FILE_URL).openStream();
            Files.copy(in, Paths.get(FILE_NAME), StandardCopyOption.REPLACE_EXISTING);
        }*/
        System.out.println(url);

        ZipInputStream zipInputStream = new ZipInputStream(Resources.getAsStream(url));
        while (zipInputStream.getNextEntry() != null) {
            byte[] bytes = new byte[1024];
            int i = 0;
            ByteArrayOutputStream os = new ByteArrayOutputStream();

            int num;
            while ((num = zipInputStream.read(bytes)) > -1) {
                // .read doesn't always fill the buffer we give it.
                // Keep calling it until we get all the bytes for this entry.
                os.write(bytes, 0, num);
            }
            ByteArrayInputStream in = new ByteArrayInputStream(os.toByteArray());
            System.out.println(os.size());
            os.close();
            ObjectInputStream ois = new ObjectInputStreamReader(in);


            while (true) {
                try {
                    int vertexID = ois.readInt();
                    int index = (int) ois.readObject();
                    CoordinateVertex vertex = new CoordinateVertex();
                    vertex.readExternal(ois);
                    if (g1.containsKey(index)) {
                        g1.get(index).setFlag(g1.get(index).getFlag() & vertex.getFlag());
                    } else {
                        vertex.createEdgeList();
                        g1.put(index, vertex);
                    }
                } catch (Exception ex) {
                    //ex.printStackTrace();
                    break;
                }
            }
            ois.close();
            in.close();
        }
    }

    public void save() {
        System.out.println("Saving...");
        //Prevent mid-save close.
        while(true) {
            try {
                File file = new File("com/regal/utility/awesome_navigation/awesome_graph.out");
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileOutputStream out = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(out);
                for (Map.Entry<Integer, Vertex> entry : g1.entrySet()) {
                    if(entry.getValue().getCoordinate().getX() > 10000) {

                    }
                    else if(!entry.getValue().getEdges().isEmpty()) {
                        Vertex value = entry.getValue();
                        oos.writeInt(value.getVertexID());
                        oos.writeObject(entry.getKey());
                        value.writeExternal(oos);
                    }
                }
                oos.flush();
                oos.close();
                System.out.println("Completed...");
                break;
            } catch (Exception e) {
                System.out.println("Problem serializing: ");
                e.printStackTrace();
                break;
            }
        }
    }

    public void save(String name) {
        System.out.println("Saving...");
        //Prevent mid-save close.
        while(true) {
            try {
                File file = new File("com/regal/utility/awesome_navigation/" + name + ".out");
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileOutputStream out = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(out);
                for (Map.Entry<Integer, Vertex> entry : g1.entrySet()) {
                    if(entry.getValue().getCoordinate().getX() > 10000) {

                    }
                    else if(!entry.getValue().getEdges().isEmpty()) {
                        Vertex value = entry.getValue();
                        oos.writeInt(value.getVertexID());
                        oos.writeObject(entry.getKey());
                        value.writeExternal(oos);
                    }
                }
                oos.flush();
                oos.close();
                System.out.println("Completed...");
                break;
            } catch (Exception e) {
                System.out.println("Problem serializing: ");
                e.printStackTrace();
                break;
            }
        }
    }

    public void saveToZip(String name) {
        System.out.println("Saving...");
        //Prevent mid-save close.
        while(true) {
            try {
                File file = new File("com/regal/utility/awesome_navigation/temp/" + name + ".out");
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileOutputStream out = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(out);
                for (Map.Entry<Integer, Vertex> entry : g1.entrySet()) {
                    if(entry.getValue().getCoordinate().getX() > 10000) {

                    }
                    else if(!entry.getValue().getEdges().isEmpty()) {
                        Vertex value = entry.getValue();
                        oos.writeInt(value.getVertexID());
                        oos.writeObject(entry.getKey());
                        value.writeExternal(oos);
                    }
                }
                oos.flush();
                oos.close();

                File file1 = new File("com/regal/utility/awesome_navigation/sections/" + name + ".nav");
                file1.createNewFile();
                FileOutputStream fos = new FileOutputStream("com/regal/utility/awesome_navigation/sections/" + name + ".nav", false);
                ZipOutputStream zos = new ZipOutputStream(fos);
                ZipEntry ze = new ZipEntry(name + ".out");
                zos.putNextEntry(ze);
                FileInputStream in = new FileInputStream(file);
                int len;
                byte[] buffer = new byte[1024];
                while ((len = in.read(buffer)) > 0)
                {
                    zos.write(buffer, 0, len);
                }
                in.close();
                zos.closeEntry();
                zos.close();

                System.out.println("Completed...");
                break;
            } catch (Exception e) {
                System.out.println("Problem serializing: ");
                e.printStackTrace();
                break;
            }
        }
    }

    public void renderPath(List<Vertex> path) {
        addRegionPath(path);
    }

    public void removeEdgesTo(Coordinate coordinate) {
        getGraph().values().forEach(coordinatePathVertex -> coordinatePathVertex.getEdges().removeIf(edge -> edge.getDestination().getCoordinate().equals(coordinate)));
    }

    private void renderPath(Vertex coorVertex, List<Vertex> path, int index, int minX, int minY, int maxX, int maxY) {
        if(coorVertex == null) {
            Environment.getLogger().debug("Null: " + index);
        }
        int flag = coorVertex.getFlag();
        boolean north = false;
        boolean south = false;
        boolean east = false;
        boolean west = false;

        boolean ne = false;
        boolean nw = false;
        boolean se = false;
        boolean sw = false;
        if ((flag & Region.CollisionFlags.BLOCKED_TILE) == 0
                && (flag & Region.CollisionFlags.HelperFlags.BLOCKED_OFF) != Region.CollisionFlags.HelperFlags.BLOCKED_OFF
                && (flag & Region.CollisionFlags.BLOCKING_FLOOR_OBJECT) == 0 && (flag & Region.CollisionFlags.UNSTEPPABLE_OBJECT) == 0
                && (flag & Region.CollisionFlags.OBJECT_TILE) == 0) {
            if ((flag & Region.CollisionFlags.EAST_BOUNDARY_OBJECT) != 0) {
                east = true;
            }
            if ((flag & Region.CollisionFlags.WEST_BOUNDARY_OBJECT) != 0) {
                west = true;
            }
            if ((flag & Region.CollisionFlags.NORTH_BOUNDARY_OBJECT) != 0) {
                north = true;
            }
            if ((flag & Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT) != 0) {
                south = true;
            }
            if ((flag & Region.CollisionFlags.NORTH_EAST_BOUNDARY_OBJECT) != 0) {
                ne = true;
            }
            if ((flag & Region.CollisionFlags.NORTH_WEST_BOUNDARY_OBJECT) != 0) {
                nw = true;
            }
            if ((flag & Region.CollisionFlags.SOUTH_WEST_BOUNDARY_OBJECT) != 0) {
                sw = true;
            }
            if ((flag & Region.CollisionFlags.SOUTH_EAST_BOUNDARY_OBJECT) != 0) {
                se = true;
            }
        } else {
            north = true;
            south = true;
            east = true;
            west = true;

            ne = true;
            nw = true;
            se = true;
            sw = true;
        }

        int col = coorVertex.getCoordinate().getX() - minX;
        int row = maxY - (coorVertex.getCoordinate().getY() - minY);

        int x;
        int y;
        if((coorVertex.getCoordinate().getPlane() == 0 || !(coorVertex instanceof CoordinateVertex)) && !coorVertex.getEdges().isEmpty()) {
            graphics2D.setColor(Color.WHITE);
            x = col * 2 - 1;
            y = row * 2 - 1;
            graphics2D.fill(new Rectangle(x, y, 2, 2));
        }

        graphics2D.setColor(Color.BLUE);
        if(path != null && path.contains(coorVertex)) {
            x = col * 2 - 1;
            y = row * 2 - 1;
            graphics2D.fill(new Rectangle(x, y, 2, 2));
        }

        if(coorVertex instanceof CoordinateVertex) {
            graphics2D.setColor(Color.RED);
        }

        if((coorVertex.getCoordinate().getPlane() == 0 || !(coorVertex instanceof CoordinateVertex)) && !coorVertex.getEdges().isEmpty()) {
            x = col * 2;
            y = row * 2 + 1;
            if (south) {
                graphics2D.drawLine(x - 1, y, x + 1, y);
            }

            x = col * 2;
            y = row * 2 - 1;
            if (north) {
                graphics2D.drawLine(x - 1, y, x + 1, y);
            }

            x = col * 2 - 1;
            y = row * 2;
            if (west) {
                graphics2D.drawLine(x, y - 1, x, y + 1);
            }

            x = col * 2 + 1;
            y = row * 2;
            if (east) {
                graphics2D.drawLine(x, y - 1, x, y + 1);
            }
        }
    }

    private void addRegionPath(List<Vertex> path) {
        Environment.getLogger().debug("Rendering Path");


        int minX = Integer.MAX_VALUE;
        int minY = Integer.MAX_VALUE;
        int maxX = -1 * Integer.MAX_VALUE;
        int maxY = -1 * Integer.MAX_VALUE;

        for (Vertex obj : g1.values()) {
            int x = obj.getCoordinate().getX();
            int y = obj.getCoordinate().getY();

            if (x < minX) {
                minX = x;
            } else if(x > maxX) {
                maxX = x;
            }
            if (y < minY) {
                minY = y;
            } else if(y > maxY) {
                maxY = y;
            }
        }

        int diffX = maxX - minX;
        int diffY = maxY - minY;
        Environment.getLogger().debug("Graph Size: " + diffX + " x " + diffY);

        BufferedImage bufferedImage = new BufferedImage(diffX, diffY, BufferedImage.TYPE_USHORT_555_RGB);
        graphics2D = bufferedImage.createGraphics();
        graphics2D.setColor(Color.BLACK);
        graphics2D.fillRect(0, 0, diffX, diffY);

        Environment.getLogger().debug("Made background");


        Vertex[] values = g1.values().toArray(new Vertex[0]);

        for (int index = 0; index < g1.size(); index++) {
            try {
                Vertex coorVertex = values[index];
                //renderPath(coorVertex, path, index, minX, minY, maxX, maxY);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Environment.getLogger().debug("Finished buildPath render");
        //graphics2D.translate(0, 0);
        //graphics2D.rotate(Math.toDegrees(270), 1000, 1000);
        graphics2D.drawRenderedImage(bufferedImage, null);

        graphics2D.dispose();

        // Get current size of heap in bytes
        long heapSize = Runtime.getRuntime().totalMemory();

        // Get maximum size of heap in bytes. The heap cannot grow beyond this size.// Any attempt will result in an OutOfMemoryException.
        long heapMaxSize = Runtime.getRuntime().maxMemory();

        // Get amount of free memory within the heap in bytes. This size will increase // after garbage collection and decrease as new objects are created.
        long heapFreeSize = Runtime.getRuntime().freeMemory();

        File file = new File("mypathimage.bmp");
        Environment.getLogger().debug(file.exists() + " " + file.getAbsolutePath());
        try {
            ImageIO.write(bufferedImage, "bmp", file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addRegion() {
        Area.Rectangular area = Region.getArea();
        List<Coordinate> coors = area.getCoordinates();
        Player loc = Players.getLocal();
        int[][] flags = null;
        if(loc != null) {
            Coordinate pos = loc.getPosition();
            if(pos != null) {
                flags = Region.getCollisionFlags(pos.getPlane());
            }
        }
        if(flags == null) {
            flags = Region.getCollisionFlags(0);
        }


        BufferedImage bufferedImage = new BufferedImage(416, 416, BufferedImage.TYPE_INT_RGB);
        graphics2D = bufferedImage.createGraphics();
        graphics2D.setColor(Color.WHITE);
        graphics2D.fillRect(0, 0, 416, 416);
        graphics2D.setColor(Color.RED);
        try {
            for (int col = 0; col < 104; col++) {
                for (int row = 0; row < 104; row++) {

                    int flag = flags[col][row];

                    int index = col * 104 + row;

                    Coordinate coordinate = coors.get(index);

                    CoordinateVertex coorVertex = new CoordinateVertex(coordinate, flag);

                    g1.put(coordinate.hashCode(), coorVertex);

                    int x = col * 4;
                    int y = row * 4 + 2;
                    if ((flag & Region.CollisionFlags.NORTH_BOUNDARY_OBJECT) != 0) {
                        graphics2D.drawLine(x - 2, y, x + 2, y);
                    }

                    x = col * 4;
                    y = row * 4 - 2;
                    if ((flag & Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT) != 0) {
                        graphics2D.drawLine(x - 2, y, x + 2, y);
                    }

                    y = row * 4;
                    x = col * 4 + 2;
                    if ((flag & Region.CollisionFlags.EAST_BOUNDARY_OBJECT) != 0) {
                        graphics2D.drawLine(x, y - 2, x, y + 2);
                    }

                    y = row * 4;
                    x = col * 4 - 2;
                    if ((flag & Region.CollisionFlags.WEST_BOUNDARY_OBJECT) != 0) {
                        graphics2D.drawLine(x, y - 2, x, y + 2);
                    }

                    if((flag & Region.CollisionFlags.BLOCKED_TILE) != 0 || (flag & Region.CollisionFlags.HelperFlags.BLOCKED_OFF) != 0
                            || (flag & Region.CollisionFlags.BLOCKING_FLOOR_OBJECT) != 0 || (flag & Region.CollisionFlags.UNSTEPPABLE_OBJECT) != 0
                            || (flag & Region.CollisionFlags.OBJECT_TILE) != 0) {
                        x = col * 4;
                        y = row * 4 + 2;
                        graphics2D.drawLine(x - 2, y, x + 2, y);
                        x = col * 4;
                        y = row * 4 - 2;
                        graphics2D.drawLine(x - 2, y, x + 2, y);
                        y = row * 4;
                        x = col * 4 - 2;
                        graphics2D.drawLine(x, y - 2, x, y + 2);
                        y = row * 4;
                        x = col * 4 + 2;
                        graphics2D.drawLine(x, y - 2, x, y + 2);
                    }
                }
            }
            //graphics2D.translate(0, 0);
            //graphics2D.rotate(3 * Math.PI / 2, 208, 208);

            graphics2D.dispose();

            File file = new File("myimage.bmp");
            Environment.getLogger().debug(file.exists() + " " + file.getAbsolutePath());
            try {
                ImageIO.write(bufferedImage, "bmp", file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void addRegion(int plane) {
        Area.Rectangular area = Region.getArea();
        List<Coordinate> coors = area.getCoordinates();
        int[][] flags = Region.getCollisionFlags(plane);

        BufferedImage bufferedImage = new BufferedImage(416, 416, BufferedImage.TYPE_INT_RGB);
        graphics2D = bufferedImage.createGraphics();
        graphics2D.setColor(Color.WHITE);
        graphics2D.fillRect(0, 0, 416, 416);
        graphics2D.setColor(Color.RED);
        try {
            for (int col = 0; col < 104; col++) {
                for (int row = 0; row < 104; row++) {

                    int flag = flags[col][row];

                    int index = col * 104 + row;

                    Coordinate coordinate = coors.get(index);

                    CoordinateVertex coorVertex = new CoordinateVertex(coordinate, flag);

                    g1.put(coordinate.hashCode(), coorVertex);

                    int x = col * 4;
                    int y = row * 4 + 2;
                    if ((flag & Region.CollisionFlags.NORTH_BOUNDARY_OBJECT) != 0) {
                        graphics2D.drawLine(x - 2, y, x + 2, y);
                    }

                    x = col * 4;
                    y = row * 4 - 2;
                    if ((flag & Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT) != 0) {
                        graphics2D.drawLine(x - 2, y, x + 2, y);
                    }

                    y = row * 4;
                    x = col * 4 + 2;
                    if ((flag & Region.CollisionFlags.EAST_BOUNDARY_OBJECT) != 0) {
                        graphics2D.drawLine(x, y - 2, x, y + 2);
                    }

                    y = row * 4;
                    x = col * 4 - 2;
                    if ((flag & Region.CollisionFlags.WEST_BOUNDARY_OBJECT) != 0) {
                        graphics2D.drawLine(x, y - 2, x, y + 2);
                    }

                    if((flag & Region.CollisionFlags.BLOCKED_TILE) != 0 || (flag & Region.CollisionFlags.HelperFlags.BLOCKED_OFF) != 0
                            || (flag & Region.CollisionFlags.BLOCKING_FLOOR_OBJECT) != 0 || (flag & Region.CollisionFlags.UNSTEPPABLE_OBJECT) != 0
                            || (flag & Region.CollisionFlags.OBJECT_TILE) != 0) {
                        x = col * 4;
                        y = row * 4 + 2;
                        graphics2D.drawLine(x - 2, y, x + 2, y);
                        x = col * 4;
                        y = row * 4 - 2;
                        graphics2D.drawLine(x - 2, y, x + 2, y);
                        y = row * 4;
                        x = col * 4 - 2;
                        graphics2D.drawLine(x, y - 2, x, y + 2);
                        y = row * 4;
                        x = col * 4 + 2;
                        graphics2D.drawLine(x, y - 2, x, y + 2);
                    }
                }
            }
            //graphics2D.translate(0, 0);
            //graphics2D.rotate(3 * Math.PI / 2, 208, 208);

            graphics2D.dispose();

            File file = new File("myimage.bmp");
            Environment.getLogger().debug(file.exists() + " " + file.getAbsolutePath());
            try {
                ImageIO.write(bufferedImage, "bmp", file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void handleEdgeAdding(Vertex[] values, int index) {
        Vertex coorVertex = values[index];
        int flag = coorVertex.getFlag();
        boolean north = true;
        boolean south = true;
        boolean east = true;
        boolean west = true;
        if ((flag & (Region.CollisionFlags.BLOCKED_TILE | Region.CollisionFlags.HelperFlags.BLOCKED_OFF
                | Region.CollisionFlags.BLOCKING_FLOOR_OBJECT | Region.CollisionFlags.UNSTEPPABLE_OBJECT |
                Region.CollisionFlags.OBJECT_TILE)) == 0) {
            if ((flag & Region.CollisionFlags.EAST_BOUNDARY_OBJECT) == 0) {
                east = false;
            }
            if ((flag & Region.CollisionFlags.WEST_BOUNDARY_OBJECT) == 0) {
                west = false;
            }
            if ((flag & Region.CollisionFlags.NORTH_BOUNDARY_OBJECT) == 0) {
                north = false;
            }
            if ((flag & Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT) == 0) {
                south = false;
            }
        }
        
        //North y increases
        //East x increases
        Vertex other;
        if (!east && (other = g1.get(values[index].getCoordinate().derive(1, 0).hashCode())) != null) {
            if((other.getFlag() & Region.CollisionFlags.WEST_BOUNDARY_OBJECT) == 0) {
                coorVertex.addEdge(new Edge(coorVertex, other, 1, null));
            }
        }
        if (!west && (other = g1.get(values[index].getCoordinate().derive(-1, 0).hashCode())) != null) {
            if((other.getFlag() & Region.CollisionFlags.EAST_BOUNDARY_OBJECT) == 0) {
                coorVertex.addEdge(new Edge(coorVertex, other, 1, null));
            }
        }
        if (!north && (other = g1.get(values[index].getCoordinate().derive(0, 1).hashCode())) != null) {
            if((other.getFlag() & Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT) == 0) {
                coorVertex.addEdge(new Edge(coorVertex, other, 1, null));
            }
        }
        if (!south && (other = g1.get(values[index].getCoordinate().derive(0, -1).hashCode())) != null) {
            if((other.getFlag() & Region.CollisionFlags.NORTH_BOUNDARY_OBJECT) == 0) {
                coorVertex.addEdge(new Edge(coorVertex, other, 1, null));
            }
        }
    }

    private void handleEdgeAdding(Vertex coorVertex, Requirement[] requirements) {
        int flag = coorVertex.getFlag();
        boolean north = true;
        boolean south = true;
        boolean east = true;
        boolean west = true;
        if ((flag & (Region.CollisionFlags.BLOCKED_TILE | Region.CollisionFlags.HelperFlags.BLOCKED_OFF
                | Region.CollisionFlags.BLOCKING_FLOOR_OBJECT | Region.CollisionFlags.UNSTEPPABLE_OBJECT |
                Region.CollisionFlags.OBJECT_TILE)) == 0) {
            if ((flag & Region.CollisionFlags.EAST_BOUNDARY_OBJECT) == 0) {
                east = false;
            }
            if ((flag & Region.CollisionFlags.WEST_BOUNDARY_OBJECT) == 0) {
                west = false;
            }
            if ((flag & Region.CollisionFlags.NORTH_BOUNDARY_OBJECT) == 0) {
                north = false;
            }
            if ((flag & Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT) == 0) {
                south = false;
            }
        }

        //North y increases
        //East x increases
        Vertex other;
        if (!east && (other = g1.get(coorVertex.getCoordinate().derive(1, 0).hashCode())) != null) {
            if((other.getFlag() & Region.CollisionFlags.WEST_BOUNDARY_OBJECT) == 0) {
                coorVertex.addEdge(new Edge(coorVertex, other, 1, requirements));
                other.addEdge(new Edge(other, coorVertex, 1, requirements));
            }
        }
        if (!west && (other = g1.get(coorVertex.getCoordinate().derive(-1, 0).hashCode())) != null) {
            if((other.getFlag() & Region.CollisionFlags.EAST_BOUNDARY_OBJECT) == 0) {
                coorVertex.addEdge(new Edge(coorVertex, other, 1, requirements));
                other.addEdge(new Edge(other, coorVertex, 1, requirements));
            }
        }
        if (!north && (other = g1.get(coorVertex.getCoordinate().derive(0, 1).hashCode())) != null) {
            if((other.getFlag() & Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT) == 0) {
                coorVertex.addEdge(new Edge(coorVertex, other, 1, requirements));
                other.addEdge(new Edge(other, coorVertex, 1, requirements));
            }
        }
        if (!south && (other = g1.get(coorVertex.getCoordinate().derive(0, -1).hashCode())) != null) {
            if((other.getFlag() & Region.CollisionFlags.NORTH_BOUNDARY_OBJECT) == 0) {
                coorVertex.addEdge(new Edge(coorVertex, other, 1, requirements));
                other.addEdge(new Edge(other, coorVertex, 1, requirements));
            }
        }
    }

    public void addEdgesBack(Vertex obj, Requirement[] requirements) {
        if(obj.getEdges() == null) {
            obj.createEdgeList();
        }

        handleEdgeAdding(obj, requirements);

        obj.getEdges().removeIf(edge -> edge.getDestination() == null);
    }

    public LinkedHashMap<Integer, Vertex> getGraph() {
        return g1;
    }

    public void swapVertex(Vertex newVertex) {
        Vertex oldVertex = g1.get(newVertex.getCoordinate().hashCode());
        if(oldVertex != null) {
            addEdgesBack(newVertex, null);
            Coordinate coor = oldVertex.getCoordinate();
            Vertex vertex = g1.get(coor.derive(0, 1).hashCode());
            swapOldDestinationsWithNew(vertex, newVertex);
            vertex = g1.get(coor.derive(0, -1).hashCode());
            swapOldDestinationsWithNew(vertex, newVertex);
            vertex = g1.get(coor.derive(1, 0).hashCode());
            swapOldDestinationsWithNew(vertex, newVertex);
            vertex = g1.get(coor.derive(-1, 0).hashCode());
            swapOldDestinationsWithNew(vertex, newVertex);
        }
        Arrays.stream(GameObjectVertices.values()).map(gameObjectVertices -> g1.get(gameObjectVertices.getSpot().hashCode())).forEach(val -> {
            if(val != null) {
                //Create edge lists if null
                if (val.getEdges() == null) {
                    val.createEdgeList();
                }
                //Go thru each vertex edges
                val.getEdges().forEach(edge -> {
                    //If they are going to the vertex we're adding, make them go to our new one.
                    if (edge.getDestination() != null && edge.getDestination().getCoordinate().equals(newVertex.getCoordinate())) {
                        edge.setDestination(newVertex);
                    }
                });
            }
        });

        Arrays.stream(NpcVertices.values()).map(gameObjectVertices -> g1.get(gameObjectVertices.getSpot().hashCode())).forEach(val -> {
            if(val != null) {
                //Create edge lists if null
                if (val.getEdges() == null) {
                    val.createEdgeList();
                }
                //Go thru each vertex edges
                val.getEdges().forEach(edge -> {
                    //If they are going to the vertex we're adding, make them go to our new one.
                    if (edge.getDestination() != null && edge.getDestination().getCoordinate().equals(newVertex.getCoordinate())) {
                        edge.setDestination(newVertex);
                    }
                });
            }
        });

        g1.put(newVertex.getCoordinate().hashCode(), newVertex);
    }

    private void swapOldDestinationsWithNew(Vertex vertex, Vertex newVertex) {
        if(vertex != null) {
            vertex.getEdges().forEach(edge -> {
                //If they are going to the vertex we're adding, make them go to our new one.
                if (edge.getDestination() != null && edge.getDestination().getCoordinate().equals(newVertex.getCoordinate())) {
                    edge.setDestination(newVertex);
                }
            });
        }
    }

    public void prune() {
        Environment.getLogger().debug("Graph Size Before Pruning: " + getGraph().values().stream().filter(vertex -> vertex.getEdges().size() >= 4).count());
        List<Vertex> removes = new ArrayList<>();
        getGraph().values().stream()
                    .filter(vertex -> vertex.getEdges().size() == 4)
                    .filter(vertex -> (vertex.getCoordinate().getX() + vertex.getCoordinate().getY()) % 2 != 0)
                    .filter(vertex -> vertex instanceof CoordinateVertex)
                    .filter(vertex -> vertex.getEdges().stream().allMatch(edge -> edge.getDestination() instanceof CoordinateVertex))
                    .forEach(vertex -> {
                        vertex.getEdges().forEach(edge -> {
                            //Iterate each edge of the vertex we are removing...
                            edge.getDestination().getEdges().stream().filter(edge1 -> edge1.getDestination().getCoordinate().equals(vertex.getCoordinate())).forEach(edge1 -> {
                                //Iterate edge(s) of destination which go to original.
                                int x = 1 * (vertex.getCoordinate().getX() - edge1.getSource().getCoordinate().getX());
                                int y = 1 * (vertex.getCoordinate().getY() - edge1.getSource().getCoordinate().getY());
                                if(getGraph().get(vertex.getCoordinate().derive(x, y).hashCode()) == null) {
                                    System.out.println("Null New Vertex: " + vertex.getCoordinate().derive(x, y));
                                }
                                edge1.setDestination(getGraph().get(vertex.getCoordinate().derive(x, y).hashCode()));
                                //System.out.println(edge1.getSource().getCoordinate() + " " + edge1.getDestination().getCoordinate());
                            });
                        });
                        removes.add(vertex);
                    });
        removes.forEach(vertex -> getGraph().remove(vertex.getCoordinate().hashCode()));
        Environment.getLogger().debug("Graph Size After Pruning: " + getGraph().values().stream().filter(vertex -> vertex.getEdges().size() >= 4).count());
    }
}
