package com.regal.utility.awesome_navigation;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.regal.utility.awesome_navigation.banking.BankAreas;
import com.regal.utility.awesome_navigation.banking.DepositBoxAreas;
import com.regal.utility.awesome_navigation.edges.Edge;
import com.regal.utility.awesome_navigation.edges.EdgeHolder;
import com.regal.utility.awesome_navigation.handles.Handleable;
import com.regal.utility.awesome_navigation.handles.Handled;
import com.regal.utility.awesome_navigation.handles.Interactions;
import com.regal.utility.awesome_navigation.requirements.Requirement;
import com.regal.utility.awesome_navigation.requirements.RequirementBuilder;
import com.regal.utility.awesome_navigation.vertices.CoordinateVertex;
import com.regal.utility.awesome_navigation.vertices.Vertex;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.entities.details.Locatable;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.location.navigation.Landmark;
import com.runemate.game.api.hybrid.location.navigation.Traversal;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.hybrid.util.Resources;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.AbstractBot;

import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.runemate.game.api.hybrid.Environment.getLogger;

public class AwesomePath {

    private List<Vertex> coordinatePath;
    private final Pattern STAMINA_PATTERN = Regex.getPatternForContainsString("Stamina potion(");
    private List<Vertex> prevPathVertexList;
    private boolean useTeleports;
    private GraphSection[] sections;

    public AwesomePath() {
        coordinatePath = new ArrayList<>();
    }

    public List<Vertex> buildPath(WeightedGraph graph, Locatable destination, Coordinate start) {
        coordinatePath = new ArrayList<>();
        AwesomePathGenerator path = new AwesomePathGenerator(useTeleports);
        getLogger().debug("Building Specific Path From: " + start + " to " + destination);
        long startTime = System.currentTimeMillis();
        coordinatePath = path.generatePath(graph, start, destination);
        long endTime = System.currentTimeMillis();
        getLogger().debug("Specific Path Generation Took: " + (endTime - startTime) + "ms");
        return coordinatePath;
    }

    public List<Vertex> buildPath(WeightedGraph graph, Landmark landmark, Coordinate start) {
        coordinatePath = new ArrayList<>();
        AwesomePathGenerator path = new AwesomePathGenerator(useTeleports);
        getLogger().debug("Building Specific Pathing From: " + start + " to " + landmark);
        long startTime = System.currentTimeMillis();
        if (landmark.equals(Landmark.BANK)) {
            coordinatePath = path.generatePath(graph, start, Arrays.stream(BankAreas.values()).map(BankAreas::getArea).toArray(Area[]::new));
        } else if(landmark.equals(Landmark.DEPOSIT_BOX)) {
            coordinatePath = path.generatePath(graph, start, Arrays.stream(DepositBoxAreas.values()).map(DepositBoxAreas::getArea).toArray(Area[]::new));
        }
        long endTime = System.currentTimeMillis();
        getLogger().debug("Specific Path Generation Took: " + (endTime - startTime) + "ms");
        return coordinatePath;
    }

    public WeightedGraph buildHighLevelPath(Locatable destination, Coordinate start) {
        AwesomePathGenerator generator = new AwesomePathGenerator(useTeleports);
        if (sections == null) {
            sections = loadSections();
        }
        List<GraphSection> path;
        path = generator.calculateShortestPathFromSourceTemp(sections, start, destination);

        if (path == null) {
            Environment.getLogger().debug("High Level Path Failed to Generate.");
            return null;
        }
        Environment.getLogger().debug("Path: " + path.stream().map(graphSection -> graphSection.getArea().toString()).collect(Collectors.joining(", ")));

        //Now lets merge the sections together and try...
        WeightedGraph graph1 = new WeightedGraph(false);
        path.forEach(graph1::merge);
        path.forEach(section -> section.getSpecificMap().clear());
        graph1.getGraph().values().forEach(vertex -> {
            vertex.setTotalCost(-1);
            vertex.setFrom(null);
            vertex.getEdges().forEach(edge -> edge.setUsed(false));
        });
        Vertex[] vals = graph1.getGraph().values().toArray(new Vertex[0]);
        for (int i = 0; i < vals.length; i++) {
            graph1.handleEdgeAdding(vals, i);
        }

        graph1.addSpecialVertices();

        return graph1;
    }

    public WeightedGraph buildHighLevelPath(Landmark destination, Coordinate start) {
        AwesomePathGenerator generator = new AwesomePathGenerator(useTeleports);
        if(sections == null) {
            sections = loadSections();
        }

        List<GraphSection> path = null;
        if (destination.equals(Landmark.BANK)) {
            path = generator.calculateShortestPathFromSource(sections, start, Arrays.stream(BankAreas.values()).map(BankAreas::getArea).toArray(Area[]::new));
        } else if(destination.equals(Landmark.DEPOSIT_BOX)) {
            path = generator.calculateShortestPathFromSource(sections, start, Arrays.stream(DepositBoxAreas.values()).map(DepositBoxAreas::getArea).toArray(Area[]::new));
        }
        if(path == null) {
            return null;
        }
        Environment.getLogger().debug("Path: " + path.stream().map(graphSection -> graphSection.getArea().toString()).collect(Collectors.joining(", ")));

        //Now lets merge the sections together and try...
        WeightedGraph graph1 = new WeightedGraph(false);
        path.forEach(graph1::merge);
        path.forEach(section -> section.getSpecificMap().clear());
        graph1.getGraph().values().forEach(vertex -> {
            vertex.setTotalCost(-1);
            vertex.setFrom(null);
            vertex.getEdges().forEach(edge -> edge.setUsed(false));
        });
        Vertex[] vals = graph1.getGraph().values().toArray(new Vertex[0]);
        for (int i = 0; i < vals.length; i++) {
            graph1.handleEdgeAdding(vals, i);
        }

        graph1.addSpecialVertices();

        return graph1;
    }

    public GraphSection[] loadSections() {
        JsonReader reader = new JsonReader(new InputStreamReader(Resources.getAsStream("com/regal/utility/awesome_navigation/sections.json")));
        JsonElement element = JsonParser.parseReader(reader);
        JsonArray sectionArray = element.getAsJsonArray();
        List<GraphSection> sections = new ArrayList<>();
        List<EdgeHolder> holders = new ArrayList<>();
        sectionArray.forEach(jsonElement -> {
            JsonObject obj = jsonElement.getAsJsonObject();
            if (obj.has("SectionName")) {
                String sectionName = obj.get("SectionName").getAsString();
                String sectionAreaStr = obj.get("SectionArea").getAsString();

                String firstCoor = sectionAreaStr.substring(sectionAreaStr.indexOf('(') + 1, sectionAreaStr.indexOf(" ->"));
                String[] vals = firstCoor.split(", ");
                String[] vals1 = sectionAreaStr.substring(sectionAreaStr.indexOf(" ->") + 4, sectionAreaStr.length() - 1).split(", ");
                Area.Rectangular area = new Area.Rectangular(new Coordinate(Integer.parseInt(vals[0]), Integer.parseInt(vals[1]), Integer.parseInt(vals[2])), new Coordinate(Integer.parseInt(vals1[0]), Integer.parseInt(vals1[1]), Integer.parseInt(vals1[2])));

                sections.add(new GraphSection(sectionName, area, new LinkedHashMap<>(), new LinkedHashMap<>()));
            } else {
                JsonArray array = obj.getAsJsonArray("Vertices");

                List<Vertex> vertices = new ArrayList<>();
                array.forEach(jsonElement1 -> {
                    int hashCode = jsonElement1.getAsInt();
                    Coordinate coordinate = new Coordinate(hashCode);
                    CoordinateVertex vertex = new CoordinateVertex(coordinate, 0);
                    vertices.add(vertex);
                    sections.stream().filter(section -> section.getArea().contains(coordinate)).findFirst().ifPresent(section -> section.getHighLevelMap().put(hashCode, vertex));

                });

                JsonArray edgesArray = obj.getAsJsonArray("Edges");
                for (int tmp = 0; tmp < edgesArray.size(); tmp++) {
                    JsonElement jsonElement1 = edgesArray.get(tmp);
                    JsonObject object = jsonElement1.getAsJsonObject();
                    JsonArray indicesJsonArray = object.get("Indices").getAsJsonArray();
                    JsonArray weightsJsonArray = object.get("Weights").getAsJsonArray();
                    JsonArray requirementsArrayJsonArray = object.get("Requirements").getAsJsonArray();

                    for (int i = 0; i < indicesJsonArray.size(); i++) {
                        int index = indicesJsonArray.get(i).getAsInt();
                        int weight = weightsJsonArray.get(i).getAsInt();
                        JsonArray innerArray = requirementsArrayJsonArray.get(i).getAsJsonArray();
                        Requirement[] requirements = new Requirement[innerArray.size()];
                        for (int j = 0; j < innerArray.size(); j++) {
                            String str = innerArray.get(j).toString();
                            str = str.substring(1, str.length() - 1);
                            if(!str.isEmpty()) {
                                requirements[j] = RequirementBuilder.buildRequirementFrom(str);
                            }
                        }

                        vertices.get(tmp).addEdge(new Edge(vertices.get(tmp), vertices.get(index), weight, requirements));
                    }

                }
            }
        });

        return sections.toArray(new GraphSection[0]);
    }

    public boolean path(List<Vertex> pathVertexList) {
        AbstractBot bot = Environment.getBot();
        Player player = Players.getLocal();
        if(player != null && pathVertexList != null && !pathVertexList.isEmpty() && bot != null) {
            Environment.getLogger().debug("[AwesomePath]: {" + pathVertexList.stream().map(vertex -> vertex.getCoordinate() + ", ").limit(10).collect(Collectors.joining()) + "}" + " Length: " + pathVertexList.size());
            boolean distanceBool = false;
            boolean toggleRun = pathVertexList.size() > 16;
            int runEnergy = Traversal.getRunEnergy();
            SpriteItem stamina;
            if (!Traversal.isRunEnabled() && toggleRun && runEnergy > PathingPlayerSense.Key.RUN_REENABLE_THRESHOLD_HIGH.getAsInteger()) {
                toggleRun();
            } else if (toggleRun && runEnergy < PathingPlayerSense.Key.AWESOME_STAMINA_ENERGY_TRIGGER.getAsInteger() && !Traversal.isStaminaEnhanced() && (stamina = Inventory.newQuery().names(STAMINA_PATTERN).results().first()) != null) {
                Environment.getLogger().info("Drinking Stamina Potion...");
                if (Interactions.interact(stamina, "Drink")) {
                    Execution.delayUntil(() -> Traversal.isStaminaEnhanced(), 600, 1200);
                }
            } else {
                Vertex vertex = null;
                int i;
                for (i = 0; i < (Math.min(pathVertexList.size(), 16)); i++) {
                    Edge usedEdge = null;
                    if (!((vertex = pathVertexList.get(i)) instanceof CoordinateVertex) || ((usedEdge = getUsedEdge(vertex)) != null)) {
                        if (Bank.isOpen()) {
                            return Bank.close();
                        }
                        Handleable handleable = null;
                        if (usedEdge != null) {
                            handleable = ((Handled) usedEdge).getHandleable();
                        }
                        if (handleable != null) {
                            getLogger().debug(handleable);
                            if (handleable.handle()) {
                                return true;
                            }
                        }
                    }
                }
                getLogger().debug("Vertex[" + i + "]: " + vertex);
                if(pathVertexList.equals(prevPathVertexList)) {
                    if(((Loggable) bot).getLogCount() >= 100) {
                        bot.stop("Pathing is continually trying to reach same position 100x.");
                    }
                }
                prevPathVertexList = pathVertexList;
                if (pathVertexList.size() < 13) {
                    distanceBool = true;
                    vertex = pathVertexList.get(pathVertexList.size() - 1);
                } else {
                    vertex = pathVertexList.get(Random.nextInt(10, 13));
                }
                Coordinate.MinimapCoordinate minimapCoor = vertex.getCoordinate().minimap();
                Environment.getLogger().info("Walking to " + minimapCoor);
                if (minimapCoor.click()) {
                    Vertex finalVertex = vertex;
                    if (!distanceBool) {
                        return Execution.delayUntil(() -> player.distanceTo(finalVertex.getCoordinate()) < PathingPlayerSense.Key.AWESOME_WEB_DISTANCE_WALK.getAsInteger(),
                                player::isMoving, 1000, 2000);
                    } else {
                        return Execution.delayUntil(() -> finalVertex.getCoordinate().equals(player.getPosition()),
                                player::isMoving, 1000, 2000);
                    }
                }
            }
        }
        return false;
    }

    private Edge getUsedEdge(Vertex vertex) {
        return vertex.getEdges().stream().filter(Edge::isUsed).filter(edge1 -> !edge1.getClass().equals(Edge.class)).findFirst().orElse(null);
    }

    public boolean toggleRun() {
        InterfaceComponent runComp = Interfaces.newQuery().containers(160).actions("Toggle Run").results().first();
        if(runComp != null) {
            Environment.getLogger().info("Toggling Run...");
            return runComp.interact("Toggle Run") && Execution.delayUntil(() -> Traversal.isRunEnabled(), 500, 1000);
        }
        Environment.getLogger().severe("Run Interface Component Was Null!!!");
        return false;
    }

    public void setUseTeleports(boolean useTeleports) {
        this.useTeleports = useTeleports;
    }
}
