package com.regal.utility.awesome_navigation.handles;

public interface Handled {
    Handleable getHandleable();
}
