package com.regal.utility.awesome_navigation.handles;

import com.regal.utility.awesome_navigation.RegalItem;
import com.regal.utility.awesome_navigation.requirements.*;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.Quest;
import com.runemate.game.api.hybrid.local.Varbit;
import com.runemate.game.api.hybrid.local.Varbits;
import com.runemate.game.api.hybrid.local.hud.InteractableRectangle;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;

import java.util.Arrays;

public enum FairyRing implements Handleable {
    ZANARIS(new Coordinate(2412, 4434, 0), "Zanaris"),
    CHASM_OF_FIRE(new Coordinate(1455, 3658, 0), 'd', 'j', 'r'),
    MUDSKIPPER_POINT(new Coordinate(2996, 3114, 0), 'a', 'i', 'q'),
    KANDARIN(new Coordinate(2780, 3613, 0), 'a', 'j', 'r'),
    EDGEVILLE(new Coordinate(3129, 3496, 0), 'd', 'k', 'r'),
    MOUNT_KARUULM(new Coordinate(1302, 3762, 0), 'c', 'i', 'r'),
    KELDAGRIM_ENTRANCE(new Coordinate(2744, 3719, 0), 'd', 'k', 's'),
    KHARIDIAN_DESERT(new Coordinate(3251, 3095, 0), 'b', 'i', 'q'),
    YANILLE(new Coordinate(2528, 3127, 0), 'c', 'i', 'q'),
    TOWER_OF_LIFE(new Coordinate(2658, 3230, 0), 'd', 'j', 'p'),
    SHILO_VILLAGE(new Coordinate(2801, 3003, 0), 'c', 'k', 'r'),
    CANIFIS(new Coordinate(3447, 3470, 0), new QuestRequirement("Priest in Peril", Quest.Status.COMPLETE), 'c', 'k', 's'),
    FISHER_REALM(new Coordinate(2650, 4730, 0), new QuestRequirement("Holy Grail", Quest.Status.COMPLETE), 'b', 'j', 'r'),
    ISLAND_SOUTH_OF_ARDOUGNE(new Coordinate(2700, 3247, 0), 'a', 'i', 'r'),
    PENGUINS(new Coordinate(2500, 3896, 0), 'a', 'j', 's'),
    PISCATORIS_HUNTER_AREA( new Coordinate(2319, 2619, 0), 'a', 'k', 'q'),
    FELDIP_HUNTER_AREA(new Coordinate(2571, 2956, 0), 'a', 'k', 's'),
    LIGHTHOUSE(new Coordinate(2503, 3636, 0), 'a', 'l', 'p'),
    HAUNTED_WOODS(new Coordinate(3597, 3495, 0), new QuestRequirement("Priest in Peril", Quest.Status.COMPLETE), 'a', 'l', 'q'),
    ABYSSAL_AREA(new Coordinate(3059, 3875, 0), 'a', 'l', 'r'),
    MCGRUBORS_WOOD(new Coordinate(2644, 3495, 0), 'a', 'l', 's'),
    SOUTH_WEST_MORT_MYRE(new Coordinate(3410, 3324, 0), new QuestRequirement("Priest in Peril", Quest.Status.COMPLETE), 'b', 'i', 'p'),
    ARDOUGNE_ZOO(new Coordinate(2635, 3266, 0), 'b', 'i', 's'),
    NEAR_ZUL_ANDRA(new Coordinate(2150, 3070, 0), new QuestRequirement("Regicide", Quest.Status.COMPLETE), 'b', 'j', 's'),
    SOUTH_OF_CASTLE_WARS(new Coordinate(2385, 3035, 0), 'b', 'k', 'p'),
    ENCHANTED_VALLEY(new Coordinate(3041, 4532, 0), 'b', 'k', 'q'),
    MORT_MYRE_SWAMP(new Coordinate(3469, 3431, 0), new QuestRequirement("Priest in Peril", Quest.Status.COMPLETE), 'b', 'k', 'r'),
    TZHAAR(new Coordinate(2437, 5126, 0), 'b', 'l', 'p'),
    LEGENDS_GUILD(new Coordinate(2740, 3351, 0), 'b', 'l', 'r'),
    MISCELLANIA(new Coordinate(2513, 3384, 0), new QuestRequirement("The Fremennik Trials", Quest.Status.COMPLETE), 'c', 'i', 'p'),
    //I believe varbit 4885 0->1 is the one for the arceuus house library.
    ARCEUUS_HOUSE_LIBRARY(new Coordinate(1639, 3868, 0), new VarbitRequirement(4885, 1), 'c', 'i', 's'),
    SINCLAIR_MANSION(new Coordinate(2705, 3576, 0), 'c', 'j', 'r'),
    COSMIC_ENTITY_PLANE(new Coordinate(2075, 4848, 0), new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), 'c', 'k', 'p'),
    APE_ATOLL(new Coordinate(2740, 2738, 0), new QuestRequirement("Monkey Madness I", Quest.Status.COMPLETE), 'c', 'l', 'r'),
    JUNGLE_SPIDERS_NEAR_YANILLE(new Coordinate(2682, 3081, 0), 'c', 'l', 's'),
    ABYSSAL_NEXUS(new Coordinate(3037, 4763, 0), 'd', 'i', 'p'),
    GORAKS_PLANE(new Coordinate(3038, 5348, 0), 'd', 'i', 'r'),
    SOUTH_OF_MUSA_POINT(new Coordinate(2900, 3111, 0), 'd', 'k', 'p'),
    DESERT_LIZARDS(new Coordinate(3423, 3016, 0), 'd', 'l', 'q'),
    POISON_WASTE(new Coordinate(2213, 3099, 0), 'd', 'l', 'r'),
    MYREQUE_HIDEOUT(new Coordinate(3447, 9824, 0), 'd', 'l', 's'),
    ;

    private final Coordinate coordinate;
    private String action;
    private Requirement requirement;
    private char[] characters;

    FairyRing(Coordinate coordinate, char... characters) {
        this.coordinate = coordinate;
        this.characters = characters;
    }

    FairyRing(Coordinate coordinate, Requirement requirement, char... characters) {
        this.coordinate = coordinate;
        this.requirement = requirement;
        this.characters = characters;
    }

    FairyRing(Coordinate coordinate, String action) {
        this.coordinate = coordinate;
        this.action = action;
    }

    public boolean teleport() {
        GameObject fairyRing;
        if((fairyRing = getLoaded()) != null) {
            if (haveDramenStaffEquipped()) {
                Coordinate pos = fairyRing.getPosition();
                if (isActionCorrect(fairyRing)) {
                    if (action != null) {
                        return Interactions.interact(fairyRing, action) && Execution.delayUntil(() -> getLoaded(pos) == null, Call.playerMovingOrAnimating(), 500, 1000);
                    }
                    return Interactions.interact(fairyRing, getLastDestinationString()) && Execution.delayUntil(() -> getLoaded(pos) == null, Call.playerMovingOrAnimating(), 500, 1000);
                } else {
                    if (isInterfaceOpen()) {
                        if (areCharactersCorrect()) {
                            return useTeleport();
                        } else {
                            InterfaceComponent listUse;
                            //573, 432, 143, 29 -> 573, 247, 143, 41
                            if ((listUse = getFromList()) != null && boundsVisible(listUse)) {
                                return useListTeleport(listUse);
                            } else if(listUse != null) {
                                scrollTo(listUse);
                            } else {
                                setCharactersToDestination(getCharacterOffsets());
                            }
                        }
                    } else {
                        return openFairyRing(fairyRing);
                    }
                }
            } else {
                return equipInventoryStaff();
            }
        }
        return false;
    }

    private boolean scrollTo(InterfaceComponent component) {
        InterfaceComponent parent = Interfaces.getAt(381, 7);
        if(parent == null) {
            return false;
        }
        InteractableRectangle bounds = component.getBounds();
        if(bounds != null) {
            if(!parent.contains(Mouse.getPosition())) {
                Mouse.move(parent);
            }
            else Mouse.scroll(!(bounds.getY() < 247));
        }
        return true;
    }

    private boolean boundsVisible(InterfaceComponent listUse) {
        InteractableRectangle bounds = listUse.getBounds();
        return bounds != null && bounds.getY() >= 247 && bounds.getY() <= 432;
    }

    private boolean equipInventoryStaff() {
        SpriteItem staff;
        return (staff = Inventory.newQuery().names("Dramen staff").results().first()) != null && Interactions.interact(staff, "Wield") && Execution.delayUntil(() -> haveDramenStaffEquipped(), 500, 1000);
    }

    private boolean haveDramenStaffEquipped() {
        return Equipment.newQuery().names("Dramen staff").results().first() != null;
    }

    public boolean handle() {
        teleport();
        return true;
    }

    public String getLastDestinationString() {
        return "Last-destination (" + (characters[0] + "" + characters[1] + "" + characters[2]).toUpperCase() + ")";
    }

    private boolean useListTeleport(InterfaceComponent listUse) {
        GameObject fairyRing = getLoaded();
        return listUse.interact("Use code") && Execution.delayUntil(() -> fairyRing == null || getLoaded(fairyRing.getPosition()) == null, Call.playerAnimating(), 500, 1000);
    }

    private boolean useTeleport() {
        InterfaceComponent useThisLoc = Interfaces.newQuery().containers(398).types(InterfaceComponent.Type.LABEL).actions("Confirm").results().first();
        GameObject fairyRing = getLoaded();
        if (useThisLoc != null) {
            return useThisLoc.interact("Confirm") && Execution.delayUntil(() -> fairyRing == null || getLoaded(fairyRing.getPosition()) == null, Call.playerMovingOrAnimating(), 1000, 2000);
        }
        return false;
    }

    private InterfaceComponent getFromList() {
        return Interfaces.newQuery().containers(381).names((characters[0] + " " + characters[1] + " " + characters[2]).toUpperCase()).actions("Use code").results().first();
    }

    public static boolean isInterfaceOpen() {
        InterfaceComponent interfac;
        return (interfac = Interfaces.newQuery().containers(398).actions("Rotate clockwise").results().first()) != null && interfac.isVisible();
    }

    public boolean openFairyRing(GameObject fairyRing) {
        return Interactions.interact(fairyRing, "Configure") && Execution.delayUntil(() -> isInterfaceOpen(), Call.playerMoving(), 500, 1000);
    }

    public static GameObject getLoaded() {
        return GameObjects.newQuery().names("Fairy ring").results().nearest();
    }

    public static GameObject getLoaded(Coordinate pos) {
        if(pos == null) {
            return null;
        }
        return GameObjects.newQuery().names("Fairy ring").on(pos).results().nearest();
    }

    public boolean isActionCorrect(GameObject fairyRing) {
        if(action != null) {
            return RegalItem.getActions(fairyRing).contains(action);
        }
        return RegalItem.getActions(fairyRing).contains(getLastDestinationString());
    }

    public boolean areCharactersCorrect() {
        return getCharacterOffsets() == -3;
    }

    public int getCharacterOffsets() {
        Varbit firstCharVarbit = Varbits.load(3985);
        Varbit secondCharVarbit = Varbits.load(3986);
        Varbit lastCharVarbit = Varbits.load(3987);
        int val;

        if (firstCharVarbit != null) {
            val = firstCharVarbit.getValue();
            if (characters[0] == 'a') {
                if (val != 0) {
                    //if > 2 -> 4 - val other direction.
                    return 4 - val;
                }
            } else if (characters[0] == 'd') {
                if (val != 1) {
                    return 1 - val;
                }
            } else if (characters[0] == 'c') {
                if (val != 2) {
                    return 2 - val;
                }
            } else if (characters[0] == 'b') {
                if (val != 3) {
                    return 3 - val;
                }
            }
        }
        if (secondCharVarbit != null) {
            val = secondCharVarbit.getValue();
            if (characters[1] == 'i') {
                if (val != 0) {
                    //if > 2 -> 4 - val other direction.
                    return 8 + 4 - val;
                }
            } else if (characters[1] == 'l') {
                if (val != 1) {
                    return 8 + 1 - val;
                }
            } else if (characters[1] == 'k') {
                if (val != 2) {
                    return 8 + 2 - val;
                }
            } else if (characters[1] == 'j') {
                if (val != 3) {
                    return 8 + 3 - val;
                }
            }
        }
        if (lastCharVarbit != null) {
            val = lastCharVarbit.getValue();
            if (characters[2] == 'p') {
                if (val != 0) {
                    //if > 2 -> 4 - val other direction.
                    return 16 + 4 - val;
                }
            } else if (characters[2] == 's') {
                if (val != 1) {
                    return 16 + 1 - val;
                }
            } else if (characters[2] == 'r') {
                if (val != 2) {
                    return 16 + 2 - val;
                }
            } else if (characters[2] == 'q') {
                if (val != 3) {
                    return 16 + 3 - val;
                }
            }
        }
        return -3;
    }


    public void setCharactersToDestination(int valToChange) {
        if (valToChange < 4) {
            if (valToChange > 2) {
                valToChange = (valToChange - 2 * -1);
            }
            setFirstCharacter(valToChange);
        } else if (valToChange < 12) {
            valToChange = valToChange - 8;
            if (valToChange > 2) {
                valToChange = (valToChange - 2 * -1);
            }
            setSecondCharacter(valToChange);
        } else {
            valToChange = valToChange - 16;
            if (valToChange > 2) {
                valToChange = (valToChange - 2 * -1);
            }
            setThirdCharacter(valToChange);
        }
    }

    private void setThirdCharacter(int valToChange) { //Counter is left
        //398, 23 -> 24 counter
        InterfaceComponent rotator;
        String action;
        if (valToChange > 0) {
            rotator = Interfaces.getAt(398, 23);
            action = "Rotate clockwise";
        } else {
            rotator = Interfaces.getAt(398, 24);
            action = "Rotate counter-clockwise";
        }
        if (rotator != null) {
            Varbit prevBit = Varbits.load(3985);
            if (prevBit != null) {
                int prevBitValue = prevBit.getValue();
                if (rotator.interact(action)) {
                    Execution.delayUntil(() -> varbitChanged(3985, prevBitValue), 1000, 2000);
                    Execution.delay(500, 1000);
                }
            }
        }
    }


    private void setSecondCharacter(int valToChange) {
        //398, 21 -> 22 counter
        InterfaceComponent rotator;
        String action;
        if (valToChange > 0) {
            rotator = Interfaces.getAt(398, 21);
            action = "Rotate clockwise";
        } else {
            rotator = Interfaces.getAt(398, 22);
            action = "Rotate counter-clockwise";
        }
        if(rotator != null) {
            Varbit prevBit = Varbits.load(3986);
            if(prevBit != null) {
                int prevBitValue = prevBit.getValue();
                if (rotator.interact(action)) {
                    Execution.delayUntil(() -> varbitChanged(3986, prevBitValue), 1000, 2000);
                    Execution.delay(500, 1000);
                }
            }
        }
    }

    private void setFirstCharacter(int valToChange) {
        ///398 19 -> 20 counter
        InterfaceComponent rotator;
        String action;
        if (valToChange > 0) {
            rotator = Interfaces.getAt(398, 19);
            action = "Rotate clockwise";
        } else {
            rotator = Interfaces.getAt(398, 20);
            action = "Rotate counter-clockwise";
        }
        if(rotator != null) {
            Varbit prevBit = Varbits.load(3987);
            if(prevBit != null) {
                int prevBitValue = prevBit.getValue();
                if (rotator.interact(action)) {
                    Execution.delayUntil(() -> varbitChanged(3987, prevBitValue), 1000, 2000);
                    Execution.delay(500, 1000);
                }
            }
        }
    }

    private boolean varbitChanged(int index, int prevBitValue) {
        Varbit tmp;
        return (tmp = Varbits.load(index)) != null && tmp.getValue() == prevBitValue;
    }

    @Override
    public String toString() {
        return "FairyRing{" +
                "coordinate=" + coordinate +
                ", action='" + action + '\'' +
                ", characters=" + Arrays.toString(characters) +
                '}';
    }

    @Override
    public Coordinate getPosition() {
        return coordinate;
    }

    public Requirement getRequirement() {
        return requirement;
    }
}