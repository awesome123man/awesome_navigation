package com.regal.utility.awesome_navigation.handles;

import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.region.Players;

import java.util.concurrent.Callable;

public class Call {

    public static Callable<Boolean> playerMoving() {
        return () -> {
            Player c = Players.getLocal();
            return c != null && c.isMoving();
        };
    }

    public static Callable<Boolean> playerAnimating() {
        return () -> {
            Player c = Players.getLocal();
            return c != null && c.getAnimationId() != -1;
        };
    }

    public static Callable<Boolean> stanceChangingOrAnimating() {
        return () -> {
            Player c;
            return (c = Players.getLocal()) != null && (c.getStanceId() != 819 || c.getAnimationId() != -1);
        };
    }

    public static Callable<Boolean> playerNotAnimating() {
        return () -> {
            Player c = Players.getLocal();
            return c != null && c.getAnimationId() == -1;
        };
    }

    public static Callable<Boolean> playerNotAnimatingOrMoving() {
        return () -> {
            Player c = Players.getLocal();
            return c != null && c.getAnimationId() == -1 && !c.isMoving();
        };
    }

    public static Callable<Boolean> playerMovingOrAnimating() {
        return () -> {
            Player c = Players.getLocal();
            return c != null && (c.isMoving() || c.getAnimationId() != -1);
        };
    }

}
