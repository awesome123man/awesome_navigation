package com.regal.utility.awesome_navigation.handles;

import com.regal.utility.awesome_navigation.RegalItem;
import com.runemate.game.api.hybrid.entities.LocatableEntity;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.Execution;

import java.util.Arrays;

public enum TravelInterface implements Handleable {

    GNOME_GLIDER_GANDIUS(Type.GNOME_GLIDER, "Gandius", new Coordinate(2970, 2973, 0)),
    GNOME_GLIDER_KAR_HEWO(Type.GNOME_GLIDER,"Kar-Hewo", new Coordinate(3284, 3212, 0)),
    GNOME_GLIDER_SINDARPOS(Type.GNOME_GLIDER, "Sindarpos", new Coordinate(2847, 3499, 0)),
    GNOME_GLIDER_TA_QUIR_PRIW(Type.GNOME_GLIDER, "Ta Quir Priw", new Coordinate(2464, 3501, 3)),
    SPIRIT_TREE_GNOME_VILLAGE(Type.SPIRIT_TREE, "1: Tree Gnome Village", new Coordinate(2542, 3170, 0)),
    SPIRIT_TREE_GNOME_STRONGHOLD(Type.SPIRIT_TREE, "2: Gnome Stronghold", new Coordinate(2461, 3444, 0)),
    SPIRIT_TREE_GE(Type.SPIRIT_TREE, "4: Grand Exchange", new Coordinate(3184, 3509, 0)),
    SPIRIT_TREE_KHAZARD(Type.SPIRIT_TREE, "3: Battlefield of Khazard", new Coordinate(2556, 3260, 0));

    private final Type type;
    private final String text;
    private final Coordinate coordinate;

    TravelInterface(Type type, String text, Coordinate coordinate) {
        this.type = type;
        this.text = text;
        this.coordinate = coordinate;
    }

    @Override
    public boolean handle() {
        if(interfaceOpen()) {
            handleInterface();
        } else {
            openInterface();
        }
        return true;
    }

    @Override
    public Coordinate getPosition() {
        return coordinate;
    }

    private boolean openInterface() {
        LocatableEntity obj = null;
        if(type.equals(Type.SPIRIT_TREE)) {
            obj = GameObjects.newQuery().names("Spirit tree").results().nearest();
        } else if(type.equals(Type.GNOME_GLIDER)) {
            obj = Npcs.newQuery().actions("Glider").surroundingsReachable().results().nearest();
        }
        if(obj != null && obj.distanceTo(coordinate) > 10) {
            return Interactions.interact(obj, RegalItem.getFirstAction(obj)) && Execution.delayUntil(() -> interfaceOpen(), Call.playerMoving(), 500, 1000);
        }
        return false;
    }

    private boolean interfaceOpen() {
        InterfaceComponent interfac;
        if (type.equals(Type.SPIRIT_TREE)) {
            return (interfac = Interfaces.newQuery().containers(type.interfaceIndex).types(type.interfaceType).texts(text).results().first()) != null && interfac.isVisible();
        }
        return (interfac = Interfaces.newQuery().containers(type.interfaceIndex).types(type.interfaceType).actions(text).results().first()) != null && interfac.isVisible();
    }

    public enum Type {
        GNOME_GLIDER(InterfaceComponent.Type.MODEL, 138), SPIRIT_TREE(InterfaceComponent.Type.LABEL, 187);

        public final InterfaceComponent.Type interfaceType;
        public final int interfaceIndex;

        Type(InterfaceComponent.Type interfaceType, int interfaceIndex) {
            this.interfaceType = interfaceType;
            this.interfaceIndex = interfaceIndex;
        }
    }

    public static Handleable[] getOfType(Type type) {
        return Arrays.stream(values()).filter(travelInterface -> travelInterface.type.equals(type)).toArray(Handleable[]::new);
    }

    public boolean handleInterface() {
        InterfaceComponent intfac;
        if (type.equals(Type.SPIRIT_TREE)) {
            return (intfac = Interfaces.newQuery().containers(type.interfaceIndex).types(type.interfaceType).texts(text).results().first()) != null && intfac.isVisible() && intfac.click() && delay();
        }
        return (intfac = Interfaces.newQuery().containers(type.interfaceIndex).types(type.interfaceType).actions(text).results().first()) != null && intfac.isVisible() && intfac.click() && delay();
    }

    private boolean delay() {
        Npc beforeNpc = Npcs.newQuery().actions("Glider", "Travel").on(coordinate).results().first();
        if (beforeNpc != null) {
            String npcName = beforeNpc.getName();
            return Execution.delayUntil(() -> Npcs.newQuery().actions("Glider", "Travel").names(npcName).on(coordinate).results().first() == null, 600, 1000);
        }
        return false;
    }

    @Override
    public String toString() {
        return "TravelInterface{" +
                "containerIndex=" + type.interfaceIndex +
                ", type=" + type +
                ", objectType=" + type.interfaceType +
                ", text='" + text + '\'' +
                ", coordinate=" + coordinate +
                '}';
    }
}
