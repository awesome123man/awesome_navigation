package com.regal.utility.awesome_navigation.handles;

import com.regal.utility.awesome_navigation.Loggable;
import com.regal.utility.awesome_navigation.Pathing;
import com.regal.utility.awesome_navigation.PathingPlayerSense;
import com.regal.utility.awesome_navigation.RegalItem;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.LocatableEntity;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.Screen;
import com.runemate.game.api.hybrid.local.hud.InteractablePoint;
import com.runemate.game.api.hybrid.local.hud.Menu;
import com.runemate.game.api.hybrid.local.hud.MenuItem;
import com.runemate.game.api.hybrid.local.hud.Model;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Health;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.hybrid.util.calculations.Distance;
import com.runemate.game.api.osrs.local.hud.interfaces.Magic;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.AbstractBot;

import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

public class Interactions {

    private final static double visibilityThreshold = PathingPlayerSense.Key.REGAL_ACCEPTABLE_VISIBILITY.getAsDouble();
    private final static int distanceLimit = PathingPlayerSense.Key.REGAL_DISTANCE_TO_WALK.getAsInteger();
    private final static Pattern useAction = Regex.getPatternContainingOneOf("Use", "use");

    private static void checkBrokenAssGarbageMenuProblem(String string) {
        if (Menu.isOpen()) {
            MenuItem interact = Menu.getItem(string);
            if (interact != null && interact.click()) {
                return;
            }
            if (Menu.close()) {
                Environment.getLogger().debug("Closed menu using Menu.close()");
            }
            if (Menu.isOpen()) {
                Environment.getLogger().debug("Menu still open!");
                InteractablePoint randomPoint = Screen.getBounds().getInteractionPoint();
                while (Menu.getBounds().contains(randomPoint)) {
                    Environment.getLogger().severe("Random point was inside of menu!");
                    randomPoint = Screen.getBounds().getInteractionPoint();
                }
                Environment.getLogger().debug("Moving mouse to " + randomPoint);
                Mouse.move(randomPoint);
            }
        }
    }

    private static void checkBrokenAssGarbageMenuProblem(Pattern string) {
        if (Menu.isOpen()) {
            MenuItem interact = Menu.getItem(string);
            if (interact != null && interact.click()) {
                return;
            }
            if (Menu.close()) {
                Environment.getLogger().debug("Closed menu using Menu.close()");
            }
            if (Menu.isOpen()) {
                Environment.getLogger().debug("Menu still open!");
                InteractablePoint randomPoint = Screen.getBounds().getInteractionPoint();
                while (Menu.getBounds().contains(randomPoint)) {
                    Environment.getLogger().severe("Random point was inside of menu!");
                    randomPoint = Screen.getBounds().getInteractionPoint();
                }
                Environment.getLogger().debug("Moving mouse to " + randomPoint);
                Mouse.move(randomPoint);
            }
        }
    }

    private static void checkAccidentallySelectedItem() {
        SpriteItem selectedItem = Inventory.getSelectedItem();
        if (selectedItem != null) {
            Environment.getLogger().warn("We have a selected inventory item while attempting to interact!");
            if (selectedItem.click()) {
                Execution.delay(50, 250);
            }
        }
    }

    private static void makeVisible(LocatableEntity entity) {
        if (Distance.to(entity) >= distanceLimit) {
            Environment.getLogger().info("Walking to " + entity);
            Pathing.pathTo(entity);
        } else if (entity.getVisibility() < visibilityThreshold) {
            Environment.getLogger().info("Turning camera to " + entity);
            Camera.concurrentlyTurnTo(entity);
        }
    }

    public static boolean interact(LocatableEntity entity, String interaction) {
        AbstractBot bot = Environment.getBot();
        if (entity == null || interaction == null || bot == null) {
            Environment.getLogger().severe("Null NPC or interaction string in interact!");
            return false;
        }
        checkBrokenAssGarbageMenuProblem(interaction);
        checkAccidentallySelectedItem();
        checkBankOpen();
        if (entity.getVisibility() < visibilityThreshold) {
            makeVisible(entity);
        }
        if (entity.getVisibility() > visibilityThreshold) {
            Environment.getLogger().info(interaction + " " + entity);
            if(((Loggable) bot).getLogCount() > 100) {
                bot.stop("We have failed to interact with: " + entity + " 100x.");
            }
            return entity.interact(interaction);
        }
        Environment.getLogger().debug("Failed to make " + entity + " visible. " + entity.getVisibility() + " | " + visibilityThreshold);
        if(((Loggable) bot).getLogCount() > 100) {
            bot.stop("We have failed to make: " + entity + " visible 100x.");
        }
        return false;
    }

    private static void checkBankOpen() {
        if(Bank.isOpen()) {
            Bank.close();
        }
    }

    public static boolean interact(LocatableEntity entity, String interaction, boolean lockCamera) {
        AbstractBot bot = Environment.getBot();
        if (entity == null || interaction == null || bot == null) {
            Environment.getLogger().severe("Null NPC or interaction string in interact!");
            return false;
        }
        checkAccidentallySelectedItem();
        if (entity.getVisibility() < visibilityThreshold) {
            if (!lockCamera) {
                makeVisible(entity);
            } else {
                Player me = Players.getLocal();
                Coordinate pos;
                if (me != null && (pos = me.getPosition()) != null) {
                    if (pos.distanceTo(entity) < PathingPlayerSense.Key.REGAL_DISTANCE_TO_WALK.getAsInteger()) {
                        Pathing.viewportTo(entity);
                    } else {
                        Pathing.pathTo(entity);
                    }
                }
            }
        } else {
            Environment.getLogger().info(interaction + " " + entity);
            if(((Loggable) bot).getLogCount() > 100) {
                bot.stop("We have failed to interact with: " + entity + " 100x.");
            }
            return entity.interact(interaction);
        }
        Environment.getLogger().debug("Failed to make " + entity + " visible. " + entity.getVisibility() + " | " + visibilityThreshold);
        if(((Loggable) bot).getLogCount() > 100) {
            bot.stop("We have failed to make: " + entity + " visible 100x.");
        }
        return false;
    }

    public static boolean interact(Coordinate coord, String interaction, boolean lockCamera) {
        AbstractBot bot = Environment.getBot();
        if (coord == null || interaction == null || bot == null) {
            Environment.getLogger().severe("Null coord or interaction string in interact!");
            return false;
        }
        checkBrokenAssGarbageMenuProblem(interaction);
        checkAccidentallySelectedItem();
        if(!interaction.contains("Cast") && Magic.HIGH_LEVEL_ALCHEMY.isSelected()) {
            Magic.HIGH_LEVEL_ALCHEMY.deactivate();
        }
        if(!lockCamera) {
            if (coord.getVisibility() < visibilityThreshold) {
                Camera.concurrentlyTurnTo(coord);
            }
        } else {
            Player me = Players.getLocal();
            Coordinate pos;
            if (me != null && (pos = me.getPosition()) != null) {
                if (!coord.isVisible() && pos.distanceTo(coord) < PathingPlayerSense.Key.REGAL_DISTANCE_TO_WALK.getAsInteger()) {
                    return Pathing.viewportTo(coord);
                } else {
                    Environment.getLogger().info(interaction + " " + coord);
                    if(((Loggable) bot).getLogCount() > 100) {
                        bot.stop("We have failed to interact with: " + coord + " 100x.");
                    }
                    return coord.interact(interaction);
                }
            }
        }
        return coord.getVisibility() >= visibilityThreshold && coord.interact(interaction);
    }

    public static boolean interact(Coordinate coord, String interaction) {
        Environment.getLogger().info(interaction + " " + coord);
        if (coord == null || interaction == null) {
            Environment.getLogger().severe("Null coord or interaction string in interact!");
            return false;
        }
        checkBrokenAssGarbageMenuProblem(interaction);
        checkAccidentallySelectedItem();
        if (coord.getVisibility() < visibilityThreshold) {
            Camera.concurrentlyTurnTo(coord);
        }
        return coord.getVisibility() >= visibilityThreshold && coord.interact(interaction);
    }

    private static boolean interact(LocatableEntity entity, Pattern interaction, boolean withItemSelected) {
        if (entity == null || interaction == null) {
            Environment.getLogger().severe("Null NPC or interaction string in interact!");
            return false;
        }
        checkBrokenAssGarbageMenuProblem(interaction);
        if (!withItemSelected) {
            checkAccidentallySelectedItem();
        }
        if (entity.getVisibility() < visibilityThreshold) {
            makeVisible(entity);
        }
        return entity.getVisibility() >= visibilityThreshold && entity.interact(interaction);
    }

    public static boolean interact(SpriteItem entity, String interaction) {
        if (entity == null || interaction == null) {
            Environment.getLogger().severe("Null NPC or interaction string in interact!");
            return false;
        }
        checkBrokenAssGarbageMenuProblem(interaction);
        checkAccidentallySelectedItem();
        return entity.interact(interaction);
    }

    private static boolean use(final SpriteItem first, final SpriteItem second) {
        if (first != null && second != null && select(first)) {
            Execution.delay(150, 800);
            return second.interact(useAction);
        } else {
            Environment.getLogger().warn("Failed to find or select " + first + " or " + second);
        }
        return false;
    }

    public static boolean useAndHover(final SpriteItem first, final SpriteItem second) {
        if (first != null && second != null && select(first)) {
            Execution.delay(150, 800);
            return second.hover();
        } else {
            Environment.getLogger().warn("Failed to select " + first + " and hover " + second);
        }
        return false;
    }

    public static boolean useQuickly(final SpriteItem first, final SpriteItem second) {
        if (first != null && second != null && select(first)) {
            Execution.delay(50, 180);
            return second.interact(useAction);
        } else {
            Environment.getLogger().warn("Failed to find or select " + first + " or " + second);
        }
        return false;
    }

    public static boolean useQuickly(final SpriteItem first, final GameObject second) {
        if (first != null && second != null && select(first)) {
            Execution.delay(50, 180);
            return interact(second, useAction, true);
        } else {
            Environment.getLogger().warn("Failed to find or select " + first + " or " + second);
        }
        return false;
    }

    public static boolean use(final SpriteItem first, final LocatableEntity second) {
        if (first != null && second != null && select(first)) {
            Execution.delay(150, 800);
            return interact(second, useAction, true);
        } else {
            Environment.getLogger().warn("Failed to find or select " + first + " or " + second);
        }
        return false;
    }

    public static boolean use(final String first, final String second) {
        return use(Inventory.newQuery().names(first).results().first(), Inventory.newQuery().names(second).results().first());
    }

    public static boolean select(final SpriteItem item) {
        SpriteItem selected = Inventory.getSelectedItem();
        if ((selected != null && selected.equals(item)) || item != null && item.interact("Use") && Execution.delayUntil(() -> Inventory.getSelectedItem() != null, 1200)) {
            return true;
        }
        Environment.getLogger().warn("Failed to select " + item);
        return false;
    }

    public static boolean eatOrDrinkAndDelay(SpriteItem food) {
        int healthBefore = Health.getCurrent();
        String action = null;
        if (food != null) {
            ItemDefinition foodDef = food.getDefinition();
            if (foodDef != null) {
                List<String> actions = foodDef.getInventoryActions();
                if (actions != null && !actions.isEmpty()) {
                    if (actions.contains("Eat")) {
                        action = "Eat";
                    } else if (actions.contains("Drink")) {
                        action = "Drink";
                    }
                } else {
                    Environment.getLogger().warn("Could not retrieve actions");
                }
            } else {
                Environment.getLogger().warn("Could not get definition of " + food);
            }
        } else {
            Environment.getLogger().warn("Null food argument in eatOrDrink");
            return false;
        }
        int slot = food.getIndex();
        String name = RegalItem.getNameOf(food);
        if (interact(food, action)) {
            return Execution.delayUntil(() -> Health.getCurrent() - healthBefore > 1 || !Objects.equals(name, RegalItem.getNameOf(Inventory.getItemIn(slot))), 1500, 2500);
        }
        return false;
    }

    public static boolean hover(LocatableEntity entity) {
        if (entity.isVisible()) {
            Model model;
            if((model = entity.getModel()) != null) {
                if(!model.contains(Mouse.getPosition())) {
                    return entity.hover();
                }
            }
        } else {
            return Camera.turnTo(entity);
        }
        return false;
    }

    public static boolean hover(LocatableEntity entity, boolean lockCamera) {
        if(entity != null) {
            Model model;
            if ((model = entity.getModel()) != null) {
                if (!model.contains(Mouse.getPosition())) {
                    if (!entity.hover()) {
                        Player player = Players.getLocal();
                        if (!lockCamera && (!entity.isVisible() && entity.distanceTo(player) < 25)) {
                            return Camera.turnTo(entity);
                        }
                    } else {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean drop(SpriteItem item) {
        int prev = Inventory.newQuery().names(RegalItem.getNameOf(item)).results().size();
        return Interactions.interact(item, "Drop") && Execution.delayUntil(() -> Inventory.newQuery().names(RegalItem.getNameOf(item)).results().size() != prev, 600, 1000);
    }

    public static boolean makeVisible(GameObject object) {
        Player player = Players.getLocal();
        return object == null || player == null || (object.distanceTo(player) < PathingPlayerSense.Key.REGAL_DISTANCE_TO_WALK.getAsInteger()) || Camera.turnTo(object);
    }
}
