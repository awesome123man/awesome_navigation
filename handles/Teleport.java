package com.regal.utility.awesome_navigation.handles;

import com.regal.utility.awesome_navigation.RegalItem;
import com.regal.utility.awesome_navigation.requirements.AreaRequirement;
import com.regal.utility.awesome_navigation.requirements.CombinedRequirement;
import com.regal.utility.awesome_navigation.requirements.InventoryRequirement;
import com.regal.utility.awesome_navigation.requirements.Requirement;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.osrs.local.hud.interfaces.Magic;
import com.runemate.game.api.script.Execution;

import java.util.Arrays;
import java.util.regex.Pattern;

public enum Teleport implements Handleable {

    GAMES_NECKLACE_BARBARIAN_OUTPOST(Regex.getPatternForContainsString("Games necklace("), new Coordinate(2522, 3571, 0), 200, new String[] {"Rub", "Barbarian Outpost."}, new String[] {"Barbarian Outpost"}, Equipment.class, Inventory.class),
    GAMES_NECKLACE_BURTHORPE(Regex.getPatternForContainsString("Games necklace("), new Coordinate(2898, 3553, 0), 200, new String[] {"Rub", "Burthorpe."}, new String[]{"Burthorpe"}, Equipment.class, Inventory.class),
    ARDOUGNE_CLOAK(Regex.getPatternForContainsString("Ardougne cloak "), new Coordinate(2605, 3218, 0), 5, new String[]{"Monastery Teleport"}, new String[] {"Kandarin Monastery"}, Equipment.class, Inventory.class),
    GLORY_EDGEVILLE(Regex.getPatternForContainsString("Amulet of glory("), new Coordinate(3087, 3496, 0),100, new String[]{"Rub","Edgeville"},new String[]{"Edgeville"},Equipment.class, Inventory.class),
    GLORY_KARAMJA(Regex.getPatternForContainsString("Amulet of glory("), new Coordinate(2918, 3176, 0), 100, new String[]{"Rub","Karamja"},new String[]{"Karamja"}, Equipment.class, Inventory.class),
    GLORY_DRAYNOR_VILLAGE(Regex.getPatternForContainsString("Amulet of glory("), new Coordinate(3105, 3251, 0), 100, new String[]{"Rub","Draynor Village"}, new String[]{"Draynor Village"}, Equipment.class, Inventory.class),
    GLORY_AL_KHARID(Regex.getPatternForContainsString("Amulet of glory("), new Coordinate(3293, 3163, 0), 100, new String[]{"Rub","Al Kharid"}, new String[]{"Al Kharid"},Equipment.class, Inventory.class),
    VARROCK_TELETAB(Regex.getPatternForContainsString("Varrock teleport"), new Coordinate(3214, 3424, 0), 150, new String[]{"Break"},null, Inventory.class),
    RING_WEALTH_GE(Regex.getPatternForContainsString("Ring of wealth("), new Coordinate(3162, 3479, 0),150, new String[]{"Rub","Grand Exchange"},new String[]{"Grand Exchange"},Equipment.class, Inventory.class),

    MAGIC_WHISTLE_LEAVE(Regex.getPatternForExactString("Magic whistle"), new Coordinate(2651, 4730, 0), new Coordinate(2741, 3235, 0), 10, new String[]{"Blow"}, new Class[]{Inventory.class},
            new InventoryRequirement("Magic whistle", 1)),
    MAGIC_WHISTLE_LEAVE_DARK(Regex.getPatternForExactString("Magic whistle"), new Coordinate(2779, 4730, 0), new Coordinate(2741, 3235, 0), 10, new String[]{"Blow"}, new Class[]{Inventory.class},
            new InventoryRequirement("Magic whistle", 1)),
    ;

    //LUMBRIDGE_HOME_TELEPORT(Magic.LUMBRIDGE_HOME_TELEPORT, new Coordinate(3220, 3219, 0), 100, new CombatRequirement(false, 10), new CooldownRequirement(30 * LimitChecker.TimeUnit.MINUTES.getMilli())),

    private final Pattern pattern;
    private final Coordinate coordinate;
    private final Requirement[] requirements;
    private final String[] invActions;
    private final String[] equipActions;
    private final Class[] types;
    private final int weight;
    private final Magic spell;
    private Coordinate in;

    Teleport(Pattern pattern, Coordinate coordinate, int weight, String[] invActions, String[] equipActions, Class... types) {
        this(null, pattern, coordinate, weight, invActions, equipActions, types, (Requirement[]) null);
    }

    Teleport(Pattern pattern, Coordinate coordinate, int weight, String[] invActions, Class[] types, Requirement... requirements) {
        this(null, pattern, coordinate, weight, invActions, null, types, requirements);
    }

    Teleport(Pattern pattern, Coordinate in, Coordinate coordinate, int weight, String[] invActions, Class[] types, Requirement... requirements) {
        this(null, pattern, coordinate, weight, invActions, null, types, requirements);
        this.in = in;
    }

    Teleport(Magic spell, Coordinate coordinate, int weight, Requirement... requirements) {
        this(spell, null, coordinate, weight, null, null, null, new CombinedRequirement(true, requirements));
    }

    Teleport(Magic spell, Pattern pattern, Coordinate coordinate, int weight, String[] invActions, String[] equipActions, Class[] types, Requirement... requirements) {
        this.spell = spell;
        this.pattern = pattern;
        this.coordinate = coordinate;
        this.weight = weight;
        this.invActions = invActions;
        this.equipActions = equipActions;
        this.types = types;
        this.requirements = requirements;
    }

    @Override
    public boolean handle() {
        SpriteItem item = null;
        Player player = Players.getLocal();
        if(types == null) {
            if (spell != null && spell.activate()) {
                Execution.delayUntil(() -> Npcs.newQuery().targeting(player).animating().results().first() != null || new Area.Circular(coordinate, 6).contains(player), Call.playerAnimating(), 1000, 2000);
            }
        } else {
            if (Arrays.asList(types).contains(Equipment.class)) {
                item = Equipment.newQuery().names(pattern).actions(equipActions).results().first();
            }
            if (item == null && Arrays.asList(types).contains(Inventory.class)) {
                item = Inventory.newQuery().names(pattern).actions(invActions).results().first();
                if (item != null) {
                    ChatDialog.Option option;
                    if ((option = ChatDialog.getOption(invActions)) != null) {
                        if (option.select()) {
                            Execution.delayUntil(() -> new Area.Circular(coordinate, 5).contains(player), Call.playerAnimating(), 1000, 2000);
                        }
                    } else if(Bank.isOpen()) {
                        Bank.close();
                    } else if (Interactions.interact(item, RegalItem.getActions(item).stream().filter(str -> Arrays.asList(invActions).contains(str)).findFirst().orElse(null))) {
                        Execution.delayUntil(() -> ChatDialog.getOption(invActions) != null || new Area.Circular(coordinate, 6).contains(player), Call.playerAnimating(), 1000, 2000);
                    }
                    return true;
                } else {
                    return false;
                }
            }
            if(Bank.isOpen()) {
                Bank.close();
            }
            else if (Interactions.interact(item, RegalItem.getActions(item).stream().filter(str -> Arrays.asList(equipActions).contains(str)).findFirst().orElse(null))) {
                Execution.delayUntil(() -> new Area.Circular(coordinate, 5).contains(player), Call.playerAnimating(), 1000, 2000);
            }
        }
        return true;
    }

    @Override
    public Coordinate getPosition() {
        return coordinate;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public Class[] getTypes() {
        return types;
    }

    public int getWeight() {
        return weight;
    }

    public Requirement[] getRequirements() {
        return requirements;
    }

    public Coordinate getIn() {
        return in;
    }
}
