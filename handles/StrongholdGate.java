package com.regal.utility.awesome_navigation.handles;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;

import java.util.Objects;

public enum StrongholdGate implements Handleable {

    GATE0(new Coordinate(1889, 5236, 0), new Coordinate(1890, 5236, 0), new Coordinate(1888, 5236, 0)),
    GATE1(new Coordinate(1904, 5242, 0), new Coordinate(1905, 5242, 0), new Coordinate(1903, 5242, 0)),
    GATE2(new Coordinate(1877, 5192, 0), new Coordinate(1877, 5193, 0), new Coordinate(1877, 5191, 0)),
    GATE3(new Coordinate(1868, 5226, 0), new Coordinate(1869, 5226, 0), new Coordinate(1867, 5226, 0)),
    GATE4(new Coordinate(1875, 5207, 0), new Coordinate(1875, 5206, 0), new Coordinate(1875, 5208, 0)),
    GATE5(new Coordinate(1884, 5243, 0), new Coordinate(1885, 5243, 0), new Coordinate(1883, 5243, 0)),
    GATE6(new Coordinate(1889, 5235, 0), new Coordinate(1890, 5235, 0), new Coordinate(1888, 5235, 0)),
    GATE7(new Coordinate(1889, 5211, 0), new Coordinate(1889, 5210, 0), new Coordinate(1889, 5212, 0)),
    GATE8(new Coordinate(1876, 5240, 0), new Coordinate(1877, 5240, 0), new Coordinate(1875, 5240, 0)),
    GATE9(new Coordinate(1859, 5238, 0), new Coordinate(1859, 5237, 0), new Coordinate(1859, 5239, 0)),
    GATE10(new Coordinate(1904, 5204, 0), new Coordinate(1905, 5204, 0), new Coordinate(1903, 5204, 0)),
    GATE11(new Coordinate(1911, 5206, 0), new Coordinate(1911, 5205, 0), new Coordinate(1911, 5207, 0)),
    GATE12(new Coordinate(1889, 5208, 0), new Coordinate(1889, 5209, 0), new Coordinate(1889, 5207, 0)),
    GATE13(new Coordinate(1858, 5238, 0), new Coordinate(1858, 5237, 0), new Coordinate(1858, 5239, 0)),
    GATE14(new Coordinate(1867, 5217, 0), new Coordinate(1868, 5217, 0), new Coordinate(1866, 5217, 0)),
    GATE15(new Coordinate(1879, 5189, 0), new Coordinate(1880, 5189, 0), new Coordinate(1878, 5189, 0)),
    GATE16(new Coordinate(1882, 5189, 0), new Coordinate(1883, 5189, 0), new Coordinate(1881, 5189, 0)),
    GATE17(new Coordinate(1879, 5240, 0), new Coordinate(1880, 5240, 0), new Coordinate(1878, 5240, 0)),
    GATE18(new Coordinate(1886, 5236, 0), new Coordinate(1887, 5236, 0), new Coordinate(1885, 5236, 0)),
    GATE19(new Coordinate(1861, 5212, 0), new Coordinate(1861, 5211, 0), new Coordinate(1861, 5213, 0)),
    GATE20(new Coordinate(1904, 5230, 0), new Coordinate(1904, 5229, 0), new Coordinate(1904, 5231, 0)),
    GATE21(new Coordinate(1911, 5209, 0), new Coordinate(1911, 5208, 0), new Coordinate(1911, 5210, 0)),
    GATE22(new Coordinate(1904, 5203, 0), new Coordinate(1905, 5203, 0), new Coordinate(1903, 5203, 0)),
    GATE23(new Coordinate(1870, 5218, 0), new Coordinate(1871, 5218, 0), new Coordinate(1869, 5218, 0)),
    GATE24(new Coordinate(1874, 5204, 0), new Coordinate(1874, 5203, 0), new Coordinate(1874, 5205, 0)),
    GATE25(new Coordinate(1859, 5235, 0), new Coordinate(1859, 5234, 0), new Coordinate(1859, 5236, 0)),
    GATE26(new Coordinate(1879, 5239, 0), new Coordinate(1880, 5239, 0), new Coordinate(1878, 5239, 0)),
    GATE27(new Coordinate(1912, 5206, 0), new Coordinate(1912, 5205, 0), new Coordinate(1912, 5207, 0)),
    GATE28(new Coordinate(1868, 5227, 0), new Coordinate(1869, 5227, 0), new Coordinate(1867, 5227, 0)),
    GATE29(new Coordinate(1877, 5195, 0), new Coordinate(1877, 5196, 0), new Coordinate(1877, 5194, 0)),
    GATE30(new Coordinate(1879, 5226, 0), new Coordinate(1879, 5227, 0), new Coordinate(1879, 5225, 0)),
    GATE31(new Coordinate(1876, 5239, 0), new Coordinate(1877, 5239, 0), new Coordinate(1875, 5239, 0)),
    GATE32(new Coordinate(1878, 5223, 0), new Coordinate(1878, 5224, 0), new Coordinate(1878, 5222, 0)),
    GATE33(new Coordinate(1905, 5233, 0), new Coordinate(1905, 5232, 0), new Coordinate(1905, 5234, 0)),
    GATE34(new Coordinate(1879, 5188, 0), new Coordinate(1880, 5188, 0), new Coordinate(1878, 5188, 0)),
    GATE35(new Coordinate(1908, 5242, 0), new Coordinate(1909, 5242, 0), new Coordinate(1907, 5242, 0)),
    GATE36(new Coordinate(1861, 5195, 0), new Coordinate(1861, 5194, 0), new Coordinate(1861, 5196, 0)),
    GATE37(new Coordinate(1876, 5192, 0), new Coordinate(1876, 5193, 0), new Coordinate(1876, 5191, 0)),
    GATE38(new Coordinate(1905, 5230, 0), new Coordinate(1905, 5229, 0), new Coordinate(1905, 5231, 0)),
    GATE39(new Coordinate(1887, 5244, 0), new Coordinate(1888, 5244, 0), new Coordinate(1886, 5244, 0)),
    GATE40(new Coordinate(1897, 5213, 0), new Coordinate(1898, 5213, 0), new Coordinate(1896, 5213, 0)),
    GATE41(new Coordinate(1861, 5209, 0), new Coordinate(1861, 5208, 0), new Coordinate(1861, 5210, 0)),
    GATE42(new Coordinate(1865, 5227, 0), new Coordinate(1866, 5227, 0), new Coordinate(1864, 5227, 0)),
    GATE43(new Coordinate(1875, 5204, 0), new Coordinate(1875, 5203, 0), new Coordinate(1875, 5205, 0)),
    GATE44(new Coordinate(1907, 5203, 0), new Coordinate(1908, 5203, 0), new Coordinate(1906, 5203, 0)),
    GATE45(new Coordinate(1858, 5235, 0), new Coordinate(1858, 5234, 0), new Coordinate(1858, 5236, 0)),
    GATE46(new Coordinate(1890, 5211, 0), new Coordinate(1890, 5210, 0), new Coordinate(1890, 5212, 0)),
    GATE47(new Coordinate(1884, 5244, 0), new Coordinate(1885, 5244, 0), new Coordinate(1883, 5244, 0)),
    GATE48(new Coordinate(1860, 5212, 0), new Coordinate(1860, 5211, 0), new Coordinate(1860, 5213, 0)),
    GATE49(new Coordinate(1904, 5233, 0), new Coordinate(1904, 5232, 0), new Coordinate(1904, 5234, 0)),
    GATE50(new Coordinate(1865, 5226, 0), new Coordinate(1866, 5226, 0), new Coordinate(1864, 5226, 0)),
    GATE51(new Coordinate(1894, 5213, 0), new Coordinate(1895, 5213, 0), new Coordinate(1893, 5213, 0)),
    GATE52(new Coordinate(1861, 5198, 0), new Coordinate(1861, 5197, 0), new Coordinate(1861, 5199, 0)),
    GATE53(new Coordinate(1860, 5195, 0), new Coordinate(1860, 5194, 0), new Coordinate(1860, 5196, 0)),
    GATE54(new Coordinate(1890, 5208, 0), new Coordinate(1890, 5209, 0), new Coordinate(1890, 5207, 0)),
    GATE55(new Coordinate(1870, 5217, 0), new Coordinate(1871, 5217, 0), new Coordinate(1869, 5217, 0)),
    GATE56(new Coordinate(1907, 5204, 0), new Coordinate(1908, 5204, 0), new Coordinate(1906, 5204, 0)),
    GATE57(new Coordinate(1897, 5212, 0), new Coordinate(1898, 5212, 0), new Coordinate(1896, 5212, 0)),
    GATE58(new Coordinate(1874, 5207, 0), new Coordinate(1874, 5206, 0), new Coordinate(1874, 5208, 0)),
    GATE59(new Coordinate(1894, 5212, 0), new Coordinate(1895, 5212, 0), new Coordinate(1893, 5212, 0)),
    GATE60(new Coordinate(1878, 5226, 0), new Coordinate(1878, 5227, 0), new Coordinate(1878, 5225, 0)),
    GATE61(new Coordinate(1912, 5209, 0), new Coordinate(1912, 5208, 0), new Coordinate(1912, 5210, 0)),
    GATE62(new Coordinate(1908, 5243, 0), new Coordinate(1909, 5243, 0), new Coordinate(1907, 5243, 0)),
    GATE63(new Coordinate(1879, 5223, 0), new Coordinate(1879, 5224, 0), new Coordinate(1879, 5222, 0)),
    GATE64(new Coordinate(1882, 5188, 0), new Coordinate(1883, 5188, 0), new Coordinate(1881, 5188, 0)),
    GATE65(new Coordinate(1860, 5209, 0), new Coordinate(1860, 5208, 0), new Coordinate(1860, 5210, 0)),
    GATE66(new Coordinate(1886, 5235, 0), new Coordinate(1887, 5235, 0), new Coordinate(1885, 5235, 0)),
    GATE67(new Coordinate(1887, 5243, 0), new Coordinate(1888, 5243, 0), new Coordinate(1886, 5243, 0)),
    GATE68(new Coordinate(1904, 5243, 0), new Coordinate(1905, 5243, 0), new Coordinate(1903, 5243, 0)),
    GATE69(new Coordinate(1860, 5198, 0), new Coordinate(1860, 5197, 0), new Coordinate(1860, 5199, 0)),
    GATE70(new Coordinate(1876, 5195, 0), new Coordinate(1876, 5196, 0), new Coordinate(1876, 5194, 0)),
    GATE71(new Coordinate(1867, 5218, 0), new Coordinate(1868, 5218, 0), new Coordinate(1866, 5218, 0)),
    GATE72(new Coordinate(2021, 5200, 0), new Coordinate(2021, 5201, 0), new Coordinate(2021, 5199, 0)),
    GATE73(new Coordinate(2040, 5223, 0), new Coordinate(2041, 5223, 0), new Coordinate(2039, 5223, 0)),
    GATE74(new Coordinate(1999, 5216, 0), new Coordinate(1998, 5216, 0), new Coordinate(2000, 5216, 0)),
    GATE75(new Coordinate(2016, 5228, 0), new Coordinate(2017, 5228, 0), new Coordinate(2015, 5228, 0)),
    GATE76(new Coordinate(2034, 5208, 0), new Coordinate(2034, 5209, 0), new Coordinate(2034, 5207, 0)),
    GATE77(new Coordinate(2020, 5200, 0), new Coordinate(2020, 5201, 0), new Coordinate(2020, 5199, 0)),
    GATE78(new Coordinate(2005, 5237, 0), new Coordinate(2005, 5236, 0), new Coordinate(2005, 5238, 0)),
    GATE79(new Coordinate(2018, 5227, 0), new Coordinate(2017, 5227, 0), new Coordinate(2019, 5227, 0)),
    GATE80(new Coordinate(2031, 5227, 0), new Coordinate(2031, 5226, 0), new Coordinate(2031, 5228, 0)),
    GATE81(new Coordinate(2020, 5240, 0), new Coordinate(2020, 5241, 0), new Coordinate(2020, 5239, 0)),
    GATE82(new Coordinate(2044, 5239, 0), new Coordinate(2044, 5238, 0), new Coordinate(2044, 5240, 0)),
    GATE83(new Coordinate(1997, 5216, 0), new Coordinate(1998, 5216, 0), new Coordinate(1996, 5216, 0)),
    GATE84(new Coordinate(2045, 5239, 0), new Coordinate(2045, 5238, 0), new Coordinate(2045, 5240, 0)),
    GATE85(new Coordinate(2039, 5245, 0), new Coordinate(2038, 5245, 0), new Coordinate(2040, 5245, 0)),
    GATE86(new Coordinate(2034, 5210, 0), new Coordinate(2034, 5209, 0), new Coordinate(2034, 5211, 0)),
    GATE87(new Coordinate(2006, 5235, 0), new Coordinate(2006, 5236, 0), new Coordinate(2006, 5234, 0)),
    GATE88(new Coordinate(2014, 5242, 0), new Coordinate(2014, 5241, 0), new Coordinate(2014, 5243, 0)),
    GATE89(new Coordinate(2046, 5195, 0), new Coordinate(2046, 5196, 0), new Coordinate(2046, 5194, 0)),
    GATE90(new Coordinate(2045, 5197, 0), new Coordinate(2045, 5196, 0), new Coordinate(2045, 5198, 0)),
    GATE91(new Coordinate(2008, 5216, 0), new Coordinate(2007, 5216, 0), new Coordinate(2009, 5216, 0)),
    GATE92(new Coordinate(2026, 5239, 0), new Coordinate(2026, 5240, 0), new Coordinate(2026, 5238, 0)),
    GATE93(new Coordinate(2032, 5198, 0), new Coordinate(2032, 5197, 0), new Coordinate(2032, 5199, 0)),
    GATE94(new Coordinate(2046, 5197, 0), new Coordinate(2046, 5196, 0), new Coordinate(2046, 5198, 0)),
    GATE95(new Coordinate(2031, 5198, 0), new Coordinate(2031, 5197, 0), new Coordinate(2031, 5199, 0)),
    GATE96(new Coordinate(2033, 5208, 0), new Coordinate(2033, 5209, 0), new Coordinate(2033, 5207, 0)),
    GATE97(new Coordinate(2037, 5245, 0), new Coordinate(2038, 5245, 0), new Coordinate(2036, 5245, 0)),
    GATE98(new Coordinate(2027, 5241, 0), new Coordinate(2027, 5240, 0), new Coordinate(2027, 5242, 0)),
    GATE99(new Coordinate(2005, 5235, 0), new Coordinate(2005, 5236, 0), new Coordinate(2005, 5234, 0)),
    GATE100(new Coordinate(2036, 5201, 0), new Coordinate(2036, 5202, 0), new Coordinate(2036, 5200, 0)),
    GATE101(new Coordinate(2040, 5222, 0), new Coordinate(2041, 5222, 0), new Coordinate(2039, 5222, 0)),
    GATE102(new Coordinate(2042, 5222, 0), new Coordinate(2041, 5222, 0), new Coordinate(2043, 5222, 0)),
    GATE103(new Coordinate(1995, 5196, 0), new Coordinate(1995, 5195, 0), new Coordinate(1995, 5197, 0)),
    GATE104(new Coordinate(1994, 5194, 0), new Coordinate(1994, 5195, 0), new Coordinate(1994, 5193, 0)),
    GATE105(new Coordinate(2019, 5242, 0), new Coordinate(2019, 5241, 0), new Coordinate(2019, 5243, 0)),
    GATE106(new Coordinate(2042, 5223, 0), new Coordinate(2041, 5223, 0), new Coordinate(2043, 5223, 0)),
    GATE107(new Coordinate(2020, 5242, 0), new Coordinate(2020, 5241, 0), new Coordinate(2020, 5243, 0)),
    GATE108(new Coordinate(2008, 5215, 0), new Coordinate(2007, 5215, 0), new Coordinate(2009, 5215, 0)),
    GATE109(new Coordinate(2037, 5244, 0), new Coordinate(2038, 5244, 0), new Coordinate(2036, 5244, 0)),
    GATE110(new Coordinate(2021, 5202, 0), new Coordinate(2021, 5201, 0), new Coordinate(2021, 5203, 0)),
    GATE111(new Coordinate(1997, 5215, 0), new Coordinate(1998, 5215, 0), new Coordinate(1996, 5215, 0)),
    GATE112(new Coordinate(2031, 5225, 0), new Coordinate(2031, 5226, 0), new Coordinate(2031, 5224, 0)),
    GATE113(new Coordinate(2039, 5244, 0), new Coordinate(2038, 5244, 0), new Coordinate(2040, 5244, 0)),
    GATE114(new Coordinate(2016, 5227, 0), new Coordinate(2017, 5227, 0), new Coordinate(2015, 5227, 0)),
    GATE115(new Coordinate(2013, 5242, 0), new Coordinate(2013, 5241, 0), new Coordinate(2013, 5243, 0)),
    GATE116(new Coordinate(2033, 5210, 0), new Coordinate(2033, 5209, 0), new Coordinate(2033, 5211, 0)),
    GATE117(new Coordinate(2005, 5194, 0), new Coordinate(2005, 5193, 0), new Coordinate(2005, 5195, 0)),
    GATE118(new Coordinate(2044, 5237, 0), new Coordinate(2044, 5238, 0), new Coordinate(2044, 5236, 0)),
    GATE119(new Coordinate(1999, 5215, 0), new Coordinate(1998, 5215, 0), new Coordinate(2000, 5215, 0)),
    GATE120(new Coordinate(1995, 5194, 0), new Coordinate(1995, 5195, 0), new Coordinate(1995, 5193, 0)),
    GATE121(new Coordinate(2037, 5201, 0), new Coordinate(2037, 5202, 0), new Coordinate(2037, 5200, 0)),
    GATE122(new Coordinate(2032, 5225, 0), new Coordinate(2032, 5226, 0), new Coordinate(2032, 5224, 0)),
    GATE123(new Coordinate(2020, 5202, 0), new Coordinate(2020, 5201, 0), new Coordinate(2020, 5203, 0)),
    GATE124(new Coordinate(2004, 5194, 0), new Coordinate(2004, 5193, 0), new Coordinate(2004, 5195, 0)),
    GATE125(new Coordinate(2014, 5240, 0), new Coordinate(2014, 5241, 0), new Coordinate(2014, 5239, 0)),
    GATE126(new Coordinate(2037, 5203, 0), new Coordinate(2037, 5202, 0), new Coordinate(2037, 5204, 0)),
    GATE127(new Coordinate(2031, 5196, 0), new Coordinate(2031, 5197, 0), new Coordinate(2031, 5195, 0)),
    GATE128(new Coordinate(2006, 5216, 0), new Coordinate(2007, 5216, 0), new Coordinate(2005, 5216, 0)),
    GATE129(new Coordinate(2032, 5227, 0), new Coordinate(2032, 5226, 0), new Coordinate(2032, 5228, 0)),
    GATE130(new Coordinate(2013, 5240, 0), new Coordinate(2013, 5241, 0), new Coordinate(2013, 5239, 0)),
    GATE131(new Coordinate(2018, 5228, 0), new Coordinate(2017, 5228, 0), new Coordinate(2019, 5228, 0)),
    GATE132(new Coordinate(2026, 5241, 0), new Coordinate(2026, 5240, 0), new Coordinate(2026, 5242, 0)),
    GATE133(new Coordinate(2045, 5195, 0), new Coordinate(2045, 5196, 0), new Coordinate(2045, 5194, 0)),
    GATE134(new Coordinate(1994, 5196, 0), new Coordinate(1994, 5195, 0), new Coordinate(1994, 5197, 0)),
    GATE135(new Coordinate(2032, 5196, 0), new Coordinate(2032, 5197, 0), new Coordinate(2032, 5195, 0)),
    GATE136(new Coordinate(2006, 5237, 0), new Coordinate(2006, 5236, 0), new Coordinate(2006, 5238, 0)),
    GATE137(new Coordinate(2019, 5240, 0), new Coordinate(2019, 5241, 0), new Coordinate(2019, 5239, 0)),
    GATE138(new Coordinate(2006, 5215, 0), new Coordinate(2007, 5215, 0), new Coordinate(2005, 5215, 0)),
    GATE139(new Coordinate(2027, 5239, 0), new Coordinate(2027, 5240, 0), new Coordinate(2027, 5238, 0)),
    GATE140(new Coordinate(2036, 5203, 0), new Coordinate(2036, 5202, 0), new Coordinate(2036, 5204, 0)),
    GATE141(new Coordinate(2045, 5237, 0), new Coordinate(2045, 5238, 0), new Coordinate(2045, 5236, 0)),
    GATE142(new Coordinate(2034, 5186, 0), new Coordinate(2035, 5186, 0), new Coordinate(2033, 5186, 0)),
    GATE143(new Coordinate(2004, 5192, 0), new Coordinate(2004, 5193, 0), new Coordinate(2004, 5191, 0)),
    GATE144(new Coordinate(2005, 5192, 0), new Coordinate(2005, 5193, 0), new Coordinate(2005, 5191, 0)),
    GATE145(new Coordinate(2034, 5185, 0), new Coordinate(2035, 5185, 0), new Coordinate(2033, 5185, 0)),
    GATE146(new Coordinate(2036, 5185, 0), new Coordinate(2035, 5185, 0), new Coordinate(2037, 5185, 0)),
    GATE147(new Coordinate(2036, 5186, 0), new Coordinate(2035, 5186, 0), new Coordinate(2037, 5186, 0)),
    GATE148(new Coordinate(2132, 5278, 0), new Coordinate(2132, 5277, 0), new Coordinate(2132, 5279, 0)),
    GATE149(new Coordinate(2137, 5262, 0), new Coordinate(2136, 5262, 0), new Coordinate(2138, 5262, 0)),
    GATE150(new Coordinate(2140, 5263, 0), new Coordinate(2139, 5263, 0), new Coordinate(2141, 5263, 0)),
    GATE151(new Coordinate(2163, 5275, 0), new Coordinate(2163, 5276, 0), new Coordinate(2163, 5274, 0)),
    GATE152(new Coordinate(2167, 5294, 0), new Coordinate(2167, 5295, 0), new Coordinate(2167, 5293, 0)),
    GATE153(new Coordinate(2167, 5262, 0), new Coordinate(2167, 5263, 0), new Coordinate(2167, 5261, 0)),
    GATE154(new Coordinate(2132, 5281, 0), new Coordinate(2132, 5280, 0), new Coordinate(2132, 5282, 0)),
    GATE155(new Coordinate(2156, 5263, 0), new Coordinate(2155, 5263, 0), new Coordinate(2157, 5263, 0)),
    GATE156(new Coordinate(2166, 5259, 0), new Coordinate(2166, 5258, 0), new Coordinate(2166, 5260, 0)),
    GATE157(new Coordinate(2149, 5299, 0), new Coordinate(2149, 5300, 0), new Coordinate(2149, 5298, 0)),
    GATE158(new Coordinate(2168, 5297, 0), new Coordinate(2168, 5298, 0), new Coordinate(2168, 5296, 0)),
    GATE159(new Coordinate(2127, 5288, 0), new Coordinate(2128, 5288, 0), new Coordinate(2126, 5288, 0)),
    GATE160(new Coordinate(2162, 5287, 0), new Coordinate(2162, 5288, 0), new Coordinate(2162, 5286, 0)),
    GATE161(new Coordinate(2130, 5293, 0), new Coordinate(2130, 5294, 0), new Coordinate(2130, 5292, 0)),
    GATE162(new Coordinate(2166, 5262, 0), new Coordinate(2166, 5263, 0), new Coordinate(2166, 5261, 0)),
    GATE163(new Coordinate(2155, 5286, 0), new Coordinate(2155, 5287, 0), new Coordinate(2155, 5285, 0)),
    GATE164(new Coordinate(2141, 5294, 0), new Coordinate(2142, 5294, 0), new Coordinate(2140, 5294, 0)),
    GATE165(new Coordinate(2149, 5302, 0), new Coordinate(2149, 5303, 0), new Coordinate(2149, 5301, 0)),
    GATE166(new Coordinate(2148, 5299, 0), new Coordinate(2148, 5300, 0), new Coordinate(2148, 5298, 0)),
    GATE167(new Coordinate(2170, 5271, 0), new Coordinate(2169, 5271, 0), new Coordinate(2171, 5271, 0)),
    GATE168(new Coordinate(2167, 5272, 0), new Coordinate(2166, 5272, 0), new Coordinate(2168, 5272, 0)),
    GATE169(new Coordinate(2167, 5297, 0), new Coordinate(2167, 5298, 0), new Coordinate(2167, 5296, 0)),
    GATE170(new Coordinate(2149, 5291, 0), new Coordinate(2150, 5291, 0), new Coordinate(2148, 5291, 0)),
    GATE171(new Coordinate(2152, 5291, 0), new Coordinate(2151, 5291, 0), new Coordinate(2153, 5291, 0)),
    GATE172(new Coordinate(2167, 5259, 0), new Coordinate(2167, 5258, 0), new Coordinate(2167, 5260, 0)),
    GATE173(new Coordinate(2155, 5289, 0), new Coordinate(2155, 5290, 0), new Coordinate(2155, 5288, 0)),
    GATE174(new Coordinate(2133, 5278, 0), new Coordinate(2133, 5277, 0), new Coordinate(2133, 5279, 0)),
    GATE175(new Coordinate(2156, 5289, 0), new Coordinate(2156, 5290, 0), new Coordinate(2156, 5288, 0)),
    GATE176(new Coordinate(2163, 5278, 0), new Coordinate(2163, 5279, 0), new Coordinate(2163, 5277, 0)),
    GATE177(new Coordinate(2156, 5286, 0), new Coordinate(2156, 5287, 0), new Coordinate(2156, 5285, 0)),
    GATE178(new Coordinate(2153, 5263, 0), new Coordinate(2152, 5263, 0), new Coordinate(2154, 5263, 0)),
    GATE179(new Coordinate(2164, 5278, 0), new Coordinate(2164, 5279, 0), new Coordinate(2164, 5277, 0)),
    GATE180(new Coordinate(2152, 5292, 0), new Coordinate(2151, 5292, 0), new Coordinate(2153, 5292, 0)),
    GATE181(new Coordinate(2140, 5262, 0), new Coordinate(2139, 5262, 0), new Coordinate(2141, 5262, 0)),
    GATE182(new Coordinate(2138, 5295, 0), new Coordinate(2139, 5295, 0), new Coordinate(2137, 5295, 0)),
    GATE183(new Coordinate(2149, 5292, 0), new Coordinate(2150, 5292, 0), new Coordinate(2148, 5292, 0)),
    GATE184(new Coordinate(2163, 5287, 0), new Coordinate(2163, 5288, 0), new Coordinate(2163, 5286, 0)),
    GATE185(new Coordinate(2131, 5296, 0), new Coordinate(2131, 5297, 0), new Coordinate(2131, 5295, 0)),
    GATE186(new Coordinate(2133, 5260, 0), new Coordinate(2133, 5261, 0), new Coordinate(2133, 5259, 0)),
    GATE187(new Coordinate(2137, 5263, 0), new Coordinate(2136, 5263, 0), new Coordinate(2138, 5263, 0)),
    GATE188(new Coordinate(2156, 5264, 0), new Coordinate(2155, 5264, 0), new Coordinate(2157, 5264, 0)),
    GATE189(new Coordinate(2124, 5288, 0), new Coordinate(2125, 5288, 0), new Coordinate(2123, 5288, 0)),
    GATE190(new Coordinate(2132, 5260, 0), new Coordinate(2132, 5261, 0), new Coordinate(2132, 5259, 0)),
    GATE191(new Coordinate(2148, 5302, 0), new Coordinate(2148, 5303, 0), new Coordinate(2148, 5301, 0)),
    GATE192(new Coordinate(2167, 5271, 0), new Coordinate(2166, 5271, 0), new Coordinate(2168, 5271, 0)),
    GATE193(new Coordinate(2132, 5257, 0), new Coordinate(2132, 5258, 0), new Coordinate(2132, 5256, 0)),
    GATE194(new Coordinate(2133, 5281, 0), new Coordinate(2133, 5280, 0), new Coordinate(2133, 5282, 0)),
    GATE195(new Coordinate(2138, 5294, 0), new Coordinate(2139, 5294, 0), new Coordinate(2137, 5294, 0)),
    GATE196(new Coordinate(2141, 5295, 0), new Coordinate(2142, 5295, 0), new Coordinate(2140, 5295, 0)),
    GATE197(new Coordinate(2124, 5287, 0), new Coordinate(2125, 5287, 0), new Coordinate(2123, 5287, 0)),
    GATE198(new Coordinate(2170, 5272, 0), new Coordinate(2169, 5272, 0), new Coordinate(2171, 5272, 0)),
    GATE199(new Coordinate(2164, 5275, 0), new Coordinate(2164, 5276, 0), new Coordinate(2164, 5274, 0)),
    GATE200(new Coordinate(2130, 5296, 0), new Coordinate(2130, 5297, 0), new Coordinate(2130, 5295, 0)),
    GATE201(new Coordinate(2163, 5290, 0), new Coordinate(2163, 5291, 0), new Coordinate(2163, 5289, 0)),
    GATE202(new Coordinate(2131, 5293, 0), new Coordinate(2131, 5294, 0), new Coordinate(2131, 5292, 0)),
    GATE203(new Coordinate(2153, 5264, 0), new Coordinate(2152, 5264, 0), new Coordinate(2154, 5264, 0)),
    GATE204(new Coordinate(2168, 5294, 0), new Coordinate(2168, 5295, 0), new Coordinate(2168, 5293, 0)),
    GATE205(new Coordinate(2127, 5287, 0), new Coordinate(2128, 5287, 0), new Coordinate(2126, 5287, 0)),
    GATE206(new Coordinate(2162, 5290, 0), new Coordinate(2162, 5291, 0), new Coordinate(2162, 5289, 0)),
    GATE207(new Coordinate(2133, 5257, 0), new Coordinate(2133, 5258, 0), new Coordinate(2133, 5256, 0)),
    GATE208(new Coordinate(2340, 5177, 0), new Coordinate(2340, 5178, 0), new Coordinate(2340, 5176, 0)),
    GATE209(new Coordinate(2357, 5222, 0), new Coordinate(2357, 5223, 0), new Coordinate(2357, 5221, 0)),
    GATE210(new Coordinate(2345, 5182, 0), new Coordinate(2344, 5182, 0), new Coordinate(2346, 5182, 0)),
    GATE211(new Coordinate(2357, 5211, 0), new Coordinate(2358, 5211, 0), new Coordinate(2356, 5211, 0)),
    GATE212(new Coordinate(2346, 5215, 0), new Coordinate(2347, 5215, 0), new Coordinate(2345, 5215, 0)),
    GATE213(new Coordinate(2335, 5208, 0), new Coordinate(2336, 5208, 0), new Coordinate(2334, 5208, 0)),
    GATE214(new Coordinate(2375, 5182, 0), new Coordinate(2375, 5183, 0), new Coordinate(2375, 5181, 0)),
    GATE215(new Coordinate(2335, 5207, 0), new Coordinate(2336, 5207, 0), new Coordinate(2334, 5207, 0)),
    GATE216(new Coordinate(2349, 5215, 0), new Coordinate(2350, 5215, 0), new Coordinate(2348, 5215, 0)),
    GATE217(new Coordinate(2364, 5206, 0), new Coordinate(2364, 5207, 0), new Coordinate(2364, 5205, 0)),
    GATE218(new Coordinate(2378, 5192, 0), new Coordinate(2377, 5192, 0), new Coordinate(2379, 5192, 0)),
    GATE219(new Coordinate(2376, 5214, 0), new Coordinate(2376, 5215, 0), new Coordinate(2376, 5213, 0)),
    GATE220(new Coordinate(2371, 5198, 0), new Coordinate(2371, 5199, 0), new Coordinate(2371, 5197, 0)),
    GATE221(new Coordinate(2371, 5195, 0), new Coordinate(2371, 5196, 0), new Coordinate(2371, 5194, 0)),
    GATE222(new Coordinate(2374, 5182, 0), new Coordinate(2374, 5183, 0), new Coordinate(2374, 5181, 0)),
    GATE223(new Coordinate(2376, 5217, 0), new Coordinate(2376, 5218, 0), new Coordinate(2376, 5216, 0)),
    GATE224(new Coordinate(2349, 5214, 0), new Coordinate(2350, 5214, 0), new Coordinate(2348, 5214, 0)),
    GATE225(new Coordinate(2364, 5184, 0), new Coordinate(2363, 5184, 0), new Coordinate(2365, 5184, 0)),
    GATE226(new Coordinate(2332, 5207, 0), new Coordinate(2333, 5207, 0), new Coordinate(2331, 5207, 0)),
    GATE227(new Coordinate(2341, 5198, 0), new Coordinate(2341, 5197, 0), new Coordinate(2341, 5199, 0)),
    GATE228(new Coordinate(2375, 5179, 0), new Coordinate(2375, 5178, 0), new Coordinate(2375, 5180, 0)),
    GATE229(new Coordinate(2340, 5201, 0), new Coordinate(2340, 5200, 0), new Coordinate(2340, 5202, 0)),
    GATE230(new Coordinate(2372, 5198, 0), new Coordinate(2372, 5199, 0), new Coordinate(2372, 5197, 0)),
    GATE231(new Coordinate(2356, 5219, 0), new Coordinate(2356, 5220, 0), new Coordinate(2356, 5218, 0)),
    GATE232(new Coordinate(2338, 5216, 0), new Coordinate(2338, 5217, 0), new Coordinate(2338, 5215, 0)),
    GATE233(new Coordinate(2357, 5219, 0), new Coordinate(2357, 5220, 0), new Coordinate(2357, 5218, 0)),
    GATE234(new Coordinate(2371, 5207, 0), new Coordinate(2371, 5208, 0), new Coordinate(2371, 5206, 0)),
    GATE235(new Coordinate(2375, 5191, 0), new Coordinate(2374, 5191, 0), new Coordinate(2376, 5191, 0)),
    GATE236(new Coordinate(2375, 5214, 0), new Coordinate(2375, 5215, 0), new Coordinate(2375, 5213, 0)),
    GATE237(new Coordinate(2378, 5191, 0), new Coordinate(2377, 5191, 0), new Coordinate(2379, 5191, 0)),
    GATE238(new Coordinate(2346, 5214, 0), new Coordinate(2347, 5214, 0), new Coordinate(2345, 5214, 0)),
    GATE239(new Coordinate(2360, 5212, 0), new Coordinate(2359, 5212, 0), new Coordinate(2361, 5212, 0)),
    GATE240(new Coordinate(2357, 5212, 0), new Coordinate(2358, 5212, 0), new Coordinate(2356, 5212, 0)),
    GATE241(new Coordinate(2361, 5184, 0), new Coordinate(2360, 5184, 0), new Coordinate(2362, 5184, 0)),
    GATE242(new Coordinate(2371, 5210, 0), new Coordinate(2371, 5211, 0), new Coordinate(2371, 5209, 0)),
    GATE243(new Coordinate(2340, 5180, 0), new Coordinate(2340, 5181, 0), new Coordinate(2340, 5179, 0)),
    GATE244(new Coordinate(2370, 5210, 0), new Coordinate(2370, 5211, 0), new Coordinate(2370, 5209, 0)),
    GATE245(new Coordinate(2338, 5213, 0), new Coordinate(2338, 5214, 0), new Coordinate(2338, 5212, 0)),
    GATE246(new Coordinate(2340, 5198, 0), new Coordinate(2340, 5197, 0), new Coordinate(2340, 5199, 0)),
    GATE247(new Coordinate(2363, 5206, 0), new Coordinate(2363, 5207, 0), new Coordinate(2363, 5205, 0)),
    GATE248(new Coordinate(2356, 5222, 0), new Coordinate(2356, 5223, 0), new Coordinate(2356, 5221, 0)),
    GATE249(new Coordinate(2370, 5207, 0), new Coordinate(2370, 5208, 0), new Coordinate(2370, 5206, 0)),
    GATE250(new Coordinate(2345, 5183, 0), new Coordinate(2344, 5183, 0), new Coordinate(2346, 5183, 0)),
    GATE251(new Coordinate(2372, 5195, 0), new Coordinate(2372, 5196, 0), new Coordinate(2372, 5194, 0)),
    GATE252(new Coordinate(2375, 5217, 0), new Coordinate(2375, 5218, 0), new Coordinate(2375, 5216, 0)),
    GATE253(new Coordinate(2341, 5180, 0), new Coordinate(2341, 5181, 0), new Coordinate(2341, 5179, 0)),
    GATE254(new Coordinate(2364, 5183, 0), new Coordinate(2363, 5183, 0), new Coordinate(2365, 5183, 0)),
    GATE255(new Coordinate(2332, 5208, 0), new Coordinate(2333, 5208, 0), new Coordinate(2331, 5208, 0)),
    GATE256(new Coordinate(2361, 5183, 0), new Coordinate(2360, 5183, 0), new Coordinate(2362, 5183, 0)),
    GATE257(new Coordinate(2364, 5209, 0), new Coordinate(2364, 5210, 0), new Coordinate(2364, 5208, 0)),
    GATE258(new Coordinate(2339, 5213, 0), new Coordinate(2339, 5214, 0), new Coordinate(2339, 5212, 0)),
    GATE259(new Coordinate(2375, 5192, 0), new Coordinate(2374, 5192, 0), new Coordinate(2376, 5192, 0)),
    GATE260(new Coordinate(2348, 5183, 0), new Coordinate(2347, 5183, 0), new Coordinate(2349, 5183, 0)),
    GATE261(new Coordinate(2363, 5209, 0), new Coordinate(2363, 5210, 0), new Coordinate(2363, 5208, 0)),
    GATE262(new Coordinate(2348, 5182, 0), new Coordinate(2347, 5182, 0), new Coordinate(2349, 5182, 0)),
    GATE263(new Coordinate(2341, 5201, 0), new Coordinate(2341, 5200, 0), new Coordinate(2341, 5202, 0)),
    GATE264(new Coordinate(2341, 5177, 0), new Coordinate(2341, 5178, 0), new Coordinate(2341, 5176, 0)),
    GATE265(new Coordinate(2360, 5211, 0), new Coordinate(2359, 5211, 0), new Coordinate(2361, 5211, 0)),
    GATE266(new Coordinate(2339, 5216, 0), new Coordinate(2339, 5217, 0), new Coordinate(2339, 5215, 0)),
    GATE267(new Coordinate(2374, 5179, 0), new Coordinate(2374, 5178, 0), new Coordinate(2374, 5180, 0)),
    GATE268(new Coordinate(2324, 5188, 0), new Coordinate(2324, 5189, 0), new Coordinate(2324, 5187, 0)),
    GATE269(new Coordinate(2359, 5232, 0), new Coordinate(2359, 5233, 0), new Coordinate(2359, 5231, 0)),
    GATE270(new Coordinate(2355, 5221, 0), new Coordinate(2355, 5220, 0), new Coordinate(2355, 5222, 0)),
    GATE271(new Coordinate(2356, 5218, 0), new Coordinate(2356, 5217, 0), new Coordinate(2356, 5219, 0)),
    GATE272(new Coordinate(2308, 5225, 0), new Coordinate(2309, 5225, 0), new Coordinate(2307, 5225, 0)),
    GATE273(new Coordinate(2323, 5240, 0), new Coordinate(2323, 5241, 0), new Coordinate(2323, 5239, 0)),
    GATE274(new Coordinate(2344, 5188, 0), new Coordinate(2345, 5188, 0), new Coordinate(2343, 5188, 0)),
    GATE275(new Coordinate(2319, 5212, 0), new Coordinate(2319, 5211, 0), new Coordinate(2319, 5213, 0)),
    GATE276(new Coordinate(2324, 5240, 0), new Coordinate(2324, 5241, 0), new Coordinate(2324, 5239, 0)),
    GATE277(new Coordinate(2361, 5204, 0), new Coordinate(2361, 5205, 0), new Coordinate(2361, 5203, 0)),
    GATE278(new Coordinate(2336, 5194, 0), new Coordinate(2337, 5194, 0), new Coordinate(2335, 5194, 0)),
    GATE279(new Coordinate(2309, 5205, 0), new Coordinate(2310, 5205, 0), new Coordinate(2308, 5205, 0)),
    GATE280(new Coordinate(2315, 5186, 0), new Coordinate(2316, 5186, 0), new Coordinate(2314, 5186, 0)),
    GATE281(new Coordinate(2360, 5232, 0), new Coordinate(2360, 5233, 0), new Coordinate(2360, 5231, 0)),
    GATE282(new Coordinate(2362, 5188, 0), new Coordinate(2363, 5188, 0), new Coordinate(2361, 5188, 0)),
    GATE283(new Coordinate(2320, 5212, 0), new Coordinate(2320, 5211, 0), new Coordinate(2320, 5213, 0)),
    GATE284(new Coordinate(2360, 5193, 0), new Coordinate(2361, 5193, 0), new Coordinate(2359, 5193, 0)),
    GATE285(new Coordinate(2318, 5187, 0), new Coordinate(2319, 5187, 0), new Coordinate(2317, 5187, 0)),
    GATE286(new Coordinate(2344, 5187, 0), new Coordinate(2345, 5187, 0), new Coordinate(2343, 5187, 0)),
    GATE287(new Coordinate(2336, 5227, 0), new Coordinate(2337, 5227, 0), new Coordinate(2335, 5227, 0)),
    GATE288(new Coordinate(2347, 5187, 0), new Coordinate(2348, 5187, 0), new Coordinate(2346, 5187, 0)),
    GATE289(new Coordinate(2323, 5191, 0), new Coordinate(2323, 5192, 0), new Coordinate(2323, 5190, 0)),
    GATE290(new Coordinate(2340, 5224, 0), new Coordinate(2340, 5223, 0), new Coordinate(2340, 5225, 0)),
    GATE291(new Coordinate(2336, 5226, 0), new Coordinate(2337, 5226, 0), new Coordinate(2335, 5226, 0)),
    GATE292(new Coordinate(2324, 5191, 0), new Coordinate(2324, 5192, 0), new Coordinate(2324, 5190, 0)),
    GATE293(new Coordinate(2366, 5218, 0), new Coordinate(2366, 5217, 0), new Coordinate(2366, 5219, 0)),
    GATE294(new Coordinate(2315, 5187, 0), new Coordinate(2316, 5187, 0), new Coordinate(2314, 5187, 0)),
    GATE295(new Coordinate(2341, 5221, 0), new Coordinate(2341, 5220, 0), new Coordinate(2341, 5222, 0)),
    GATE296(new Coordinate(2362, 5189, 0), new Coordinate(2363, 5189, 0), new Coordinate(2361, 5189, 0)),
    GATE297(new Coordinate(2360, 5194, 0), new Coordinate(2361, 5194, 0), new Coordinate(2359, 5194, 0)),
    GATE298(new Coordinate(2359, 5189, 0), new Coordinate(2360, 5189, 0), new Coordinate(2358, 5189, 0)),
    GATE299(new Coordinate(2356, 5221, 0), new Coordinate(2356, 5220, 0), new Coordinate(2356, 5222, 0)),
    GATE300(new Coordinate(2355, 5245, 0), new Coordinate(2354, 5245, 0), new Coordinate(2356, 5245, 0)),
    GATE301(new Coordinate(2352, 5245, 0), new Coordinate(2351, 5245, 0), new Coordinate(2353, 5245, 0)),
    GATE302(new Coordinate(2320, 5215, 0), new Coordinate(2320, 5214, 0), new Coordinate(2320, 5216, 0)),
    GATE303(new Coordinate(2340, 5221, 0), new Coordinate(2340, 5220, 0), new Coordinate(2340, 5222, 0)),
    GATE304(new Coordinate(2336, 5237, 0), new Coordinate(2337, 5237, 0), new Coordinate(2335, 5237, 0)),
    GATE305(new Coordinate(2312, 5205, 0), new Coordinate(2313, 5205, 0), new Coordinate(2311, 5205, 0)),
    GATE306(new Coordinate(2323, 5188, 0), new Coordinate(2323, 5189, 0), new Coordinate(2323, 5187, 0)),
    GATE307(new Coordinate(2336, 5238, 0), new Coordinate(2337, 5238, 0), new Coordinate(2335, 5238, 0)),
    GATE308(new Coordinate(2333, 5194, 0), new Coordinate(2334, 5194, 0), new Coordinate(2332, 5194, 0)),
    GATE309(new Coordinate(2363, 5194, 0), new Coordinate(2364, 5194, 0), new Coordinate(2362, 5194, 0)),
    GATE310(new Coordinate(2362, 5204, 0), new Coordinate(2362, 5205, 0), new Coordinate(2362, 5203, 0)),
    GATE311(new Coordinate(2365, 5218, 0), new Coordinate(2365, 5217, 0), new Coordinate(2365, 5219, 0)),
    GATE312(new Coordinate(2355, 5246, 0), new Coordinate(2354, 5246, 0), new Coordinate(2356, 5246, 0)),
    GATE313(new Coordinate(2319, 5215, 0), new Coordinate(2319, 5214, 0), new Coordinate(2319, 5216, 0)),
    GATE314(new Coordinate(2336, 5193, 0), new Coordinate(2337, 5193, 0), new Coordinate(2335, 5193, 0)),
    GATE315(new Coordinate(2318, 5186, 0), new Coordinate(2319, 5186, 0), new Coordinate(2317, 5186, 0)),
    GATE316(new Coordinate(2324, 5243, 0), new Coordinate(2324, 5244, 0), new Coordinate(2324, 5242, 0)),
    GATE317(new Coordinate(2311, 5224, 0), new Coordinate(2312, 5224, 0), new Coordinate(2310, 5224, 0)),
    GATE318(new Coordinate(2359, 5235, 0), new Coordinate(2359, 5236, 0), new Coordinate(2359, 5234, 0)),
    GATE319(new Coordinate(2309, 5204, 0), new Coordinate(2310, 5204, 0), new Coordinate(2308, 5204, 0)),
    GATE320(new Coordinate(2365, 5221, 0), new Coordinate(2365, 5220, 0), new Coordinate(2365, 5222, 0)),
    GATE321(new Coordinate(2355, 5218, 0), new Coordinate(2355, 5217, 0), new Coordinate(2355, 5219, 0)),
    GATE322(new Coordinate(2323, 5243, 0), new Coordinate(2323, 5244, 0), new Coordinate(2323, 5242, 0)),
    GATE323(new Coordinate(2359, 5188, 0), new Coordinate(2360, 5188, 0), new Coordinate(2358, 5188, 0)),
    GATE324(new Coordinate(2366, 5221, 0), new Coordinate(2366, 5220, 0), new Coordinate(2366, 5222, 0)),
    GATE325(new Coordinate(2312, 5204, 0), new Coordinate(2313, 5204, 0), new Coordinate(2311, 5204, 0)),
    GATE326(new Coordinate(2333, 5226, 0), new Coordinate(2334, 5226, 0), new Coordinate(2332, 5226, 0)),
    GATE327(new Coordinate(2333, 5193, 0), new Coordinate(2334, 5193, 0), new Coordinate(2332, 5193, 0)),
    GATE328(new Coordinate(2341, 5224, 0), new Coordinate(2341, 5223, 0), new Coordinate(2341, 5225, 0)),
    GATE329(new Coordinate(2311, 5225, 0), new Coordinate(2312, 5225, 0), new Coordinate(2310, 5225, 0)),
    GATE330(new Coordinate(2333, 5238, 0), new Coordinate(2334, 5238, 0), new Coordinate(2332, 5238, 0)),
    GATE331(new Coordinate(2333, 5237, 0), new Coordinate(2334, 5237, 0), new Coordinate(2332, 5237, 0)),
    GATE332(new Coordinate(2362, 5207, 0), new Coordinate(2362, 5208, 0), new Coordinate(2362, 5206, 0)),
    GATE333(new Coordinate(2360, 5235, 0), new Coordinate(2360, 5236, 0), new Coordinate(2360, 5234, 0)),
    GATE334(new Coordinate(2347, 5188, 0), new Coordinate(2348, 5188, 0), new Coordinate(2346, 5188, 0)),
    GATE335(new Coordinate(2361, 5207, 0), new Coordinate(2361, 5208, 0), new Coordinate(2361, 5206, 0)),
    GATE336(new Coordinate(2333, 5227, 0), new Coordinate(2334, 5227, 0), new Coordinate(2332, 5227, 0)),
    GATE337(new Coordinate(2352, 5246, 0), new Coordinate(2351, 5246, 0), new Coordinate(2353, 5246, 0)),
    GATE338(new Coordinate(2363, 5193, 0), new Coordinate(2364, 5193, 0), new Coordinate(2362, 5193, 0)),
    GATE339(new Coordinate(2308, 5224, 0), new Coordinate(2309, 5224, 0), new Coordinate(2307, 5224, 0)),
    ;

    private final Coordinate gatePos;
    private final Coordinate inner;
    private final Coordinate outer;
    private final String[] CHAT_DIALOG_ANSWERS = new String[]{
            "Do not visit the website and report the player who messaged you.",
            "Don't tell them anything and click the 'Report Abuse' button.",
            "No.",
            "Don't share your information and report the player.",
            "The birthday of a famous person or event.",
            "Through account settings on oldschool.runescape.com",
            "Don't give them the information and send an 'Abuse report'.",
            "Decline the offer and report that player.",
            "Report the incident and do not click any links.",
            "Do not visit the website and report the player who messaged you.",
            "Don't give your password to anyone. Not even close friends.",
            "Set up 2 set authentication with my email provider.",
            "Nothing, it's a fake.",
            "Me.",
            "Talk to any banker.",
            "Secure my device and reset my password.",
            "Virus scan my device then change my password.",
            "Nobody",
            "Report the stream as a scam. Real Jagex streams have a 'verified' mark.", //Not sure if it has a .
            "Politely tell them no then use the report abuse button.",
            "To help me recover my password if I forget it or if it is stolen.",
            "Use the recover a lost password section.",
            "Only on the RuneScape website.",
            "Nowhere.",
            "No, it might steal my password",
            "To help me recover my password if I forget it or if it is stolen.",
            "Read the text and follow the advice given",
            "Don't type in my password backwards and report the player.",
            "No, you should never buy an account.",
            "No way! You'll just take my gold for your own! Reported!",
            "Report the player for phishing.",
            "Use the Account Recovery System.",
            "Delete it - it's a fake!",
            "Authenticator and tow-step login on my registered email.",
            "Don't give them my password.",
            "Delete it - it's a fake!"
    };


    StrongholdGate(Coordinate gate, Coordinate inner, Coordinate outer) {
        this.gatePos = gate;
        this.inner = inner;
        this.outer = outer;
    }

    @Override
    public boolean handle() {
        GameObject gateObj;
        if((gateObj = GameObjects.newQuery().names("Gate of War", "Rickety door", "Portal of Death", "Oozing barrier").actions("Open").on(gatePos).results().first()) != null) {
            String text = ChatDialog.getText();
            if(text != null && ChatDialog.isOpen()) {
                ChatDialog.Continue cont;
                if ((cont = ChatDialog.getContinue()) != null) {
                    if (cont.select()) {
                        final Player player = Players.getLocal();
                        if (player != null) {
                            Coordinate prevPos = player.getPosition();
                            if(text.startsWith("Correct!") || text.startsWith("Well done!")) {
                                Execution.delayUntil(() -> !Objects.equals(player.getPosition(), prevPos), Call.playerMovingOrAnimating(), 500, 1000);
                            } else {
                                Execution.delayUntil(() -> !Objects.equals(text, ChatDialog.getText()), 500, 1000);
                            }
                        }
                        return true;
                    }
                }
            } else {
                ChatDialog.Option answer;
                if((answer = ChatDialog.getOption(CHAT_DIALOG_ANSWERS)) != null) {
                    if(answer.select()) {
                        Execution.delayUntil(() -> ChatDialog.getContinue() != null, 1000, 2000);
                        return true;
                    }
                } else {
                    if(Interactions.interact(gateObj, "Open")) {
                        final Player player = Players.getLocal();
                        if(player != null) {
                            Coordinate further;
                            if(player.distanceTo(outer) > player.distanceTo(inner)) {
                                further = outer;
                            } else {
                                further = inner;
                            }
                            Execution.delayUntil(() -> Objects.equals(player.getPosition(), further) || ChatDialog.isOpen(), Call.playerMovingOrAnimating(), 500, 1000);
                        }
                        return true;
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public Coordinate getPosition() {
        return gatePos;
    }

    public Coordinate getInner() {
        return inner;
    }

    public Coordinate getOuter() {
        return outer;
    }
}
