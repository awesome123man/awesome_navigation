package com.regal.utility.awesome_navigation.handles;

import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.script.Execution;

public class InterfaceHandler {

    private String option;
    private final String action;
    private int container = -1;
    private boolean toggleReminder = false;
    private final int[] CONATINER_IDS = new int[] {569, 579};
    private final int SPRITE_ID = 941;

    public InterfaceHandler(String option, String action, boolean toggleReminder) {
        this.option = option;
        this.action = action;
        this.toggleReminder = toggleReminder;
    }

    public InterfaceHandler(int container, String action) {
        this.container = container;
        this.action = action;
    }

    public boolean handleWarningInterface() {
        InterfaceComponent reminderOption;
        if (toggleReminder && (reminderOption = Interfaces.newQuery().containers(CONATINER_IDS).sprites(SPRITE_ID).results().first()) != null && reminderOption.isVisible()) {
            if (reminderOption.interact("Off/On")) {
                Execution.delayUntil(() -> Interfaces.newQuery().containers(CONATINER_IDS).sprites(SPRITE_ID).results().first() == null, 500, 1000);
                return true;
            }
        }

        InterfaceComponent yesOption;
        if (option != null) {
            if ((yesOption = Interfaces.newQuery().containers(CONATINER_IDS).texts(option).actions(action).results().first()) != null && yesOption.isVisible()) {
                if (yesOption.interact(action)) {
                    Execution.delayUntil(() -> Interfaces.newQuery().containers(CONATINER_IDS).texts(option).actions(action).results().first() == null, 500, 1000);
                    return true;
                }
            }
        }
        InterfaceComponent component;
        if (container != -1) {
            if ((component = Interfaces.newQuery().containers(container).actions(action).results().first()) != null) {
                if (component.interact(action)) {
                    Execution.delayUntil(() -> Interfaces.newQuery().containers(container).actions(action).results().first() == null, 500, 1000);
                    return true;
                }
            }
        }
        return false;
    }
}
