package com.regal.utility.awesome_navigation.handles;

import com.regal.utility.awesome_navigation.RegalItem;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class NpcObjectHandle implements Handleable {

    private final String[] names;
    private final String[] actions;
    private final Area npcArea;
    private final String[] chatOptions;
    private final Coordinate source;
    private final Coordinate destination;

    public NpcObjectHandle(String[] names, String[] actions, Area npcArea, String[] chatOptions, Coordinate source, Coordinate destination) {
        this.names = names;
        this.actions = actions;
        this.npcArea = npcArea;
        this.chatOptions = chatOptions;
        this.source = source;
        this.destination = destination;
    }

    @Override
    public boolean handle() {
        ChatDialog.Option option;
        ChatDialog.Continue continu;
        if((option = ChatDialog.getOption(chatOptions)) != null) {
            if(option.select()) {
                Execution.delayUntil(() -> optionGone(option), 2000, 3000);
            }
            return true;
        } else if((continu = getChatContinue()) != null) {
            handleContinue(continu);
            return true;
        } else {
            Npc obj;
            if ((obj = getObject()) != null) {
                List<String> actions = RegalItem.getActions(obj);
                String interact = Arrays.stream(this.actions).filter(actions::contains).findFirst().orElse(null);
                if (interact != null) {
                    if (Interactions.interact(obj, interact)) {
                        Execution.delayUntil(this::playerPosEqualsOut, Call.playerMovingOrAnimating(), 500, 1000);
                    }
                }
                return true;
            }
        }
        return false;
    }

    public Npc getObject() {
        return Npcs.newQuery().names(names).actions(actions).within(npcArea).results().first();
    }

    private boolean optionGone(ChatDialog.Option option) {
        List<ChatDialog.Option> options;
        return (options = ChatDialog.getOptions()) == null || !options.contains(option);
    }

    private boolean playerPosEqualsOut() {
        Player player = Players.getLocal();
        return player == null || Objects.equals(player.getPosition(), destination);
    }

    private void handleContinue(ChatDialog.Continue continu) {
        if(continu.select()) {
            Execution.delayUntil(() -> ChatDialog.getContinue() == null, 400, 800);
        }
    }

    private ChatDialog.Continue getChatContinue() {
        return ChatDialog.getContinue();
    }


    @Override
    public Coordinate getPosition() {
        return source;
    }

    @Override
    public String toString() {
        return "NpcObjectHandle{" +
                "names=" + Arrays.toString(names) +
                ", actions=" + Arrays.toString(actions) +
                ", npcArea=" + npcArea +
                ", chatOptions=" + Arrays.toString(chatOptions) +
                ", source=" + source +
                ", destination=" + destination +
                '}';
    }
}
