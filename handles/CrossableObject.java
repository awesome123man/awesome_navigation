package com.regal.utility.awesome_navigation.handles;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.GameObjectQueryBuilder;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.regal.utility.awesome_navigation.RegalItem;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;

public enum CrossableObject implements Handleable {
    //VARBIT 5090 -> 1 when leaving brutal blacks.
    KALPHITE_LAIR_ENTRANCE(new Coordinate(3228, 3108, 0), new Coordinate(3484, 9510, 2), "Tunnel entrance", "Climb-down", "Rope"),
    KALPHITE_LAIR_EXIT(new Coordinate(3483, 9510, 2), new Coordinate(3229, 3108, 0), "Rope", "Climb-up"),
    TAVERLY_TUBE_BLUE_DRAGONS_ENTRANCE(new Coordinate(2887, 9799, 0), new Coordinate(2892, 9799, 0), "Obstacle pipe", "Squeeze-through"),
    TAVERLY_TUBE_BLUE_DRAGONS_EXIT(new Coordinate(2890, 9799, 0), new Coordinate(2886, 9799, 0), "Obstacle pipe", "Squeeze-through"),
    LUMBRIDGE_SWAMP_DUNGEON_ENTRANCE(new Coordinate(3169, 3172, 0), new Coordinate(3168, 9572, 0), "Dark hole", "Climb-down"),
    LUMBRIDGE_SWAMP_DUNGEON_EXIT(new Coordinate(3169, 9572, 0), new Coordinate(3168, 3172, 0), "Climbing rope", "Climb"),
    RIMMINGTON_GANGPLANK_CORSAIR(new Coordinate(2909, 3228, 1), new Coordinate(2909, 3226, 0), "Gangplank", "Cross"),
    CORSAIR_GANGPLANK_RIMMINGTON(new Coordinate(2578, 2838, 1), new Coordinate(2578, 2840, 0), "Gangplank", "Cross"),
    ARDY_GANGPLANK_BRIMHAVEN(new Coordinate(2774, 3234, 1), new Coordinate(2772, 3234, 0), "Gangplank", "Cross"),
    BRIMHAVEN_GANGPLANK_ARDY(new Coordinate(2683, 3269, 1), new Coordinate(2683, 3271, 0), "Gangplank", "Cross"),

    HOLE_CORSAIR_TO_OGRESS(new Coordinate(2523, 2861, 0), new Coordinate(2012, 9006, 1), "Hole", "Enter"),
    VINE_LADDER_OGRESS_CORSAIR(new Coordinate(2012, 9005, 1), new Coordinate(2523, 2862, 0), "Vine ladder", "Climb"),
    MT_KARUULM_ROCKS_EAST(new Coordinate(1321, 10205, 0), new Coordinate(1322, 10205, 0), "Rocks", "Climb"),
    MT_KARUULM_ROCKS_EAST_1(new Coordinate(1321, 10205, 0), new Coordinate(1320, 10205, 0), "Rocks", "Climb"),
    MT_KARUULM_STAIRS_EAST(new Coordinate(1330, 10205, 0), new Coordinate(1334, 10205, 1), "Steps", "Climb"),
    MT_KARUULM_STAIRS_EAST_1(new Coordinate(1333, 10205, 1), new Coordinate(1329, 10205, 0), "Steps", "Climb"),
    MT_KARUULM_STAIRS_TOP(new Coordinate(1317, 10189, 2), new Coordinate(1313, 10188, 1), "Steps", "Climb"),
    MT_KARUULM_STAIRS_TOP_1(new Coordinate(1314, 10189, 1), new Coordinate(1318, 10188, 2), "Steps", "Climb"),
    MT_KARUULM_ELEVATOR(new Coordinate(1311, 3807, 0), new Coordinate(1311, 10188, 0), "Elevator", "Activate"),
    MT_KARUULM_ELEVATOR_1(new Coordinate(1311, 10187, 0), new Coordinate(1311, 3807, 0), "Cave exit", "Exit"),
    PRIEST_IN_PERIL_GATE_1(new Coordinate(3431, 9897, 0), new Coordinate(3431, 9897, 0), "Gate", "Open"),
    PRIEST_IN_PERIL_GATE_2(new Coordinate(3405, 9895, 0), new Coordinate(3405, 9895, 0), "Gate", "Open"),
    PRIEST_IN_PERIL_GATE_2a(new Coordinate(3405, 9895, 0), new Coordinate(3405, 9894, 0), "Gate", "Open"),
    VARROCK_PRIEST_IN_PERIL_GATE(new Coordinate(3319, 3468, 0), new Coordinate(3318, 3468, 0), "Gate", "Open"),
    VARROCK_PRIEST_IN_PERIL_GATE_1(new Coordinate(3431, 9897, 0), new Coordinate(3430, 9897, 0), "Gate", "Open"),
    TAVERLY_DUNGEON_ENTRANCE(new Coordinate(2884, 3397, 0), new Coordinate(2884, 9798, 0), "Ladder", "Climb-down"),
    TAVERLY_DUNEGON_EXIT(new Coordinate(2884, 9797, 0), new Coordinate(2884, 3398, 0), "Ladder", "Climb-up"),
    GRAND_TREE_BOTTOM_LADDER_UP(new Coordinate(2466, 3495, 0), new Coordinate(2466, 3494, 1), "Ladder", "Climb-up"),
    GRAND_TREE_1ST_LADDER_UP(new Coordinate(2466, 3495, 1), new Coordinate(2466, 3494, 2), "Ladder", "Climb-up"),
    GRAND_TREE_1ST_LADDER_DOWN(new Coordinate(2466, 3495, 1), new Coordinate(2466, 3494, 0), "Ladder", "Climb-down"),
    GRAND_TREE_2ND_LADDER_UP(new Coordinate(2466, 3495, 2), new Coordinate(2466, 3494, 3), "Ladder", "Climb-up"),
    GRAND_TREE_2ND_LADDER_DOWN(new Coordinate(2466, 3495, 2), new Coordinate(2466, 3494, 1), "Ladder", "Climb-down"),
    GRAND_TREE_3RD_LADDER_DOWN(new Coordinate(2466, 3495, 3), new Coordinate(2466, 3496, 2), "Ladder", "Climb-down"),
    MT_KARUULM_LAVA_GAP_WEST(new Coordinate(1271, 10174, 0), new Coordinate(1271, 10175, 0), "Lava gap", "Jump"),
    MT_KARUULM_LAVA_GAP_WEST_1(new Coordinate(1271, 10171, 0), new Coordinate(1271, 10170, 0), "Lava gap", "Jump"),
    MT_KARUULM_ROCKS_WEST(new Coordinate(1302, 10205, 0), new Coordinate(1303, 10205, 0), "Rocks", "Climb"),
    MT_KARUULM_ROCKS_WEST_1(new Coordinate(1302, 10205, 0), new Coordinate(1301, 10205, 0), "Rocks", "Climb"),
    BRIMHAVEN_DUNGEON_ENTRANCE(new Coordinate(2743, 3153, 0), new Coordinate(2713, 9564, 0), "Dungeon entrance", "Pay", new String[]{"Yes"}),
    BRIMHAVEN_DUNGEON_EXIT(new Coordinate(2714, 9564, 0), new Coordinate(2745, 3152, 0), "Exit", "Leave"),
    BRIMHAVEN_VINES_1(new Coordinate(2690, 9564, 0), new Coordinate(2689, 9564, 0), "Vines", "Chop-down"),
    BRIMHAVEN_VINES_2(new Coordinate(2690, 9564, 0), new Coordinate(2691, 9564, 0), "Vines", "Chop-down"),
    BRIMHAVEN_VINES_3(new Coordinate(2675, 9479, 0), new Coordinate(2676, 9479, 0), "Vines", "Chop-down"),
    BRIMHAVEN_VINES_4(new Coordinate(2694, 9482, 0), new Coordinate(2693, 9482, 0), "Vines", "Chop-down"),
    BRIMHAVEN_STEPPING_STONES_1(new Coordinate(2649, 9561, 0), new Coordinate(2647, 9557, 0), "Stepping stone", "Jump-from"),
    BRIMHAVEN_STEPPING_STONES_1A(new Coordinate(2647, 9558, 0), new Coordinate(2649, 9562, 0), "Stepping stone", "Jump-from"),
    FREMENNIK_SLAYER_DUNGEON_SHORTCUT_1(new Coordinate(2734, 10008, 0), new Coordinate(2730, 10008, 0), "Crevice", "Squeeze-through"),
    FREMENNIK_SLAYER_DUNGEON_SHORTCUT_1A(new Coordinate(2731, 10008, 0), new Coordinate(2735, 10008, 0), "Crevice", "Squeeze-through"),
    FREMENNIK_SLAYER_KURASK_STEPS(new Coordinate(2703, 9990, 0), new Coordinate(2703, 9989, 0), "Steps", "Climb"),
    FREMENNIK_SLAYER_KURASK_STEPSA(new Coordinate(2703, 9990, 0), new Coordinate(2703, 9991, 0), "Steps", "Climb"),
    TAVERLY_STEPS_TO_TASK_ONLY(new Coordinate(2881, 9825, 0), new Coordinate(2880, 9825, 1), "Steps", "Climb"),
    TAVERLY_STEPS_TO_TASK_ONLY_1(new Coordinate(2881, 9825, 1), new Coordinate(2883, 9825, 0), "Steps", "Climb"),
    CHASM_OF_FIRE_ENTRANCE(new Coordinate(1436, 3671, 0), new Coordinate(1435, 10077, 3), "Chasm", "Enter"),
    CHASM_OF_FIRE_EXIT(new Coordinate(1435, 10078, 3), new Coordinate(1435, 3671, 0), "Rope", "Climb-up"),
    CHASM_OF_FIRE_TOP_LIFT_DOWN(new Coordinate(1437, 10094, 3), new Coordinate(1438, 10093, 2), "Lift", "Enter"),
    CHASM_OF_FIRE_MIDDLE_LIFT_UP(new Coordinate(1437, 10094, 2), new Coordinate(1438, 10093, 3), "Lift", "Enter"),
    CHASM_OF_FIRE_MIDDLE_LIFT_DOWN(new Coordinate(1458, 10095, 2), new Coordinate(1457, 10096, 1), "Lift", "Enter"),
    CHASM_OF_FIRE_BOTTOM_LIFT_UP(new Coordinate(1458, 10095, 1), new Coordinate(1457, 10096, 2), "Lift", "Enter"),
    ASGARNIAN_WYVERN_TASK_ONLY_STEPS_UP(new Coordinate(3060, 9556, 0), new Coordinate(3060, 9557, 0), "Steps", "Climb"),
    ASGARNIAN_WYVERN_TASK_ONLY_STEPS_DOWN(new Coordinate(3060, 9556, 0), new Coordinate(3060, 9555, 0), "Steps", "Climb"),
    ASGARNIAN_WYVERN_ICY_CAVERN_ENTRANCE(new Coordinate(3055, 9561, 0), new Coordinate(3055, 9555, 0), "Icy Cavern", "Enter", new String[]{"Enter, and don't show the warning again."}),
    ASGARNIAN_WYVERN_ICY_CAVERN_EXIT(new Coordinate(3055, 9556, 0), new Coordinate(3055, 9562, 0), "Icy Cavern", "Enter"),
    SLAYER_TOWER_SPIKY_CHAIN_DOWN(new Coordinate(3422, 3550, 1), new Coordinate(3422, 3549, 0), "Spikey chain", "Climb-down"),
    SLAYER_TOWER_SPIKY_CHAIN_UP(new Coordinate(3422, 3550, 0), new Coordinate(3422, 3549, 1), "Spikey chain", "Climb-up"),
    SLAYER_TOWER_BASE_STAIRCASE_UP(new Coordinate(3434, 3537, 0), new Coordinate(3433, 3537, 1), "Staircase", "Climb-up"),
    SLAYER_TOWER_FIRST_FLOOR_STAIRCASE_DOWN(new Coordinate(3434, 3537, 1), new Coordinate(3438, 3537, 0), "Staircase", "Climb-down"),
    BRIMHAVEN_DUNGEON_STAIRS_TO_GREATER_DEMONS(new Coordinate(2637, 9516, 0), new Coordinate(2636, 9510, 2), "Stairs", "Walk-up"),
    BRIMHAVEN_DUNGEON_STAIRS_DOWN_FROM_GREATER_DEMONS(new Coordinate(2635, 9511, 2), new Coordinate(2637, 9517, 0), "Stairs", "Walk-down"),
    SLAYER_TOWER_LADDER_DOWN_TASK_ONLY(new Coordinate(3417, 3535, 0), new Coordinate(3412, 9932, 3), "Ladder", "Climb-down"),
    SLAYER_TOWER_LADDER_UP_TASK_ONLY(new Coordinate(3412, 9931, 3), new Coordinate(3417, 3536, 0), "Ladder", "Climb-up"),

    BURTHORPE_STILE(new Coordinate(2817, 3562, 0), new Coordinate(2817, 3564, 0), "Stile", "Climb-over"),
    BURTHORPE_STILE_1(new Coordinate(2817, 3563, 0), new Coordinate(2817, 3561, 0), "Stile", "Climb-over"),
    BURTHORPE_FIRST_ROCKS(new Coordinate(2856, 3612, 0), new Coordinate(2856, 3614, 0), "Rocks", "Climb"),
    BURTHORPE_FIRST_ROCKS_1(new Coordinate(2856, 3612, 0), new Coordinate(2856, 3611, 0), "Rocks", "Climb"),
    BURTHORPE_SECOND_ROCKS(new Coordinate(2834, 3628, 0), new Coordinate(2834, 3629, 0), "Rocks", "Climb"),
    BURTHORPE_SECOND_ROCKS_1(new Coordinate(2834, 3628, 0), new Coordinate(2834, 3627, 0), "Rocks", "Climb"),
    SECRET_DOOR_TO_TROLL_STRONGHOLD(new Coordinate(2827, 3647, 0), new Coordinate(2823, 10050, 0), "Secret Door", "Open"),
    SECRET_DOOR_FROM_TROLL_STRONGHOLD(new Coordinate(2823, 10049, 0), new Coordinate(2827, 3646, 0), "Exit", "Open"),
    TROLL_STRONGHOLD_LOWER_STAIRS(new Coordinate(2852, 10106, 0), new Coordinate(2852, 10109, 1), "Stone Staircase", "Climb-up"),
    TROLL_STRONGHOLD_MIDDLE_STAIRS(new Coordinate(2852, 10108, 1), new Coordinate(2852, 10105, 0), "Stone Staircase", "Climb-down"),
    FREMENNIK_ROCKS_TO_TROLL_STRONGHOLD_LAST(new Coordinate(2821, 3635, 0), new Coordinate(2820, 3635, 0), "Rocks", "Climb"),
    FREMENNIK_ROCKS_TO_TROLL_STRONGHOLD_LAST_1(new Coordinate(2821, 3635, 0), new Coordinate(2822, 3635, 0), "Rocks", "Climb"),
    WITCHAVEN_OLD_RUIN_ENTRANCE(new Coordinate(2696, 3283, 0), new Coordinate(2696, 9683, 0), "Old ruin entrance", "Climb-down"),
    WITCHAVEN_DUNGEON_EXIT(new Coordinate(2696, 9682, 0), new Coordinate(2697, 3283, 0), "Exit", "Climb-up"),
    ZANARIS_JUTTING_WALL_HIGHER(new Coordinate(2409, 4401, 0), new Coordinate(2409, 4400, 0), "Jutting wall", "Squeeze-past"),
    ZANARIS_JUTTING_WALL_HIGHER_1(new Coordinate(2409, 4401, 0), new Coordinate(2409, 4402, 0), "Jutting wall", "Squeeze-past"),
    ZANARIS_JUTTING_WALL_LOWER(new Coordinate(2400, 4403, 0), new Coordinate(2400, 4404, 0), "Jutting wall", "Squeeze-past"),
    ZANARIS_JUTTING_WALL_LOWER_1(new Coordinate(2400, 4403, 0), new Coordinate(2400, 4402, 0), "Jutting wall", "Squeeze-past"),
    WATERFALL_LOG_RAFT(new Coordinate(2509, 3493, 0), new Coordinate(2512, 3481, 0), "Log raft", "Board"),
    WATERFALL_ROPE_ON_ROCK(new Coordinate(2512, 3468, 0), new Coordinate(2513, 3468, 0), "Rock", null, "Rope"),
    WATERFALL_ROPE_ON_TREE(new Coordinate(2512, 3465, 0), new Coordinate(2511, 3463, 0), "Dead Tree", null, "Rope"),
    WATERFALL_DUNGEON_DOOR(new Coordinate(2511, 3464, 0), new Coordinate(2575, 9862, 0), "Door", "Open"),
    WATERFALL_DUNGEON_DOOR_1(new Coordinate(2575, 9861, 0), new Coordinate(2511, 3463, 0), "Door", "Open"),
    WATERFALL_EXIT_BARREL(new Coordinate(2512, 3463, 0), new Coordinate(2527, 3415, 0), "Barrel", "Get in"),

    LUMBRIDGE_TO_CELLAR_HOLE(new Coordinate(3221, 9618, 0), new Coordinate(3219, 9618, 0), "Hole", "Squeeze-through"),
    LUMBRIDGE_TO_CELLAR_HOLE_1(new Coordinate(3219, 9618, 0), new Coordinate(3221, 9618, 0), "Hole", "Squeeze-through", new InterfaceHandler("Yes I am sure.", "Yes", true)),

    SHILO_INNER_GATE_OUT(new Coordinate(2867, 2952, 0), new Coordinate(2868, 2952, 0), "Wooden gate", "Open"),
    //SHILO_INNER_GATE_IN(new Coordinate(2867, 2952, 0), new Coordinate(2866, 2952, 0), "Wooden gate", "Open"),

    SHILO_METAL_GATE_OUT(new Coordinate(2875, 2952, 0), new Coordinate(2874, 2952, 0), "Metal gate", "Open"),
    //SHILO_METAL_GATE_IN(new Coordinate(2875, 2952, 0), new Coordinate(2874, 2952, 0), "Metal gate", "Open"),

    SHILO_BROKEN_CART_OUT(new Coordinate(2877, 2952, 0), new Coordinate(2880, 2952, 0), "Broken cart", "Climb over"),
    SHILO_BROKEN_CART_IN(new Coordinate(2879, 2952, 0), new Coordinate(2876, 2952, 0), "Broken cart", "Climb over", new String[]{"Yes, I am very nimble and agile!"}),

    SHILO_SLAYER_LADDER_UP(new Coordinate(2871, 2971, 0), new Coordinate(2871, 2970, 1), "Ladder", "Climb-up"),
    SHILO_SLAYER_LADDER_DOWN(new Coordinate(2871, 2971, 1), new Coordinate(2871, 2970, 0), "Ladder", "Climb-down"),

    ZANARIS_MYSTERIOUS_RUINS(new Coordinate(2407, 4376, 0), new Coordinate(2142, 4813, 0), "Mysterious ruins", "Enter", "Cosmic talisman"),
    SHILO_MYSTERIOUS_RUINS(new Coordinate(2868, 3018, 0), new Coordinate(2398, 4838, 0), "Mysterious ruins", "Enter", "Nature talisman"),
    COSMIC_PORTAL_ZANARIS_1(new Coordinate(2121, 4833, 0), new Coordinate(2405, 4381, 0), "Portal", "Use"),
    COSMIC_PORTAL_ZANARIS_2(new Coordinate(2142, 4854, 0), new Coordinate(2405, 4381, 0), "Portal", "Use"),
    COSMIC_PORTAL_ZANARIS_3(new Coordinate(2163, 4833, 0), new Coordinate(2405, 4381, 0), "Portal", "Use"),
    COSMIC_PORTAL_ZANARIS_4(new Coordinate(2142, 4812, 0), new Coordinate(2405, 4381, 0), "Portal", "Use"),
    NATURE_PORTAL_SHILO(new Coordinate(2400, 4834, 0), new Coordinate(2868, 3017, 0), "Portal", "Use"),

    GLARIALS_TOMBSTONE_ENTER(new Coordinate( 2558, 3444, 0 ),  new Coordinate( 2555, 9844, 0 ), "Glarial's Tombstone",null,"Glarial's pebble"),

    STRONGHOLD_WAR_TO_FAMINE(new Coordinate(1902, 5222, 0), new Coordinate(2042, 5245, 0), "Ladder", "Climb-down", new InterfaceHandler("Yes - I know that it may be\r\ndangerous down there", "Yes", true)),
    STRONGHOLD_FAMINE_TO_WAR(new Coordinate(2042, 5246, 0), new Coordinate(1859, 5243, 0), "Ladder", "Climb-up"),

    STRONGHOLD_FAMINE_TO_PESTILENCE(new Coordinate(2026, 5218, 0), new Coordinate(2123, 5252, 0), "Ladder", "Climb-down", new InterfaceHandler("Yes - I know that it may be\r\ndangerous down there", "Yes", true)),
    STRONGHOLD_PESTILENCE_TO_FAMINE(new Coordinate(2123, 5251, 0), new Coordinate(2042, 5245, 0), "Dripping vine", "Climb-up"),

    STRONGHOLD_PESTILENCE_TO_DEATH(new Coordinate(2148, 5284, 0), new Coordinate(2358, 5215, 0), "Dripping vine", "Climb-down", new InterfaceHandler("Yes - I know that it may be\r\ndangerous down there", "Yes", true)),
    STRONGHOLD_DEATH_TO_PESTILENCE(new Coordinate(2358, 5216, 0), new Coordinate(2123, 5252, 0), "Boney ladder", "Climb-up"),

    STRONGHOLD_DEATH_CHAIN_OUT(new Coordinate(2350, 5215, 0), new Coordinate(3081, 3421, 0), "Bone Chain", "Climb-up"),

    //WAR_PORTAL_VARP = 802 -> 1, bit 2309 -> 1
    //SECOND is bit 2310 -> 1
    STRONGHOLD_WAR_PORTAL(new Coordinate(1863, 5238, 0), new Coordinate(1914, 5222, 0), "Portal", "Use"),
    STRONGHOLD_FAMINE_PORTAL(new Coordinate(2039, 5240, 0), new Coordinate(2021, 5223, 0), "Portal", "Use"),
    STRONGHOLD_PESTILENCE_PORTAL(new Coordinate(2120, 5258, 0), new Coordinate(2146, 5287, 0), "Portal", "Use"),
    STRONGHOLD_DEATH_PORTAL(new Coordinate(2365, 5212, 0), new Coordinate(2344, 5216, 0), "Portal", "Use"),

    BARBARIAN_VILLAGE_TO_STRONGHOLD(new Coordinate(3081, 3420, 0), new Coordinate(1859, 5243, 0), "Entrance", "Climb-down"),
    STRONGHOLD_TOP_TO_BARB_VILLAGE(new Coordinate(1859, 5244, 0), new Coordinate(3081, 3421, 0), "Ladder", "Climb-up"),

    ISLE_OF_SOULS_CAVE(new Coordinate(2309, 2919, 0), new Coordinate(2167, 9308, 0), "Cave", "Enter"),
    ISLE_OF_SOULS_CAVE_1(new Coordinate(2168, 9308, 0), new Coordinate(2310, 2919, 0), "Opening", "Exit"),

    SOUL_WARS_PORTAL(new Coordinate(3082, 3473, 0), new Coordinate(2206, 2858, 0), "Soul Wars Portal", "Enter"),
    SOUL_WARS_PORTAL_1(new Coordinate(2205, 2857, 0), new Coordinate(3081, 3475, 0), "Portal", "Edgeville"),

    //new Coordinate(3424, 3476, 0), new Coordinate(3424, 3476, 0), new Coordinate(3423, 3476, 0), new String[] {"Ornate railing"}, new String[]{"Squeeze-through"}
    CANIFIS_ORNATE_RAILING_TOP(new Coordinate(3424, 3476, 0), new Coordinate(3423, 3476, 0), "Ornate railing", "Squeeze-through"),
    CANIFIS_ORNATE_RAILING_TOP_1(new Coordinate(3424, 3476, 0), new Coordinate(3424, 3476, 0), "Ornate railing", "Squeeze-through"),

    TREE_GNOME_CRUMBLED_WALL(new Coordinate(2509, 3253, 0), new Coordinate(2509, 3255, 0), "Crumbled wall","Climb-over", new InterfaceHandler(229, "Continue")),
    BK_FORT_DDOR_IN(new Coordinate(3020, 3515, 0), new Coordinate(3020, 3515, 0), "Door", "Open", new String[]{"I don't care. I'm going in anyway."}),
    WITCH_HOUSE_SHED_DOOR(new Coordinate(2934, 3463, 0), new Coordinate(2934, 3463, 0), "Door", null, "Key"),

    FISHER_REALM_1_WHISTLE(new Coordinate(2741, 3235, 0), new Coordinate(2806, 4717, 0), null, "Blow", "Magic whistle"),
    FISHER_REALM_2_WHISTLE(new Coordinate(2741, 3235, 0), new Coordinate(2678, 4715, 0), null, "Blow", "Magic whistle"),

    ENTRANA_GANGPLANK_RIMMINGTON(new Coordinate(3048, 3232, 1), new Coordinate(3048, 3234, 0), "Gangplank", "Cross"),

    HAM_STOREROOM_LOCKED_DOOR_1_IN(new Coordinate(2567, 5192, 0), new Coordinate(2567, 5192, 0), "Door", "Pick-lock"),
    HAM_STOREROOM_LOCKED_DOOR_1_OUT(new Coordinate(2567, 5192, 0), new Coordinate(2566, 5192, 0), "Door", "Open"),

    HAM_STOREROOM_LOCKED_DOOR_2_IN(new Coordinate(2567, 5198, 0), new Coordinate(2567, 5198, 0), "Door", "Pick-lock"),
    HAM_STOREROOM_LOCKED_DOOR_2_OUT(new Coordinate(2567, 5198, 0), new Coordinate(2566, 5198, 0), "Door", "Open"),

    HAM_STOREROOM_LOCKED_DOOR_3_IN(new Coordinate(2576, 5198, 0), new Coordinate(2576, 5198, 0), "Door", "Pick-lock"),
    HAM_STOREROOM_LOCKED_DOOR_3_OUT(new Coordinate(2576, 5198, 0), new Coordinate(2577, 5198, 0), "Door", "Open"),

    HAM_STOREROOM_LOCKED_DOOR_4_IN(new Coordinate(2576, 5192, 0), new Coordinate(2576, 5192, 0), "Door", "Pick-lock"),
    HAM_STOREROOM_LOCKED_DOOR_4_OUT(new Coordinate(2576, 5192, 0), new Coordinate(2577, 5192, 0), "Door", "Open"),

    HAM_STOREROOM_CRACK_1(new Coordinate(2569, 5190, 0), new Coordinate(2569, 5189, 0), "Crack", "Squeeze-through"),
    //HAM_STOREROOM_CRACK_1a(new Coordinate(2569, 5190, 0), new Coordinate(2569, 5190, 0), "Crack", "Squeeze-through"),

    HAM_STOREROOM_CRACK_2(new Coordinate(2569, 5194, 0), new Coordinate(2569, 5195, 0), "Crack", "Squeeze-through"),

    HAM_STOREROOM_LADDER(new Coordinate(2567, 5185, 0), new Coordinate(3166, 9623, 0), "Ladder", "Climb-up"),
    HAM_HIDEOUT_TRAPDOOR(new Coordinate(3166, 9622, 0), new Coordinate(2568, 5185, 0), new String[] {"Hidden trapdoor", "Trapdoor"}, new String[] {"Pick-lock", "Climb-down"}),

    HAM_HIDEOUT_DOOR(new Coordinate(3158, 9640, 0), new Coordinate(3159, 9639, 0), "Door", "Open"),

    HAM_HIDEOUT_LADDER(new Coordinate(3149, 9653, 0), new Coordinate(3165, 3251, 0), "Ladder", "Climb-up"),
    HAM_ENTRANCE_TRAPDOOR(new Coordinate(3166, 3252, 0), new Coordinate(3149, 9652, 0), new String[]{"Trapdoor"}, new String[] {"Pick-Lock", "Climb-down"}),

    LIZARDMAN_SHAMAN_ENTRANCE_DWELLING(new Coordinate(1292, 3658, 0), new Coordinate(1292, 10057, 0), "Lizard dwelling", "Enter"),
    MYSTICAL_BARRIER_DWELLING_1(new Coordinate(1292, 10092, 0), new Coordinate(1292, 10093, 0), "Mystical barrier", "Pass"),

    BOAT_TO_UNGAEL(new Coordinate(2638, 3696, 0), new Coordinate(2277, 4035, 0), "Fremennik Boat", "Travel"),

    SHAMAN_MYSTICAL_BARRIER_1(new Coordinate(1292, 10092, 0), new Coordinate(1292, 10093, 0), new String[]{"Mystical barrier", "Mystical barrier (orange)"}, new String[]{"Pass"}),
    SHAMAN_MYSTICAL_BARRIER_2(new Coordinate(1297, 10096, 0), new Coordinate(1298, 10096, 0), new String[]{"Mystical barrier", "Mystical barrier (orange)"}, new String[]{"Pass"}),
    SHAMAN_MYSTICAL_BARRIER_3(new Coordinate(1308, 10096, 0), new Coordinate(1309, 10096, 0), new String[]{"Mystical barrier", "Mystical barrier (orange)"}, new String[]{"Pass"}),
    SHAMAN_MYSTICAL_BARRIER_4(new Coordinate(1317, 10096, 0), new Coordinate(1318, 10096, 0), new String[]{"Mystical barrier", "Mystical barrier (orange)"}, new String[]{"Pass"}),
    SHAMAN_MYSTICAL_BARRIER_5(new Coordinate(1325, 10096, 0), new Coordinate(1326, 10096, 0), new String[]{"Mystical barrier", "Mystical barrier (orange)"}, new String[]{"Pass"}),

    FARMING_GUILD_ENTRANCE_DOOR_1(new Coordinate(1249, 3723, 0), new Coordinate(1249, 3722, 0), "Door", "Open"),

    PORT_PHASMATYS_GANGPLANK_BILL(new Coordinate(3710, 3496, 0), new Coordinate(3712, 3496, 1), "Gangplank", "Cross"),
    PORT_PHASMATYS_GANGPLANK_BILL_1(new Coordinate(3711, 3496, 1), new Coordinate(3709, 3496, 0), "Gangplank", "Cross"),

    MOS_LE_HARMLESS_GANGPLANK_BILL(new Coordinate(3684, 2951, 1), new Coordinate(3684, 2953, 0), "Gangplank", "Cross"),
    MOS_LE_HARMLESS_GANGPLANK_BILL_1(new Coordinate(3684, 2952, 0), new Coordinate(3684, 2950, 1), "Gangplank", "Cross"),

    CAVE_HORROR_ENTRANCE(new Coordinate(3748, 2973, 0), new Coordinate(3748, 9373, 0), "Cave entrance", "Enter", new String[]{"Yes, I think I am fully prepared."}),
    CAVE_HORROR_EXIT(new Coordinate(3749, 9373, 0), new Coordinate(3749, 2973, 0), "Cave", "Exit"),

    ;


    private final Coordinate coordinate;
    private final Coordinate out;
    private String[] names;
    private String name;
    private String interaction;
    private String[] interactions;
    private String[] chatOptions;
    private InterfaceHandler handler;
    private String itemName;

    CrossableObject(Coordinate coordinate, Coordinate out, String name, String interaction) {
        this.coordinate = coordinate;
        this.out = out;
        this.name = name;
        this.interaction = interaction;
    }

    CrossableObject(Coordinate coordinate, Coordinate out, String name, String interaction, String[] chatOptions) {
        this.coordinate = coordinate;
        this.out = out;
        this.name = name;
        this.interaction = interaction;
        this.chatOptions = chatOptions;
    }

    CrossableObject(Coordinate coordinate, Coordinate out, String[] name, String[] interaction) {
        this.coordinate = coordinate;
        this.out = out;
        this.names = name;
        this.interactions = interaction;
    }

    CrossableObject(Coordinate coordinate, Coordinate out, String name, String interaction, InterfaceHandler handler) {
        this.coordinate = coordinate;
        this.out = out;
        this.name = name;
        this.interaction = interaction;
        this.handler = handler;
    }

    CrossableObject(Coordinate coordinate, Coordinate out, String name, String interaction, String itemName) {
        this.coordinate = coordinate;
        this.out = out;
        this.name = name;
        this.interaction = interaction;
        this.itemName = itemName;
    }

    @Override
    public boolean handle() {
        GameObject obj;
        if(handler != null && handler.handleWarningInterface()) {
            return true;
        }
        if((interaction != null || interactions != null) && (obj = getObject()) != null) {
            ChatDialog.Option option;
            ChatDialog.Continue cont;
            if (chatOptions != null && (option = ChatDialog.getOption(chatOptions)) != null) {
                if (option.select()) {
                    Execution.delayUntil(() -> ChatDialog.getOption(chatOptions) == null, 500, 1000);
                }
            } else if (chatOptions != null && (cont = ChatDialog.getContinue()) != null) {
                if (cont.select()) {
                    Execution.delay(500, 1000);
                }
            } else if (chatOptions == null && (cont = ChatDialog.getContinue()) != null) {
                if (cont.select()) {
                    Execution.delay(500, 1000);
                }
            } else {
                if (interactions != null) {
                    List<String> actions = RegalItem.getActions(obj);
                    interaction = Arrays.stream(interactions).filter(actions::contains).findFirst().orElse(null);
                }
                if (Interactions.interact(obj, interaction)) {
                    Player player = Players.getLocal();
                    if (player != null) {
                        if (Objects.equals(player.getPosition(), out)) {
                            Execution.delayUntil(() -> !playerPosEqualsOut() && !player.isMoving(), Call.playerMovingOrAnimating(), 500, 1000);
                            return true;
                        }
                    }
                    if (interaction.equalsIgnoreCase("Pick-lock")) {
                        Execution.delayUntil(() -> playerPosEqualsOut() || (interactions != null && getObject(interaction) != null), Call.playerMovingOrAnimating(), 3000, 4000);
                    } else {
                        Execution.delayUntil(() -> playerPosEqualsOut() || (interactions != null && getObject(interaction) != null), movingOrAnimatingNoTarget(), 500, 1000);
                    }
                }
            }
            return true;
        } else if(itemName != null && name != null) {
            handleUsingItemOnObject();
            return true;
        } else if(name == null && itemName != null) {
            handleItemWithoutObject();
            return true;
        } else {
            return false;
        }
    }

    private Callable<Boolean> movingOrAnimatingNoTarget() {
        Player player = Players.getLocal();
        return () -> player != null && (player.isMoving() || (player.getAnimationId() != -1 && player.getTarget() == null));
    }

    private void handleItemWithoutObject() {
        SpriteItem item;
        if ((item = Inventory.newQuery().names(itemName).results().sortByIndex().first()) != null) {
            if (item.interact(interaction)) {
                Execution.delayUntil(this::playerPosEqualsOut, 1800, 2400);
            }
        } else {
            Environment.getBot().stop("We do not have a " + itemName + " to use action " + interaction);
        }
    }

    private void handleUsingItemOnObject() {
        SpriteItem item;
        if((item = Inventory.newQuery().names(itemName).results().sortByIndex().first()) != null) {
            if(Objects.equals(Inventory.getSelectedItem(), item)) {
                GameObject obj;
                if((obj = getObjectWithoutInteraction()) != null) {
                    if(obj.isVisible()) {
                        Environment.getLogger().debug("Using: " + itemName + " on " + name);
                        if(obj.interact("Use", itemName + " -> " + name)) {
                            Execution.delayUntil(() -> getObject() != null || playerPosEqualsOut(), Call.playerMovingOrAnimating(), 500, 1000);
                        }
                    } else {
                        Interactions.makeVisible(obj);
                    }
                }
            } else {
                Interactions.select(item);
            }
        } else {
            Environment.getBot().stop("We do not have a " + itemName + " to use on " + name);
        }
    }

    private GameObject getObjectWithoutInteraction() {
        return GameObjects.newQuery().names(name).on(coordinate).results().first();
    }

    private boolean playerPosEqualsOut() {
        Player player = Players.getLocal();
        return player == null || Objects.equals(player.getPosition(), out);
    }

    public GameObject getObject() {
        GameObjectQueryBuilder query = GameObjects.newQuery();
        if(interactions != null) {
            query = query.actions(interactions);
        }
        else if(interaction != null) {
            query = query.actions(interaction);
        }
        if(name != null) {
            query = query.names(name);
        }
        if(names != null) {
            query = query.names(names);
        }
        return query.on(coordinate).results().first();
    }


    private GameObject getObject(String interaction) {
        GameObjectQueryBuilder query = GameObjects.newQuery();
        if(interactions != null) {
            System.out.println(Arrays.toString(Arrays.stream(interactions).filter(s -> !s.equals(interaction)).toArray()));
            query = query.actions(Arrays.stream(interactions).filter(s -> !s.equals(interaction)).toArray(String[]::new));
        }
        else if(this.interaction != null) {
            query = query.actions(interaction);
        }
        if(name != null) {
            query = query.names(name);
        }
        else if(names != null) {
            query = query.names(names);
        }
        System.out.println(query.on(coordinate).results().first());
        return query.on(coordinate).results().first();
    }

    @Override
    public Coordinate getPosition() {
        return coordinate;
    }

    public Coordinate getOutput() {
        return out;
    }
}
