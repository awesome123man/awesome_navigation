package com.regal.utility.awesome_navigation.handles;

import com.regal.utility.awesome_navigation.Pathing;
import com.regal.utility.awesome_navigation.RegalItem;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.GameObjectQueryBuilder;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class GameObjectHandle implements Handleable {


    private final String[] names;
    private final String[] actions;
    private final int maxDistance;
    private final Coordinate position;
    private final Coordinate out;

    public GameObjectHandle(String[] names, String[] actions, int maxDistance, Coordinate position, Coordinate out) {
        this.names = names;
        this.actions = actions;
        this.maxDistance = maxDistance;
        this.position = position;
        this.out = out;
    }

    @Override
    public boolean handle() {
        GameObject obj;
        if ((obj = getObject()) != null || maxDistance > -1) {
            Player player = Players.getLocal();
            if (player != null) {
                if (maxDistance != -1 && position.distanceTo(player) > maxDistance) {
                    Pathing.pathTo(position);
                    return true;
                }
            }
            List<String> actions = RegalItem.getActions(obj);
            String interact = Arrays.stream(this.actions).filter(actions::contains).findFirst().orElse(null);
            if (interact != null) {
                if (Interactions.interact(obj, interact)) {
                    Execution.delayUntil(this::playerPosEqualsOut, () -> Call.playerMovingOrAnimating().call() || new Area.Circular(new Coordinate(1324, 3789, 0), 2).contains(player), 500, 1000);
                }
            }
            return true;
        }
        return false;
    }

    public GameObject getObject() {
        GameObjectQueryBuilder builder = GameObjects.newQuery();
        if(names != null) {
            builder = builder.names(names);
        }
        if(actions != null) {
            builder = builder.actions(actions);
        }
        return builder.on(position).results().first();
    }

    private boolean playerPosEqualsOut() {
        Player player = Players.getLocal();
        return player == null || Objects.equals(player.getPosition(), out);
    }

    @Override
    public Coordinate getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "GameObjectHandle{" +
                "names=" + Arrays.toString(names) +
                ", actions=" + Arrays.toString(actions) +
                ", maxDistance=" + maxDistance +
                ", position=" + position +
                ", out=" + out +
                '}';
    }
}
