package com.regal.utility.awesome_navigation.handles;

import com.regal.utility.awesome_navigation.RegalItem;
import com.regal.utility.awesome_navigation.requirements.EquipmentRequirement;
import com.regal.utility.awesome_navigation.requirements.Requirement;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;

public enum OpenableObject implements Handleable {

    GATE_FALADOR_1(new Coordinate(2933, 3320, 0), "Gate", "Open"),
    GATE_FALADOR_2(new Coordinate(2934, 3320, 0), "Gate", "Open"),
    TAVERLY_ENTRANCE_GATE_1(new Coordinate(2935, 3450, 0), "Gate", "Open"),
    TAVERLY_ENTRANCE_GATE_2(new Coordinate(2935, 3451, 0), "Gate", "Open"),
    GRAND_TREE_ENTRANCE_DOOR(new Coordinate(2466, 3492, 0), "Tree Door", "Open", Act.CROSSING),
    GRAND_TREE_ENTRANCE_DOOR_2(new Coordinate(2465, 3492, 0), "Tree Door", "Open", Act.CROSSING),
    GATE_LUMBRIDGE_COWS(new Coordinate(3253, 3267, 0), "Gate", "Open"),
    GATE_LUMBRIDGE_COWS_2(new Coordinate(3253, 3266, 0), "Gate", "Open"),
    YANILLE_GATE_OUTER(new Coordinate(2532, 3091, 0), "Large door", "Open"),
    YANILLE_GATE_OUTER_2(new Coordinate(2532, 3092, 0), "Large door", "Open"),
    YANILLE_GATE_INNER(new Coordinate(2539, 3091, 0), "Large door", "Open"),
    YANILLE_GATE_INNER_2(new Coordinate(2539, 3092, 0), "Large door", "Open"),
    SLAYER_TOWER_FIRST_FLOOR_DOOR(new Coordinate(3426, 3555, 1), "Door", "Open", Act.CROSSING),
    SLAYER_TOWER_FIRST_FLOOR_DOOR_1(new Coordinate(3427, 3555, 1), "Door", "Open", Act.CROSSING),
    MUSA_POINT_BRIMHAVEN_GATE_1(new Coordinate(2816, 3182, 0), "Gate", "Open"),
    MUSA_POINT_BRIMHAVEN_GATE_2(new Coordinate(2816, 3183, 0), "Gate", "Open"),
    BURTHORPE_GATE_FIRST(new Coordinate(2824, 3555, 0), "Gate", "Open"),
    BURTHORPE_GATE_FIRST_1(new Coordinate(2824, 3555, 0), "Gate", "Open"),
    BURTHORPE_HOUSE_DOOR(new Coordinate(2822, 3555, 0), "Door", "Open"),
    BURTHORPE_HOUSE_DOOR_1(new Coordinate(2822, 3555, 0), "Door", "Open"),
    BURTHORPE_HOUSE_DOOR_PASS(new Coordinate(2820, 3557, 0), "Door", "Open"),
    BURTHORPE_HOUSE_DOOR_PASS_1(new Coordinate(2820, 3557, 0), "Door", "Open"),
    TROLL_STRONGHOLD_MIDDLE_PRISON_DOOR(new Coordinate(2848, 10107, 1), "Prison Door", "Unlock"),
    TROLL_STRONGHOLD_MIDDLE_PRISON_DOOR_1(new Coordinate(2848, 10107, 1), "Prison Door", "Unlock"),
    TROLL_STRONGHOLD_MIDDLE_TROLL_SECTION_DOOR(new Coordinate(2833, 10070, 1), "Door", "Open"),
    TROLL_STRONGHOLD_MIDDLE_TROLL_SECTION_DOOR_1(new Coordinate(2833, 10070, 1), "Door", "Open"),
    LUMBRIDGE_TO_ZANARIS_DOOR(new Coordinate(3202, 3169, 0), "Door", "Open", Act.CROSSING, new EquipmentRequirement(new String[]{"Dramen staff", "Lunar staff"}, 1)),
    ZANARIS_TO_LUMBRIDGE_RING(new Coordinate(2452, 4473, 0),  "Fairy ring", "Use", Act.CROSSING, new EquipmentRequirement(new String[]{"Dramen staff", "Lunar staff"}, 1)),
    WITCHAVEN_HELLHOUNDS_DOOR(new Coordinate(2727, 9690, 0),"Door", "Open", Act.CROSSING),
    WATERFALL_GATE_OUTER(new Coordinate(2528, 3495, 0), "Gate", "Open"),
    WATERFALL_GATE_OUTER_1(new Coordinate(2528, 3496, 0), "Gate", "Open"),
    WATERFALL_HOUSE_DOOR(new Coordinate(2525, 3495, 0), "Door", "Open"),
    WATERFALL_GATE_INNER(new Coordinate(2513, 3494, 0), "Gate", "Open"),
    WATERFALL_GATE_INNER_1(new Coordinate(2513, 3495, 0), "Gate", "Open"),
    WATERFALL_DUNGEON_FIRE_GIANT_ROOM_DOOR_OUTER(new Coordinate(2576, 9882, 0), "Large door", "Open"),
    WATERFALL_DUNGEON_FIRE_GIANT_ROOM_DOOR_INNER(new Coordinate(2576, 9884, 0), "Large door", "Open"),
    LUMBRIDGE_COOK_TUTOR_DOOR(new Coordinate(3235, 3198, 0), "Door", "Open"),
    TAVERLY_DUNGEON_DUSTY_KEY_GATE(new Coordinate(2924, 9803, 0), "Gate", "Open"),
    TAVERLY_DUNGEON_INNER_GUARD_DOOR(new Coordinate(2892, 9826, 0), "Door", "Open"),
    TAVERLY_DUNGEON_INNER_GUARD_DOOR_1(new Coordinate(2893, 9826, 0), "Door", "Open"),

    ;

    private final Coordinate position;
    private final String name;
    private final String action;
    private Act act;
    private Requirement requirement;

    OpenableObject(Coordinate position, String name, String action) {
        this.position = position;
        this.name = name;
        this.action = action;
    }

    OpenableObject(Coordinate position, String name, String action, Act act) {
        this.position = position;
        this.name = name;
        this.action = action;
        this.act = act;
    }

    OpenableObject(Coordinate position, String name, String action, Act act, Requirement requirement) {
        this.position = position;
        this.name = name;
        this.action = action;
        this.act = act;
        this.requirement = requirement;
    }

    public boolean isThere() {
        return GameObjects.newQuery().names(name).actions(action).on(position).results().first() != null;
    }

    @Override
    public boolean handle() {
        if(requirement instanceof EquipmentRequirement && Equipment.newQuery().names(((EquipmentRequirement) requirement).getItemNames()).results().first() == null) {
            //We need to equip it.
            SpriteItem item;
            return Interactions.interact((item = Inventory.newQuery().names(((EquipmentRequirement) requirement).getItemNames()).results().first()), RegalItem.getFirstAction(item)) &&
                    Execution.delayUntil(() -> Equipment.newQuery().names(((EquipmentRequirement) requirement).getItemNames()).results().first() != null, 500, 1000);
        }
        GameObject obj;
        if((obj = GameObjects.newQuery().names(name).actions(action).on(position).results().first()) != null) {
            if(Interactions.interact(obj, action)) {
                if (act != null && act.equals(Act.CROSSING)) {
                    Execution.delayUntil(() -> !isThere(), Call.playerMovingOrAnimating(), 500, 1000);
                    Execution.delayUntil(() -> Call.playerMoving().call(), Call.playerMovingOrAnimating(), 500, 1000);
                    Execution.delayUntil(() -> !Call.playerMoving().call(), Call.playerMovingOrAnimating(), 500, 1000);
                } else {
                    Execution.delayUntil(() -> !isThere() && !Call.playerMoving().call(), Call.playerMovingOrAnimating(), 500, 1000);
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public Coordinate getPosition() {
        return position;
    }

    private enum Act {
        DISSAPEARING, CROSSING
    }

    @Override
    public String toString() {
        return "OpenableObject{" +
                "position=" + position +
                ", name='" + name + '\'' +
                ", action='" + action + '\'' +
                ", act=" + act +
                '}';
    }
}
