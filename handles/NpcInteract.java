package com.regal.utility.awesome_navigation.handles;

import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.entities.definitions.NpcDefinition;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.Execution;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public enum NpcInteract implements Handleable {
    MOUNTAIN_GUIDE_KOUREND_OUTER(new Coordinate(1401, 3536, 0), new Coordinate(1274, 3560, 0), "Mountain Guide", "Travel", new Area.Rectangular(new Coordinate(1269, 3562, 0), new Coordinate(1281, 3552, 0)), null),
    MOUNTAIN_GUIDE_KOUREND_INNER(new Coordinate(1275, 3559, 0), new Coordinate(1402, 3536, 0), "Mountain Guide", "Travel", "Talk-to", new Area.Rectangular(new Coordinate(1395, 3541, 0), new Coordinate(1407, 3531, 0)), null),
    MISTAG_MINES_TO_CELLAR(new Coordinate(3319, 9615, 0), new Coordinate(3230, 9610, 0), "Mistag", "Cellar", new Area.Rectangular(new Coordinate(3306, 9625, 0), new Coordinate(3328, 9604, 0)), null),
    KAZGAR_CELLAR_TO_MINES(new Coordinate(3230, 9611, 0), new Coordinate(3316, 9612, 0), "Kazgar", "Mines", new Area.Rectangular(new Coordinate(3221, 9633, 0), new Coordinate(3242, 9606, 0)), null),
    MOSOL_REI(new Coordinate(2880, 2951, 0), new Coordinate(2866, 2952, 0), "Mosol Rei", "Follow", new Area.Rectangular(new Coordinate(2879, 2957, 0), new Coordinate(2886, 2945, 0)), null),

    ELKOY_IN(new Coordinate(2507, 3191, 0), new Coordinate(2515, 3159, 0), "Elkoy", "Follow", new Area.Polygonal(new Coordinate(2498, 3189, 0),
            new Coordinate(2498, 3195, 0), new Coordinate(2506, 3195, 0), new Coordinate(2506, 3191, 0), new Coordinate(2508, 3191, 0),
            new Coordinate(2508, 3190, 0), new Coordinate(2504, 3190, 0), new Coordinate(2503, 3187, 0), new Coordinate(2501, 3187, 0),
            new Coordinate(2501, 3189, 0)), null),
    ELKOY_OUT(new Coordinate(2514, 3158, 0), new Coordinate(2506, 3191, 0), "Elkoy", "Follow", new Area.Rectangular(new Coordinate(2514, 3158, 0), new Coordinate(2518, 3160, 0)), null),
    MONK_ENTRANA_LEAVE(new Coordinate(2835, 3335, 0), new Coordinate(3048, 3231, 1), "Monk of Entrana", "Take-boat",
            new Area.Rectangular(new Coordinate(2831, 3335, 0), new Coordinate(2839, 3336, 0)), null),
    BILL_TEACH_LEAVE_PHASMATYS(new Coordinate(3714, 3497, 1), new Coordinate(3683, 2948, 1), "Bill Teach", "Travel", new Area.Rectangular(new Coordinate(3711, 3507, 1), new Coordinate(3717, 3489, 1)), null),
    BILL_TEACH_LEAVE_MOS_LE_HARMLESS(new Coordinate(3683, 2948, 1), new Coordinate(3714, 3497, 1), "Bill Teach", "Travel", new Area.Rectangular(new Coordinate(3672, 2951, 1), new Coordinate(3692, 2945, 1)), null),

    ;

    private final Coordinate position;
    private final Coordinate out;
    private final String npcName;
    private String beforeAction;
    private final Area npcArea;
    private final String[] chatOptions;
    private final String action;

    NpcInteract(Coordinate position, Coordinate out, String npcName, String action, Area npcArea, String[] chatOptions) {
        this.position = position;
        this.out = out;
        this.npcName = npcName;
        this.action = action;
        this.npcArea = npcArea;
        this.chatOptions = chatOptions;
    }

    NpcInteract(Coordinate position, Coordinate out, String npcName, String action, String beforeAction, Area npcArea, String[] chatOptions) {
        this.position = position;
        this.out = out;
        this.npcName = npcName;
        this.action = action;
        this.beforeAction = beforeAction;
        this.npcArea = npcArea;
        this.chatOptions = chatOptions;
    }


    @Override
    public boolean handle() {
        ChatDialog.Option option;
        ChatDialog.Continue continu;
        if((option = getChatOption()) != null) {
            handleChat(option);
        } else if((continu = getChatContinue()) != null) {
            handleContinue(continu);
        } else {
            Npc npc;
            if((npc = getNpc(action)) != null) {
                handleInteraction(npc, action);
            } else if((npc = getNpc(beforeAction)) != null) {
                handleInteraction(npc, beforeAction);
            }
        }
        return true;
    }

    private boolean handleInteraction(Npc npc, String action) {
        return Interactions.interact(npc, action) && Execution.delayUntil(() -> ChatDialog.getContinue() != null
        || ChatDialog.getOptions() != null || getNpc(action) == null, Call.playerMovingOrAnimating(), 1000, 2000);
    }

    private Npc getNpc(String action) {
        Npc npc = Npcs.newQuery().names(npcName).actions(action).within(npcArea).results().nearest();
        if(npc == null) {
            return Npcs.newQuery().results().stream().filter(npc1 -> {
                NpcDefinition def = npc1.getDefinition();
                if(def != null) {
                    Collection<NpcDefinition> transformations = def.getTransformations();
                    NpcDefinition correctDef = transformations.stream().filter(npcDefinition -> npcDefinition.getActions().contains(action) && Objects.equals(npcDefinition.getName(), npcName)).findFirst().orElse(null);
                    return correctDef != null;
                }
                return false;
            }).findFirst().orElse(null);
        }
        return null;
    }

    private void handleContinue(ChatDialog.Continue continu) {
        if(continu.select()) {
            Execution.delayUntil(() -> ChatDialog.getContinue() == null, 400, 800);
        }
    }

    private ChatDialog.Continue getChatContinue() {
        return ChatDialog.getContinue();
    }

    private void handleChat(ChatDialog.Option option) {
        if(option.select()) {
            Execution.delayUntil(() -> optionGone(option), 1000, 2000);
        }
    }

    private boolean optionGone(ChatDialog.Option option) {
        List<ChatDialog.Option> options;
        return (options = ChatDialog.getOptions()) == null || !options.contains(option);
    }

    private ChatDialog.Option getChatOption() {
        if(chatOptions != null) {
            ChatDialog.Option option;
            if((option = ChatDialog.getOption(chatOptions)) != null) {
                return option;
            }
        }
        return null;
    }

    @Override
    public Coordinate getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "NpcInteract{" +
                "position=" + position +
                ", out=" + out +
                ", npcName='" + npcName + '\'' +
                ", action='" + action + '\'' +
                ", npcArea=" + npcArea +
                ", chatOptions=" + Arrays.toString(chatOptions) +
                '}';
    }

    public Coordinate getOutput() {
        return out;
    }

    public Area getArea() {
        return npcArea;
    }
}
