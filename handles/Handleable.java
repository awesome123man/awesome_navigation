package com.regal.utility.awesome_navigation.handles;

import com.runemate.game.api.hybrid.location.Coordinate;

public interface Handleable {
    boolean handle();
    Coordinate getPosition();
}
