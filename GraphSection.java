package com.regal.utility.awesome_navigation;

import com.regal.utility.awesome_navigation.banking.BankAreas;
import com.regal.utility.awesome_navigation.edges.Edge;
import com.regal.utility.awesome_navigation.vertices.CoordinateVertex;
import com.regal.utility.awesome_navigation.vertices.Vertex;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class GraphSection {

    private final String name;
    private final Area area;
    private int cost;
    private GraphSection from;
    private LinkedHashMap<Integer, Vertex> specificMap;
    private final LinkedHashMap<Integer, Vertex> highLevelMap;
    //public WeightedGraph graph;

    GraphSection(String name, Area area, LinkedHashMap<Integer, Vertex> map, LinkedHashMap<Integer, Vertex> highLevelMap) {
        this.name = name;
        this.area = area;
        //this.graph = new WeightedGraph(false);
        this.specificMap = map;
        this.highLevelMap = highLevelMap;
        cost = 0;
    }

    public void loadGraph(boolean addEdges) {
        WeightedGraph graph = new WeightedGraph(false);
        try {
            graph.readFromZip("com/regal/utility/awesome_navigation/sections/" + name + ".nav");
            if(addEdges) {
                Vertex[] vals = graph.getGraph().values().toArray(new Vertex[0]);
                for (int i = 0; i < vals.length; i++) {
                    graph.handleEdgeAdding(vals, i);
                }
            }
            specificMap = graph.getGraph();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<GraphSection> splitGraph(WeightedGraph graph, int length) {
        int minX = Integer.MAX_VALUE;
        int minY = Integer.MAX_VALUE;

        int maxX = Integer.MIN_VALUE;
        int maxY = Integer.MIN_VALUE;

        for (Vertex value : graph.getGraph().values()) {
            if (value.getCoordinate() != null) {
                if (value.getCoordinate().getX() > maxX) {
                    maxX = value.getCoordinate().getX();
                }
                if (value.getCoordinate().getY() > maxY) {
                    maxY = value.getCoordinate().getY();
                }
                if (value.getCoordinate().getX() < minX) {
                    minX = value.getCoordinate().getX();
                }
                if (value.getCoordinate().getY() < minY) {
                    minY = value.getCoordinate().getY();
                }
            }
        }

        System.out.println("MaxX: " + maxX + " | MaxY: " + maxY);
        System.out.println("MinX: " + minX + " | MinY: " + minY);
        int numYSectors = (int) Math.ceil((maxY - minY) / (double) length);
        int numXSectors = (int) Math.ceil((maxX - minX) / (double) length);
        int numSectors = (numYSectors * numXSectors);
        System.out.println("Number of Sectors: " + numSectors);

        LinkedHashMap<Integer, Vertex>[] array = new LinkedHashMap[numSectors];
        List<GraphSection> graphSections = new ArrayList<>();
        int count = 0;
        for (int x = 0; x < numXSectors; x++) {
            for (int y = 0; y < numYSectors; y++) {
                LinkedHashMap<Integer, Vertex> map = new LinkedHashMap<>();
                int sectorMinX = (x * 104) + minX;
                int sectorMinY = (y * 104) + minY;

                for (int sectorX = sectorMinX; sectorX < sectorMinX + 104; sectorX++) {
                    for (int sectorY = sectorMinY; sectorY < sectorMinY + 104; sectorY++) {
                        for (int i = 0; i < 4; i++) {
                            Vertex val = graph.getGraph().get(new Coordinate(sectorX, sectorY, i).hashCode());
                            if (val != null) {
                                map.put(val.getCoordinate().hashCode(), val);

                            }
                        }
                    }
                }

                Area area = new Area.Rectangular(new Coordinate(sectorMinX, sectorMinY, 0), new Coordinate(sectorMinX + 103, sectorMinY + 103, 3));
                GraphSection section = new GraphSection("Section" + count, area, new LinkedHashMap(), new LinkedHashMap());
                graphSections.add(x * numYSectors + y, section);
                //System.out.println(x * numYSectors + y + " | " + count);
                array[x * numYSectors + y] = map;
                count++;
            }
        }

        try {


            List<Integer> indicesToRemove = new ArrayList<>();
            for (int i = array.length - 1; i >= 0; i--) {
                if (array[i] == null || array[i].values().size() <= 0) {
                    indicesToRemove.add(i);
                }
            }

            List<LinkedHashMap<Integer, Vertex>> list = new ArrayList<>(Arrays.asList(array));
            indicesToRemove.forEach(integer -> list.remove((int) integer));
            indicesToRemove.forEach(integer -> graphSections.remove((int) integer));

            LinkedHashMap[] filteredArray = list.toArray(new LinkedHashMap[0]);

            //Arrays.stream(filteredArray).forEach(map -> System.out.println(map.values().size()));

            AwesomePathGenerator generator = new AwesomePathGenerator();

            LinkedHashMap foundMap = Arrays.stream(filteredArray).filter(map -> map.containsKey(new Coordinate(2193, 2857, 0).hashCode())).findFirst().orElse(null);
            if (foundMap != null) {
                System.out.println("Removed? " + foundMap.remove(new Coordinate(2193, 2857, 0).hashCode()));
            }
            foundMap = Arrays.stream(filteredArray).filter(map -> map.containsKey(new Coordinate(2522, 3481, 0).hashCode())).findFirst().orElse(null);
            if (foundMap != null) {
                System.out.println("Removed? " + foundMap.remove(new Coordinate(2522, 3481, 0).hashCode()));
            }
            foundMap = Arrays.stream(filteredArray).filter(map -> map.containsKey(new Coordinate(2522, 3480, 0).hashCode())).findFirst().orElse(null);
            if (foundMap != null) {
                System.out.println("Removed? " + foundMap.remove(new Coordinate(2522, 3480, 0).hashCode()));
            }

            for (int i = 0; i < filteredArray.length; i++) {
                LinkedHashMap<Integer, Vertex> map = filteredArray[i];
                GraphSection section = graphSections.get(i);
                section.specificMap = map;

                //map.values().forEach(vertex -> vertex.getEdges().forEach(edge -> edge.setUsed(false)));
                List<Vertex> externalConnectedVertices = map.values().stream().filter(entry -> entry.getEdges().stream().filter(edge -> edge.getDestination() != null).anyMatch(edge -> !section.area.contains(edge.getDestination().getCoordinate()) || !edge.getClass().equals(Edge.class))).collect(Collectors.toList());
                for (Vertex vertex : externalConnectedVertices) {
                    List<Edge> externalConnections = vertex.getEdges().stream().filter(edge -> (!section.area.contains(edge.getDestination().getCoordinate()) && !edge.isUsed()) || !edge.getClass().equals(Edge.class)).collect(Collectors.toList());
                    externalConnections.forEach(edge -> {
                        GraphSection sector = graphSections.stream().filter(sect -> sect.area.contains(edge.getDestination().getCoordinate())).findFirst().orElse(null);
                        if (sector != null) {
                            if (!section.highLevelMap.containsKey(vertex.getCoordinate().hashCode())) {
                                Vertex temp = new CoordinateVertex(vertex.getCoordinate(), vertex.getFlag());
                                Vertex corrTemp;
                                if (!sector.highLevelMap.containsKey(edge.getDestination().getCoordinate().hashCode())) {
                                    corrTemp = new CoordinateVertex(edge.getDestination().getCoordinate(), edge.getDestination().getFlag());
                                } else {
                                    corrTemp = sector.highLevelMap.get(edge.getDestination().getCoordinate().hashCode());
                                }
                                temp.addEdge(new Edge(temp, corrTemp, edge.getWeight(), edge.getRequirements()));
                                if(edge.getClass().equals(Edge.class)) {
                                    corrTemp.addEdge(new Edge(corrTemp, temp, edge.getWeight(), edge.getRequirements()));
                                }
                                sector.highLevelMap.put(corrTemp.getCoordinate().hashCode(), corrTemp);
                                section.highLevelMap.put(vertex.getCoordinate().hashCode(), temp);
                            } else {
                                Vertex temp = section.highLevelMap.get(vertex.getCoordinate().hashCode());
                                Vertex corrTemp;
                                if ((corrTemp = sector.highLevelMap.get(edge.getDestination().getCoordinate().hashCode())) == null) {
                                    corrTemp = new CoordinateVertex(edge.getDestination().getCoordinate(), edge.getDestination().getFlag());
                                    sector.highLevelMap.put(corrTemp.getCoordinate().hashCode(), corrTemp);
                                }
                                if(edge.getClass().equals(Edge.class)) {
                                    corrTemp.addEdge(new Edge(corrTemp, temp, edge.getWeight(), edge.getRequirements()));
                                }
                                temp.addEdge(new Edge(temp, corrTemp, edge.getWeight(), edge.getRequirements()));
                            }
                            edge.setUsed(true);
                            LinkedHashMap<Integer, Vertex> reachable;
                            if ((reachable = generator.getReachableVertices(map, vertex.getCoordinate(), false, 50)) != null) {
                                reachable.values().forEach(vert -> vert.getEdges().forEach(edg -> {
                                    if (sector.area.contains(edg.getDestination().getCoordinate())) {
                                        edg.setUsed(true);
                                    }
                                }));
                            }

                            //sectionsAdded.add(sector);
                        }
                    });
                    //vertex.getEdges().removeIf(edge -> !externalConnections.contains(edge));
                }
            }

            /*GraphSection sect = graphSections.stream().filter(section -> section.getArea().contains(new Coordinate(1322, 3774, 0))).findFirst().orElse(null);
            GraphSection sect1 = graphSections.stream().filter(section -> section.getArea().contains(new Coordinate(1321, 3774, 0))).findFirst().orElse(null);
            if(sect != null && sect1 != null) {
                Vertex vertex;
                Vertex vertex1;
                sect.getHighLevelMap().put(new Coordinate(1322, 3774, 0).hashCode(), (vertex = new CoordinateVertex(new Coordinate(1322, 3774, 0), 0)));
                sect1.getHighLevelMap().put(new Coordinate(1322, 3774, 0).hashCode(), new CoordinateVertex(new Coordinate(1322, 3774, 0), 0));
            }*/

            for (BankAreas value : BankAreas.values()) {
                graphSections.stream().filter(section -> section.getArea().contains(value.getArea())).findFirst().ifPresent(containedSection -> containedSection.getHighLevelMap().put(value.getArea().getCenter().hashCode(), new CoordinateVertex(value.getArea().getCenter(), 0)));
            }
            //Connect vertices that reach each other with edges.
            graphSections.forEach(section -> section.highLevelMap.values().forEach(vertex -> section.highLevelMap.values().stream().filter(vertex1 -> !vertex1.equals(vertex)).forEach(vertex1 -> {
                section.getSpecificMap().values().forEach(vertex2 -> vertex2.getEdges().forEach(edge -> {
                    edge.setUsed(false);
                    vertex2.setFrom(null);
                    vertex2.setTotalCost(-1);
                }));
                List<Edge> edges = generator.generateEdges(vertex, vertex1, section.getSpecificMap());
                vertex.addEdge(edges);
            })));
                /*if(section.specificMap.containsKey(new Coordinate(3264, 3224, 0).hashCode())) {
                    WeightedGraph graph1 = new WeightedGraph(false);
                    section.specificMap.forEach((integer, vertex) -> graph1.getGraph().put(integer, vertex));
                    System.out.println(section.highLevelMap.values().stream().map((vertex) ->
                            "graph.getGraph().put(" + vertex.getCoordinate().hashCode() + ", new GameObjectVertex(new " + vertex.getCoordinate() + ", " + vertex.getFlag() + "));\n" +
                                    "graph.getGraph().get(" + vertex.getCoordinate().hashCode() + ").addEdge(new ArrayList<>(Arrays.asList(" + vertex.getEdges().stream().map(edge -> "new Edge(graph.getGraph().get(" + edge.getSource().getCoordinate().hashCode() + "), graph.getGraph().get(" + edge.getDestination().getCoordinate().hashCode() + "), 1, null)").collect(Collectors.joining(", ")) + "))"
                    ).collect(Collectors.joining(");\n")));
                    graph1.save("Test");
                }*/
            //System.out.println((double) i / filteredArray.length);
            //System.out.println(section.map.size());
            //System.out.println(section.area + " | vertices: " + section.highLevelMap.size() + " | " + section.highLevelMap.values().stream().map(vertex -> "[" + vertex.getCoordinate() + " " + vertex.getEdges().stream().map(Edge::toString).collect(Collectors.joining(", ")) + "]").collect(Collectors.joining(", ")));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return graphSections;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public Area getArea() {
        return area;
    }

    public GraphSection getFrom() {
        return from;
    }

    public void setFrom(GraphSection from) {
        this.from = from;
    }

    public LinkedHashMap<Integer, Vertex> getSpecificMap() {
        return specificMap;
    }

    public LinkedHashMap<Integer, Vertex> getHighLevelMap() {
        return highLevelMap;
    }

    @Override
    public String toString() {
        return "GraphSection{" +
                "name=" + name +
                "area=" + area +
                ", High Level=" + highLevelMap.size() +
                ", Specifics=" + specificMap.size() +
                ", cost=" + cost +
                '}';
    }

    public String getName() {
        return name;
    }

    public void saveGraph() {
        WeightedGraph graph = new WeightedGraph(false);
        specificMap.forEach((integer, vertex) -> {
            graph.getGraph().put(integer, vertex);
        });
        graph.saveToZip(name);
        specificMap = graph.getGraph();
    }

    public void addSpecialEdges() {
        WeightedGraph graph = new WeightedGraph(false);
        specificMap.forEach((integer, vertex) -> {
            graph.getGraph().put(integer, vertex);
        });
        graph.addSpecialVertices();
        specificMap = graph.getGraph();
    }
}
