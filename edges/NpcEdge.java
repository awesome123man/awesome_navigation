package com.regal.utility.awesome_navigation.edges;

import com.regal.utility.awesome_navigation.handles.Handleable;
import com.regal.utility.awesome_navigation.handles.Handled;
import com.regal.utility.awesome_navigation.handles.NpcObjectHandle;
import com.regal.utility.awesome_navigation.requirements.Requirement;
import com.regal.utility.awesome_navigation.vertices.Vertex;
import com.runemate.game.api.hybrid.location.Area;

public class NpcEdge extends Edge implements Handled {

    private final Handleable handleable;
    private final String[] names;
    private final String[] actions;
    private final Area npcArea;
    private final String[] chatOptions;
    private boolean swapSource;

    public NpcEdge(Vertex source, Vertex destination, int weight, Requirement[] requirements, Handleable handleable) {
        super(source, destination, weight, requirements);
        this.handleable = handleable;
        this.names = null;
        this.actions = null;
        this.npcArea = null;
        this.chatOptions = null;
    }

    public NpcEdge(Vertex source, Vertex destination, int weight, Requirement[] requirements, String[] names, String[] actions, Area npcArea, String[] chatOptions, boolean swapSource) {
        super(source, destination, weight, requirements);
        this.swapSource = swapSource;
        this.handleable = null;
        this.names = names;
        this.actions = actions;
        this.npcArea = npcArea;
        this.chatOptions = chatOptions;
    }

    @Override
    public Handleable getHandleable() {
        if(handleable == null) {
            if(swapSource) {
                return new NpcObjectHandle(names, actions, npcArea, chatOptions, getDestination().getCoordinate(), getSource().getCoordinate());
            }
            return new NpcObjectHandle(names, actions, npcArea, chatOptions, getSource().getCoordinate(), getDestination().getCoordinate());
        }
        return handleable;
    }

    @Override
    public String toString() {
        return "NpcEdge{" +
                "handleable=" + handleable +
                '}';
    }
}
