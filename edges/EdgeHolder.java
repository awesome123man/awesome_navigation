package com.regal.utility.awesome_navigation.edges;

import com.regal.utility.awesome_navigation.requirements.Requirement;
import com.runemate.game.api.hybrid.location.Coordinate;

import java.util.List;

public class EdgeHolder {

    private final Coordinate source;
    private final Coordinate destination;
    private final int weight;
    private final List<Requirement> requirementList;

    public EdgeHolder(Coordinate source, Coordinate destination, int weight, List<Requirement> requirementList) {
        this.source = source;
        this.destination = destination;
        this.weight = weight;
        this.requirementList = requirementList;
    }

    public Coordinate getSource() {
        return source;
    }

    public Coordinate getDestination() {
        return destination;
    }

    public List<Requirement> getRequirementList() {
        return requirementList;
    }

    public int getWeight() {
        return weight;
    }
}
