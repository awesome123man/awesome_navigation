package com.regal.utility.awesome_navigation.edges;

import com.regal.utility.awesome_navigation.WeightedGraph;
import com.regal.utility.awesome_navigation.requirements.Requirement;
import com.regal.utility.awesome_navigation.requirements.Requirements;
import com.regal.utility.awesome_navigation.vertices.Vertex;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;

import java.util.Arrays;

public enum RequirementEdges {
    TWISTED_BANSHEE_CATACOMBS_KOUREND(new Area.Rectangular(new Coordinate(1632, 9994, 0), new Coordinate(1632, 9990, 0)), Requirements.BANSHEE),
    TWISTED_BANSHEE_CATACOMBS_KOUREND_1(new Area.Rectangular(new Coordinate(1617, 10002, 0), new Coordinate(1629, 10002, 0)), Requirements.BANSHEE),
    IRON_STEEL_DRAGONS_CATACOMBS_KOUREND(new Area.Rectangular(new Coordinate(1616, 10053, 0), new Coordinate(1615, 10051, 0)), Requirements.DRAGON_BREATH),
    IRON_STEEL_DRAGONS_CATACOMBS_KOUREND_1(new Area.Rectangular(new Coordinate(1604, 10047, 0), new Coordinate(1608, 10047, 0)), Requirements.DRAGON_BREATH),
    ;


    private final Area area;
    private final Requirement[] requirements;

    RequirementEdges(Area area, Requirement[] requirements){
        this.area = area;
        this.requirements = requirements;
    }

    RequirementEdges(Area area, Requirements requirements){
        this.area = area;
        this.requirements = new Requirement[] {requirements.getRequirements()};
    }

    public static void addEdgesToGraph(WeightedGraph weightedGraph) {
        Arrays.stream(values()).forEach(requirementEdges -> {
            requirementEdges.area.getCoordinates().forEach(coordinate -> {
                Vertex vertex = weightedGraph.getGraph().get(coordinate.hashCode());
                if(vertex != null) {
                    vertex.getEdges().forEach(edge -> edge.setRequirements(requirementEdges.requirements));
                }
            });
        });
    }
}
