package com.regal.utility.awesome_navigation.edges;

import com.regal.utility.awesome_navigation.AwesomePathGenerator;
import com.regal.utility.awesome_navigation.GraphSection;
import com.regal.utility.awesome_navigation.WeightedGraph;
import com.regal.utility.awesome_navigation.handles.Teleport;
import com.regal.utility.awesome_navigation.requirements.CombinedRequirement;
import com.regal.utility.awesome_navigation.requirements.EquipmentRequirement;
import com.regal.utility.awesome_navigation.requirements.InventoryRequirement;
import com.regal.utility.awesome_navigation.requirements.Requirement;
import com.regal.utility.awesome_navigation.vertices.CoordinateVertex;
import com.regal.utility.awesome_navigation.vertices.Vertex;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Coordinate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public enum TeleportEdges {

    GAMES_NECKLACE(Teleport.GAMES_NECKLACE_BARBARIAN_OUTPOST, Teleport.GAMES_NECKLACE_BURTHORPE),
    ARDOUGNE_CLOAK(Teleport.ARDOUGNE_CLOAK),
    GLORY(Teleport.GLORY_EDGEVILLE, Teleport.GLORY_KARAMJA, Teleport.GLORY_AL_KHARID, Teleport.GLORY_DRAYNOR_VILLAGE),

    //SPELLBOOK_TELEPORTS(Teleport.LUMBRIDGE_HOME_TELEPORT),
    ;

    private final Teleport[] teleports;

    TeleportEdges(Teleport... teleports) {
        this.teleports = teleports;
    }

    public static void addEdgesToGraph(WeightedGraph graph, Coordinate position) {
        if(position != null) {
            Arrays.stream(values()).flatMap(teleportEdges -> Arrays.stream(teleportEdges.teleports)).forEach(teleport -> {
                Vertex vertex;
                if ((vertex = graph.getGraph().get(position.hashCode())) != null) {
                    List<Requirement> requirements = new ArrayList<>();
                    if(teleport.getTypes() != null) {
                        List<Class> types = Arrays.asList(teleport.getTypes());
                        if (types.contains(Inventory.class)) {
                            requirements.add(new InventoryRequirement(teleport.getPattern(), 1));
                        }
                        if (types.contains(Equipment.class)) {
                            requirements.add(new EquipmentRequirement(teleport.getPattern(), 1));
                        }
                    } else {
                        requirements.addAll(Arrays.asList(teleport.getRequirements()));
                    }
                    vertex.addEdge(new TeleportEdge(vertex, graph.getGraph().get(teleport.getPosition().hashCode()), teleport.getWeight(), new Requirement[]{new CombinedRequirement(false, requirements.toArray(new Requirement[0]))}, teleport));
                }
            });
        }
    }

    public static void addEdgesToMap(GraphSection[] sections, Coordinate position) {
        if(position != null) {
            Arrays.stream(values()).flatMap(teleportEdges -> Arrays.stream(teleportEdges.teleports)).forEach(teleport -> {
                GraphSection teleportEndSection = Arrays.stream(sections).filter(section -> section.getArea().contains(teleport.getPosition())).findFirst().orElse(null);
                GraphSection teleportStartSection = Arrays.stream(sections).filter(section -> section.getArea().contains(position)).findFirst().orElse(null);
                if(teleportEndSection != null && teleportStartSection != null) {
                    teleportStartSection.getHighLevelMap().putIfAbsent(position.hashCode(), new CoordinateVertex(position, 0));
                    Vertex teleportStartVertex;
                    if ((teleportStartVertex = teleportStartSection.getHighLevelMap().get(position.hashCode())) != null) {
                        List<Requirement> requirements = new ArrayList<>();
                        if(teleport.getTypes() != null) {
                            List<Class> types = Arrays.asList(teleport.getTypes());
                            if (types.contains(Inventory.class)) {
                                requirements.add(new InventoryRequirement(teleport.getPattern(), 1));
                            }
                            if (types.contains(Equipment.class)) {
                                requirements.add(new EquipmentRequirement(new Pattern[]{teleport.getPattern()}, 1));
                            }
                        } else {
                            requirements.addAll(Arrays.asList(teleport.getRequirements()));
                        }
                        teleportEndSection.getHighLevelMap().putIfAbsent(teleport.getPosition().hashCode(), new CoordinateVertex(teleport.getPosition(), 0));
                        Vertex teleportEndVertex = teleportEndSection.getHighLevelMap().get(teleport.getPosition().hashCode());
                        teleportStartVertex.addEdge(new TeleportEdge(teleportStartVertex, teleportEndVertex, teleport.getWeight(), new Requirement[]{new CombinedRequirement(false, requirements.toArray(new Requirement[0]))}, teleport));
                    }
                }
            });
            GraphSection teleportStartSection = Arrays.stream(sections).filter(section -> section.getArea().contains(position)).findFirst().orElse(null);
            Environment.getLogger().debug("Teleport: " + (teleportStartSection != null ? teleportStartSection.getHighLevelMap().get(position.hashCode()) : null));
        }
    }

    public static void addEdgesToMap(GraphSection[] sections, AwesomePathGenerator generator) {
        Arrays.stream(values()).flatMap(teleportEdges -> Arrays.stream(teleportEdges.teleports)).forEach(teleport -> {
            GraphSection teleportEndSection = Arrays.stream(sections).filter(section -> section.getArea().contains(teleport.getPosition())).findFirst().orElse(null);
            if (teleportEndSection != null) {
                teleportEndSection.getHighLevelMap().putIfAbsent(teleport.getPosition().hashCode(), new CoordinateVertex(teleport.getPosition(), 0));
                Vertex teleportEndVertex = teleportEndSection.getHighLevelMap().get(teleport.getPosition().hashCode());

                //Connect teleport end to high level
                //System.out.println(teleportEndSection.getHighLevelMap().size() + " " + teleportEndSection.getSpecificMap().size());
                teleportEndSection.getHighLevelMap().values().stream().filter(vertex1 -> !vertex1.equals(teleportEndVertex)).forEach(vertex1 -> {
                    teleportEndSection.getSpecificMap().values().forEach(vertex2 -> vertex2.getEdges().forEach(edge -> {
                        edge.setUsed(false);
                        vertex2.setFrom(null);
                        vertex2.setTotalCost(-1);
                    }));
                    List<Edge> edges = generator.generateEdges(teleportEndVertex, vertex1, teleportEndSection.getSpecificMap());
                    teleportEndVertex.getEdges().addAll(edges);
                });

                //System.out.println("Connected: " + teleportEndVertex.hashCode());
            }
        });
    }

    public static void removeOldEdges(WeightedGraph graph, Coordinate prevPos) {
        if(prevPos != null) {
            Vertex vertex = graph.getGraph().get(prevPos.hashCode());
            if(vertex != null) {
                vertex.getEdges().removeIf(edge -> edge instanceof TeleportEdge);
            }
        }
    }

    public static void removeOldEdges(GraphSection[] sections, Coordinate prevPos) {
        GraphSection sect = Arrays.stream(sections).filter(section -> section.getHighLevelMap().containsKey(prevPos.hashCode())).findFirst().orElse(null);
        if(sect != null) {
            Vertex vertex = sect.getHighLevelMap().get(prevPos.hashCode());
            vertex.getEdges().removeIf(edge -> edge instanceof TeleportEdge);
        }
    }
}
