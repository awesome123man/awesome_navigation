package com.regal.utility.awesome_navigation.edges;

import com.regal.utility.awesome_navigation.handles.GameObjectHandle;
import com.regal.utility.awesome_navigation.handles.Handleable;
import com.regal.utility.awesome_navigation.handles.Handled;
import com.regal.utility.awesome_navigation.requirements.Requirement;
import com.regal.utility.awesome_navigation.vertices.Vertex;

import java.util.Arrays;

public class GameObjectEdge extends Edge implements Handled {

    private final Handleable handleable;
    private final String[] names;
    private final String[] actions;
    private final int maxDistance;
    private final boolean reverse;

    public GameObjectEdge(Vertex source, Vertex destination, int weight, Requirement[] requirements, Handleable handleable) {
        super(source, destination, weight, requirements);
        this.handleable = handleable;
        this.names = null;
        this.actions = null;
        this.maxDistance = -1;
        reverse = false;
    }

    public GameObjectEdge(Vertex source, Vertex destination, int weight, Requirement[] requirements, String[] names, String[] actions, int maxDistance, boolean reverse) {
        super(source, destination, weight, requirements);
        this.reverse = reverse;
        this.handleable = null;
        this.names = names;
        this.actions = actions;
        this.maxDistance = maxDistance;
    }

    @Override
    public Handleable getHandleable() {
        if(handleable == null) {
            if(reverse) {
                return new GameObjectHandle(names, actions, maxDistance, getDestination().getCoordinate(), getSource().getCoordinate());
            }
            return new GameObjectHandle(names, actions, maxDistance, getSource().getCoordinate(), getDestination().getCoordinate());
        }
        return handleable;
    }

    @Override
    public boolean equals(Object edge) {
        if(edge instanceof Edge) {
            Edge edge1 = (Edge) edge;
            return edge1.getSource() != null && edge1.getDestination() != null && getSource().getCoordinate().equals(edge1.getSource().getCoordinate()) && getDestination().getCoordinate().equals(edge1.getDestination().getCoordinate());
        }
        return false;
    }

    @Override
    public String toString() {
        return "GameObjectEdge{" +
                "handleable=" + getHandleable() +
                ", used=" + isUsed() +
                ", requirements=" + Arrays.toString(getRequirements()) +
                '}';
    }
}
