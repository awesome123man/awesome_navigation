package com.regal.utility.awesome_navigation.edges;


import com.regal.utility.awesome_navigation.requirements.Requirement;
import com.regal.utility.awesome_navigation.vertices.Vertex;

import java.util.Arrays;

public class Edge {

    private Vertex source;
    private Vertex destination;
    private final int weight;
    private transient Requirement[] requirements;
    private transient boolean used;

    public Edge(Vertex source, Vertex destination, int weight, Requirement[] requirements) {
        this.source = source;
        this.destination = destination;
        this.weight = weight;
        this.requirements = requirements;
    }

    public Edge(Edge other) {
        this.source = other.source;
        this.destination = other.destination;
        this.weight = other.weight;
    }

    public Vertex getSource() {
        return source;
    }

    public Vertex getDestination() {
        return destination;
    }

    public int getWeight() {
        return weight;
    }

    public void setSource(Vertex vertex) {
        this.source = vertex;
    }

    public void setDestination(Vertex vertex) {
        this.destination = vertex;
    }

    @Override
    public boolean equals(Object edge) {
        if(edge instanceof Edge) {
            Edge edge1 = (Edge) edge;
            return edge1.getSource() != null && edge1.getDestination() != null && source.getCoordinate().equals(edge1.getSource().getCoordinate()) && destination.getCoordinate().equals(edge1.getDestination().getCoordinate());
        }
        return false;
    }

    @Override
    public String toString() {
        return "Edge{" +
                "source=" + source.getCoordinate() +
                ", destination=" + ((getDestination() == null) ? null : getDestination().getCoordinate()) +
                ", weight=" + weight +
                ", requirements=" + Arrays.toString(requirements) +
                ", used=" + used +
                ", from=" + (source.getFrom() != null ? source.getFrom().getCoordinate() : "null") +
                '}';
    }

    public Requirement[] getRequirements() {
        return requirements;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public boolean isUsed() {
        return used;
    }

    public void setRequirements(Requirement[] requirements) {
        this.requirements = requirements;
    }
}
