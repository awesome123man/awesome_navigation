package com.regal.utility.awesome_navigation.edges;

import com.regal.utility.awesome_navigation.handles.Handleable;
import com.regal.utility.awesome_navigation.handles.Handled;
import com.regal.utility.awesome_navigation.requirements.Requirement;
import com.regal.utility.awesome_navigation.vertices.Vertex;

public class TeleportEdge extends Edge implements Handled {

    private final Handleable handleable;

    public TeleportEdge(Vertex source, Vertex destination, int weight, Requirement[] requirements, Handleable handleable) {
        super(source, destination, weight, requirements);
        this.handleable = handleable;
    }

    @Override
    public Handleable getHandleable() {
        return handleable;
    }

    @Override
    public String toString() {
        return "TeleportEdge{" +
                "destination=" + ((getDestination() == null) ? "null" : getDestination().getCoordinate()) +
                ", handleable=" + handleable +
                ", used=" + isUsed() +
                ", weight=" + getWeight() +
                '}';
    }
}
