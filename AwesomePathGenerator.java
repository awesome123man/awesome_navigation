package com.regal.utility.awesome_navigation;

import com.regal.utility.awesome_navigation.edges.Edge;
import com.regal.utility.awesome_navigation.edges.TeleportEdges;
import com.regal.utility.awesome_navigation.handles.Teleport;
import com.regal.utility.awesome_navigation.requirements.CombinedRequirement;
import com.regal.utility.awesome_navigation.requirements.Requirement;
import com.regal.utility.awesome_navigation.vertices.Vertex;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.details.Locatable;
import com.runemate.game.api.hybrid.local.Quests;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.script.framework.AbstractBot;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AwesomePathGenerator {

    private boolean useTeleports;
    private Coordinate prevTeleportStart;

    public AwesomePathGenerator(boolean useTeleports) {
        this.useTeleports = useTeleports;
    }

    public AwesomePathGenerator() {

    }

    public List<Vertex> generatePath(WeightedGraph weightedGraph, Coordinate start, Locatable end) {
        LinkedHashMap<Integer, Vertex> graph = weightedGraph.getGraph();
        graph.forEach((integer, vertex) -> {
            vertex.setFrom(null);
            vertex.setTotalCost(-1);
            vertex.getEdges().forEach(edge -> edge.setUsed(false));
        });
        List<Vertex> generatedPath;
        if(end instanceof Area) {
            generatedPath = generatePath(weightedGraph, start, new Area[]{(Area) end});
        }
        else {
            generatedPath = generatePath(weightedGraph, start, new Area[]{end.getArea()});
        }
        return generatedPath;
    }

    public List<Vertex> generatePath(WeightedGraph weightedGraph, Coordinate start, Area[] ends) {
        LinkedHashMap<Integer, Vertex> graph = weightedGraph.getGraph();
        if (prevTeleportStart != null) {
            TeleportEdges.removeOldEdges(weightedGraph, prevTeleportStart);
        }
        if (useTeleports) {
            TeleportEdges.addEdgesToGraph(weightedGraph, start);
            prevTeleportStart = start;
        }
        Vertex startVertex = graph.get(start.hashCode());
        if (startVertex != null && !startVertex.getEdges().isEmpty()) {
            return calculateShortestPathFromSource(startVertex, ends);
        }
        Environment.getLogger().severe("Start Vertex NULL! " + startVertex);
        return null;
    }

    public LinkedHashMap<Integer, Vertex> getReachableVertices(LinkedHashMap<Integer, Vertex> graph, Coordinate start, boolean canUseRequirements) {
        Vertex startVertex = graph.get(start.hashCode());
        if (startVertex == null) {
            for (int x = -1; x < 2; x++) {
                for (int y = -1; y < 2; y++) {
                    startVertex = graph.get(start.derive(x, y).hashCode());
                    if (startVertex != null) {
                        break;
                    }
                }
            }
        }

        if (startVertex != null && !startVertex.getEdges().isEmpty()) {
            graph.forEach((integer, vertex) -> {
                vertex.setFrom(null);
                vertex.setTotalCost(-1);
                //vertex.getEdges().forEach(edge -> edge.setUsed(false));
            });
            return getReachableVertices(graph, startVertex, canUseRequirements);
        }
        return null;
    }

    public LinkedHashMap<Integer, Vertex> getReachableVertices(LinkedHashMap<Integer, Vertex> graph, Coordinate start, boolean canUseRequirements, int maxWeight) {
        Vertex startVertex = graph.get(start.hashCode());
        if (startVertex == null) {
            for (int x = -1; x < 2; x++) {
                for (int y = -1; y < 2; y++) {
                    startVertex = graph.get(start.derive(x, y).hashCode());
                    if (startVertex != null) {
                        break;
                    }
                }
            }
        }
        if (startVertex != null && !startVertex.getEdges().isEmpty()) {
            graph.forEach((integer, vertex) -> {
                vertex.setFrom(null);
                vertex.setTotalCost(-1);
                //vertex.getEdges().forEach(edge -> edge.setUsed(false));
            });
            return getReachableVertices(graph, startVertex, canUseRequirements, maxWeight);
        }
        return null;
    }


    private LinkedHashMap<Integer, Vertex> getReachableVertices(LinkedHashMap<Integer, Vertex> map, Vertex source, boolean canUseRequirements) {
        source.setTotalCost(0);

        LinkedHashMap<Integer, Vertex> settledNodes = new LinkedHashMap<>();
        Set<Vertex> unsettledNodes = new HashSet<>();

        unsettledNodes.add(source);

        while (unsettledNodes.size() != 0) {
            Vertex currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);
            for (Edge edge : currentNode.getEdges()) {
                Vertex adjacentNode = edge.getDestination();
                int edgeWeight = edge.getWeight();
                if ((edge.getRequirements() == null || (canUseRequirements && Arrays.stream(edge.getRequirements()).filter(Objects::nonNull).allMatch(requirement -> requirement.checkRequirement() == null))) && adjacentNode != null && map.get(adjacentNode.getCoordinate().hashCode()) != null && adjacentNode.getFrom() == null) {
                    calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                    unsettledNodes.add(adjacentNode);
                }
            }
            settledNodes.put(currentNode.getCoordinate().hashCode(), currentNode);
        }
        return settledNodes;
    }

    private LinkedHashMap<Integer, Vertex> getReachableVertices(LinkedHashMap<Integer, Vertex> map, Vertex source, boolean canUseRequirements, int maxWeight) {
        source.setTotalCost(0);

        LinkedHashMap<Integer, Vertex> settledNodes = new LinkedHashMap<>();
        Set<Vertex> unsettledNodes = new HashSet<>();

        unsettledNodes.add(source);

        while (unsettledNodes.size() != 0) {
            Vertex currentNode = getLowestDistanceNode(unsettledNodes);
            if(currentNode.getTotalCost() > maxWeight) {
                return settledNodes;
            }
            unsettledNodes.remove(currentNode);
            for (Edge edge : currentNode.getEdges()) {
                Vertex adjacentNode = edge.getDestination();
                int edgeWeight = edge.getWeight();
                if ((edge.getRequirements() == null || (canUseRequirements && Arrays.stream(edge.getRequirements()).filter(Objects::nonNull).allMatch(requirement -> requirement.checkRequirement() == null))) && adjacentNode != null && map.get(adjacentNode.getCoordinate().hashCode()) != null && adjacentNode.getFrom() == null) {
                    calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                    unsettledNodes.add(adjacentNode);
                }
            }
            settledNodes.put(currentNode.getCoordinate().hashCode(), currentNode);
        }
        return settledNodes;
    }

    private LinkedHashMap<Integer, Vertex> getReachableVerticesTemporary(LinkedHashMap<Integer, Vertex> map, Vertex source, boolean canUseRequirements) {
        if (source == null) {
            return new LinkedHashMap<>();
        }
        source.setTotalCost(0);

        LinkedHashMap<Integer, Vertex> settledNodes = new LinkedHashMap<>();
        Set<Vertex> unsettledNodes = new HashSet<>(new ArrayList<>(Arrays.asList(source)));

        while (unsettledNodes.size() != 0) {
            Vertex currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);
            for (Edge edge : currentNode.getEdges()) {
                Vertex adjacentNode = edge.getDestination();
                if (((edge.getRequirements() == null) || (canUseRequirements && Arrays.stream(edge.getRequirements()).filter(Objects::nonNull).allMatch(requirement -> requirement.checkRequirement() == null))) && adjacentNode != null && map.get(adjacentNode.getCoordinate().hashCode()) != null && !settledNodes.containsKey(adjacentNode.getCoordinate().hashCode())) {
                    calculateMinimumDistanceTemp(adjacentNode, edge.getWeight(), currentNode);
                    unsettledNodes.add(adjacentNode);
                }
            }
            settledNodes.put(currentNode.getCoordinate().hashCode(), currentNode);
        }
        return settledNodes;
    }

    private LinkedHashMap<Integer, Vertex> getReachableVerticesTemporary(GraphSection section, List<Vertex> sources, boolean canUseRequirements) {
        LinkedHashMap<Integer, Vertex> map = section.getSpecificMap();
        if (sources == null || sources.isEmpty()) {
            return new LinkedHashMap<>();
        }
        sources.removeIf(Objects::isNull);
        sources.forEach(vertex -> vertex.setTotalCost(0));

        LinkedHashMap<Integer, Vertex> settledNodes = new LinkedHashMap<>();

        Set<Vertex> unsettledNodes = new HashSet<>(sources);

        while (unsettledNodes.size() != 0) {
            Vertex currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);
            for (Edge edge : currentNode.getEdges()) {
                Vertex adjacentNode = edge.getDestination();
                if (!currentNode.getCoordinate().equals(adjacentNode.getCoordinate()) && section.getArea().contains(adjacentNode.getCoordinate())) {
                    if (((edge.getRequirements() == null) || (canUseRequirements && Arrays.stream(edge.getRequirements()).filter(Objects::nonNull).allMatch(requirement -> requirement.checkRequirement() == null))) && adjacentNode != null && map.get(adjacentNode.getCoordinate().hashCode()) != null && !settledNodes.containsKey(adjacentNode.getCoordinate().hashCode())) {
                        calculateMinimumDistanceTemp(adjacentNode, edge.getWeight(), currentNode);
                        unsettledNodes.add(adjacentNode);
                    }
                }
            }
            settledNodes.put(currentNode.getCoordinate().hashCode(), currentNode);
        }
        return settledNodes;
    }

    public List<Vertex> calculateShortestPathFromSource(Vertex source, Area[] ends) {
        source.setTotalCost(0);

        //Set<Vertex> settledNodes = new HashSet<>();
        Set<Vertex> unsettledNodes = new HashSet<>();

        unsettledNodes.add(source);
        while (unsettledNodes.size() != 0) {
            Vertex currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);
            for (Edge edge : currentNode.getEdges()) {
                if (edge.getRequirements() == null || Arrays.stream(edge.getRequirements()).filter(Objects::nonNull).allMatch(requirement -> requirement.checkRequirement() == null)) {
                    Vertex adjacentNode = edge.getDestination();
                    int edgeWeight = edge.getWeight();
                    if (adjacentNode != null && adjacentNode.getFrom() == null) {
                        calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                        unsettledNodes.add(adjacentNode);
                    }
                }
            }
            for (Area currArea : ends) {
                if (currArea.contains(currentNode.getCoordinate())) {
                    Vertex end = currentNode;
                    List<Vertex> path = new ArrayList<>();
                    AbstractBot bot;
                    while (end.getFrom() != null && (bot = Environment.getBot()) != null && !bot.isStopped() && !end.equals(source)) {
                        Vertex finalEnd = end;
                        end.getFrom().getEdges().stream().filter(edge -> Objects.equals(edge.getDestination(), finalEnd)).findFirst().ifPresent(edge1 -> edge1.setUsed(true));
                        path.add(0, end);
                        end = end.getFrom();
                    }
                    path.add(0, source);
                    return path;
                }
            }
        }
        Environment.getLogger().debug("Path Not Found...");
        return null;
    }

    public List<GraphSection> calculateShortestPathFromSource(GraphSection[] sections, Coordinate start, Locatable end1) {
        Area end;
        if (end1 instanceof Area) {
            end = (Area) end1;
        } else {
            end = new Area.Absolute(end1.getPosition());
        }

        GraphSection startSection = Arrays.stream(sections).filter(graphSection -> graphSection.getArea().contains(start)).findFirst().orElse(null);
        if(useTeleports) {
            TeleportEdges.addEdgesToMap(sections, start);
        }

        Arrays.stream(sections).forEach(graphSection -> {
            graphSection.setCost(0);
            graphSection.getHighLevelMap().values().forEach(vertex -> {
                vertex.setFrom(null);
                vertex.setTotalCost(-1);
                vertex.getEdges().forEach(edge -> edge.setUsed(false));
            });
        });

        GraphSection endSection = Arrays.stream(sections).filter(graphSection -> graphSection.getArea().contains(end.getCenter())).findFirst().orElse(null);
        if (startSection == null || endSection == null) {
            return null;
        }
        //Build path in end section to the nearest incoming edge...

        startSection.loadGraph(true);
        startSection.addSpecialEdges();

        Environment.getLogger().debug("Start Section: " + startSection);

        endSection.loadGraph(true);
        endSection.addSpecialEdges();

        Environment.getLogger().debug("End Section: " + endSection);

        Collection<Vertex> vals = getReachableVerticesTemporary(endSection, end.getCoordinates().stream().map(coor -> endSection.getSpecificMap().get(coor.hashCode())).collect(Collectors.toList()), true).values();
        Map<Integer, Coordinate> reachableEnds = endSection.getHighLevelMap().values().stream()
                .map(Vertex::getCoordinate)
                .filter(coordinate -> vals.stream()
                        .filter(vertex1 -> endSection.getHighLevelMap().containsKey(vertex1.getCoordinate().hashCode())).map(vertex1 -> vertex1.getCoordinate().hashCode())
                        .collect(Collectors.toList()).contains(coordinate.hashCode())).collect(Collectors.toMap(Coordinate::hashCode, Function.identity()));

        final LinkedHashMap<Integer, Vertex> reachableStart = getReachableVerticesTemporary(startSection, new ArrayList<>(Arrays.asList(startSection.getSpecificMap().get(start.hashCode()))), true);

        HashSet<Vertex> unsettledNodes = startSection.getHighLevelMap().values().stream().filter(vertex -> reachableStart.values().stream().filter(vertex1 -> startSection.getHighLevelMap().containsKey(vertex1.getCoordinate().hashCode())).map(vertex1 -> vertex1.getCoordinate().hashCode()).collect(Collectors.toList()).contains(vertex.getCoordinate().hashCode())).collect(Collectors.toCollection(HashSet::new));
        List<Coordinate> startCoors = unsettledNodes.stream().map(Vertex::getCoordinate).collect(Collectors.toList());

        unsettledNodes.forEach(vertex -> {
            vertex.setTotalCost(reachableStart.get(vertex.getCoordinate().hashCode()).getTotalCost());
            //vertex.setFrom(null);
        });


        startSection.getSpecificMap().values().stream().filter(vertex -> !startCoors.contains(vertex.getCoordinate())).forEach(vertex -> {
            //vertex.setFrom(null);
            vertex.setTotalCost(-1);
            //vertex.getEdges().forEach(edge -> edge.setUsed(false));
        });

        endSection.getHighLevelMap().values().forEach(vertex -> vertex.setTotalCost(-1));

        int minTotalCost = -1;
        boolean allowedToEnd = false;
        Set<GraphSection> sectionsUsed = null;
        Set<Integer> endsReached = new HashSet<>();
        Set<Vertex> settledNodes = new HashSet<>();

        while (unsettledNodes.size() != 0) {
            Vertex currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);
            if (currentNode != null && currentNode.getTotalCost() > minTotalCost && minTotalCost > -1) {
                Environment.getLogger().debug("MinTotalCost: " + minTotalCost);
                allowedToEnd = true;
            } else {
                if (currentNode != null) {
                    for (Edge edge : currentNode.getEdges()) {
                        if (!edge.isUsed() && !settledNodes.contains(edge.getDestination())) {
                            if ((edge.getRequirements() == null || Arrays.stream(edge.getRequirements()).filter(Objects::nonNull).allMatch(requirement -> requirement.checkRequirement() == null))) {
                                Vertex adjacentNode = edge.getDestination();
                                int edgeWeight = edge.getWeight();
                                if (adjacentNode != null) {
                                    calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                                    unsettledNodes.add(adjacentNode);
                                }
                            }
                            edge.setUsed(true);
                        }
                    }
                    settledNodes.add(currentNode);
                }
            }
            if (currentNode != null && (allowedToEnd || (currentNode.getFrom() != null && reachableEnds.containsKey(currentNode.getCoordinate().hashCode())))) {
                if (allowedToEnd) {
                    //Try to build from end to edge used to enter.
                    TeleportEdges.removeOldEdges(sections, start);
                    return new ArrayList<>(sectionsUsed);
                } else if (!endsReached.contains(currentNode.getCoordinate().hashCode())) {
                    endSection.getSpecificMap().values().stream().filter(vertex -> !reachableEnds.containsKey(vertex.getCoordinate().hashCode())).forEach(vertex -> {
                        vertex.setFrom(null);
                        vertex.setTotalCost(-1);
                        vertex.getEdges().forEach(edge -> edge.setUsed(false));
                    });

                    Vertex found = vals.stream().filter(vertex -> currentNode.getCoordinate().equals(vertex.getCoordinate())).findFirst().orElse(null);
                    if (found != null) {
                        if (currentNode.getTotalCost() + found.getTotalCost() < minTotalCost || minTotalCost < 0) {
                            minTotalCost = currentNode.getTotalCost() + found.getTotalCost();
                            List<Vertex> path = new ArrayList<>();
                            Vertex endNode = currentNode;
                            AbstractBot bot;
                            while (endNode != null && !path.contains(endNode) && (bot = Environment.getBot()) != null && !bot.isStopped()) {
                                path.add(0, endNode);
                                endNode = endNode.getFrom();
                            }
                            sectionsUsed = new HashSet<>();
                            for (Vertex vertex : path) {
                                GraphSection sect = Arrays.stream(sections).filter(graphSection -> graphSection.getArea().contains(vertex.getCoordinate())).findFirst().orElse(null);
                                sectionsUsed.add(sect);
                            }
                            sectionsUsed.add(startSection);
                            sectionsUsed.add(endSection);
                            endsReached.add(currentNode.hashCode());
                            if(end.contains(currentNode.getCoordinate())) {
                                allowedToEnd = true;
                            }
                        }
                    }
                }
            }
        }
        TeleportEdges.removeOldEdges(sections, start);
        Environment.getLogger().debug("Path Not Found...");
        return sectionsUsed == null ? null : new ArrayList<>(sectionsUsed);
    }

    public List<GraphSection> calculateShortestPathFromSourceTemp(GraphSection[] sections, Coordinate start, Locatable end1) {
        Area end;
        if (end1 instanceof Area) {
            end = (Area) end1;
        } else {
            end = new Area.Absolute(end1.getPosition());
        }

        GraphSection startSection = Arrays.stream(sections).filter(graphSection -> graphSection.getArea().contains(start)).findFirst().orElse(null);
        if(useTeleports) {
            TeleportEdges.addEdgesToMap(sections, start);
        }

        Arrays.stream(sections).forEach(graphSection -> {
            graphSection.setCost(0);
            graphSection.getHighLevelMap().values().forEach(vertex -> {
                vertex.setFrom(null);
                vertex.setTotalCost(-1);
                vertex.getEdges().forEach(edge -> edge.setUsed(false));
            });
        });

        GraphSection endSection = Arrays.stream(sections).filter(graphSection -> graphSection.getArea().contains(end.getCenter())).findFirst().orElse(null);
        if (startSection == null || endSection == null) {
            return null;
        }
        //Build path in end section to the nearest incoming edge...

        startSection.loadGraph(true);
        startSection.addSpecialEdges();

        Environment.getLogger().debug("Start Section: " + startSection);

        endSection.loadGraph(true);
        endSection.addSpecialEdges();

        Environment.getLogger().debug("End Section: " + endSection);

        Collection<Vertex> vals = getReachableVerticesTemporary(endSection, end.getCoordinates().stream().map(coor -> endSection.getSpecificMap().get(coor.hashCode())).collect(Collectors.toList()), true).values();

        endSection.getHighLevelMap().forEach((integer, vertex) -> {
            Vertex foundReachableVertex;
            if((foundReachableVertex = vals.stream().filter(reachableVertex -> reachableVertex.getCoordinate().equals(vertex.getCoordinate())).findFirst().orElse(null)) != null) {
                Vertex endVertEdge = end.getCoordinates().stream().map(coor -> endSection.getSpecificMap().get(coor.hashCode())).filter(Objects::nonNull).findFirst().orElse(null);
                vertex.addEdge(new Edge(vertex, endVertEdge, foundReachableVertex.getTotalCost(), null));
            }
        });

        final LinkedHashMap<Integer, Vertex> reachableStart = getReachableVerticesTemporary(startSection, new ArrayList<>(Arrays.asList(startSection.getSpecificMap().get(start.hashCode()))), true);
        //System.out.println(Arrays.toString(reachableStart.values().stream().map(Vertex::getCoordinate).toArray()));

        HashSet<Vertex> unsettledNodes = startSection.getHighLevelMap().values().stream().filter(vertex -> reachableStart.values().stream().filter(vertex1 -> startSection.getHighLevelMap().containsKey(vertex1.getCoordinate().hashCode())).map(vertex1 -> vertex1.getCoordinate().hashCode()).collect(Collectors.toList()).contains(vertex.getCoordinate().hashCode())).collect(Collectors.toCollection(HashSet::new));
        List<Coordinate> startCoors = unsettledNodes.stream().map(Vertex::getCoordinate).collect(Collectors.toList());

        unsettledNodes.forEach(vertex -> {
            vertex.setTotalCost(reachableStart.get(vertex.getCoordinate().hashCode()).getTotalCost());
            //vertex.setFrom(null);
        });


        startSection.getSpecificMap().values().stream().filter(vertex -> !startCoors.contains(vertex.getCoordinate())).forEach(vertex -> {
            //vertex.setFrom(null);
            vertex.setTotalCost(-1);
            //vertex.getEdges().forEach(edge -> edge.setUsed(false));
        });

        endSection.getHighLevelMap().values().forEach(vertex -> vertex.setTotalCost(-1));

        Set<GraphSection> sectionsUsed = null;
        Set<Vertex> settledNodes = new HashSet<>();

        while (unsettledNodes.size() != 0) {
            Vertex currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);
            if (currentNode != null) {
                currentNode.getTotalCost();
            }
            if (currentNode != null) {
                for (Edge edge : currentNode.getEdges()) {
                    if (!edge.getDestination().getCoordinate().equals(currentNode.getCoordinate()) && !edge.isUsed() && !settledNodes.contains(edge.getDestination())) {
                        if ((edge.getRequirements() == null || Arrays.stream(edge.getRequirements()).filter(Objects::nonNull).allMatch(requirement -> requirement.checkRequirement() == null))) {
                            Vertex adjacentNode = edge.getDestination();
                            int edgeWeight = edge.getWeight();
                            if (adjacentNode != null) {
                                calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                                unsettledNodes.add(adjacentNode);
                            }
                        }
                        edge.setUsed(true);
                    }
                }
                settledNodes.add(currentNode);
            }
            if (currentNode != null && (currentNode.getFrom() != null && vals.stream().anyMatch(vertex -> vertex.getCoordinate().equals(currentNode.getCoordinate())))) {
                    List<Vertex> path = new ArrayList<>();
                    Vertex endNode = currentNode;
                    AbstractBot bot;
                    while (endNode != null && !path.contains(endNode) && (bot = Environment.getBot()) != null && !bot.isStopped()) {
                        path.add(0, endNode);
                        endNode = endNode.getFrom();
                    }
                    sectionsUsed = new HashSet<>();
                    for (Vertex vertex : path) {
                        GraphSection sect = Arrays.stream(sections).filter(graphSection -> graphSection.getArea().contains(vertex.getCoordinate())).findFirst().orElse(null);
                        sectionsUsed.add(sect);
                    }
                    sectionsUsed.add(startSection);
                    sectionsUsed.add(endSection);
                    //Try to build from end to edge used to enter.
                    TeleportEdges.removeOldEdges(sections, start);
                    return new ArrayList<>(sectionsUsed);
            }
        }
        TeleportEdges.removeOldEdges(sections, start);
        Environment.getLogger().debug("Path Not Found...");
        return null;
    }

    public List<GraphSection> calculateShortestPathFromSource(GraphSection[] sections, Coordinate start, Locatable dest, Locatable returnDest) {
        Area end;
        if (dest instanceof Area) {
            end = (Area) dest;
        } else {
            end = new Area.Absolute(dest.getPosition());
        }

        GraphSection startSection = Arrays.stream(sections).filter(graphSection -> graphSection.getArea().contains(start)).findFirst().orElse(null);
        if(useTeleports) {
            TeleportEdges.addEdgesToMap(sections, start);
        }
        Arrays.stream(sections).forEach(graphSection -> {
            graphSection.setCost(0);
            graphSection.getHighLevelMap().values().forEach(vertex -> {
                vertex.setFrom(null);
                vertex.setTotalCost(-1);
                vertex.getEdges().forEach(edge -> edge.setUsed(false));
            });
        });

        Area finalEnd = end;
        GraphSection endSection = Arrays.stream(sections).filter(graphSection -> graphSection.getArea().contains(finalEnd.getCenter())).findFirst().orElse(null);
        if (startSection == null || endSection == null) {
            return null;
        }
        //Build path in end section to nearest incoming edge...

        startSection.loadGraph(true);
        startSection.addSpecialEdges();

        Environment.getLogger().debug("Start Section: " + startSection);

        endSection.loadGraph(true);
        endSection.addSpecialEdges();

        Environment.getLogger().debug("End Section: " + endSection);

        GraphSection finalEndSection = endSection;
        Collection<Vertex> vals = getReachableVerticesTemporary(endSection, end.getCoordinates().stream().map(coor -> finalEndSection.getSpecificMap().get(coor.hashCode())).collect(Collectors.toList()), true).values();
        Collection<Vertex> finalVals1 = vals;
        Map<Integer, Coordinate> reachableEnds = endSection.getHighLevelMap().values().stream()
                .map(Vertex::getCoordinate)
                .filter(coordinate -> finalVals1.stream()
                        .filter(vertex1 -> finalEndSection.getHighLevelMap().containsKey(vertex1.getCoordinate().hashCode())).map(vertex1 -> vertex1.getCoordinate().hashCode())
                        .collect(Collectors.toList()).contains(coordinate.hashCode())).collect(Collectors.toMap(Coordinate::hashCode, Function.identity()));

        LinkedHashMap<Integer, Vertex> reachableStart = getReachableVerticesTemporary(startSection, new ArrayList<>(Arrays.asList(startSection.getSpecificMap().get(start.hashCode()))), true);

        HashSet<Vertex> unsettledNodes = startSection.getHighLevelMap().values().stream().filter(vertex -> reachableStart.values().stream().filter(vertex1 -> startSection.getHighLevelMap().containsKey(vertex1.getCoordinate().hashCode())).map(vertex1 -> vertex1.getCoordinate().hashCode()).collect(Collectors.toList()).contains(vertex.getCoordinate().hashCode())).collect(Collectors.toCollection(HashSet::new));
        List<Coordinate> startCoors = unsettledNodes.stream().map(Vertex::getCoordinate).collect(Collectors.toList());

        unsettledNodes.forEach(vertex -> {
            vertex.setTotalCost(reachableStart.get(vertex.getCoordinate().hashCode()).getTotalCost());
            //vertex.setFrom(null);
        });


        List<Coordinate> finalStartCoors = startCoors;
        startSection.getSpecificMap().values().stream().filter(vertex -> !finalStartCoors.contains(vertex.getCoordinate())).forEach(vertex -> vertex.setTotalCost(-1));

        endSection.getHighLevelMap().values().forEach(vertex -> vertex.setTotalCost(-1));

        int minTotalCost;
        Set<GraphSection> sectionsUsed;
        HashMap<Vertex, Set<GraphSection>> listOfSectionsUsedSets = new HashMap<>();
        Set<Integer> endsReached = new HashSet<>();

        while (unsettledNodes.size() != 0) {
            Vertex currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);
                if (currentNode != null) {
                    for (Edge edge : currentNode.getEdges()) {
                        if (!edge.isUsed()) {
                            if ((edge.getRequirements() == null || Arrays.stream(edge.getRequirements()).filter(Objects::nonNull).allMatch(requirement -> requirement.checkRequirement() == null))) {
                                Vertex adjacentNode = edge.getDestination();
                                int edgeWeight = edge.getWeight();
                                if (adjacentNode != null) {
                                    calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                                    unsettledNodes.add(adjacentNode);
                                }
                            }
                            edge.setUsed(true);
                        }
                    }
                }
            if (currentNode != null && currentNode.getFrom() != null && reachableEnds.containsKey(currentNode.getCoordinate().hashCode())) {
                if (!endsReached.contains(currentNode.getCoordinate().hashCode())) {
                    Map<Integer, Coordinate> finalReachableEnds = reachableEnds;
                    endSection.getSpecificMap().values().stream().filter(vertex -> !finalReachableEnds.containsKey(vertex.getCoordinate().hashCode())).forEach(vertex -> {
                        vertex.setFrom(null);
                        vertex.setTotalCost(-1);
                        vertex.getEdges().forEach(edge -> edge.setUsed(false));
                    });

                    Vertex found = vals.stream().filter(vertex -> currentNode.getCoordinate().equals(vertex.getCoordinate())).findFirst().orElse(null);
                    if (found != null) {
                        minTotalCost = currentNode.getTotalCost() + found.getTotalCost();
                        List<Vertex> path = new ArrayList<>();
                        AbstractBot bot;
                        Vertex endNode = currentNode;
                        while (endNode != null && !path.contains(endNode) && (bot = Environment.getBot()) != null && !bot.isStopped()) {
                            path.add(0, endNode);
                            endNode = endNode.getFrom();
                        }
                        sectionsUsed = new HashSet<>();
                        for (Vertex vertex : path) {
                            GraphSection sect = Arrays.stream(sections).filter(graphSection -> graphSection.getArea().contains(vertex.getCoordinate())).findFirst().orElse(null);
                            sectionsUsed.add(sect);
                        }
                        sectionsUsed.add(startSection);
                        sectionsUsed.add(endSection);
                        currentNode.setTotalCost(minTotalCost);
                        listOfSectionsUsedSets.put(currentNode, sectionsUsed);
                        endsReached.add(currentNode.hashCode());
                    }
                }
            }
        }

        //----------------- Start Return Calculation

        if (returnDest instanceof Area) {
            end = (Area) returnDest;
        } else {
            end = new Area.Absolute(returnDest.getPosition());
        }

        Set<GraphSection> startSections = Arrays.stream(sections).filter(graphSection -> listOfSectionsUsedSets.keySet().stream().anyMatch(vertex -> graphSection.getArea().contains(vertex.getCoordinate()))).collect(Collectors.toSet());
        if(useTeleports) {
            TeleportEdges.addEdgesToMap(sections, start);
        }
        Arrays.stream(sections).forEach(graphSection -> {
            graphSection.setCost(0);
            graphSection.getHighLevelMap().values().forEach(vertex -> {
                vertex.setFrom(null);
                vertex.setTotalCost(-1);
                vertex.getEdges().forEach(edge -> edge.setUsed(false));
            });
        });

        Area finalEnd1 = end;
        endSection = Arrays.stream(sections).filter(graphSection -> graphSection.getArea().contains(finalEnd1.getCenter())).findFirst().orElse(null);
        if (startSections.isEmpty() || endSection == null) {
            return null;
        }
        //Build path in end section to nearest incoming edge...

        startSections.forEach(section -> {
            section.loadGraph(true);
            section.addSpecialEdges();
        });

        Environment.getLogger().debug("Start Section: " + startSection);

        endSection.loadGraph(true);
        endSection.addSpecialEdges();

        Environment.getLogger().debug("End Section: " + endSection);

        GraphSection finalEndSection1 = endSection;
        vals = getReachableVerticesTemporary(endSection, end.getCoordinates().stream().map(coor -> finalEndSection1.getSpecificMap().get(coor.hashCode())).collect(Collectors.toList()), true).values();
        Collection<Vertex> finalVals = vals;
        reachableEnds = endSection.getHighLevelMap().values().stream()
                .map(Vertex::getCoordinate)
                .filter(coordinate -> finalVals.stream()
                        .filter(vertex1 -> finalEndSection1.getHighLevelMap().containsKey(vertex1.getCoordinate().hashCode())).map(vertex1 -> vertex1.getCoordinate().hashCode())
                        .collect(Collectors.toList()).contains(coordinate.hashCode())).collect(Collectors.toMap(Coordinate::hashCode, Function.identity()));

        for (GraphSection section : startSections) {
            reachableStart.putAll(getReachableVerticesTemporary(section, new ArrayList<>(Arrays.asList(section.getSpecificMap().get(start.hashCode()))), true));
        }

        //TODO: Finish this
        //unsettledNodes = new HashSet<>(startSection.getHighLevelMap().values().stream().filter(vertex -> finalReachableStart.values().stream().filter(vertex1 -> finalReachableStart.getHighLevelMap().containsKey(vertex1.getCoordinate().hashCode())).map(vertex1 -> vertex1.getCoordinate().hashCode()).collect(Collectors.toList()).contains(vertex.getCoordinate().hashCode())).collect(Collectors.toList()));
        startCoors = unsettledNodes.stream().map(Vertex::getCoordinate).collect(Collectors.toList());

        unsettledNodes.forEach(vertex -> {
            vertex.setTotalCost(reachableStart.get(vertex.getCoordinate().hashCode()).getTotalCost());
            //vertex.setFrom(null);
        });


        List<Coordinate> finalStartCoors1 = startCoors;
        startSection.getSpecificMap().values().stream().filter(vertex -> !finalStartCoors1.contains(vertex.getCoordinate())).forEach(vertex -> {
            vertex.setTotalCost(-1);
        });

        endSection.getHighLevelMap().values().forEach(vertex -> vertex.setTotalCost(-1));

        minTotalCost = -1;
        boolean allowedToEnd = false;
        sectionsUsed = null;
        endsReached = new HashSet<>();

        while (unsettledNodes.size() != 0) {
            Vertex currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);
            if (currentNode != null && currentNode.getTotalCost() > minTotalCost && minTotalCost > -1) {
                Environment.getLogger().debug("MinTotalCost: " + minTotalCost);
                allowedToEnd = true;
            } else {
                if (currentNode != null) {
                    for (Edge edge : currentNode.getEdges()) {
                        if (!edge.isUsed()) {
                            if ((edge.getRequirements() == null || Arrays.stream(edge.getRequirements()).filter(Objects::nonNull).allMatch(requirement -> requirement.checkRequirement() == null))) {
                                Vertex adjacentNode = edge.getDestination();
                                int edgeWeight = edge.getWeight();
                                if (adjacentNode != null) {
                                    calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                                    unsettledNodes.add(adjacentNode);
                                }
                            }
                            edge.setUsed(true);
                        }
                    }
                }
            }
            if (currentNode != null && (allowedToEnd || (currentNode.getFrom() != null && reachableEnds.containsKey(currentNode.getCoordinate().hashCode())))) {
                if (allowedToEnd) {
                    //Try to build from end to edge used to enter.
                    TeleportEdges.removeOldEdges(sections, start);
                    return new ArrayList<>(sectionsUsed);
                } else if (!endsReached.contains(currentNode.getCoordinate().hashCode())) {
                    Map<Integer, Coordinate> finalReachableEnds1 = reachableEnds;
                    endSection.getSpecificMap().values().stream().filter(vertex -> !finalReachableEnds1.containsKey(vertex.getCoordinate().hashCode())).forEach(vertex -> {
                        vertex.setFrom(null);
                        vertex.setTotalCost(-1);
                        vertex.getEdges().forEach(edge -> edge.setUsed(false));
                    });

                    Vertex found = vals.stream().filter(vertex -> currentNode.getCoordinate().equals(vertex.getCoordinate())).findFirst().orElse(null);
                    if (found != null) {
                        if (currentNode.getTotalCost() + found.getTotalCost() < minTotalCost || minTotalCost < 0) {
                            minTotalCost = currentNode.getTotalCost() + found.getTotalCost();
                            List<Vertex> path = new ArrayList<>();
                            AbstractBot bot;
                            Vertex endNode = currentNode;
                            while (endNode != null && !path.contains(endNode) && (bot = Environment.getBot()) != null && !bot.isStopped()) {
                                path.add(0, endNode);
                                endNode = endNode.getFrom();
                            }
                            sectionsUsed = new HashSet<>();
                            for (Vertex vertex : path) {
                                GraphSection sect = Arrays.stream(sections).filter(graphSection -> graphSection.getArea().contains(vertex.getCoordinate())).findFirst().orElse(null);
                                sectionsUsed.add(sect);
                            }
                            sectionsUsed.add(startSection);
                            sectionsUsed.add(endSection);
                            endsReached.add(currentNode.hashCode());
                            if(end.contains(currentNode.getCoordinate())) {
                                allowedToEnd = true;
                            }
                        }
                    }
                }
            }
        }
        TeleportEdges.removeOldEdges(sections, start);
        Environment.getLogger().debug("Path Not Found...");
        return null;
    }

    public List<GraphSection> calculateShortestPathFromSource(GraphSection[] sections, Coordinate start, Area[] ends) {
        Arrays.stream(sections).forEach(graphSection -> {
            graphSection.setCost(0);
            graphSection.getHighLevelMap().values().forEach(vertex -> {
                vertex.setFrom(null);
                vertex.setTotalCost(-1);
                vertex.getEdges().forEach(edge -> edge.setUsed(false));
            });
        });


        GraphSection startSection = Arrays.stream(sections).filter(graphSection -> graphSection.getArea().contains(start)).findFirst().orElse(null);

        Set<GraphSection> endSections = Arrays.stream(sections).filter(graphSection -> Arrays.stream(ends).anyMatch(area -> graphSection.getArea().contains(area))).collect(Collectors.toSet());
        if (startSection == null || endSections.isEmpty()) {
            return null;
        }
        Environment.getLogger().debug("End Sections: " + endSections);
        //Build path in end section to nearest incoming edge...

        startSection.loadGraph(true);
        startSection.addSpecialEdges();
        if(useTeleports) {
            TeleportEdges.addEdgesToMap(sections, start);
        }
        Environment.getLogger().debug("Start Section: " + startSection);

        final LinkedHashMap<Integer, Vertex> reachableStart = getReachableVerticesTemporary(startSection, new ArrayList<>(Arrays.asList(startSection.getSpecificMap().get(start.hashCode()))), true);
        HashSet<Vertex> unsettledNodes = startSection.getHighLevelMap().values().stream().filter(vertex -> reachableStart.values().stream().filter(vertex1 -> startSection.getHighLevelMap().containsKey(vertex1.getCoordinate().hashCode())).map(vertex1 -> vertex1.getCoordinate().hashCode()).collect(Collectors.toList()).contains(vertex.getCoordinate().hashCode())).collect(Collectors.toCollection(HashSet::new));
        List<Coordinate> startCoors = unsettledNodes.stream().map(Vertex::getCoordinate).collect(Collectors.toList());

        unsettledNodes.forEach(vertex -> {
            vertex.setTotalCost(reachableStart.get(vertex.getCoordinate().hashCode()).getTotalCost());
            //vertex.setFrom(null);
        });

        startSection.getSpecificMap().values().stream().filter(vertex -> !startCoors.contains(vertex.getCoordinate())).forEach(vertex -> {
            //vertex.setFrom(null);
            vertex.setTotalCost(-1);
            //vertex.getEdges().forEach(edge -> edge.setUsed(false));
        });

        endSections.forEach(section -> section.getSpecificMap().values().forEach(vertex -> {
            vertex.setFrom(null);
            vertex.setTotalCost(-1);
        }));

        while (unsettledNodes.size() != 0) {
            Vertex currentNode = getLowestDistanceNode(unsettledNodes);
            if (currentNode != null) {
                currentNode.getTotalCost();
            }
            unsettledNodes.remove(currentNode);
            if (currentNode != null) {
                for (Edge edge : currentNode.getEdges()) {
                    if (!edge.isUsed()) {
                        if ((edge.getRequirements() == null || Arrays.stream(edge.getRequirements()).filter(Objects::nonNull).allMatch(requirement -> requirement.checkRequirement() == null))) {
                            Vertex adjacentNode = edge.getDestination();
                            int edgeWeight = edge.getWeight();
                            if (adjacentNode != null && adjacentNode.getFrom() == null) {
                                calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                                unsettledNodes.add(adjacentNode);
                                edge.setUsed(true);

                            }
                        }
                    }
                }

                GraphSection endSection;
                if (Arrays.stream(ends).anyMatch(area -> area.contains(currentNode.getCoordinate()))) {
                    endSection = endSections.stream().filter(section -> section.getArea().contains(currentNode.getCoordinate())).findFirst().orElse(null);
                    if (endSection != null && (endSection.getSpecificMap() == null || endSection.getSpecificMap().isEmpty())) {
                        endSection.loadGraph(true);
                        endSection.addSpecialEdges();
                    }
                    List<Vertex> path = new ArrayList<>();
                    AbstractBot bot;
                    Vertex endNode1 = currentNode;
                    while (endNode1 != null && !path.contains(endNode1) && (bot = Environment.getBot()) != null && !bot.isStopped()) {
                        path.add(0, endNode1);
                        endNode1 = endNode1.getFrom();
                    }
                    Set<GraphSection> sectionsUsed = new HashSet<>();
                    for (Vertex vertex : path) {
                        GraphSection sect = Arrays.stream(sections).filter(graphSection -> graphSection.getArea().contains(vertex.getCoordinate())).findFirst().orElse(null);
                        sectionsUsed.add(sect);
                    }
                    sectionsUsed.add(startSection);
                    sectionsUsed.add(endSection);
                    TeleportEdges.removeOldEdges(sections, start);
                    return new ArrayList<>(sectionsUsed);
                }
            }
        }
        TeleportEdges.removeOldEdges(sections, start);
        Environment.getLogger().debug("Path Not Found...");
        return null;
    }


    private Vertex getLowestDistanceNode(Set<Vertex> unsettledNodes) {
        Vertex lowestDistanceNode = null;
        int lowestDistance = Integer.MAX_VALUE;
        for (Vertex node : unsettledNodes) {
            int nodeDistance = node.getTotalCost();
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                lowestDistanceNode = node;
            }
        }
        return lowestDistanceNode;
    }

    private void calculateMinimumDistance(Vertex evaluationNode,
                                          Integer edgeWeigh, Vertex sourceNode) {
        Integer sourceDistance = sourceNode.getTotalCost();
        if (sourceDistance + edgeWeigh < evaluationNode.getTotalCost() || evaluationNode.getTotalCost() < 1) {
            evaluationNode.setTotalCost(sourceDistance + edgeWeigh);
            evaluationNode.setFrom(sourceNode);
        }
    }

    private void calculateMinimumDistanceTemp(Vertex evaluationNode,
                                          Integer edgeWeigh, Vertex sourceNode) {
        Integer sourceDistance = sourceNode.getTotalCost();
        if (sourceDistance + edgeWeigh < evaluationNode.getTotalCost() || evaluationNode.getTotalCost() < 1) {
            evaluationNode.setTotalCost(sourceDistance + edgeWeigh);
            //evaluationNode.setFrom(sourceNode);
        }
    }

    public List<Edge> generateEdges(Vertex start1, Vertex end1, LinkedHashMap<Integer, Vertex> map) {
        Vertex start = map.get(start1.getCoordinate().hashCode());
        Vertex end = map.get(end1.getCoordinate().hashCode());

        if (start == null || end == null) {
            return new ArrayList<>();
        }

        start.setTotalCost(0);

        Set<Vertex> unsettledNodes = new HashSet<>();

        unsettledNodes.add(start);

        List<Edge> edges = new ArrayList<>();

        while (unsettledNodes.size() != 0) {
            Vertex currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);

            for (Edge edge : currentNode.getEdges()) {
                if (!edge.isUsed() && map.containsKey(edge.getDestination().getCoordinate().hashCode())) {
                    Vertex adjacentNode = edge.getDestination();
                    int edgeWeight = edge.getWeight();
                    if (adjacentNode != null && adjacentNode.getFrom() == null) {
                        calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                        edge.setUsed(true);
                        unsettledNodes.add(adjacentNode);
                    }
                }
            }
            if (currentNode.getCoordinate().equals(end.getCoordinate())) {
                List<Requirement> requirements = new ArrayList<>();
                while (currentNode != null && currentNode.getFrom() != null && !currentNode.equals(start)) {
                    Vertex finalCurrentNode = currentNode;
                    Edge usedEdge = currentNode.getFrom().getEdges().stream().filter(edge -> edge.getDestination().equals(finalCurrentNode)).findFirst().orElse(null);
                    if (usedEdge != null) {
                        if (usedEdge.getRequirements() != null) {
                            if (requirements.stream().noneMatch(requirement -> Arrays.equals(usedEdge.getRequirements(), ((CombinedRequirement) requirement).getRequirements()))) {
                                requirements.add(new CombinedRequirement(true, usedEdge.getRequirements()));
                            }
                        }
                    }
                    currentNode = currentNode.getFrom();
                }
                if (requirements.isEmpty()) {
                    edges.add(new Edge(start1, end1, end.getTotalCost(), null));
                } else {
                    edges.add(new Edge(start1, end1, end.getTotalCost(), requirements.toArray(new Requirement[0])));
                }
            }
        }
        return edges;
    }
}