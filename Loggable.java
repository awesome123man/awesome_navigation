package com.regal.utility.awesome_navigation;

/**
 * Interface for use in Pathing Logic as well as Interaction Logic.
 * Allows the bot to know if something has become stuck,
 * so you can keep track of how many repetitive actions the bot has taken.
 *
 * IMPORTANT: Keep track of the log count in the class that implements BotLoggerListener,
 * we check if the message logged was different from the previous, and if it was,
 * reset the count, if it isn't different, increment it.
 */
public interface Loggable {
    /**
     * Gets the number of repetitive actions the bot has taken.
     * @return The log counter.
     */
    int getLogCount();
}
