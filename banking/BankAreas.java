package com.regal.utility.awesome_navigation.banking;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;

public enum BankAreas {

    LUMBRIDGE("Lumbridge", new Area.Rectangular(new Coordinate(3207, 3220, 2), new Coordinate(3210, 3215, 2))),
    ARDOUGNE_WEST("Ardougne West", new Area.Rectangular(new Coordinate(2612, 3335, 0), new Coordinate(2621, 3332, 0))),
    ARDOUGNE_EAST("Ardougne East", new Area.Rectangular(new Coordinate(2649, 3287, 0), new Coordinate(2655, 3280, 0))),
    FALADOR_WEST("Falador West", new Area.Rectangular(new Coordinate(2943, 3373, 0), new Coordinate(2947, 3368, 0))),
    DRAYNOR("Draynor", new Area.Rectangular(new Coordinate(3092, 3240, 0), new Coordinate(3097, 3246, 0))),
    AL_KHARID("Al Kharid", new Area.Rectangular(new Coordinate(3272, 3173, 0), new Coordinate(3269, 3161, 0))),
    CANIFIS("Canifis", new Area.Rectangular(new Coordinate(3509, 3483, 0), new Coordinate(3512, 3477, 0))),
    VARROCK_EAST("Varrock East", new Area.Rectangular(new Coordinate(3250, 3421, 0), new Coordinate(3257, 3419, 0))),
    VARROCK_WEST("Varrock West", new Area.Rectangular(new Coordinate(3181, 3433, 0), new Coordinate(3185, 3447, 0))),
    SEERS("Seers", new Area.Rectangular(new Coordinate(2721, 3492, 0), new Coordinate(2729, 3490, 0))),
    PORT_PISCARILIUS("Port Piscarilius", new Area.Rectangular(new Coordinate(1811, 3784, 0), new Coordinate(1794, 3790, 0))),
    GNOME_STRONGHOLD_ABOVE_SLAYER_DUNGEON("Gnome Stronghold Slayer", new Area.Rectangular(new Coordinate(2443, 3423, 1), new Coordinate(2448, 3428, 1))),
    ROGUES_DEN("Rogue's Den", new Area.Rectangular(new Coordinate(3049, 4964, 1), new Coordinate(3036, 4979, 1))),
    ZANARIS("Zanaris", new Area.Rectangular(new Coordinate(2379, 4463, 0), new Coordinate(2387, 4453, 0))),
    KOUREND_CASTLE("Kourend Castle", new Area.Rectangular(new Coordinate(1613, 3678, 2), new Coordinate(1610, 3683, 2))),
    MOUNT_QUIDAMORTEM("Mount Quidamortem", new Area.Rectangular(new Coordinate(1251, 3573, 0), new Coordinate(1257, 3568, 0))),
    YANILLE("Yanille", new Area.Rectangular(new Coordinate( 2609, 3094, 0 ), new Coordinate( 2613, 3090, 0 ))),
    PORT_KHAZARD("Port Khazard", new Area.Rectangular(new Coordinate(2658, 3163, 0), new Coordinate(2663, 3158, 0))),
    MOUNT_KARUULM("Mount Karuulm", new Area.Rectangular(new Coordinate(1320, 3827, 0), new Coordinate(1328, 3820, 0))),
    SHILO_VILLAGE("Shilo Village", new Area.Rectangular(new Coordinate(2848, 2958, 0), new Coordinate(2856, 2950, 0))),
    FARMING_GUILD("Farming Guild", new Area.Rectangular(new Coordinate(1251, 3743, 0), new Coordinate(1255, 3738, 0))),
    PISCATORIS("Piscatoris", new Area.Rectangular(new Coordinate(2327, 3693, 0), new Coordinate(2332, 3686, 0))),
    ;

    private final String loc;
    private final Area.Rectangular area;

    BankAreas(String loc, Area.Rectangular area) {
        this.loc = loc;
        this.area = area;
    }

    public String getLocation() {
        return loc;
    }

    public Area.Rectangular getArea() {
        return area;
    }

    @Override
    public String toString() {
        return "Bank Area: " + loc + " | " +  area;
    }

}
