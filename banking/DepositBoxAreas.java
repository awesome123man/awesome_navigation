package com.regal.utility.awesome_navigation.banking;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;

public enum DepositBoxAreas {

    PORT_SARIM("Port Sarim", new Area.Rectangular(new Coordinate(3042, 3237, 0), new Coordinate(3048, 3234, 0)));


    private final String loc;
    private final Area area;

    DepositBoxAreas(String loc, Area area) {
        this.loc = loc;
        this.area = area;
    }

    public String getLocation() {
        return loc;
    }

    public Area getArea() {
        return area;
    }

    @Override
    public String toString() {
        return "Bank Area: " + loc + " | " +  area;
    }

}
