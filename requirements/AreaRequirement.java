package com.regal.utility.awesome_navigation.requirements;

import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Players;

public class AreaRequirement implements Requirement {
    private Area area;

    public AreaRequirement(Area area) {
        this.area = area;
    }

    public AreaRequirement() {

    }

    @Override
    public Requirement[] checkRequirement() {
        Player player = Players.getLocal();
        return player != null && area.contains(player) ? null : new Requirement[]{this};
    }

    @Override
    public String toString() {
        return "AreaRequirement{" +
                "area=" + area +
                '}';
    }

    @Override
    public Requirement readFromString(String s) {
        s = s.replace("AreaRequirement{area=Area(", "");
        int endIndex = s.indexOf(')');
        int middleIndex = s.indexOf('-') - 1;
        String first = s.substring(0, middleIndex).replace(" ", "");
        String last = s.substring(middleIndex + 3, endIndex).replace(" ", "");


        //Rectangular area
        String[] firstInts = first.split(",");
        String[] lastInts = last.split(",");
        area = new Area.Rectangular(new Coordinate(toInt(firstInts[0]), toInt(firstInts[1]), toInt(firstInts[2])), new Coordinate(toInt(lastInts[0]), toInt(lastInts[1]), toInt(lastInts[2])));
        return this;
    }

    private int toInt(String val) {
        try {
            return Integer.parseInt(val);
        } catch (Exception ignored) {
        }
        return 0;
    }

    public Area getArea() {
        return area;
    }
}
