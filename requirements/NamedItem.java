package com.regal.utility.awesome_navigation.requirements;

import java.util.regex.Pattern;

public interface NamedItem {
    String[] getItemNames();
    Pattern[] getPatterns();
    int getItemQuantity();
}
