package com.regal.utility.awesome_navigation.requirements;

import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.util.StopWatch;

import java.util.concurrent.TimeUnit;

public class CombatRequirement implements Requirement {

    private boolean inCombat;
    private int secondsOut;
    private StopWatch outOfCombatTimer;
    private int init = 10;

    public CombatRequirement(boolean inCombat, int secondsOut){
        this.inCombat = inCombat;
        this.secondsOut = secondsOut;
    }

    public CombatRequirement() {

    }

    @Override
    public Requirement[] checkRequirement() {
        Player player;
        if((player = Players.getLocal()) != null) {
            if(outOfCombatTimer == null) {
                outOfCombatTimer = new StopWatch();
            }
            if(Npcs.newQuery().targeting(player).animating().results().first() == null) {
                if(!outOfCombatTimer.isRunning()) {
                    outOfCombatTimer.start();
                }
                return outOfCombatTimer.getRuntime(TimeUnit.SECONDS) + init > secondsOut ? null : new Requirement[] {this};
            } else {
                init = 0;
                outOfCombatTimer.reset();
            }
        }
        return new Requirement[] {this};
    }

    @Override
    public String toString() {
        return "CombatRequirement{" +
                "inCombat=" + inCombat +
                ", secondsOut=" + secondsOut +
                '}';
    }

    @Override
    public Requirement readFromString(String s) {
        s = s.replace("CombatRequirement{inCombat=", "");
        int indexComma = s.indexOf(",");
        this.inCombat = Boolean.parseBoolean(s.substring(0, indexComma));
        s = s.substring(indexComma + 13);
        this.secondsOut = Integer.parseInt(s.substring(0, s.length() - 1));
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CombatRequirement that = (CombatRequirement) o;
        return inCombat == that.inCombat && secondsOut == that.secondsOut;
    }
}
