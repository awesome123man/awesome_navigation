package com.regal.utility.awesome_navigation.requirements;

import com.runemate.game.api.hybrid.local.Varbit;
import com.runemate.game.api.hybrid.local.Varbits;

public class VarbitRequirement implements Requirement {

    private int varbitIndex;
    private int value;

    public VarbitRequirement(int varbitIndex, int value) {
        this.varbitIndex = varbitIndex;
        this.value = value;
    }

    public VarbitRequirement() {

    }

    @Override
    public Requirement[] checkRequirement() {
        Varbit bit;
        return ((bit = Varbits.load(varbitIndex)) != null && bit.getValue() == value) ? null : new Requirement[] {this};
    }

    @Override
    public Requirement readFromString(String s) {
        s = s.replace("VarbitRequirement{varbitIndex=", "");
        int index = s.indexOf(',');
        varbitIndex = Integer.parseInt(s.substring(0, index));
        value = Integer.parseInt(s.substring(index + 8, s.length() - 1));
        return this;
    }

    @Override
    public String toString() {
        return "VarbitRequirement{" +
                "varbitIndex=" + varbitIndex +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VarbitRequirement that = (VarbitRequirement) o;
        return varbitIndex == that.varbitIndex && value == that.value;
    }
}
