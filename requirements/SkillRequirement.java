package com.regal.utility.awesome_navigation.requirements;

import com.runemate.game.api.hybrid.local.Skill;

public class SkillRequirement implements Requirement {

    private Skill skill;
    private int level;

    public SkillRequirement(Skill skill, int level){
        this.skill = skill;
        this.level = level;
    }

    public SkillRequirement() {

    }

    @Override
    public Requirement[] checkRequirement() {
        if(skill.isMembersOnly()) {
            if(new SubscriptionRequirement(true).checkRequirement() != null) {
                return new Requirement[] {this};
            }
        }
        return skill.getCurrentLevel() >= level ? null : new Requirement[] {this};
    }

    @Override
    public Requirement readFromString(String s) {
        s = s.replace("SkillRequirement{skill=", "");
        int index = s.indexOf(',');
        skill = Skill.valueOf(s.substring(0, index));
        level = Integer.parseInt(s.substring(index + 8, s.length() - 1));
        return this;
    }

    @Override
    public String toString() {
        return "SkillRequirement{" +
                "skill=" + skill.name() +
                ", level=" + level +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SkillRequirement that = (SkillRequirement) o;
        return level == that.level && skill == that.skill;
    }

    public Skill getSkill() {
        return skill;
    }

    public int getLevel() {
        return level;
    }
}
