package com.regal.utility.awesome_navigation.requirements;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class CombinedRequirement implements Requirement {

    private Requirement[] requirements;
    private boolean andOperation;

    public CombinedRequirement(boolean andOperation, Requirement... requirements) {
        this.requirements = requirements;
        this.andOperation = andOperation;
    }

    public CombinedRequirement() {

    }

    @Override
    public Requirement[] checkRequirement() {
        if(andOperation) {
            Optional<Requirement> optionalRes;
            if((optionalRes = Arrays.stream(requirements).filter(Objects::nonNull).filter(requirement -> requirement.checkRequirement() != null).findAny()).isPresent()) {
                return new Requirement[] {optionalRes.get()};
            }
            return null;
        }
        int startLen = requirements.length;
        List<Requirement> sorted = Arrays.stream(requirements).filter(Objects::nonNull).filter(requirement -> requirement.checkRequirement() != null).collect(Collectors.toList());
        if(sorted.size() != startLen) {
            return null;
        }
        return requirements;
    }

    @Override
    public Requirement readFromString(String s) {
        try {
            //System.out.println(s);
            s = s.replace("CombinedRequirement{requirements=[", "");
            s = s.substring(0, s.length() - 1);
            int indexToRemove = s.lastIndexOf(']');
            //System.out.println(s);
            String requirementsArrayString = s.substring(0, indexToRemove);
            String[] requirementsToParse = requirementsArrayString.split("}, ");
            for (int i = 0; i < requirementsToParse.length - 1; i++) {
                requirementsToParse[i] = requirementsToParse[i] + "}";
            }
            Requirement[] requirements = new Requirement[requirementsToParse.length];
            for (int i = 0; i < requirementsToParse.length; i++) {
                requirements[i] = RequirementBuilder.buildRequirementFrom(requirementsToParse[i]);
            }
            if(Arrays.stream(requirements).allMatch(Objects::isNull)) {
                return null;
            }
            s = s.substring(indexToRemove + 16);
            boolean and = Boolean.parseBoolean(s);
            //System.out.println("Returned: " + new CombinedRequirement(requirements, and));
            this.requirements = requirements;
            this.andOperation = and;
            return this;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public boolean isAndOperation() {
        return andOperation;
    }

    @Override
    public String toString() {
        return "CombinedRequirement{" +
                "requirements=" + Arrays.toString(requirements) +
                ", andOperation=" + andOperation +
                '}';
    }

    public Requirement[] getRequirements() {
        return requirements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CombinedRequirement that = (CombinedRequirement) o;
        return andOperation == that.andOperation && Arrays.equals(requirements, that.requirements);
    }
}
