package com.regal.utility.awesome_navigation.requirements;

import com.regal.utility.awesome_navigation.RegalItem;

import java.util.Arrays;
import java.util.regex.Pattern;

public class InventoryRequirement implements Requirement, NamedItem {

    private int id = -1;
    private String[] itemNames;
    private int itemQuantity;
    private Pattern[] patterns;

    public InventoryRequirement(String itemName, int itemQuantity) {
        this.itemNames = new String[] {itemName};
        this.itemQuantity = itemQuantity;
    }

    public InventoryRequirement(String itemName) {
        this.itemNames = new String[] {itemName};
        this.itemQuantity = 1;
    }

    public InventoryRequirement(String[] itemName, int itemQuantity) {
        this.itemNames = itemName;
        this.itemQuantity = itemQuantity;
    }

    public InventoryRequirement(Pattern pattern, int itemQuantity) {
        this.patterns = new Pattern[] {pattern};
        this.itemQuantity = itemQuantity;
    }

    public InventoryRequirement(Pattern[] patterns, int itemQuantity) {
        this.patterns = patterns;
        this.itemQuantity = itemQuantity;
    }

    public InventoryRequirement(String itemName, int id, int itemQuantity) {
        this.itemNames = new String[] {itemName};
        this.id = id;
        this.itemQuantity = itemQuantity;
    }

    public InventoryRequirement() {

    }

    @Override
    public Requirement[] checkRequirement() {
        if(itemQuantity == 0) {
            return null;
        }
        if(itemNames != null) {
            if(id != -1) {
                if(itemQuantity < 0) {
                    return RegalItem.getQuantityOfInventory(id, itemNames) > 0 ? null : new Requirement[] {this};
                }
                if(RegalItem.getQuantityOfInventory(id, itemNames) >= itemQuantity) {
                    return null;
                }
            }
            if(itemQuantity < 0) {
                return RegalItem.getQuantityOfInventory(itemNames) > 0 ? null : new Requirement[] {this};
            }
            if(RegalItem.getQuantityOfInventory(itemNames) >= itemQuantity) {
                return null;
            }
        } else {
            if(itemQuantity < 0) {
                return RegalItem.getQuantityOfInventory(patterns) > 0 ? null : new Requirement[] {this};
            }
            if(RegalItem.getQuantityOfInventory(patterns) >= itemQuantity) {
                return null;
            }
        }
        return new Requirement[] {this};
    }

    @Override
    public Requirement readFromString(String s) {
        s = s.replace("InventoryRequirement{itemName=", "");
        if(s.charAt(0) == '[') {
            String arrayToParse = s.substring(1, s.indexOf(']'));
            itemNames = arrayToParse.split(", ");
            s = s.substring(s.indexOf(']') + 6);
        } else {
            s = s.substring(9);
        }
        int index = s.indexOf(",");
        String id = s.substring(0, index);
        this.id = Integer.parseInt(id);
        s = s.substring(index + 11);
        index = s.indexOf(",");
        String quantity = s.substring(0, index);
        this.itemQuantity = Integer.parseInt(quantity);
        s = s.substring(14);
        if(s.charAt(0) == '[') {
            String arrayToParse = s.substring(1, s.indexOf(']'));
            patterns = Arrays.stream(arrayToParse.split(", ")).map(Pattern::compile).toArray(Pattern[]::new);
        }
        return this;
    }

    public String[] getItemNames() {
        return itemNames;
    }

    public Pattern[] getPatterns() {
        return patterns;
    }

    public int getItemQuantity() {
        return itemQuantity;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "InventoryRequirement{" +
                "itemName=" + Arrays.toString(itemNames) +
                ", id=" + id +
                ", quantity=" + itemQuantity +
                ", patterns=" + Arrays.toString(patterns) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InventoryRequirement that = (InventoryRequirement) o;
        return itemQuantity == that.itemQuantity && Arrays.equals(itemNames, that.itemNames) && Arrays.equals(patterns, that.patterns) && id == that.id;
    }
}
