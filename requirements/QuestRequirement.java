package com.regal.utility.awesome_navigation.requirements;

import com.runemate.game.api.hybrid.local.Quest;

import java.util.Objects;

public class QuestRequirement implements Requirement {

    private Quest.Status status;
    private String questName;
    private boolean isLockedState;
    private boolean alreadyTrue;

    public QuestRequirement(String questName, Quest.Status status) {
        this(questName, status, false);
    }

    public QuestRequirement(String questName, Quest.Status status, boolean isLockedState) {
        this.questName = questName;
        this.status = status;
        this.isLockedState = isLockedState;
    }

    public QuestRequirement() {

    }

    @Override
    public Requirement[] checkRequirement() {
        if(alreadyTrue) {
            return null;
        }
        Quests quest = Quests.getQuest(questName);
        if (isLockedState) {
            alreadyTrue = quest != null && quest.getStatus().ordinal() == status.ordinal();
        } else {
            alreadyTrue = quest != null && quest.getStatus().ordinal() >= status.ordinal();
        }
        return alreadyTrue ? null : new Requirement[] {this};
    }

    @Override
    public Requirement readFromString(String s) {
        s = s.replace("QuestRequirement{status=", "");
        int index = s.indexOf(',');
        status = Quest.Status.valueOf(s.substring(0, index));
        s = s.substring(index + 13);
        index = s.indexOf(',');
        questName = s.substring(0, index - 1);
        s = s.substring(index + 16, s.indexOf('}'));
        isLockedState = Boolean.parseBoolean(s);
        return this;
    }

    @Override
    public String toString() {
        return "QuestRequirement{" +
                "status=" + status +
                ", questName='" + questName + '\'' +
                ", isLockedState=" + isLockedState +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestRequirement that = (QuestRequirement) o;
        return alreadyTrue == that.alreadyTrue && status == that.status && Objects.equals(questName, that.questName);
    }

    public String getQuestName() {
        return questName;
    }
}
