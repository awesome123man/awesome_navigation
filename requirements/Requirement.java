package com.regal.utility.awesome_navigation.requirements;

public interface Requirement {
    Requirement[] checkRequirement();
    Requirement readFromString(String s);
}
