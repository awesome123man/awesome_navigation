package com.regal.utility.awesome_navigation.requirements;

import com.runemate.game.api.hybrid.local.WorldOverview;
import com.runemate.game.api.hybrid.local.Worlds;

public class SubscriptionRequirement implements Requirement {

    private boolean subscribed;
    private boolean alreadyTrue;

    public SubscriptionRequirement(boolean subscribed){
        this.subscribed = subscribed;
    }

    public SubscriptionRequirement() {

    }

    @Override
    public Requirement[] checkRequirement() {
        if(alreadyTrue) {
            return null;
        }
        WorldOverview worldOverview;
        return (alreadyTrue = ((worldOverview = Worlds.getCurrentOverview())) != null && worldOverview.isMembersOnly() == subscribed) ? null : new Requirement[] {this};
    }

    @Override
    public Requirement readFromString(String s) {
        s = s.replace("SubscriptionRequirement{subscribed=", "");
        subscribed = Boolean.parseBoolean(s.substring(0, s.length() - 1));
        return this;
    }

    @Override
    public String toString() {
        return "SubscriptionRequirement{" +
                "subscribed=" + subscribed +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubscriptionRequirement that = (SubscriptionRequirement) o;
        return subscribed == that.subscribed && alreadyTrue == that.alreadyTrue;
    }
}
