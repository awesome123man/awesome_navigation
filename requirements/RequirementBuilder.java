package com.regal.utility.awesome_navigation.requirements;

import com.runemate.game.api.hybrid.Environment;

public class RequirementBuilder {

    public static Requirement buildRequirementFrom(String s) {
        Requirement requirement;
        if(s.contains("CombinedRequirement")) {
            requirement = new CombinedRequirement();
        } else if (s.contains("CombatLevelRequirement")) {
            requirement = new CombatLevelRequirement();
        }  else if(s.contains("InventoryRequirement")) {
            requirement = new InventoryRequirement();
        } else if(s.contains("EquipmentRequirement")) {
            requirement = new EquipmentRequirement();
        } else if(s.contains("QuestRequirement")) {
            requirement = new QuestRequirement();
        } else if(s.contains("SkillRequirement")) {
            requirement = new SkillRequirement();
        } else if(s.contains("SubscriptionRequirement")) {
            requirement = new SubscriptionRequirement();
        } else if(s.contains("VarbitRequirement")) {
            requirement = new VarbitRequirement();
        } else if(s.contains("VarpRequirement")) {
            requirement = new VarpRequirement();
        } else if(s.contains("AreaRequirement")) {
            requirement = new AreaRequirement();
        }
        else {
            if (!s.isEmpty()) {
                Environment.getLogger().debug("FAILED TO BUILD REQUIREMENT: " + s);
            }
            return null;
        }
        if(requirement.readFromString(s) == null) {
            return null;
        }
        return requirement;
    }
}
