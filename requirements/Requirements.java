package com.regal.utility.awesome_navigation.requirements;

import com.runemate.game.api.hybrid.util.Regex;

import java.util.Arrays;

public enum Requirements {
    DRAGON_BREATH(new CombinedRequirement(false, new EquipmentRequirement(new String[] {"Anti-dragon shield", "Dragonfire shield"}, 1), new InventoryRequirement(new String[] {"Anti-dragon shield", "Dragonfire shield"}, 1))),
    BANSHEE(new EquipmentRequirement(new String[]{"Slayer helmet", "Slayer helmet (i)", "Ear muffs"}, 1)),
    NOSE_PEG(new EquipmentRequirement(new String[]{"Slayer helmet", "Slayer helmet (i)", "Nose peg"}, 1)),
    DRAMEN_STAFF(new CombinedRequirement(false, new EquipmentRequirement(new String[]{"Dramen staff", "Lunar staff"}, 1), new InventoryRequirement("Dramen staff", 1))),
    HEAT_PROTECTING_FOOTWEAR(new EquipmentRequirement(new String[]{"Boots of stone", "Granite boots", "Boots of brimstone"}, 1)),
    LIGHT_SOURCES(
            new CombinedRequirement(false, new EquipmentRequirement(Regex.getPatternForContainsString("Kandarin headgear"), 1), new InventoryRequirement(Regex.getPatternForContainsString("Kandarin headgear"), 1))
    ),
    WITCHWOOD_ICON(new EquipmentRequirement(new String[]{"Slayer helmet", "Slayer helmet (i)", "Witchwood icon"}, 1))

    ;

    private final Requirement requirements;

    Requirements(Requirement requirements) {
        this.requirements = requirements;
    }

    public Requirement getRequirements() {
        return requirements;
    }
}
