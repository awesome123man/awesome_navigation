package com.regal.utility.awesome_navigation.requirements;

import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;

import java.util.Arrays;
import java.util.regex.Pattern;

public class EquipmentRequirement implements Requirement, NamedItem {

    private String[] itemName;
    private int quantity;
    private Pattern[] patterns;

    public EquipmentRequirement(String[] itemName, int quantity) {
        this.itemName = itemName;
        this.quantity = quantity;
    }

    public EquipmentRequirement(String itemName, int quantity) {
        this.itemName = new String[] {itemName};
        this.quantity = quantity;
    }

    public EquipmentRequirement(Pattern[] itemName, int quantity) {
        this.patterns = itemName;
        this.quantity = quantity;
    }

    public EquipmentRequirement(Pattern itemName, int quantity) {
        this.patterns = new Pattern[] {itemName};
        this.quantity = quantity;
    }

    public EquipmentRequirement() {

    }

    @Override
    public Requirement[] checkRequirement() {
        if(quantity == 0) {
            return null;
        }
        SpriteItem item;
        if(itemName != null) {
            if(quantity < 0) {
                if (Equipment.newQuery().names(itemName).results().first() != null) {
                    return null;
                }
            }
            else if((item = Equipment.newQuery().names(itemName).results().first()) != null && item.getQuantity() >= quantity) {
                return null;
            }
        }
        else if(quantity < 0) {
            if(Equipment.newQuery().names(patterns).results().first() != null) {
                return null;
            }
        }
        else if((item = Equipment.newQuery().names(patterns).results().first()) != null && item.getQuantity() >= quantity) {
            return null;
        }
        return new Requirement[] {this};
    }

    @Override
    public Requirement readFromString(String s) {
        s = s.replace("EquipmentRequirement{itemName=", "");
        if(s.charAt(0) == '[') {
            String arrayToParse = s.substring(1, s.indexOf(']'));
            itemName = arrayToParse.split(", ");
            s = s.substring(s.indexOf(']') + 12);
        } else {
            s = s.substring(15);
        }
        String quantity = s.substring(0, 1);
        this.quantity = Integer.parseInt(quantity);
        s = s.substring(14);
        if(s.charAt(0) == '[') {
            String arrayToParse = s.substring(1, s.indexOf(']'));
            patterns = Arrays.stream(arrayToParse.split(", ")).map(Pattern::compile).toArray(Pattern[]::new);
        }
        return this;
    }

    @Override
    public String toString() {
        return "EquipmentRequirement{" +
                "itemName=" + Arrays.toString(itemName) +
                ", quantity=" + quantity +
                ", patterns=" + Arrays.toString(patterns) +
                '}';
    }

    public String[] getItemNames() {
        return itemName;
    }

    public Pattern[] getPatterns() {
        return patterns;
    }

    public int getItemQuantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EquipmentRequirement that = (EquipmentRequirement) o;
        return quantity == that.quantity && Arrays.equals(itemName, that.itemName) && Arrays.equals(patterns, that.patterns);
    }
}
