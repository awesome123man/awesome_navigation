package com.regal.utility.awesome_navigation.requirements;

import com.runemate.game.api.hybrid.util.Regex;

import java.util.regex.Pattern;

public enum RequirementItem {

    PRAYER_RESTORING(Regex.getPatternForContainsString("Prayer potion"), Regex.getPatternForContainsString("Super restore")),
    DRAGONBREATH_PROTECTING("Anti-dragon shield", "Dragonfire shield"),
    POISON_CURING(Regex.getPatternForContainsString("ntipoison")),
    REFLECTING("V's shield", "Mirror shield"),
    ZANARIS_TELEPORTING("Dramen staff", "Lunar staff");


    private Pattern[] patterns;
    private String[] strings;

    RequirementItem(Pattern... patterns) {
        this.patterns = patterns;
    }

    RequirementItem(String... strings) {
        this.strings = strings;
    }

    public Pattern[] getPatterns() {
        return patterns;
    }

    public String[] getStrings() {
        return strings;
    }
}
