package com.regal.utility.awesome_navigation.requirements;

import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.region.Players;

public class CombatLevelRequirement implements Requirement {

    private int combatLevelReq;
    private boolean alreadyTrue;

    public CombatLevelRequirement(int combatLevelReq){
        this.combatLevelReq = combatLevelReq;
    }

    public CombatLevelRequirement() {

    }

    @Override
    public Requirement[] checkRequirement() {
        if(alreadyTrue) {
            return null;
        }
        Player player;
        if((player = Players.getLocal()) != null) {
            return (alreadyTrue = player.getCombatLevel() >= combatLevelReq) ? null : new Requirement[] {this};
        }
        return null;
    }

    @Override
    public String toString() {
        return "CombatLevelRequirement{" +
                "combatLevelReq=" + combatLevelReq +
                '}';
    }

    @Override
    public Requirement readFromString(String s) {
        s = s.replace("CombatLevelRequirement{combatLevelReq=", "");
        s = s.substring(0, s.length() - 1);
        return new CombatLevelRequirement(Integer.parseInt(s));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CombatLevelRequirement that = (CombatLevelRequirement) o;
        return combatLevelReq == that.combatLevelReq && alreadyTrue == that.alreadyTrue;
    }

    public int getCombatLevelReq() {
        return combatLevelReq;
    }
}
