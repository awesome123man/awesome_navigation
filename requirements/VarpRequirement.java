package com.regal.utility.awesome_navigation.requirements;

import com.runemate.game.api.hybrid.local.Varps;

public class VarpRequirement implements Requirement {


    private boolean negateCompare;
    private int index;
    private int value;
    private int bitIndex = -1;
    private boolean lockedState;

    public VarpRequirement(int index, int value) {
        this(index, value, -1, true);
    }

    public VarpRequirement(int index, int value, int bitIndex) {
        this(index, value, bitIndex, true);
    }

    public VarpRequirement(int index, int value, boolean lockedState) {
        this(index, value, -1, lockedState);
    }

    public VarpRequirement(int index, int value, boolean lockedState, boolean negateCompare) {
        this(index, value, -1, lockedState);
        this.negateCompare = negateCompare;
    }


    public VarpRequirement(int index, int value, int bitIndex, boolean lockedState) {
        this.index = index;
        this.value = value;
        this.bitIndex = bitIndex;
        this.lockedState = lockedState;
    }

    public VarpRequirement() {

    }

    @Override
    public Requirement[] checkRequirement() {
        if (!lockedState) {
            if(negateCompare) {
                return ((bitIndex > -1 && Varps.getAt(index).getValueOfBit(bitIndex) <= value) || (bitIndex < 0 && Varps.getAt(index).getValue() <= value)) ? null : new Requirement[] {this};
            }
            return ((bitIndex > -1 && Varps.getAt(index).getValueOfBit(bitIndex) >= value) || (bitIndex < 0 && Varps.getAt(index).getValue() >= value)) ? null : new Requirement[]{this};
        }
        return ((bitIndex > -1 && Varps.getAt(index).getValueOfBit(bitIndex) == value) || (bitIndex < 0 && Varps.getAt(index).getValue() == value)) ? null : new Requirement[] {this};
    }

    @Override
    public Requirement readFromString(String s) {
        s = s.replace("VarpRequirement{index=", "");
        int index = s.indexOf(',');
        this.index = Integer.parseInt(s.substring(0, index));
        value = Integer.parseInt(s.substring(index + 8, s.lastIndexOf(',')));
        index = s.lastIndexOf(',');
        bitIndex = Integer.parseInt(s.substring(index + 11, s.length() - 1));
        return this;
    }

    @Override
    public String toString() {
        return "VarpRequirement{" +
                "index=" + index +
                ", value=" + value +
                ", bitIndex=" + bitIndex +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VarpRequirement that = (VarpRequirement) o;
        return index == that.index && value == that.value && bitIndex == that.bitIndex;
    }

    public enum KnownVarps {
        //-40 = 12 mins left. -> 0
        ANTIDOTE_PLUS_PLUS(102),;

        private final int index;

        KnownVarps(int index) {
            this.index = index;
        }

        public int getIndex() {
            return index;
        }
    }

}
