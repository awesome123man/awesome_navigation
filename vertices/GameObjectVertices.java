package com.regal.utility.awesome_navigation.vertices;

import com.regal.utility.awesome_navigation.WeightedGraph;
import com.regal.utility.awesome_navigation.edges.Edge;
import com.regal.utility.awesome_navigation.edges.GameObjectEdge;
import com.regal.utility.awesome_navigation.handles.*;
import com.regal.utility.awesome_navigation.requirements.*;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.local.Quest;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Region;
import com.runemate.game.api.hybrid.util.Regex;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

public enum GameObjectVertices {
    //Shortcuts and Stairs
    LUMBRIDGE_STAIRCASE_TOP(new Coordinate(3205, 3208, 2), new Coordinate(3205, 3209, 2), new Coordinate(3206, 3208, 1), new String[]{"Staircase"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    LUMBRIDGE_STAIR_SECOND_FLOOR_UP(new Coordinate(3205, 3208, 1), new Coordinate(3206, 3208, 1), new Coordinate(3205, 3209, 2), new String[]{"Staircase"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    LUMBRIDGE_STAIR_SECOND_FLOOR_DOWN(new Coordinate(3204, 3207, 1), new Coordinate(3205, 3209, 1), new Coordinate(3206, 3208, 0), new String[]{"Staircase"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    LUMBRIDGE_STAIR_BOTTOM(new Coordinate(3205, 3208, 0), new Coordinate(3206, 3208, 0), new Coordinate(3205, 3209, 1), new String[]{"Staircase"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    CRUMBLING_WALL_FALADOR(new Coordinate(2935, 3355, 0), new Coordinate(2934, 3355, 0), new Coordinate(2936, 3355, 0), new String[]{"Crumbling wall"}, new String[]{"Climb-over"}, 2, Region.CollisionFlags.BLOCKED_TILE, true, new SkillRequirement[]{new SkillRequirement(Skill.AGILITY, 5)}),
    UNDERWALL_TUNNEL_INNER_FALADOR(new Coordinate(2948, 3312, 0), new Coordinate(2948, 3313, 0), new Coordinate(2948, 3309, 0), new String[]{"Underwall tunnel"}, new String[]{"Climb-into"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 26)),
    UNDERWALL_TUNNER_OUTER_FALADOR(new Coordinate(2948, 3310, 0), new Coordinate(2948, 3309, 0), new Coordinate(2948, 3313, 0), new String[]{"Underwall tunnel"}, new String[]{"Climb-into"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 26)),
    TAVERLY_DUNGEON_ENTRANCE_LADDER(CrossableObject.TAVERLY_DUNGEON_ENTRANCE, new Coordinate(2884, 3398, 0), new Coordinate(2884, 9798, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    TAVERLY_DUNGEON_EXIT_LADDER(CrossableObject.TAVERLY_DUNEGON_EXIT, new Coordinate(2884, 9798, 0), new Coordinate(2884, 3398, 0), 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, false),
    TAVERLY_DUNGEON_DUSTY_KEY_GATE(OpenableObject.TAVERLY_DUNGEON_DUSTY_KEY_GATE, new Coordinate(2924, 9803, 0), new Coordinate(2923, 9803, 0), 2, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true, new InventoryRequirement("Dusty key")),
    GE_UNDERWALL_TUNNEL_1(new Coordinate(3141, 3513, 0), new Coordinate(3142, 3513, 0), new Coordinate(3137, 3516, 0), new String[]{"Underwall tunnel"}, new String[]{"Climb-into"}, 1, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 21)),
    GE_UNDERWALL_TUNNEL_2(new Coordinate(3138, 3516, 0), new Coordinate(3137, 3516, 0), new Coordinate(3142, 3513, 0), new String[]{"Underwall tunnel"}, new String[]{"Climb-into"}, 1, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 21)),
    CANIFIS_ENTRANCE_TRAPDOOR(new Coordinate(3422, 3485, 0), new Coordinate(3423, 3485, 0), new Coordinate(3440, 9887, 0), new String[]{"Trapdoor"}, new String[]{"Open", "Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    HOLY_BARRIER(new Coordinate(3440, 9886, 0), new Coordinate(3440, 9887, 0), new Coordinate(3423, 3485, 0), new String[]{"Holy barrier"}, new String[]{"Pass-through"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new QuestRequirement("Priest in Peril", Quest.Status.COMPLETE)),
    PRIEST_IN_PERIL_DUNGEON_LADDER(new Coordinate(3405, 9907, 0), new Coordinate(3405, 9906, 0), new Coordinate(3405, 3506, 0), new String[]{"Ladder"}, new String[]{"Climb-up"}, 1, Region.CollisionFlags.BLOCKED_TILE, false),
    PRIEST_IN_PERIL_TOP_DUNGEON_TRAPDOOR(new Coordinate(3405, 3507, 0), new Coordinate(3405, 3506, 0), new Coordinate(3405, 9906, 0), new String[]{"Trapdoor"}, new String[]{"Open", "Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new QuestRequirement("Priest in Peril", Quest.Status.IN_PROGRESS)),
    FALADOR_LADDER_TO_MINING_GUILD(new Coordinate(3020, 9739, 0), new Coordinate(3021, 9739, 0), new Coordinate(3021, 3339, 0), new String[]{"Ladder"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.MINING, 60)),
    FALADOR_LADDER_TO_MINING_GUILD_2(new Coordinate(3020, 3339, 0), new Coordinate(3021, 3339, 0), new Coordinate(3021, 9739, 0), new String[]{"Ladder"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.MINING, 60)),
    ORNATE_RAILING_TOP(new Coordinate(3424, 3476, 0), new Coordinate(3424, 3476, 0), new Coordinate(3423, 3476, 0), new String[]{"Ornate railing"}, new String[]{"Squeeze-through"}, 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true, new SkillRequirement(Skill.AGILITY, 65)),
    CANIFIS_ROCKS_TOP(new Coordinate(3425, 3476, 0), new Coordinate(3424, 3476, 0), new Coordinate(3427, 3477, 0), new String[]{"Rocks"}, new String[]{"Climb"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement[]{new SkillRequirement(Skill.AGILITY, 65)}),
    CANIFIS_ROCKS_BOTTOM(new Coordinate(3426, 3477, 0), new Coordinate(3427, 3477, 0), new Coordinate(3424, 3476, 0), new String[]{"Rocks"}, new String[]{"Climb"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement[]{new SkillRequirement(Skill.AGILITY, 65)}),
    ORNATE_RAILILNG_BOTTOM(new Coordinate(3425, 3484, 0), new Coordinate(3425, 3484, 0), new Coordinate(3425, 3483, 0), new String[]{"Ornate railing"}, new String[]{"Squeeze-through"}, 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true, new SkillRequirement[]{new SkillRequirement(Skill.AGILITY, 65)}),
    GANGPLANK_PORT_SARIM(new Coordinate(3031, 3217, 1), new Coordinate(3033, 3217, 1), new Coordinate(3029, 3217, 0), new String[]{"Gangplank"}, new String[]{"Cross"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    GANGPLANK_KARAMJA(new Coordinate(2956, 3144, 1), new Coordinate(2956, 3142, 1), new Coordinate(2956, 3147, 0), new String[]{"Gangplank"}, new String[]{"Cross"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    TRAPDOOR_WEREWOLF_AGILITY(new Coordinate(3543, 3462, 0), new Coordinate(3543, 3463, 0), new Coordinate(3549, 9865, 0), new String[]{"Trapdoor"}, new String[]{"Open", "Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new EquipmentRequirement(new String[]{"Ring of charos"}, 1), new SkillRequirement(Skill.AGILITY, 60)),
    LADDER_WEREWOLF_AGILITY(new Coordinate(3549, 9864, 0), new Coordinate(3549, 9865, 0), new Coordinate(3543, 3463, 0), new String[]{"Ladder"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    VARROCK_BANK_STAIRCASE_EAST(new Coordinate(3255, 3421, 0), new Coordinate(3256, 3420, 0), new Coordinate(3257, 3421, 1), new String[]{"Staircase"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    VARROCK_BANK_STAIRCASE_EAST_DOWN(new Coordinate(3255, 3421, 1), new Coordinate(3257, 3421, 1), new Coordinate(3256, 3420, 0), new String[]{"Staircase"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    GANGPLANK_PORT_SARIM_VEOS(new Coordinate(3055, 3243, 1), new Coordinate(3055, 3242, 1), new Coordinate(3055, 3245, 0), new String[]{"Gangplank"}, new String[]{"Cross"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SubscriptionRequirement(true)),
    GANGPLANK_PORT_PISCARILIUS_VEOS(new Coordinate(1824, 3693, 1), new Coordinate(1824, 3694, 1), new Coordinate(1824, 3691, 0), new String[]{"Gangplank"}, new String[]{"Cross"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SubscriptionRequirement(true)),
    DWARVEN_MINE_FALADOR_SC(new Coordinate(3034, 9806, 0), new Coordinate(3035, 9806, 0), new Coordinate(3028, 9806, 0), new String[]{"Crevice"}, new String[]{"Squeeze-through"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 42)),
    DWARVEN_MINE_FALADOR_SC_1(new Coordinate(3029, 9806, 0), new Coordinate(3028, 9806, 0), new Coordinate(3035, 9806, 0), new String[]{"Crevice"}, new String[]{"Squeeze-through"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 42)),
    FALADOR_DWARVEN_STAIRS_UP(new Coordinate(3059, 9776, 0), new Coordinate(3058, 9777, 0), new Coordinate(3061, 3377, 0), new String[]{"Staircase"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    FALADOR_DWARVEN_STAIRS_DOWN(new Coordinate(3059, 3376, 0), new Coordinate(3061, 3377, 0), new Coordinate(3058, 9777, 0), new String[]{"Staircase"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    FALADOR_DWARVEN_MINE_DOOR(new Coordinate(3061, 3374, 0), new Coordinate(3061, 3375, 0), new Coordinate(3061, 3373, 0), new String[]{"Door"}, new String[]{"Open"}, 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    TAVERLY_DUNGEON_PIPE_BLUE_DRAGONS(CrossableObject.TAVERLY_TUBE_BLUE_DRAGONS_ENTRANCE, new Coordinate(2886, 9799, 0), new Coordinate(2892, 9799, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 70)),
    TAVERLY_DUNGEON_PIPE_BLUE_DRAGONS_1(CrossableObject.TAVERLY_TUBE_BLUE_DRAGONS_EXIT, new Coordinate(2892, 9799, 0), new Coordinate(2886, 9799, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 70)),
    FREMENNICK_SLAYER_DUNGEON_EXIT(new Coordinate(2809, 10001, 0), new Coordinate(2808, 10001, 0), new Coordinate(2796, 3615, 0), new String[]{"Tunnel"}, new String[]{"Enter"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    FREMENNICK_SLAYER_DUNGEON_ENTRANCE(new Coordinate(2797, 3614, 0), new Coordinate(2796, 3615, 0), new Coordinate(2808, 10001, 0), new String[]{"Cave Entrance"}, new String[]{"Enter"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    ASGARNIAN_DUNGEON_ENTRANCE(new Coordinate(3008, 3150, 0), new Coordinate(3009, 3150, 0), new Coordinate(3009, 9550, 0), new String[]{"Trapdoor"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    ASGARNIAN_DUNGEON_EXIT(new Coordinate(3008, 9550, 0), new Coordinate(3009, 9550, 0), new Coordinate(3009, 3150, 0), new String[]{"Ladder"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, false),
    EDGEVILLE_DUNGEON_ENTRANCE(new Coordinate(3097, 3468, 0), new Coordinate(3096, 3468, 0), new Coordinate(3096, 9867, 0), new String[]{"Trapdoor"}, new String[]{"Open", "Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    EDGEVILLE_DUNGEON_EXIT(new Coordinate(3097, 9867, 0), new Coordinate(3096, 9867, 0), new Coordinate(3096, 3468, 0), new String[]{"Ladder"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, false),
    GNOME_STRONGHOLD_SLAYER_DUNGEON_ENTRANCE(new Coordinate(2428, 3423, 0), new Coordinate(2430, 3424, 0), new Coordinate(2429, 9824, 0), new String[]{"Cave"}, new String[]{"Enter"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    GNOME_STRONGHOLD_SLAYER_DUNGEON_EXIT(new Coordinate(2430, 9824, 0), new Coordinate(2429, 9824, 0), new Coordinate(2430, 3424, 0), new String[]{"Tunnel"}, new String[]{"Use"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    GNOME_STRONHOLD_STAIRS_SLAYER_UP(new Coordinate(2444, 3414, 0), new Coordinate(2446, 3415, 0), new Coordinate(2445, 3416, 1), new String[]{"Staircase"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    GNOME_STRONGHOLD_STAIRS_SLAYER_DOWN(new Coordinate(2445, 3415, 1), new Coordinate(2445, 3416, 1), new Coordinate(2446, 3415, 0), new String[]{"Staircase"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    ZANARIS_TO_LUMBRIDGE_RING(OpenableObject.ZANARIS_TO_LUMBRIDGE_RING, new Coordinate(2452, 4472, 0), new Coordinate(3200, 3169, 0), 2, 0, false, new QuestRequirement("Lost City", Quest.Status.COMPLETE)),
    WOODEN_LOG_SHORTCUT_KARAMJA(new Coordinate(2907, 3049, 0), new Coordinate(2906, 3049, 0), new Coordinate(2910, 3049, 0), new String[]{"A wooden log"}, new String[]{"Cross"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    WOODEN_LOG_SHORTCUT_KARAMJA_1(new Coordinate(2909, 3049, 0), new Coordinate(2910, 3049, 0), new Coordinate(2906, 3049, 0), new String[]{"A wooden log"}, new String[]{"Cross"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    GRAND_TREE_BOTTOM_LADDER_UP(CrossableObject.GRAND_TREE_BOTTOM_LADDER_UP, new Coordinate(2466, 3494, 0), new Coordinate(2466, 3494, 1), 2, 0, false),
    GRAND_TREE_1ST_LADDER_UP(CrossableObject.GRAND_TREE_1ST_LADDER_UP, new Coordinate(2466, 3494, 1), new Coordinate(2466, 3494, 2), 2, 0, false),
    GRAND_TREE_1ST_LADDER_DOWN(CrossableObject.GRAND_TREE_1ST_LADDER_DOWN, new Coordinate(2466, 3494, 1), new Coordinate(2466, 3494, 0), 2, 0, false),
    GRAND_TREE_2ND_LADDER_UP(CrossableObject.GRAND_TREE_2ND_LADDER_UP, new Coordinate(2466, 3494, 2), new Coordinate(2466, 3494, 3), 2, 0, false),
    GRAND_TREE_2ND_LADDER_DOWN(CrossableObject.GRAND_TREE_2ND_LADDER_DOWN, new Coordinate(2466, 3494, 2), new Coordinate(2466, 3494, 1), 2, 0, false),
    GRAND_TREE_3RD_LADDER_DOWN(CrossableObject.GRAND_TREE_3RD_LADDER_DOWN, new Coordinate(2466, 3494, 3), new Coordinate(2466, 3494, 2), 2, 0, false),
    ROGUES_DEN_EXIT_PASSAGE(new Coordinate(3061, 4986, 1), new Coordinate(3061, 4985, 1), new Coordinate(2906, 3537, 0), new String[]{"Passageway"}, new String[]{"Enter"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    ROGUES_DEN_ENTRANCE_TRAPDOOR(new Coordinate(2905, 3537, 0), new Coordinate(2906, 3537, 0), new Coordinate(3061, 4985, 1), new String[]{"Trapdoor"}, new String[]{"Enter"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    ROPE_TO_BRIMHAVEN_BRONZE_DRAGONS(new Coordinate(2759, 3062, 0), new Coordinate(2761, 3063, 0), new Coordinate(2734, 9478, 0), new String[]{"Rope"}, new String[]{"Climb"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new VarpRequirement(393, 4), Requirements.DRAGON_BREATH.getRequirements()),
    CREVICE_FROM_BRIMHAVEN_BRONZE_DRAGONS(new Coordinate(2734, 9477, 0), new Coordinate(2734, 9478, 0), new Coordinate(2761, 3063, 0), new String[]{"Crevice"}, new String[]{"Use"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new VarpRequirement(393, 4)),
    MOUNT_KARUULM_LOWER_CLIFFSIDE_CLIMB_UP(new Coordinate(1324, 3778, 0), new Coordinate(1324, 3777, 0), new Coordinate(1324, 3785, 0), new String[]{"Rocks"}, new String[]{"Climb"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 29)),
    MOUNT_KARUULM_LOWER_CLIFFSIDE_CLIMB_DOWN(new Coordinate(1324, 3784, 0), new Coordinate(1324, 3785, 0), new Coordinate(1324, 3777, 0), new String[]{"Rocks"}, new String[]{"Climb"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 29)),
    MOUNT_KARUULM_UPPER_CLIFFSIDE_CLIMB_UP(new Coordinate(1324, 3788, 0), new Coordinate(1324, 3787, 0), new Coordinate(1324, 3795, 0), new String[]{"Rocks"}, new String[]{"Climb"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 62)),
    MOUNT_KARUULM_UPPER_CLIFFSIDE_CLIMB_DOWN(new Coordinate(1324, 3794, 0), new Coordinate(1324, 3795, 0), new Coordinate(1324, 3787, 0), new String[]{"Rocks"}, new String[]{"Climb"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 62)),
    CATACOMBS_OF_COUREND_BASE_ENTRANCE(new Coordinate(1638, 3673, 0), new Coordinate(1639, 3673, 0), new Coordinate(1666, 10050, 0), new String[]{"Statue"}, new String[]{"Investigate"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    CATACOMBS_OF_COUREND_BASE_EXIT(new Coordinate(1666, 10051, 0), new Coordinate(1666, 10050, 0), new Coordinate(1639, 3673, 0), new String[]{"Vine ladder"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    KOUREND_CASTLE_NORTH_STAIRS_GROUND_UP(new Coordinate(1615, 3680, 0), new Coordinate(1618, 3680, 0), new Coordinate(1614, 3680, 1), new String[]{"Staircase"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    KOUREND_CASTLE_NORTH_STAIRS_FIRST_DOWN(new Coordinate(1615, 3680, 1), new Coordinate(1614, 3680, 1), new Coordinate(1618, 3680, 0), new String[]{"Staircase"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    KOUREND_CASTLE_NORTH_DOOR_FIRST_TO_SPIRAL_STAIRCASE(new Coordinate(1616, 3684, 1), new Coordinate(1616, 3685, 1), new Coordinate(1616, 3683, 1), new String[]{"Door"}, new String[]{"Open"}, 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    KOUREND_CASTLE_NORTH_FIRST_SPIRAL_STAIRCASE_UP(new Coordinate(1616, 3687, 1), new Coordinate(1615, 3687, 1), new Coordinate(1616, 3686, 2), new String[]{"Staircase"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    KOUREND_CASTLE_NORTH_SECOND_SPIRAL_STAIRCASE_DOWN(new Coordinate(1616, 3687, 2), new Coordinate(1616, 3686, 2), new Coordinate(1615, 3687, 1), new String[]{"Staircase"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    KELDAGRIM_ENTRANCE_OUTSIDE_IN(new Coordinate(2731, 3713, 0), new Coordinate(2730, 3713, 0), new Coordinate(2773, 10162, 0), new String[]{"Tunnel"}, new String[]{"Enter"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    KELDAGRIM_ENTRANCE_OUTSIDE_OUT(new Coordinate(2772, 10161, 0), new Coordinate(2773, 10162, 0), new Coordinate(2730, 3713, 0), new String[]{"Tunnel"}, new String[]{"Enter"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    KALPHITE_TASK_CAVE_IN(new Coordinate(3320, 3122, 0), new Coordinate(3321, 3122, 0), new Coordinate(3305, 9497, 0), new String[]{"Cave"}, new String[]{"Enter"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    KALPHITE_TASK_CAVE_OUT(new Coordinate(3306, 9497, 0), new Coordinate(3305, 9497, 0), new Coordinate(3321, 3122, 0), new String[]{"Crevice"}, new String[]{"Use"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    KALPHITE_LAIR_IN(CrossableObject.KALPHITE_LAIR_ENTRANCE, new Coordinate(3229, 3108, 0), CrossableObject.KALPHITE_LAIR_ENTRANCE.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new CombinedRequirement(false, new AreaRequirement(new Area.Rectangular(new Coordinate(3223, 3111, 0), new Coordinate(3231, 3104, 0))), new AreaRequirement(new Area.Rectangular(new Coordinate(3453, 9534, 2), new Coordinate(3520, 9470, 2))), new InventoryRequirement("Rope", 1))),
    KALPHITE_LAIR_OUT(CrossableObject.KALPHITE_LAIR_EXIT, new Coordinate(3484, 9510, 2), new Coordinate(3229, 3108, 0), 2, 0, false, new CombinedRequirement(false, new AreaRequirement(new Area.Rectangular(new Coordinate(3223, 3111, 0), new Coordinate(3231, 3104, 0))), new AreaRequirement(new Area.Rectangular(new Coordinate(3453, 9534, 2), new Coordinate(3520, 9470, 2))), new InventoryRequirement("Rope", 1))),
    LUMBRIDGE_SWAMP_SLAYER_DUNGEON(CrossableObject.LUMBRIDGE_SWAMP_DUNGEON_ENTRANCE, new Coordinate(3168, 3172, 0), new Coordinate(3168, 9572, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false, new InventoryRequirement(Regex.getPatternForContainsString("lantern"), 1)),
    LUMBRIDGE_SWAMP_SLAYER_DUNGEON_EXIT(CrossableObject.LUMBRIDGE_SWAMP_DUNGEON_EXIT, new Coordinate(3168, 9572, 0), new Coordinate(3168, 3172, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    RIMMINGTON_SHIP_GANGPLANK_FROM_CORSAIR(CrossableObject.RIMMINGTON_GANGPLANK_CORSAIR, new Coordinate(2909, 3229, 1), new Coordinate(2909, 3226, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    CORSAIR_SHIP_GANGPLANK_FROM_RIMMINGTON(CrossableObject.CORSAIR_GANGPLANK_RIMMINGTON, new Coordinate(2578, 2837, 1), new Coordinate(2578, 2840, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    ARDY_GANGPLANK_FROM_BRIMHAVEN(CrossableObject.BRIMHAVEN_GANGPLANK_ARDY, new Coordinate(2683, 3268, 1), CrossableObject.BRIMHAVEN_GANGPLANK_ARDY.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BRIMHAVEN_GANGPLANK_FROM_ARDY(CrossableObject.ARDY_GANGPLANK_BRIMHAVEN, new Coordinate(2775, 3234, 1), CrossableObject.ARDY_GANGPLANK_BRIMHAVEN.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    RIMMINGTON_GANGPLANK_FROM_ENTRANA(CrossableObject.ENTRANA_GANGPLANK_RIMMINGTON, new Coordinate(3048, 3231, 1), CrossableObject.ENTRANA_GANGPLANK_RIMMINGTON.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    CORSAIR_HOLE_TO_OGRESS(CrossableObject.HOLE_CORSAIR_TO_OGRESS, new Coordinate(2523, 2862, 0), CrossableObject.HOLE_CORSAIR_TO_OGRESS.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    OGRESS_TO_CORSAIR(CrossableObject.VINE_LADDER_OGRESS_CORSAIR, CrossableObject.HOLE_CORSAIR_TO_OGRESS.getOutput(), CrossableObject.VINE_LADDER_OGRESS_CORSAIR.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    MT_KARUULM_ROCKS_EAST(CrossableObject.MT_KARUULM_ROCKS_EAST, CrossableObject.MT_KARUULM_ROCKS_EAST_1.getOutput(), CrossableObject.MT_KARUULM_ROCKS_EAST.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    MT_KARUULM_ROCKS_EAST_1(CrossableObject.MT_KARUULM_ROCKS_EAST_1, CrossableObject.MT_KARUULM_ROCKS_EAST.getOutput(), CrossableObject.MT_KARUULM_ROCKS_EAST_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    MT_KARUULM_STEPS_EAST(CrossableObject.MT_KARUULM_STAIRS_EAST, CrossableObject.MT_KARUULM_STAIRS_EAST_1.getOutput(), CrossableObject.MT_KARUULM_STAIRS_EAST.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    MT_KARUULM_STEPS_EAST_1(CrossableObject.MT_KARUULM_STAIRS_EAST_1, CrossableObject.MT_KARUULM_STAIRS_EAST.getOutput(), CrossableObject.MT_KARUULM_STAIRS_EAST_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    MT_KARUULM_STEPS_UP(CrossableObject.MT_KARUULM_STAIRS_TOP, CrossableObject.MT_KARUULM_STAIRS_TOP_1.getOutput(), CrossableObject.MT_KARUULM_STAIRS_TOP.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    MT_KARUULM_STEPS_UP_1(CrossableObject.MT_KARUULM_STAIRS_TOP_1, CrossableObject.MT_KARUULM_STAIRS_TOP.getOutput(), CrossableObject.MT_KARUULM_STAIRS_TOP_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    MT_KARUULM_ELEVATOR_DOWN(CrossableObject.MT_KARUULM_ELEVATOR, CrossableObject.MT_KARUULM_ELEVATOR_1.getOutput(), CrossableObject.MT_KARUULM_ELEVATOR.getOutput(), 2, 0, false),
    MT_KARUULM_ELEVATOR_UP(CrossableObject.MT_KARUULM_ELEVATOR_1, CrossableObject.MT_KARUULM_ELEVATOR.getOutput(), CrossableObject.MT_KARUULM_ELEVATOR_1.getOutput(), 2, 0, false),
    MT_KARUULM_LAVA_GAP(CrossableObject.MT_KARUULM_LAVA_GAP_WEST, CrossableObject.MT_KARUULM_LAVA_GAP_WEST.getOutput(), CrossableObject.MT_KARUULM_LAVA_GAP_WEST_1.getOutput(), 2, 0, false),
    MT_KARUULM_LAVA_GAP_1(CrossableObject.MT_KARUULM_LAVA_GAP_WEST_1, CrossableObject.MT_KARUULM_LAVA_GAP_WEST_1.getOutput(), CrossableObject.MT_KARUULM_LAVA_GAP_WEST.getOutput(), 2, 0, false),
    MT_KARUULM_ROCKS_WEST(CrossableObject.MT_KARUULM_ROCKS_WEST, CrossableObject.MT_KARUULM_ROCKS_WEST_1.getOutput(), CrossableObject.MT_KARUULM_ROCKS_WEST.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    MT_KARUULM_ROCKS_WEST_1(CrossableObject.MT_KARUULM_ROCKS_WEST_1, CrossableObject.MT_KARUULM_ROCKS_WEST.getOutput(), CrossableObject.MT_KARUULM_ROCKS_WEST_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BRIMHAVEN_DUNGEON_ENTRANCE(CrossableObject.BRIMHAVEN_DUNGEON_ENTRANCE, CrossableObject.BRIMHAVEN_DUNGEON_EXIT.getOutput(), CrossableObject.BRIMHAVEN_DUNGEON_ENTRANCE.getOutput(), 20, Region.CollisionFlags.BLOCKED_TILE, false, new CombinedRequirement(false, new VarbitRequirement(5628, 1), new InventoryRequirement("Coins", 875))),
    BRIMHAVEN_DUNGEON_EXIT(CrossableObject.BRIMHAVEN_DUNGEON_EXIT, CrossableObject.BRIMHAVEN_DUNGEON_ENTRANCE.getOutput(), CrossableObject.BRIMHAVEN_DUNGEON_EXIT.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BRIMHAVEN_DUNGEON_VINES_1(CrossableObject.BRIMHAVEN_VINES_1, CrossableObject.BRIMHAVEN_VINES_2.getOutput(), CrossableObject.BRIMHAVEN_VINES_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new InventoryRequirement(Pattern.compile("\\w*(?<!Blessed) axe"), 1)),
    BRIMHAVEN_DUNGEON_VINES_2(CrossableObject.BRIMHAVEN_VINES_2, CrossableObject.BRIMHAVEN_VINES_1.getOutput(), CrossableObject.BRIMHAVEN_VINES_2.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new InventoryRequirement(Pattern.compile("\\w*(?<!Blessed) axe"), 1)),
    BRIMHAVEN_DUNGEON_VINES_3(CrossableObject.BRIMHAVEN_VINES_3, new Coordinate(2674, 9479, 0), CrossableObject.BRIMHAVEN_VINES_3.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, true, new InventoryRequirement(Pattern.compile("\\w*(?<!Blessed) axe"), 1)),
    BRIMHAVEN_DUNGEON_VINES_4(CrossableObject.BRIMHAVEN_VINES_4, new Coordinate(2695, 9482, 0), CrossableObject.BRIMHAVEN_VINES_4.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, true, new InventoryRequirement(Pattern.compile("\\w*(?<!Blessed) axe"), 1)),
    BRIMHAVEN_STEPPING_STONES_1(CrossableObject.BRIMHAVEN_STEPPING_STONES_1, CrossableObject.BRIMHAVEN_STEPPING_STONES_1A.getOutput(), CrossableObject.BRIMHAVEN_STEPPING_STONES_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BRIMHAVEN_STEPPING_STONES_1A(CrossableObject.BRIMHAVEN_STEPPING_STONES_1A, CrossableObject.BRIMHAVEN_STEPPING_STONES_1.getOutput(), CrossableObject.BRIMHAVEN_STEPPING_STONES_1A.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    FREMENNIK_SLAYER_DUNGEON_SHORTCUT_1(CrossableObject.FREMENNIK_SLAYER_DUNGEON_SHORTCUT_1, CrossableObject.FREMENNIK_SLAYER_DUNGEON_SHORTCUT_1A.getOutput(), CrossableObject.FREMENNIK_SLAYER_DUNGEON_SHORTCUT_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 62)),
    FREMENNIK_SLAYER_DUNGEON_SHORTCUT_1A(CrossableObject.FREMENNIK_SLAYER_DUNGEON_SHORTCUT_1A, CrossableObject.FREMENNIK_SLAYER_DUNGEON_SHORTCUT_1.getOutput(), CrossableObject.FREMENNIK_SLAYER_DUNGEON_SHORTCUT_1A.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 62)),
    FREMENNIK_SLAYER_STEPS_KURASK(CrossableObject.FREMENNIK_SLAYER_KURASK_STEPS, CrossableObject.FREMENNIK_SLAYER_KURASK_STEPSA.getOutput(), CrossableObject.FREMENNIK_SLAYER_KURASK_STEPS.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    FREMENNIK_SLAYER_STEPS_KURASK_1(CrossableObject.FREMENNIK_SLAYER_KURASK_STEPSA, CrossableObject.FREMENNIK_SLAYER_KURASK_STEPS.getOutput(), CrossableObject.FREMENNIK_SLAYER_KURASK_STEPSA.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    TAVERLY_TASK_ONLY_STEPS(CrossableObject.TAVERLY_STEPS_TO_TASK_ONLY, CrossableObject.TAVERLY_STEPS_TO_TASK_ONLY_1.getOutput(), CrossableObject.TAVERLY_STEPS_TO_TASK_ONLY.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    TAVERLY_TASK_ONLY_STEPS_1(CrossableObject.TAVERLY_STEPS_TO_TASK_ONLY_1, CrossableObject.TAVERLY_STEPS_TO_TASK_ONLY.getOutput(), CrossableObject.TAVERLY_STEPS_TO_TASK_ONLY_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    CHASM_OF_FIRE_ENTRANCE(CrossableObject.CHASM_OF_FIRE_ENTRANCE, CrossableObject.CHASM_OF_FIRE_EXIT.getOutput(), CrossableObject.CHASM_OF_FIRE_ENTRANCE.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    CHASM_OF_FIRE_EXIT(CrossableObject.CHASM_OF_FIRE_EXIT, CrossableObject.CHASM_OF_FIRE_ENTRANCE.getOutput(), CrossableObject.CHASM_OF_FIRE_EXIT.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    CHASM_OF_FIRE_TOP_LIFT_DOWN(CrossableObject.CHASM_OF_FIRE_TOP_LIFT_DOWN, CrossableObject.CHASM_OF_FIRE_MIDDLE_LIFT_UP.getOutput(), CrossableObject.CHASM_OF_FIRE_TOP_LIFT_DOWN.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    CHASM_OF_FIRE_MIDDLE_LIFT_DOWN(CrossableObject.CHASM_OF_FIRE_MIDDLE_LIFT_DOWN, CrossableObject.CHASM_OF_FIRE_BOTTOM_LIFT_UP.getOutput(), CrossableObject.CHASM_OF_FIRE_MIDDLE_LIFT_DOWN.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    CHASM_OF_FIRE_MIDDLE_LIFT_UP(CrossableObject.CHASM_OF_FIRE_MIDDLE_LIFT_UP, CrossableObject.CHASM_OF_FIRE_TOP_LIFT_DOWN.getOutput(), CrossableObject.CHASM_OF_FIRE_MIDDLE_LIFT_UP.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    CHASM_OF_FIRE_BOTTOM_LIFT_UP(CrossableObject.CHASM_OF_FIRE_BOTTOM_LIFT_UP, CrossableObject.CHASM_OF_FIRE_MIDDLE_LIFT_DOWN.getOutput(), CrossableObject.CHASM_OF_FIRE_BOTTOM_LIFT_UP.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    ASGARNIAN_DUNGEON_WYVERN_STAIRS_UP(CrossableObject.ASGARNIAN_WYVERN_TASK_ONLY_STEPS_UP, CrossableObject.ASGARNIAN_WYVERN_TASK_ONLY_STEPS_DOWN.getOutput(), CrossableObject.ASGARNIAN_WYVERN_TASK_ONLY_STEPS_UP.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    ASGARNIAN_DUNGEON_WYVERN_STAIRS_DOWN(CrossableObject.ASGARNIAN_WYVERN_TASK_ONLY_STEPS_DOWN, CrossableObject.ASGARNIAN_WYVERN_TASK_ONLY_STEPS_UP.getOutput(), CrossableObject.ASGARNIAN_WYVERN_TASK_ONLY_STEPS_DOWN.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    ASGARNIAN_DUNGEON_WYVERN_ENTRANCE(CrossableObject.ASGARNIAN_WYVERN_ICY_CAVERN_ENTRANCE, CrossableObject.ASGARNIAN_WYVERN_ICY_CAVERN_EXIT.getOutput(), CrossableObject.ASGARNIAN_WYVERN_ICY_CAVERN_ENTRANCE.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    ASGARNIAN_DUNGEON_WYVERN_EXIT(CrossableObject.ASGARNIAN_WYVERN_ICY_CAVERN_EXIT, CrossableObject.ASGARNIAN_WYVERN_ICY_CAVERN_ENTRANCE.getOutput(), CrossableObject.ASGARNIAN_WYVERN_ICY_CAVERN_EXIT.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    SLAYER_TOWER_CHAIN_UP(CrossableObject.SLAYER_TOWER_SPIKY_CHAIN_UP, CrossableObject.SLAYER_TOWER_SPIKY_CHAIN_DOWN.getOutput(), CrossableObject.SLAYER_TOWER_SPIKY_CHAIN_UP.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 61), Requirements.NOSE_PEG.getRequirements()),
    SLAYER_TOWER_CHAIN_DOWN(CrossableObject.SLAYER_TOWER_SPIKY_CHAIN_DOWN, CrossableObject.SLAYER_TOWER_SPIKY_CHAIN_UP.getOutput(), CrossableObject.SLAYER_TOWER_SPIKY_CHAIN_DOWN.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 61)),
    SLAYER_TOWER_BASE_STAIRS_UP(CrossableObject.SLAYER_TOWER_BASE_STAIRCASE_UP, CrossableObject.SLAYER_TOWER_FIRST_FLOOR_STAIRCASE_DOWN.getOutput(), CrossableObject.SLAYER_TOWER_BASE_STAIRCASE_UP.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    SLAYER_TOWER_FIRST_FLOOR_STAIRS_DOWN(CrossableObject.SLAYER_TOWER_FIRST_FLOOR_STAIRCASE_DOWN, CrossableObject.SLAYER_TOWER_BASE_STAIRCASE_UP.getOutput(), CrossableObject.SLAYER_TOWER_FIRST_FLOOR_STAIRCASE_DOWN.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BRIMHAVEN_DUNGEON_STAIRS_GREATER_DEMONS(CrossableObject.BRIMHAVEN_DUNGEON_STAIRS_TO_GREATER_DEMONS, CrossableObject.BRIMHAVEN_DUNGEON_STAIRS_DOWN_FROM_GREATER_DEMONS.getOutput(), CrossableObject.BRIMHAVEN_DUNGEON_STAIRS_TO_GREATER_DEMONS.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BRIMHAVEN_DUNGEON_STAIRS_GREATER_DEMONS_1(CrossableObject.BRIMHAVEN_DUNGEON_STAIRS_DOWN_FROM_GREATER_DEMONS, CrossableObject.BRIMHAVEN_DUNGEON_STAIRS_TO_GREATER_DEMONS.getOutput(), CrossableObject.BRIMHAVEN_DUNGEON_STAIRS_DOWN_FROM_GREATER_DEMONS.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    SLAYER_TOWER_LADDER_TASK_ONLY_1(CrossableObject.SLAYER_TOWER_LADDER_DOWN_TASK_ONLY, CrossableObject.SLAYER_TOWER_LADDER_UP_TASK_ONLY.getOutput(), CrossableObject.SLAYER_TOWER_LADDER_DOWN_TASK_ONLY.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    SLAYER_TOWER_LADDER_TASK_ONLY_2(CrossableObject.SLAYER_TOWER_LADDER_UP_TASK_ONLY, CrossableObject.SLAYER_TOWER_LADDER_DOWN_TASK_ONLY.getOutput(), CrossableObject.SLAYER_TOWER_LADDER_UP_TASK_ONLY.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    WITCHAVEN_DUNEGON_ENTRANCE(CrossableObject.WITCHAVEN_OLD_RUIN_ENTRANCE, CrossableObject.WITCHAVEN_DUNGEON_EXIT.getOutput(), CrossableObject.WITCHAVEN_OLD_RUIN_ENTRANCE.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    WITCHAVEN_DUNEGON_EXIT(CrossableObject.WITCHAVEN_DUNGEON_EXIT, CrossableObject.WITCHAVEN_OLD_RUIN_ENTRANCE.getOutput(), CrossableObject.WITCHAVEN_DUNGEON_EXIT.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    ZANARIS_JUTTING_WALL_HIGHER(CrossableObject.ZANARIS_JUTTING_WALL_HIGHER, CrossableObject.ZANARIS_JUTTING_WALL_HIGHER_1.getOutput(), CrossableObject.ZANARIS_JUTTING_WALL_HIGHER.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 66)),
    ZANARIS_JUTTING_WALL_HIGHER_1(CrossableObject.ZANARIS_JUTTING_WALL_HIGHER_1, CrossableObject.ZANARIS_JUTTING_WALL_HIGHER.getOutput(), CrossableObject.ZANARIS_JUTTING_WALL_HIGHER_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 66)),
    ZANARIS_JUTTING_WALL_LOWER(CrossableObject.ZANARIS_JUTTING_WALL_LOWER, CrossableObject.ZANARIS_JUTTING_WALL_LOWER_1.getOutput(), CrossableObject.ZANARIS_JUTTING_WALL_LOWER.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 46)),
    ZANARIS_JUTTING_WALL_LOWER_1(CrossableObject.ZANARIS_JUTTING_WALL_LOWER_1, CrossableObject.ZANARIS_JUTTING_WALL_LOWER.getOutput(), CrossableObject.ZANARIS_JUTTING_WALL_LOWER_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new SkillRequirement(Skill.AGILITY, 46)),
    MAGIC_WHISTLE_FISHER_REALM(Teleport.MAGIC_WHISTLE_LEAVE, Teleport.MAGIC_WHISTLE_LEAVE.getIn(), Teleport.MAGIC_WHISTLE_LEAVE.getPosition(), 1, 0, false, Teleport.MAGIC_WHISTLE_LEAVE.getRequirements()),
    MAGIC_WHISTLE_FISHER_REALM_DARK(Teleport.MAGIC_WHISTLE_LEAVE_DARK, Teleport.MAGIC_WHISTLE_LEAVE_DARK.getIn(), Teleport.MAGIC_WHISTLE_LEAVE_DARK.getPosition(), 1, 0, false, Teleport.MAGIC_WHISTLE_LEAVE.getRequirements()),

    //Dark sage addons
    WIZARD_TOWER_LADDER_UP(new Coordinate(3103, 9576, 0), new Coordinate(3104, 9576, 0), new Coordinate(3105, 3162, 0), new String[]{"Ladder"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    WIZARD_TOWER_LADDER_DOWN(new Coordinate(3104, 3162, 0), new Coordinate(3105, 3162, 0), new Coordinate(3104, 9576, 0), new String[]{"Ladder"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    LUMBRIDGE_CHURCH_DOOR(new Coordinate(3238, 3210, 0), new Coordinate(3237, 3210, 0), new Coordinate(3238, 3210, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    RESTLESS_GHOST_DOOR(new Coordinate(3247, 3193, 0), new Coordinate(3246, 3193, 0), new Coordinate(3247, 3193, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    FATHER_URHNEY_SHACK_DOOR(new Coordinate(3147, 3172, 0), new Coordinate(3147, 3172, 0), new Coordinate(3147, 3173, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    WIZARD_TOWER_MAIN_DOOR(new Coordinate(3109, 3167, 0), new Coordinate(3109, 3167, 0), new Coordinate(3109, 3166, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    WIZARD_TOWER_G_DOOR_1(new Coordinate(3107, 3162, 0), new Coordinate(3107, 3163, 0), new Coordinate(3107, 3161, 0), new String[]{"Door"}, new String[]{"Open"}, 2, Region.CollisionFlags.BLOCKED_TILE, true),
    WIZARD_TOWER_B_DOOR_1(new Coordinate(3108, 9570, 0), new Coordinate(3108, 9570, 0), new Coordinate(3107, 9570, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    WIZARD_TOWER_B_DOOR_2(new Coordinate(3111, 9559, 0), new Coordinate(3110, 9559, 0), new Coordinate(3111, 9559, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    WIZARD_TOWER_STAIR_DOOR(new Coordinate(3107, 3162, 0), new Coordinate(3107, 3163, 0), new Coordinate(3106, 3162, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true),
    WIZARD_TOWER_STAIR_GROUND(new Coordinate(3103, 3159, 0), new Coordinate(3104, 3161, 0), new Coordinate(3104, 3161, 1), new String[]{"Staircase"}, new String[]{"Climb-up"}, 3, Region.CollisionFlags.BLOCKED_TILE, false),
    WIZARD_TOWER_STAIR_MIDDLE_UP(new Coordinate(3103, 3159, 1), new Coordinate(3104, 3161, 1), new Coordinate(3104, 3161, 2), new String[]{"Staircase"}, new String[]{"Climb-up"}, 3, Region.CollisionFlags.BLOCKED_TILE, false),
    WIZARD_TOWER_STAIR_MIDDLE_DOWN(new Coordinate(3103, 3159, 1), new Coordinate(3104, 3161, 1), new Coordinate(3104, 3161, 0), new String[]{"Staircase"}, new String[]{"Climb-down"}, 3, Region.CollisionFlags.BLOCKED_TILE, false),
    WIZARD_TOWER_STAIR_TOP(new Coordinate(3103, 3159, 2), new Coordinate(3104, 3161, 2), new Coordinate(3104, 3161, 1), new String[]{"Staircase"}, new String[]{"Climb-down"}, 3, Region.CollisionFlags.BLOCKED_TILE, false),
    WIZARD_TOWER_DEMON_DOOR(new Coordinate(3108, 3162, 2), new Coordinate(3107, 3162, 2), new Coordinate(3108, 3162, 2), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true),
    LUMBRIDGE_CASTLE_DUKE_DOOR(new Coordinate(3207, 3222, 1), new Coordinate(3207, 3222, 1), new Coordinate(3208, 3222, 1), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true),
    LUMBRIDGE_PUB_DOOR(new Coordinate(3230, 3235, 0), new Coordinate(3230, 3235, 0), new Coordinate(3230, 3236, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    RIMMINGTON_WITCH_SHACK_DOOR(new Coordinate(2964, 3206, 0), new Coordinate(2964, 3206, 0), new Coordinate(2965, 3206, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true),
    WITCHS_POTION_RAT_HOUSE_DOOR(new Coordinate(2956, 3206, 0), new Coordinate(2956, 3206, 0), new Coordinate(2956, 3205, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    ARCEUUS_GENERAL_STORE_DOOR(new Coordinate(1718, 3727, 0), new Coordinate(1717, 3727, 0), new Coordinate(1717, 3728, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    ARCEUUS_GENERAL_STORE_DOOR_2(new Coordinate(1718, 3728, 0), new Coordinate(1717, 3728, 0), new Coordinate(1718, 3728, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    LOVAKENGJ_GENERAL_STORE_DOOR(new Coordinate(1551, 3754, 0), new Coordinate(1551, 3754, 0), new Coordinate(1551, 3755, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    //southwest?
    HOSIDIUS_GENERAL_STORE_DOOR(new Coordinate(1770, 3590, 0), new Coordinate(1770, 3590, 0), new Coordinate(1770, 3589, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    SANFEW_HUT_STAIRS_UP(new Coordinate(2898, 3428, 0), new Coordinate(2897, 3428, 0), new Coordinate(2898, 3427, 1), new String[]{"Staircase"}, new String[]{"Climb-up"}, 3, Region.CollisionFlags.BLOCKED_TILE, false),
    SANFEW_HUT_STAIRS_DOWN(new Coordinate(2898, 3428, 1), new Coordinate(2898, 3427, 1), new Coordinate(2897, 3428, 0), new String[]{"Staircase"}, new String[]{"Climb-down"}, 3, Region.CollisionFlags.BLOCKED_TILE, false),
    DWARF_CANNON_GATE(new Coordinate(2567, 3456, 0), new Coordinate(2567, 3455, 0), new Coordinate(2567, 3456, 0), new String[]{"Gate"}, new String[]{"Open"}, 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    DWARF_CANNON_GATE_1(new Coordinate(2568, 3456, 0), new Coordinate(2568, 3455, 0), new Coordinate(2568, 3456, 0), new String[]{"Gate"}, new String[]{"Open"}, 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    DWARF_CANNON_TOWER_TOP_DOWN(new Coordinate(2570, 3443, 2), new Coordinate(2569, 3443, 2), new Coordinate(2569, 3443, 1), new String[]{"Ladder"}, new String[]{"Climb-down"}, 1, Region.CollisionFlags.BLOCKED_TILE, false),
    DWARF_CANNON_TOWER_MID_UP(new Coordinate(2570, 3443, 1), new Coordinate(2569, 3443, 1), new Coordinate(2569, 3443, 2), new String[]{"Ladder"}, new String[]{"Climb-up"}, 1, Region.CollisionFlags.BLOCKED_TILE, false),
    DWARF_CANNON_TOWER_MID_DOWN(new Coordinate(2570, 3441, 1), new Coordinate(2570, 3442, 1), new Coordinate(2570, 3442, 0), new String[]{"Ladder"}, new String[]{"Climb-down"}, 1, Region.CollisionFlags.BLOCKED_TILE, false),
    DWARF_CANNON_TOWER_GROUND(new Coordinate(2570, 3441, 0), new Coordinate(2570, 3442, 0), new Coordinate(2570, 3442, 1), new String[]{"Ladder"}, new String[]{"Climb-up"}, 1, Region.CollisionFlags.BLOCKED_TILE, false),
    TREE_GNOME_VILLA_LADDER_DOWN(new Coordinate(2533, 3155, 0), new Coordinate(2533, 3156, 0), new Coordinate(2533, 9556, 0), new String[]{"Ladder"}, new String[]{"Climb-down"}, 1, Region.CollisionFlags.BLOCKED_TILE, false),
    TREE_GNOME_VILLA_LADDER_UP(new Coordinate(2533, 9555, 0), new Coordinate(2533, 9556, 0), new Coordinate(2533, 3156, 0), new String[]{"Ladder"}, new String[]{"Climb-up"}, 1, Region.CollisionFlags.BLOCKED_TILE, false),
    GLARIALS_TOMB_LADDER_UP(new Coordinate(2556, 9844, 0), new Coordinate(2557, 9844, 0), new Coordinate(2557, 3444, 0), new String[]{"Ladder"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    DWARF_CANNON_GOBLIN_CAVE_ENTER(new Coordinate(2622, 3392, 0), new Coordinate(2623, 3391, 0), new Coordinate(2619, 9797, 0), new String[]{"Cave Entrance"}, new String[]{"Enter"}, 3, Region.CollisionFlags.BLOCKED_TILE, false),
    DWARF_CANNON_MUD_PILE(new Coordinate(2621, 9796, 0), new Coordinate(2620, 9797, 0), new Coordinate(2623, 3391, 0), new String[]{"Mud pile"}, new String[]{"Climb-over"}, 3, Region.CollisionFlags.BLOCKED_TILE, false),
    MONKS_FRIEND_LADDER_DOWN(new Coordinate(2561, 3222, 0), new Coordinate(2561, 3223, 0), new Coordinate(2561, 9621, 0), new String[]{"Ladder"}, new String[]{"Climb-down"}, 1, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    MONKS_FRIEND_LADDER_UP(new Coordinate(2561, 9622, 0), new Coordinate(2561, 9621, 0), new Coordinate(2561, 3223, 0), new String[]{"Ladder"}, new String[]{"Climb-up"}, 1, Region.CollisionFlags.BLOCKED_TILE, false),
    MONKS_FRIEND_DOOR(new Coordinate(2565, 9612, 0), new Coordinate(2565, 9613, 0), new Coordinate(2565, 9611, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    DRAYNOR_PIG_PEN_GATE(new Coordinate(3077, 3258, 0), new Coordinate(3078, 3258, 0), new Coordinate(3078, 3259, 0), new String[]{"Gate"}, new String[]{"Open"}, 1, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    CONTACT_BANK_LADDER_UP(new Coordinate(2799, 5159, 0), new Coordinate(2799, 5160, 0), new Coordinate(3315, 2796, 0), new String[]{"Ladder"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    CONTACT_BANK_LADDER_DOWN(new Coordinate(3315, 2797, 0), new Coordinate(3315, 2796, 0), new Coordinate(2800, 5160, 0), new String[]{"Ladder"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    DWARF_CANNON_NULODIAN_HOUSE(new Coordinate(3015, 3453, 0), new Coordinate(3015, 3453, 0), new Coordinate(3014, 3453, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true),
    HAZEEL_CULT_HOUSE_STAIR_UP(new Coordinate(2568, 3268, 0), new Coordinate(2568, 3271, 0), new Coordinate(2568, 3267, 1), new String[]{"Staircase"}, new String[]{"Climb-up"}, 1, Region.CollisionFlags.BLOCKED_TILE, false),
    HAZEEL_CULT_HOUSE_STAIR_DOWN(new Coordinate(2568, 3268, 1), new Coordinate(2568, 3267, 1), new Coordinate(2568, 3271, 0), new String[]{"Staircase"}, new String[]{"Climb-down"}, 1, Region.CollisionFlags.BLOCKED_TILE, false),
    HAZEEL_CULT_HOUSE_DOOR_1(new Coordinate(2565, 3272, 0), new Coordinate(2565, 3271, 0), new Coordinate(2565, 3272, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    HAZEEL_CULT_HOUSE_DOOR_2(new Coordinate(2567, 3271, 0), new Coordinate(2567, 3271, 0), new Coordinate(2568, 3271, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true),
    HAZEEL_CULT_HOUSE_DOOR_3(new Coordinate(2570, 3271, 0), new Coordinate(2569, 3271, 0), new Coordinate(2570, 3271, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    HAZEEL_CULT_HOUSE_DOOR_4(new Coordinate(2573, 3271, 0), new Coordinate(2573, 3270, 0), new Coordinate(2573, 3271, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    HAZEEL_CULT_HOUSE_DOOR_5(new Coordinate(2573, 3270, 0), new Coordinate(2573, 3270, 0), new Coordinate(2572, 3270, 0), new String[]{"Door"}, new String[]{"Close"}, 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    HAZEEL_CULT_HOUSE_DOOR_6(new Coordinate(2569, 3273, 0), new Coordinate(2569, 3274, 0), new Coordinate(2569, 3272, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    HAZEEL_CULT_HOUSE_LADDER_DOWN(new Coordinate(2570, 3267, 0), new Coordinate(2570, 3268, 0), new Coordinate(2570, 9668, 0), new String[]{"Ladder"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    HAZEEL_CULT_HOUSE_LADDER_UP(new Coordinate(2570, 9667, 0), new Coordinate(2570, 9668, 0), new Coordinate(2570, 3268, 0), new String[]{"Ladder"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    HAZEEL_CULT_HOUSE_DOOR_7(new Coordinate(2566, 3268, 1), new Coordinate(2567, 3268, 1), new Coordinate(2566, 3268, 1), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true), //EAST_BOUNDARY_OBJECT
    HAZEEL_CULT_HOUSE_DOOR_8(new Coordinate(2572, 3268, 1), new Coordinate(2571, 3268, 1), new Coordinate(2572, 3268, 1), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true), //WEST_BOUNDARY_OBJECT
    HAZEEL_CULT_HOUSE_WALL_1(new Coordinate(2572, 3270, 1), new Coordinate(2572, 3270, 1), new Coordinate(2572, 3271, 1), new String[]{"Wall"}, new String[]{"Knock-at"}, 2, Region.CollisionFlags.NORTH_EAST_BOUNDARY_OBJECT, true),
    HAZEEL_CULT_HOUSE_LADDER_2_UP(new Coordinate(2573, 3271, 1), new Coordinate(2573, 3270, 1), new Coordinate(2572, 3271, 2), new String[]{"Ladder"}, new String[]{"CLimb-up"}, 1, Region.CollisionFlags.BLOCKED_TILE, false),
    HAZEEL_CULT_HOUSE_LADDER_2_DOWN(new Coordinate(2573, 3271, 2), new Coordinate(2572, 3271, 2), new Coordinate(2573, 3270, 1), new String[]{"Ladder"}, new String[]{"Climb-down"}, 1, Region.CollisionFlags.BLOCKED_TILE, false),
    HAZEEL_CULT_CAVE_ENTER(new Coordinate(2585, 3233, 0), new Coordinate(2585, 3238, 0), new Coordinate(2570, 9682, 0), new String[]{"Cave entrance"}, new String[]{"Enter"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    HAZEEL_CULT_CAVE_EXIT(new Coordinate(2570, 9683, 0), new Coordinate(2570, 9682, 0), new Coordinate(2586, 3238, 0), new String[]{"Stairs"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    HAZEEL_CULT_RAFT_1(new Coordinate(2567, 9679, 0), new Coordinate(2567, 9680, 0), new Coordinate(2606, 9692, 0), new String[]{"Raft"}, new String[]{"Board"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    HAZEEL_CULT_RAFT_2(new Coordinate(2606, 9693, 0), new Coordinate(2606, 9692, 0), new Coordinate(2567, 9680, 0), new String[]{"Raft"}, new String[]{"Board"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    TREE_GNOME_EAST_DOOR(new Coordinate(2524, 3254, 0), new Coordinate(2524, 3254, 0), new Coordinate(2524, 3255, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),

    TREE_GNOME_CRUMBLED_WALL(CrossableObject.TREE_GNOME_CRUMBLED_WALL, new Coordinate(2509, 3252, 0), new Coordinate(2509, 3255, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    TREE_GNOME_INSIDE_DOOR(new Coordinate(2505, 3256, 0), new Coordinate(2506, 3256, 0), new Coordinate(2505, 3255, 0), new String[]{"Door"}, new String[]{"Open"}, 1, Region.CollisionFlags.BLOCKED_TILE, true),
    TREE_GNOME_DOOR_OUT(new Coordinate(2502, 3250, 0), new Coordinate(2502, 3251, 0), new Coordinate(2502, 3250, 0), new String[]{"Door"}, new String[]{"Open"}, 3, Region.CollisionFlags.NORTH_EAST_BOUNDARY_OBJECT, false),
    TREE_GNOME_LADDER_UP(new Coordinate(2503, 3252, 0), new Coordinate(2503, 3253, 0), new Coordinate(2503, 3253, 1), new String[]{"Ladder"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    TREE_GNOME_LADDER_DOWN(new Coordinate(2503, 3252, 1), new Coordinate(2503, 3253, 1), new Coordinate(2503, 3253, 0), new String[]{"Ladder"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    WATERFALL_CELL_DOOR_IN(new Coordinate(2515, 9575, 0), new Coordinate(2515, 9575, 0), new Coordinate(2515, 9576, 0), new String[]{"Door"}, new String[]{"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, false, new InventoryRequirement("Key", 293, 1)),
    WATERFALL_CELL_DOOR_OUT(new Coordinate(2515, 9575, 0), new Coordinate(2515, 9576, 0), new Coordinate(2515, 9575, 0), new String[]{"Door"}, new String[]{"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, false),
    WATERFALL_TOMBSTONE(CrossableObject.GLARIALS_TOMBSTONE_ENTER, new Coordinate(2557, 3444, 0), CrossableObject.GLARIALS_TOMBSTONE_ENTER.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new InventoryRequirement("Glarial's pebble", 1)),
    WATERFALL_TOMB_EXIT(new Coordinate(2556, 9844, 0), new Coordinate(2557, 9844, 0), new Coordinate(2557, 3444, 0), new String[]{"Ladder"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    TREE_GNOME_RAILING(new Coordinate(2515, 3161, 0), new Coordinate(2515, 3160, 0), new Coordinate(2515, 3161, 0), new String[]{"Loose Railing"}, new String[]{"Squeeze-through"}, 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    TAVERLY_FIRST_GATE_1(new Coordinate(2889, 9830, 0), new Coordinate(2888, 9830, 0), new Coordinate(2889, 9830, 0), new String[]{"Prison door"}, new String[]{"Open"}, 1, Region.CollisionFlags.NORTH_WEST_BOUNDARY_OBJECT, true),
    TAVERLY_FIRST_GATE_2(new Coordinate(2889, 9831, 0), new Coordinate(2888, 9831, 0), new Coordinate(2889, 9831, 0), new String[]{"Prison door"}, new String[]{"Open"}, 1, Region.CollisionFlags.NORTH_WEST_BOUNDARY_OBJECT, true),
    TAVERLY_INNER_GATE_1(OpenableObject.TAVERLY_DUNGEON_INNER_GUARD_DOOR, OpenableObject.TAVERLY_DUNGEON_INNER_GUARD_DOOR.getPosition(), OpenableObject.TAVERLY_DUNGEON_INNER_GUARD_DOOR.getPosition().derive(0, -1), 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    TAVERLY_INNER_GATE_2(OpenableObject.TAVERLY_DUNGEON_INNER_GUARD_DOOR_1, OpenableObject.TAVERLY_DUNGEON_INNER_GUARD_DOOR_1.getPosition(), OpenableObject.TAVERLY_DUNGEON_INNER_GUARD_DOOR_1.getPosition().derive(0, -1), 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),

    SINCLAIR_GATE_1(new Coordinate(2741, 3555, 0), new Coordinate(2741, 3555, 0), new Coordinate(2741, 3556, 0), new String[]{"Gate"}, new String[]{"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    SINCLAIR_GATE_2(new Coordinate(2742, 3555, 0), new Coordinate(2742, 3555, 0), new Coordinate(2742, 3556, 0), new String[]{"Gate"}, new String[]{"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    ARTHUR_MANSION_GATE_1(new Coordinate(2757, 3482, 0), new Coordinate(2757, 3482, 0), new Coordinate(2757, 3483, 0), new String[]{"Gate"}, new String[]{"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    ARTHUR_MANSION_GATE_2(new Coordinate(2758, 3482, 0), new Coordinate(2758, 3482, 0), new Coordinate(2758, 3483, 0), new String[]{"Gate"}, new String[]{"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    FALADOR_CASTLE_WEST_DOOR_1(new Coordinate(2965, 3338, 0), new Coordinate(2965, 3338, 0), new Coordinate(2964, 3338, 0), new String[]{"Castle door"}, new String[]{"Open"}, 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    FALADOR_CASTLE_WEST_DOOR_2(new Coordinate(2965, 3339, 0), new Coordinate(2965, 3339, 0), new Coordinate(2964, 3339, 0), new String[]{"Castle door"}, new String[]{"Open"}, 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    FAL_CASTLE_W_P0_STAIR_UP(new Coordinate(2954, 3338, 0), new Coordinate(2955, 3337, 0), new Coordinate(2956, 3338, 1), new String[]{"Staircase"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    FAL_CASTLE_W_P1_STAIR_DOWN(new Coordinate(2955, 3338, 1), new Coordinate(2956, 3338, 1), new Coordinate(2955, 3337, 0), new String[]{"Staircase"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    FAL_CASLTE_W_P1_STAIR_UP(new Coordinate(2960, 3338, 1), new Coordinate(2960, 3340, 1), new Coordinate(2959, 3339, 2), new String[]{"Staircase"}, new String[]{"Climb-up"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    FAL_CASTLE_W_P2_STAIR_DOWN(new Coordinate(2960, 3339, 2), new Coordinate(2959, 3339, 2), new Coordinate(2960, 3340, 1), new String[]{"Staircase"}, new String[]{"Climb-down"}, 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BK_FORT_P0_DOOR_S_1(new Coordinate(3016, 3514, 0), new Coordinate(3016, 3514, 0), new Coordinate(3016, 3515, 0), new String[]{"Sturdy door"}, new String[]{"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    BK_FORT_P0_WALL(new Coordinate(3016, 3517, 0), new Coordinate(3016, 3516, 0), new Coordinate(3016, 3517, 0), new String[]{"Wall"}, new String[]{"Push"}, 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    BK_FORT_P0_LADDER_UP_1(new Coordinate(3015, 3519, 0), new Coordinate(3015, 3518, 0), new Coordinate(3015, 3518, 1), "Ladder", "Climb-up"),
    BK_FORT_P1_LADDER_DOWN_1(new Coordinate(3015, 3519, 1), new Coordinate(3015, 3518, 1), new Coordinate(3015, 3518, 0), "Ladder", "Climb-down"),
    BK_FORT_P1_LADDER_UP_1(new Coordinate(3016, 3519, 1), new Coordinate(3016, 3518, 1), new Coordinate(3016, 3518, 2), "Ladder", "Climb-up"),
    BK_FORT_P2_LADDER_DOWN_1(new Coordinate(3016, 3519, 2), new Coordinate(3016, 3518, 2), new Coordinate(3016, 3518, 1), "Ladder", "Climb-down"),
    BK_FORT_P2_LADDER_DOWN_2(new Coordinate(3017, 3516, 2), new Coordinate(3017, 3515, 2), new Coordinate(3017, 3515, 1), "Ladder", "Climb-down"),
    BK_FORT_P1_LADDER_UP_2(new Coordinate(3017, 3516, 1), new Coordinate(3017, 3515, 1), new Coordinate(3017, 3515, 2), "Ladder", "Climb-up"),
    BK_FORT_P1_DOOR_1(new Coordinate(3019, 3515, 1), new Coordinate(3019, 3515, 1), new Coordinate(3020, 3515, 1), "East"),
    BK_FORT_P1_LADDER_UP_3(new Coordinate(3023, 3513, 1), new Coordinate(3023, 3514, 1), new Coordinate(3023, 3514, 2), "Ladder", "Climb-up"),
    BK_FORT_P2_LADDER_DOWN_3(new Coordinate(3023, 3513, 2), new Coordinate(3023, 3514, 2), new Coordinate(3023, 3514, 1), "Ladder", "Climb-down"),
    BK_FORT_P2_LADDER_DOWN_4(new Coordinate(3025, 3513, 2), new Coordinate(3025, 3514, 2), new Coordinate(3025, 3514, 1), "Ladder", "Climb-down"),
    BK_FORT_P1_LADDER_UP_4(new Coordinate(3025, 3513, 1), new Coordinate(3025, 3514, 1), new Coordinate(3025, 3514, 2), "Ladder", "Climb-up"),
    BK_FORT_P1_DOOR_S_1(new Coordinate(3025, 3511, 1), new Coordinate(3026, 3511, 1), new Coordinate(3025, 3511, 1), new String[]{"Sturdy door"}, "East"),
    BK_FORT_P1_LADDER_DOWN_2(new Coordinate(3021, 3510, 1), new Coordinate(3022, 3510, 1), new Coordinate(3022, 3510, 0), "Ladder", "Climb-down" ),
    BK_FORT_P0_LADDER_UP_2(new Coordinate(3021, 3510, 0), new Coordinate(3022, 3510, 0), new Coordinate(3022, 3510, 1), "Ladder", "Climb-up"),

    //door didnt work either direction?
    BK_FORT_P0_DOOR_IN(CrossableObject.BK_FORT_DDOR_IN, new Coordinate(3020, 3515, 0), new Coordinate(3019, 3515, 0), 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, false),
    BK_FORT_P0_DOOR_OUT(new Coordinate(3020, 3515, 0), new Coordinate(3019, 3515, 0), new Coordinate(3020, 3515, 0), "Door", "Open", "West", 1, false),

    BK_FORT_P1_LADDER_DOWN_3(new Coordinate(3022, 3518, 1), new Coordinate(3022, 3517, 1), new Coordinate(3022, 3517, 0), "Ladder", "Climb-down"),
    BK_FORT_P0_LADDER_UP_3(new Coordinate(3022, 3518, 0), new Coordinate(3022, 3517, 0), new Coordinate(3022, 3517, 1), "Ladder", "Climb-up"),
    BK_FORT_P1_WALL(new Coordinate(3030, 3510, 1), new Coordinate(3030, 3510, 1), new Coordinate(3030, 3509, 1), "Wall", "Push", "South"),
    DORIC_SMITHERY_DOOR(new Coordinate(2949, 3450, 0), new Coordinate(2949, 3450, 0), new Coordinate(2950, 3450, 0), "East"),
    ELENA_HOUSE_DOOR(new Coordinate(2592, 3339, 0), new Coordinate(2592, 3339, 0), new Coordinate(2592, 3338, 0), "S"),
    ELENA_P_P0_DOOR_1(new Coordinate(2570, 3333, 0), new Coordinate(2570, 3333, 0), new Coordinate(2571, 3333, 0), "E"),
    ELENA_P_P0_DOOR_2(new Coordinate(2574, 3333, 0), new Coordinate(2573, 3333, 0), new Coordinate(2574, 3333, 0), "W"),
    ELENA_P_P0_DOOR_3(new Coordinate(2578, 3333, 0), new Coordinate(2577, 3333, 0), new Coordinate(2578, 3333, 0), "W"),
    JERICO_DOOR_1(new Coordinate(2610, 3324, 0), new Coordinate(2610, 3324, 0), new Coordinate(2611, 3324, 0), "E"),
    DOOR_FOR_MED_GOWN(new Coordinate(2519, 3275, 0), new Coordinate(2519, 3275, 0), new Coordinate(2518, 3275, 0), "W"),
    MOURN_HQ_MAIN_DOOR(new Coordinate(2551, 3320, 0), new Coordinate(2551, 3320, 0), new Coordinate(2551, 3321, 0), "N", new EquipmentRequirement("Medical gown", 1)),
    MOURN_HQ_FENCE(new Coordinate(2541, 3331, 0), new Coordinate(2541, 3331, 0), new Coordinate(2542, 3331, 0), "Fence", "Squeeze-through", "E"),
    MOURN_HQ_STAIR_UP(new Coordinate(2542, 3324, 0), new Coordinate(2544, 3325, 0), new Coordinate(2544, 3225, 1), "Staircase", "Climb-up"),
    MOURN_HQ_STAIR_DOWN(new Coordinate(2543, 3325, 1), new Coordinate(2544, 3225, 1), new Coordinate(2544, 3325, 0), "Staircase", "Climb-down"),
    MOURN_HQ_P0_DOOR_1(new Coordinate(2546, 3325, 0), new Coordinate(2546, 3326, 0), new Coordinate(2546, 3325, 0), "E"),
    MOURN_HQ_P1_DOOR(new Coordinate(2547, 3325, 1), new Coordinate(2546, 3325, 1), new Coordinate(2547, 3325, 1), "W"),
    MOURN_HQ_P1_GATE_1(new Coordinate(2551, 3325, 1), new Coordinate(2551, 3325, 1), new Coordinate(2552, 3325, 1), "Gate", "Open", "E", new InventoryRequirement("Key", 423, 1)),
    MOURN_HQ_P1_GATE_2(new Coordinate(2551, 3326, 1), new Coordinate(2551, 3326, 1), new Coordinate(2552, 3326, 1), "Gate", "Open", "E", new InventoryRequirement("Key", 423, 1)),
    CHEMIST_DOOR_1(new Coordinate(2932, 3214, 0), new Coordinate(2932, 3213, 0), new Coordinate(2932, 3214, 0), "S"),
    //Varrock Southeast objects //verify gate needs dialog? //crossable
    V_SE_GATE_1(new Coordinate(3264, 3405, 0), new Coordinate(3263, 3405, 0), new Coordinate(3264, 3405, 0), "Gate", "Open", "W"),
    V_SE_GATE_2(new Coordinate(3264, 3406, 0), new Coordinate(3263, 3406, 0), new Coordinate(3264, 3406, 0), "Gate", "Open", "W"),
    V_SE_CLOTHES_SHOP_DOOR(new Coordinate(3277, 3397, 0), new Coordinate(3277, 3397, 0), new Coordinate(3278, 3397, 0), "E"),
    V_SE_G_DOOR_1(new Coordinate(3278, 3382, 0), new Coordinate(3278, 3382, 0), new Coordinate(3279, 3382, 0), "E"),
    V_SE_G_DOOR_2(new Coordinate(3282, 3382, 0), new Coordinate(3282, 3382, 0), new Coordinate(2383, 3382, 0), "Bedroom door", "Open", "E", new CombinedRequirement(true, new EquipmentRequirement("Priest gown bottom", 1), new EquipmentRequirement("Priest gown top", 1))),
    SAWMILL_BROKEN_FENCE(new Coordinate(3308, 3492, 0), new Coordinate(3308, 3492, 0), new Coordinate(3308, 3494, 0), "Broken fence", "Climb-over"),
    SAWMILL_LADDER_UP(new Coordinate(3310, 3509, 0), new Coordinate(3310, 3508, 0), new Coordinate(3310, 3508, 1), "Ladder", "Climb-up"),
    SAWMILL_LADDER_DOWN(new Coordinate(3310, 3509, 1), new Coordinate(3310, 3508, 1), new Coordinate(3310, 3508, 0), "Ladder", "Climb-down"),
    PIPE_ENTER_W_ARDY(new Coordinate(2514, 9737, 0), new Coordinate(2514, 9739, 0), new Coordinate(2529, 3304, 0), "Pipe", "Climb-up"),
    MANHOLE_OUT_W_ARDY(new Coordinate(2529, 3303, 0), new Coordinate(2529, 3304, 0), new Coordinate(2514, 9739, 0), "Manhole", "Climb-down"),
    DUG_HOLE_ENTER(new Coordinate(2566, 3332, 0), new Coordinate(2566, 3331, 0), new Coordinate(2518, 9759, 0), "Dug hole", "Climb-down", new QuestRequirement("Plague City", Quest.Status.IN_PROGRESS, true)),
    MUD_PILE_EXIT(new Coordinate(2519, 9759, 0), new Coordinate(2518, 9759, 0), new Coordinate(2566, 3331, 0), "Mud pile", "Climb"),
    BOOK_HOUSE_DOOR(new Coordinate(2531, 3328, 0), new Coordinate(2531, 3328, 0), new Coordinate(2531, 3329, 0), "N"),
    BOOK_HOUSE_STAIR_UP(new Coordinate(2527, 3332, 0), new Coordinate(2527, 3331, 0), new Coordinate(2527, 3331, 1), "Stairs", "Walk-up"),
    BOOK_HOUSE_STAIR_DOWN(new Coordinate(2527, 3332, 1), new Coordinate(2527, 3331, 1), new Coordinate(2527, 3331, 0), "Stairs", "Walk-down"),
    W_ARD_HALL_MAIN_DOOR_1(new Coordinate(2526, 3311, 0), new Coordinate(2526, 3311, 0), new Coordinate(2526, 3312, 0), "N"),
    W_ARD_HALL_MAIN_DOOR_2(new Coordinate(2525, 3311, 0), new Coordinate(2525, 3311, 0), new Coordinate(2525, 3312, 0), "N"),
    W_ARD_HALL_DOOR_1(new Coordinate(2530, 3314, 0), new Coordinate(2529, 3314, 0), new Coordinate(2530, 3314, 0), "W"),
    W_ARD_X_DOOR(new Coordinate(2540, 3273, 0), new Coordinate(2540, 3273, 0), new Coordinate(2540, 3272, 0), "S"),
    W_ARD_SPOOKY_STAIR_UP(new Coordinate(2536, 9671, 0), new Coordinate(2537, 9670, 0), new Coordinate(2536, 3271, 0), "Spooky stairs", "Walk-up"),
    W_ARD_SPOOKY_STAIR_DOWN(new Coordinate(2536, 3268, 0), new Coordinate(2536, 3271, 0), new Coordinate(2537, 9670, 0), "Spooky stairs", "Walk-down"),
    W_ARD_X_DOOR_IN(new Coordinate(2539, 9672, 0), new Coordinate(2539, 9672, 0), new Coordinate(2540, 9672, 0),"Door", "Open", "W", 2, false, new InventoryRequirement("A small key", 1)),
    W_ARD_X_DOOR_OUT(new Coordinate(2539, 9672, 0), new Coordinate(2540, 9672, 0), new Coordinate(2539, 9672, 0), "Door", "Open", "W", 2, false),
    VARROCK_LIBRARY_DOOR(new Coordinate(3210, 3490, 0), new Coordinate(3210, 3489, 0), new Coordinate(3210, 3490, 0), "S"),
    ARTHUR_MANSION_P0_L_DOOR_1(new Coordinate(2757, 3503, 0), new Coordinate(2757, 3502, 0), new Coordinate(2757, 3503, 0), "Large door", "Open", "S"),
    ARTHUR_MANSION_P0_L_DOOR_2(new Coordinate(2758, 3503, 0), new Coordinate(2758, 3502, 0), new Coordinate(2758, 3503, 0), "Large door", "Open", "S"),
    ARTHUR_MANSION_P1_L_DOOR_1(new Coordinate(2761, 3505, 1), new Coordinate(2761, 3505, 1), new Coordinate(2761, 3506, 1), "Large door", "Open", "N"),
    ARTHUR_MANSION_P1_L_DOOR_2(new Coordinate(2762, 3505, 1), new Coordinate(2762, 3505, 1), new Coordinate(2762, 3506, 1), "Large door", "Open", "N"),
    ARTHUR_MANSION_P0_E_DOOR_1(new Coordinate(2766, 3503, 0), new Coordinate(2766, 3503, 0), new Coordinate(2766, 3504, 0), "N"),
    ARTHUR_MANSION_P0_E_DOOR_2(new Coordinate(2766, 3496, 0), new Coordinate(2766, 3496, 0), new Coordinate(2766, 3495, 0), "S"),
    ARTHUR_MANSION_P0_STAIR_UP(new Coordinate(2750, 3509, 0), new Coordinate(2751, 3508, 0), new Coordinate(2751, 3513, 1), "Staircase", "Climb-up"),
    ARTHUR_MANSION_P0_STAIR_DOWN(new Coordinate(2750, 3511, 1), new Coordinate(2751, 3513, 1), new Coordinate(2751, 3508, 0), "Staircase", "Climb-down"),
    ARTHUR_MANSION_P0_LADDER_UP_1(new Coordinate(2769, 3493, 0), new Coordinate(2768, 3493, 0), new Coordinate(2768, 3493, 1), "Ladder", "Climb-up"),
    ARTHUR_MANSION_P1_LADDER_DOWN_1(new Coordinate(2769, 3493, 1), new Coordinate(2768, 3493, 1), new Coordinate(2768, 3493, 0), "Ladder", "Climb-down"),
    ARTHUR_MANSION_P1_LADDER_UP_1(new Coordinate(2767, 3491, 1), new Coordinate(2767, 3492, 1), new Coordinate(2767, 3492, 2), "Ladder", "Climb-up"),
    ARTHUR_MANSION_P2_LADDER_DOWN_1(new Coordinate(2767, 3491, 2), new Coordinate(2767, 3492, 2), new Coordinate(2767, 3492, 1), "Ladder", "Climb-down"),
    ARTHUR_MANSION_P1_DOOR_1(new Coordinate(2764, 3503, 1), new Coordinate(2764, 3503, 1), new Coordinate(2765, 3503, 1), "E"),
    VARROCK_CHAOS_ALTAR_DOOR(new Coordinate(3255, 3388, 0), new Coordinate(3255, 3388, 0), new Coordinate(3256, 3388, 0), "E"),
    CATHERBY_PESTICIDE_DOOR_1(new Coordinate(2806, 3452, 0), new Coordinate(2805, 3452, 0), new Coordinate(2806, 3452, 0), "W"),
    LA_FEYE_L_DOOR_1(new Coordinate(2776, 3401, 0), new Coordinate(2776, 3401, 0), new Coordinate(2775, 3401, 0), toArray("Large door"), "W"),
    LA_FEYE_L_DOOR_2(new Coordinate(2776, 3400, 0), new Coordinate(2776, 3400, 0), new Coordinate(2775, 3400, 0), toArray("Large door"), "W"),
    LA_FEYE_P0_STAIR_UP(new Coordinate(2769, 3404, 0), new Coordinate(2769, 3403, 0), new Coordinate(2769, 3407, 1), "Staircase", "Climb-up"),
    LA_FEYE_P1_STAIR_DOWN(new Coordinate(2769, 3405, 1), new Coordinate(2769, 3407, 1), new Coordinate(2769, 3403, 0), "Staircase", "Climb-down"),
    LA_FEYE_P1_STAIR_UP(new Coordinate(2769, 3398, 1), new Coordinate(2769, 3397, 1), new Coordinate(2769, 3401, 2), "Staircase", "Climb-up"),
    LA_FEYE_P2_STAIR_DOWN(new Coordinate(2769, 3399, 2), new Coordinate(2769, 3401, 2), new Coordinate(2769, 3397, 1), "Staircase", "Climb-down"),
    GENDER_SWAP_DOOR(new Coordinate(2922, 3323, 0), new Coordinate(2922, 3323, 0), new Coordinate(2921, 3323, 0), "W"),
    FAL_CASTLE_EAST_DOOR_1(new Coordinate(2981, 3340, 0), new Coordinate(2981, 3340, 0), new Coordinate(2982, 3340, 0), toArray("Castle door"), "E"),
    FAL_CASTLE_EAST_DOOR_2(new Coordinate(2981, 3341, 0), new Coordinate(2981, 3341, 0), new Coordinate(2982, 3341, 0), toArray("Castle door"), "W"),
    FAL_CASTLE_EAST_DOOR_3(new Coordinate(2985, 3341, 0), new Coordinate(2985, 3341, 0), new Coordinate(2986, 3341, 0), "E"),
    FAL_CASTLE_EAST_DOOR_4(new Coordinate(2991, 3341, 0), new Coordinate(2990, 3341, 0), new Coordinate(2991, 3341, 0), "W"),
    FAL_CASTLE_E_P1_DOOR_1(new Coordinate(2991, 3341, 1), new Coordinate(2991, 3341, 1), new Coordinate(2990, 3341, 1), "W"),
    FAL_CASTLE_E_P2_DOOR_1(new Coordinate(2982, 3337, 2), new Coordinate(2982, 3337, 2), new Coordinate(2982, 3336, 2), "S"),
    FAL_CASTLE_E_P0_LADDER_UP(new Coordinate(2994, 3341, 0), new Coordinate(2993, 3341, 0), new Coordinate(2993, 3341, 1), "Ladder", "Climb-up"),
    FAL_CASTLE_E_P1_LADDER_DOWN(new Coordinate(2994, 3341, 1), new Coordinate(2993, 3341, 1), new Coordinate(2993, 3341, 0), "Ladder", "Climb-down"),
    FAL_CASTLE_E_P1_LADDER_UP(new Coordinate(2996, 3341, 1), new Coordinate(2995, 3341, 1), new Coordinate(2995, 3341, 2), "Ladder", "Climb-up"),
    FAL_CASTLE_E_P2_LADDER_DOWN(new Coordinate(2996, 3341, 2), new Coordinate(2995, 3341, 2), new Coordinate(2995, 3341, 1), "Ladder", "Climb-down"),
    FAL_CASTLE_E_P1_STAIR_UP(new Coordinate(2984, 3337, 1), new Coordinate(2984, 3336, 1), new Coordinate(2984, 3340, 2), "Staircase", "Climb-up"),
    FAL_CASTLE_E_P2_STAIR_DOWN(new Coordinate(2984, 3338, 2), new Coordinate(2984, 3340, 2), new Coordinate(2984, 3336, 1), "Staircase", "Climb-down"),
    WITCH_HOUSE_STAIR_UP(new Coordinate(2906, 3469, 0), new Coordinate(2906, 3468, 0), new Coordinate(906, 3472, 1), "Staircase", "Walk-up"),
    WTICH_HOUSE_STAIR_DOWN(new Coordinate(2906, 3470, 1), new Coordinate(906, 3472, 1), new Coordinate(2906, 3468, 0), "Staircase", "Walk-down"),
    WITCH_HOUSE_LADDER_UP(new Coordinate(2907, 9876, 0), new Coordinate(2906, 9876, 0), new Coordinate(2906, 3476, 0), "Ladder", "Climb-up"),
    WITCH_HOUSE_LADDER_DOWN(new Coordinate(2907, 3476, 0), new Coordinate(2906, 3476, 0), new Coordinate(2906, 9876, 0), "Ladder", "Climb-down"),
    WITCH_HOUSE_GATE_1(new Coordinate(2902, 9873, 0), new Coordinate(2903, 9873, 0), new Coordinate(2902, 9873, 0), "Gate", "Open", "E"),
    WITCH_HOUSE_GATE_2(new Coordinate(2902, 9874, 0), new Coordinate(2903, 9874, 0), new Coordinate(2902, 9874, 0), "Gate", "Open", "E"),
    WITCH_HOUSE_DOOR_1(new Coordinate(2900, 3473, 0), new Coordinate(2900, 3473, 0), new Coordinate(2901, 3473, 0), "E", new InventoryRequirement("Door key", 2409, 1)),
    WITCH_HOUSE_DOOR_2(new Coordinate(2901, 3465, 0), new Coordinate(2901, 3466, 0), new Coordinate(2901, 3465, 0), "N"),
    WITCH_HOUSE_DOOR_3(new Coordinate(2902, 3467, 0), new Coordinate(2902, 3478, 0), new Coordinate(2902, 3467, 0), "N"),
    WITCH_HOUSE_DOOR_4(new Coordinate(2902, 3474, 0), new Coordinate(2902, 3474, 0), new Coordinate(2901, 3475, 0), "N"),
    WITCH_HOUSE_DDOR_5(CrossableObject.WITCH_HOUSE_SHED_DOOR, new Coordinate(2933, 3463, 0), CrossableObject.WITCH_HOUSE_SHED_DOOR.getOutput(), 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, false, new InventoryRequirement("Key", 2411, 1)),
    WITCH_HOUSE_DOOR_6(new Coordinate(2934, 3463, 0), new Coordinate(2934, 3463, 0), new Coordinate(2933, 3463, 0), "W", false),
    SINCLAIR_L_DOOR_1(new Coordinate(2741, 3572, 0), new Coordinate(2741, 3572, 0), new Coordinate(2741, 3573, 0), toArray("Large door"), "N"),
    SINCLAIR_L_DOOR_2(new Coordinate(2740, 3572, 0), new Coordinate(2740, 3572, 0), new Coordinate(2740, 3573, 0), toArray("Large door"), "N"),
    SINCLAIR_P0_DOOR_1(new Coordinate(2731, 3579, 0), new Coordinate(2731, 3579, 0), new Coordinate(2731, 3580, 0), "N"),
    SINCLAIR_P0_DOOR_2(new Coordinate(2745, 3575, 0), new Coordinate(2744, 3575, 0), new Coordinate(2745, 3575, 0), "W"),
    SINCLAIR_P0_DOOR_3(new Coordinate(2746, 3576, 0), new Coordinate(2746, 3576, 0), new Coordinate(2746, 3577, 0), "N"),
    SINCLAIR_P0_DOOR_4(new Coordinate(2741, 3576, 0), new Coordinate(2741, 3575, 0), new Coordinate(2741, 3576, 0), "S"),
    SINCLAIR_P0_DOOR_5(new Coordinate(2738, 3578, 0), new Coordinate(2738, 3578, 0), new Coordinate(2737, 3578, 0), "W"),
    SINCLAIR_P0_DOOR_6(new Coordinate(2735, 3575, 0), new Coordinate(2736, 3575, 0), new Coordinate(2735, 3575, 0), "E"),
    SINCLAIR_P0_DOOR_7(new Coordinate(2735, 3578, 0), new Coordinate(2736, 3578, 0), new Coordinate(2735, 3578, 0), "E"),
    SINCLAIR_P0_DOOR_8(new Coordinate(2735, 3580, 0), new Coordinate(2736, 3580, 0), new Coordinate(2735, 3580, 0), "E"),
    SINCLAIR_STAIR_UP(new Coordinate(2736, 3581, 0), new Coordinate(2736, 3580, 0), new Coordinate(2736, 3580, 1), "Staircase", "Climb-up"),
    SINCLAIR_STAIR_DOWN(new Coordinate(2736, 3581, 1), new Coordinate(2736, 3580, 1), new Coordinate(2736, 3580, 0), "Staircase", "Climb-down"),
    SINCLAIR_P1_DOOR_1(new Coordinate(2735, 3580, 1), new Coordinate(2736, 3580, 1), new Coordinate(2735, 3580, 1), "E"),
    SINCLAIR_P1_DOOR_2(new Coordinate(2735, 3578, 1), new Coordinate(2736, 3578, 1), new Coordinate(2735, 3578, 1), "E"),
    SINCLAIR_P1_DOOR_3(new Coordinate(2736, 3577, 1), new Coordinate(2736, 3577, 1), new Coordinate(2736, 3576, 1), "S"),
    SINCLAIR_P1_DOOR_4(new Coordinate(2740, 3578, 1), new Coordinate(2740, 3577, 1), new Coordinate(2740, 3578, 1), "S"),
    SINCLAIR_P1_DOOR_5(new Coordinate(2744, 3577, 1), new Coordinate(2744, 3577, 1), new Coordinate(2744, 3576, 1), "S"),
    SINCLAIR_P1_DOOR_6(new Coordinate(2745, 3578, 1), new Coordinate(2744, 3579, 1), new Coordinate(2745, 3578, 1), "W"),
    SINCLAIR_P1_DOOR_7(new Coordinate(2745, 3581, 1), new Coordinate(2744, 3581, 1), new Coordinate(2745, 3581, 1), "W"),
    PIP_MONASTERY_CELL_DOOR(new Coordinate(3415, 3489, 2), new Coordinate(3415, 3489, 2), new Coordinate(3416, 3489, 2), "E"),
    PIP_MONASTERY_L_DOOR_1(new Coordinate(3408, 3488, 0), new Coordinate(3408, 3488, 0), new Coordinate(3409, 3488, 0), toArray("Large door"), "E"),
    PIP_MONASTERY_L_DOOR_2(new Coordinate(3408, 3489, 0), new Coordinate(3408, 3489, 0), new Coordinate(3409, 3489, 0), toArray("Large door"), "E"),
    PIP_MONASTERY_P0_STAIR_UP(new Coordinate(3417, 3484, 0), new Coordinate(3417, 3486, 0), new Coordinate(3416, 3485, 1), "Staircase", "Climb-up"),
    PIP_MONASTERY_P1_STAIR_DOWN(new Coordinate(3417, 3485, 1), new Coordinate(3416, 3485, 1), new Coordinate(3417, 3486, 0), "Staircase", "Climb-down"),
    PIP_MONASTERY_P1_LADDER_UP(new Coordinate(3410, 3485, 1), new Coordinate(3411, 3485, 1), new Coordinate(3411, 3485, 2), "Ladder", "Climb-up"),
    PIP_MONASTERY_P2_LADDER_DOWN(new Coordinate(3410, 3485, 2), new Coordinate(3411, 3485, 2), new Coordinate(3411, 3485, 1), "Ladder", "Climb-down"),
    SIR_GALAHAD_DOOR(new Coordinate(2609, 3474, 0), new Coordinate(2608, 3474, 0), new Coordinate(2609, 3474, 0), "W"),
    //draynor manor
    DRAY_MANOR_L_ENTER_1(new Coordinate(3109, 3353, 0), new Coordinate(3109, 3353, 0), new Coordinate(3109, 3354, 0), "N", false),
    DRAY_MANOR_L_ENTER_2(new Coordinate(3108, 3353, 0), new Coordinate(3108, 3353, 0), new Coordinate(3108, 3354, 0), "N", false),
    DRAY_MANOR_P0_DOOR_1(new Coordinate(3109, 3358, 0), new Coordinate(3109, 3357, 0), new Coordinate(3109, 3358, 0), "S"),
    DRAY_MANOR_P0_DOOR_2(new Coordinate(3106, 3368, 0), new Coordinate(3106, 3368, 0), new Coordinate(3106, 3369, 0), "N"),
    DRAY_MANOR_P0_DOOR_3(new Coordinate(3120, 3356, 0), new Coordinate(3119, 3356, 0), new Coordinate(3120, 3356, 0), "W"),
    DRAY_MANOR_P0_EXIT(new Coordinate(3123, 3361, 0), new Coordinate(3123, 3360, 0), new Coordinate(3123, 3361, 0), "S" , false),
    DRAY_MANOR_P2_DOOR_1(new Coordinate(3106, 3361, 2), new Coordinate(3106, 3362, 2), new Coordinate(3106, 3361, 2), "N"),
    DRAY_MANOR_P0_STAIR_UP(new Coordinate(3108, 3362, 0), new Coordinate(3108, 3361, 0), new Coordinate(3108, 3366, 1), "Staircase", "Climb-up"),
    DRAY_MANOR_P1_STAIR_DOWN(new Coordinate(3108, 3364, 1), new Coordinate(3108, 3366, 1), new Coordinate(3108, 3361, 0), "Staircase", "Climb-down"),
    DRAY_MANOR_P1_STAIR_UP(new Coordinate(3104, 3362, 1), new Coordinate(3106, 3363, 1), new Coordinate(3105, 3364, 2), "Staircase", "Climb-up"),
    DRAY_MANOR_P2_STAIR_DOWN(new Coordinate(3105, 3363, 2), new Coordinate(3105, 3364, 2), new Coordinate(3106, 3363, 1), "Staircase", "Climb-down"),
    GOB_VILL_EAST_B_DOOR(new Coordinate(2958, 3506, 0), new Coordinate(2958, 3506, 0), new Coordinate(2959, 3506, 0), "E"),
    //Fisher Realm 1-2
    F_R_1_P0_DOOR_1(new Coordinate(2761, 4684, 0), new Coordinate(2761, 4684, 0), new Coordinate(2761, 4683, 0), "S"),
    F_R_1_P1_DOOR_1(new Coordinate(2766, 4684, 1), new Coordinate(2765, 4684, 1), new Coordinate(2766, 4684, 1), "W"),
    F_R_1_P1_DOOR_2(new Coordinate(2766, 4688, 1), new Coordinate(2765, 4688, 1), new Coordinate(2767, 4688, 1), null),
    F_R_1_STAIR_UP(new Coordinate(2761, 4680, 0), new Coordinate(2763, 4681, 0), new Coordinate(2762, 4682, 1), "Staircase", "Climb-up"),
    F_R_1_STAIR_DOWN(new Coordinate(2762, 4681, 1), new Coordinate(2762, 4682, 1), new Coordinate(2763, 4681, 0), "Staircase", "Climb-down"),
    F_R_2_L_DOOR_1(new Coordinate(2634, 4693, 0), new Coordinate(2634, 4693, 0), new Coordinate(2634, 4692, 0), toArray("Large door"), "S"),
    F_R_2_L_DOOR_2(new Coordinate(2635, 4693, 0), new Coordinate(2635, 4693, 0), new Coordinate(2635, 4692, 0), toArray("Large door"), "S"),
    F_R_2_P0_DOOR_1(new Coordinate(2638, 4688, 0), new Coordinate(2638, 4689, 0), new Coordinate(2638, 4687, 0), null),
    F_R_2_P0_DOOR_2(new Coordinate(2633, 4684, 0), new Coordinate(2633, 4684, 0), new Coordinate(2633, 4683, 0), "S"),
    F_R_2_P0_DOOR_3(new Coordinate(2645, 4684, 0), new Coordinate(2645, 4684, 0), new Coordinate(2646, 4684, 0), "E"),
    F_R_2_P1_DOOR_1(new Coordinate(2638, 4684, 1), new Coordinate(2637, 4684, 1), new Coordinate(2638, 4684, 1), "W"),
    F_R_2_P1_DOOR_2(new Coordinate(2638, 4688, 1), new Coordinate(2639, 4688, 1), new Coordinate(2637, 4688, 1), null),
    F_R_2_P0_STAIR_UP_1(new Coordinate(2633, 4680, 0), new Coordinate(2635, 4681, 0), new Coordinate(2634, 4682, 1), "Staircase", "Climb-up"),
    F_R_2_P1_STAIR_DOWN_1(new Coordinate(2634, 4681, 1), new Coordinate(2634, 4682, 1), new Coordinate(2635, 4681, 0), "Staircase", "Climb-down"),
    F_R_2_P0_STAIR_UP_2(new Coordinate(2648, 4683, 0), new Coordinate(2650, 4684, 0), new Coordinate(2649, 4685, 1), "Staircase", "Climb-up"),
    F_R_2_P1_STAIR_DOWN_2(new Coordinate(2649, 4684, 1), new Coordinate(2649, 4685, 1), new Coordinate(2650, 4684, 0), "Staircase", "Climb-down"),
    F_R_2_P1_LADDER_UP(new Coordinate(2651, 4684, 1), new Coordinate(2650, 4684, 1), new Coordinate(2650, 4684, 2), "Ladder", "Climb-up"),
    F_R_2_P2_LADDER_DOWN(new Coordinate(2651, 4684, 2), new Coordinate(2650, 4684, 2), new Coordinate(2650, 4684, 1), "Ladder", "Climb-down"),
    F_R_1_WHISTLE_TO(CrossableObject.FISHER_REALM_1_WHISTLE, new Coordinate(2741, 3235, 0), CrossableObject.FISHER_REALM_1_WHISTLE.getOutput(), 5, 0, false,
            new CombinedRequirement(true, new CombinedRequirement(true, new InventoryRequirement("Holy table napkin", 1), new InventoryRequirement("Magic whistle", 1)),
                    new AreaRequirement(new Area.Rectangular(new Coordinate(2741, 3235, 0), new Coordinate(2742, 3236, 0))))),
    F_R_2_WHISTLE_TO(CrossableObject.FISHER_REALM_2_WHISTLE, new Coordinate(2741, 3235, 0), CrossableObject.FISHER_REALM_2_WHISTLE.getOutput(), 5, 0, false,
            new CombinedRequirement(true, new CombinedRequirement(true, new InventoryRequirement("Holy table napkin", 1), new InventoryRequirement("Magic whistle", 1)),
                    new AreaRequirement(new Area.Rectangular(new Coordinate(2741, 3235, 0), new Coordinate(2742, 3236, 0))))),
    EXAM_CENTRE_DOOR_1(new Coordinate(3357, 3344, 0), new Coordinate(3356, 3344, 0), new Coordinate(3357, 3344, 0), "W"),
    EXAM_CENTRE_DOOR_2(new Coordinate(3352, 3337, 0), new Coordinate(3352, 3338, 0), new Coordinate(3352, 3337, 0), "N"),
    DIG_WINCH_1_1(new Coordinate(3352, 3417, 0), new Coordinate(3354, 3417, 0), new Coordinate(3369, 9827, 0), "Winch", "Operate",
            new QuestRequirement("The Dig Site", Quest.Status.IN_PROGRESS)),
    DIG_WINCH_1_2(new Coordinate(3352, 3417, 0), new Coordinate(3354, 3417, 0), new Coordinate(3369, 9763, 0), "Winch", "Operate",
            new CombinedRequirement(false, new QuestRequirement("The Dig Site", Quest.Status.IN_PROGRESS),
                    new QuestRequirement("The Dig Site", Quest.Status.COMPLETE))),
    DIG_WINCH_2_1(new Coordinate(3370, 3428, 0), new Coordinate(3370, 3427, 0), new Coordinate(3352, 9817, 0), "Winch", "Operate",
                    new QuestRequirement("The Dig Site", Quest.Status.IN_PROGRESS)),
    DIG_ROPE_UP_1_1(new Coordinate(3369, 9826, 0), new Coordinate(3369, 9827, 0), new Coordinate(3354, 3417, 0), "Rope", "Climb-up"),
    DIG_ROPE_UP_1_2(new Coordinate(3369, 9762, 0), new Coordinate(3369, 9763, 0), new Coordinate(3354, 3417, 0), "Rope", "Climb-up"),
    DIG_ROPE_UP_2_1(new Coordinate(3352, 9816, 0), new Coordinate(3352, 9817, 0), new Coordinate(3370, 3427, 0), "Rope", "Climb-up"),
    // x = W/E(-/+), y = N/S(+/-)

    // new Coordinate(), new Coordinate(), new Coordinate(),

    /*

     */

    //Gates and Doors
    GATE_FALADOR_1(OpenableObject.GATE_FALADOR_1, new Coordinate(2933, 3319, 0), new Coordinate(2933, 3320, 0), 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    GATE_FALADOR_2(OpenableObject.GATE_FALADOR_2, new Coordinate(2934, 3319, 0), new Coordinate(2934, 3320, 0), 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    TAVERLY_ENTRANCE_GATE_1(OpenableObject.TAVERLY_ENTRANCE_GATE_1, new Coordinate(2934, 3450, 0), new Coordinate(2936, 3450, 0), 1, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true, new SubscriptionRequirement(true)),
    TAVERLY_ENTRANCE_GATE_2(OpenableObject.TAVERLY_ENTRANCE_GATE_2, new Coordinate(2934, 3451, 0), new Coordinate(2936, 3451, 0), 1, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true, new SubscriptionRequirement(true)),
    AL_KHARID_TOLL_GATE(new Coordinate(3268, 3227, 0), new Coordinate(3267, 3227, 0), new Coordinate(3268, 3227, 0), new String[] {"Gate"}, new String[] {"Pay-toll(10gp)"}, 20, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true, new CombinedRequirement(false, new InventoryRequirement("Coins", 10), new QuestRequirement("Prince Ali Rescue", Quest.Status.COMPLETE))),
    LUMBRIDGE_TOLL_GATE(new Coordinate(3268, 3228, 0), new Coordinate(3267, 3228, 0), new Coordinate(3268, 3228, 0), new String[] {"Gate"}, new String[] {"Pay-toll(10gp)"}, 20, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true, new CombinedRequirement(false, new InventoryRequirement("Coins", 10), new QuestRequirement("Prince Ali Rescue", Quest.Status.COMPLETE))),
    VARROCK_SOUTH_FENCE(new Coordinate(3240, 3335, 0), new Coordinate(3240, 3334, 0), new Coordinate(3240, 3336, 0), new String[] {"Fence"}, new String[] {"Jump-over"}, 2, 3, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true, new SkillRequirement(Skill.AGILITY, 13)),
    PRIEST_IN_PERIL_DUNGEON_GATE_1(CrossableObject.PRIEST_IN_PERIL_GATE_1, new Coordinate(3432, 9897, 0), new Coordinate(3431, 9897, 0), 2, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true),
    PRIEST_IN_PERIL_DUNGEON_GATE_2(CrossableObject.PRIEST_IN_PERIL_GATE_2, new Coordinate(3405, 9894, 0), new Coordinate(3405, 9895, 0), 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    VARROCK_TO_PRIEST_IN_PERIL_GATE(new Coordinate(3319, 3468, 0), new Coordinate(3319, 3468, 0), new Coordinate(3320, 3468, 0), new String[] {"Gate"}, new String[] {"Open"}, 1, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true, new SubscriptionRequirement(true)),
    VARROCK_TO_PRIEST_IN_PERIL_GATE_1(new Coordinate(3319, 3467, 0), new Coordinate(3319, 3467, 0), new Coordinate(3320, 3467, 0), new String[] {"Gate"}, new String[] {"Open"}, 1, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true, new SubscriptionRequirement(true)),
    VARROCK_TO_DIGSITE_GATE_1(new Coordinate(3312, 3331, 0), new Coordinate(3311, 3331, 0), new Coordinate(3313, 3331, 0), new String[] {"Gate"}, new String[] {"Open"}, 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true, new SubscriptionRequirement(true)),
    VARROCK_TO_DIGSITE_GATE_2(new Coordinate(3312, 3332, 0), new Coordinate(3311, 3332, 0), new Coordinate(3313, 3332, 0), new String[] {"Gate"}, new String[] {"Open"}, 1, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true, new SubscriptionRequirement(true)),
    CRAFTING_GUILD_DOOR(new Coordinate(2933, 3289, 0), new Coordinate(2933, 3290, 0), new Coordinate(2933, 3288, 0), new String[] {"Guild Door"}, new String[]{"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true, new SkillRequirement(Skill.CRAFTING, 40)),
    EDGEVILLE_DUNGEON_ENTRANCE_GATE(new Coordinate(3103, 9910, 0), new Coordinate(3104, 9910, 0), new Coordinate(3102, 9910, 0), new String[]{"Gate"}, new String[]{"Open"}, 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    EDGEVILLE_DUNGEON_ENTRANCE_GATE_1(new Coordinate(3103, 9909, 0), new Coordinate(3104, 9909, 0), new Coordinate(3102, 9909, 0), new String[]{"Gate"}, new String[]{"Open"}, 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    GNOME_STRONGHOLD_GATE_ENTRANCE(new Coordinate(2459, 3383, 0), new Coordinate(2461, 3385, 0), new Coordinate(2461, 3382, 0), new String[]{"Gate"}, new String[]{"Open"}, 2, Region.CollisionFlags.BLOCKED_TILE, true),
    LUMBRIDGE_DOOR_TO_ZANARIS(OpenableObject.LUMBRIDGE_TO_ZANARIS_DOOR, new Coordinate(3201, 3169, 0), new Coordinate(2452, 4472, 0), 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, false, new QuestRequirement("Lost City", Quest.Status.COMPLETE), Requirements.DRAMEN_STAFF.getRequirements()),
    GRAND_TREE_ENTRANCE_DOOR(OpenableObject.GRAND_TREE_ENTRANCE_DOOR, new Coordinate(2466, 3491, 0), new Coordinate(2466, 3493, 0), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    GRAND_TREE_ENTRANCE_DOOR_2(OpenableObject.GRAND_TREE_ENTRANCE_DOOR_2, new Coordinate(2465, 3491, 0), new Coordinate(2465, 3493, 0), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    //Currently object spot not exists
    ROGUES_DEN_DOOR(new Coordinate(3061, 4984, 1), new Coordinate(3061, 4983, 1), new Coordinate(3061, 4985, 1), new String[]{"Door"}, new String[]{"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    BURTHORPE_BAR_DOOR_NORTH(new Coordinate(2907, 3544, 0), new Coordinate(2907, 3543, 0), new Coordinate(2907, 3545, 0), new String[]{"Door"}, new String[]{"Open"}, 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true),
    SLAYER_TOWER_ENTRANCE_DOOR(new Coordinate(3429, 3535, 0), new Coordinate(3429, 3534, 0), new Coordinate(3429, 3536, 0), new String[] {"Door"}, new String[] {"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    SLAYER_TOWER_ENTRANCE_DOOR_1(new Coordinate(3428, 3535, 0), new Coordinate(3428, 3534, 0), new Coordinate(3428, 3536, 0), new String[] {"Door"}, new String[] {"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    LUMBRIDGE_COWS_GATE_1(OpenableObject.GATE_LUMBRIDGE_COWS, new Coordinate(3251, 3267, 0), new Coordinate(3254, 3267, 0), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    LUMBRIDGE_COWS_GATE_2(OpenableObject.GATE_LUMBRIDGE_COWS_2, new Coordinate(3251, 3266, 0), new Coordinate(3254, 3266, 0), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    YANILLE_GATE_OUTER_1(OpenableObject.YANILLE_GATE_OUTER, new Coordinate(2531, 3091, 0), new Coordinate(2534, 3091, 0), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    YANILLE_GATE_OUTER_2(OpenableObject.YANILLE_GATE_OUTER_2, new Coordinate(2531, 3092, 0), new Coordinate(2534, 3092, 0), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    YANILLE_GATE_INNER_1(OpenableObject.YANILLE_GATE_INNER, new Coordinate(2537, 3091, 0), new Coordinate(2540, 3091, 0), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    YANILLE_GATE_INNER_2(OpenableObject.YANILLE_GATE_INNER_2, new Coordinate(2537, 3092, 0), new Coordinate(2540, 3092, 0), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    SLAYER_TOWER_FIRST_FLOOR_DOOR(OpenableObject.SLAYER_TOWER_FIRST_FLOOR_DOOR, new Coordinate(3426, 3554, 1), new Coordinate(3426, 3556, 1), 2, Region.CollisionFlags.BLOCKED_TILE, true, Requirements.NOSE_PEG.getRequirements()),
    SLAYER_TOWER_FIRST_FLOOR_DOOR_1(OpenableObject.SLAYER_TOWER_FIRST_FLOOR_DOOR_1, new Coordinate(3427, 3554, 1), new Coordinate(3427, 3556, 1), 2, Region.CollisionFlags.BLOCKED_TILE, true, Requirements.NOSE_PEG.getRequirements()),
    MUSA_POINT_BRIMHAVEN_GATE(OpenableObject.MUSA_POINT_BRIMHAVEN_GATE_1, new Coordinate(2817, 3182, 0), new Coordinate(2815, 3182, 0), 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    MUSA_POINT_BRIMHAVEN_GATE_1(OpenableObject.MUSA_POINT_BRIMHAVEN_GATE_2, new Coordinate(2817, 3183, 0), new Coordinate(2815, 3183, 0), 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    WITCHAVEN_HELLHOUND_DOOR(OpenableObject.WITCHAVEN_HELLHOUNDS_DOOR, new Coordinate(2726, 9690, 0), new Coordinate(2728, 9690, 0), 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),

    WATERFALL_GATE_OUTER(OpenableObject.WATERFALL_GATE_OUTER, new Coordinate(2528, 3495, 0), new Coordinate(2527, 3495, 0), 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    WATERFALL_GATE_OUTER_1(OpenableObject.WATERFALL_GATE_OUTER_1, new Coordinate(2528, 3496, 0), new Coordinate(2527, 3496, 0), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    WATERFALL_HOUSE_DOOR(OpenableObject.WATERFALL_HOUSE_DOOR, new Coordinate(2525, 3495, 0), new Coordinate(2524, 3495, 0), 2, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true),
    WATERFALL_GATE_INNER(OpenableObject.WATERFALL_GATE_INNER, new Coordinate(2512, 3494, 0), new Coordinate(2513, 3494, 0), 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    WATERFALL_GATE_INNER_1(OpenableObject.WATERFALL_GATE_INNER_1, new Coordinate(2512, 3495, 0), new Coordinate(2513, 3495, 0), 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),
    WATERFALL_LOG_RAFT(CrossableObject.WATERFALL_LOG_RAFT, new Coordinate(2510, 3494, 0), new Coordinate(2512, 3481, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    WATERFALL_DUNGEON_DOOR(CrossableObject.WATERFALL_DUNGEON_DOOR, CrossableObject.WATERFALL_DUNGEON_DOOR_1.getOutput(), CrossableObject.WATERFALL_DUNGEON_DOOR.getOutput(), 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, false),
    WATERFALL_DUNGEON_DOOR_1(CrossableObject.WATERFALL_DUNGEON_DOOR_1, CrossableObject.WATERFALL_DUNGEON_DOOR.getOutput(), CrossableObject.WATERFALL_DUNGEON_DOOR_1.getOutput(), 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, false),
    WATERFALL_DUNGEON_FIRE_GIANT_ROOM_DOOR_OUTER(OpenableObject.WATERFALL_DUNGEON_FIRE_GIANT_ROOM_DOOR_OUTER, new Coordinate(2576, 9881, 0), new Coordinate(2576, 9883, 0), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    WATERFALL_DUNGEON_FIRE_GIANT_ROOM_DOOR_INNER(OpenableObject.WATERFALL_DUNGEON_FIRE_GIANT_ROOM_DOOR_INNER, new Coordinate(2576, 9883, 0), new Coordinate(2576, 9885, 0), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    WATERFALL_EXIT_BARREL(CrossableObject.WATERFALL_EXIT_BARREL, new Coordinate(2511, 3463, 0), CrossableObject.WATERFALL_EXIT_BARREL.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    WATERFALL_ROPE_ON_ROCK(CrossableObject.WATERFALL_ROPE_ON_ROCK, new Coordinate(2512, 3476, 0), CrossableObject.WATERFALL_ROPE_ON_ROCK.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new InventoryRequirement("Rope", 1)),
    WATERFALL_ROPE_ON_TREE(CrossableObject.WATERFALL_ROPE_ON_TREE, new Coordinate(2513, 3465, 0), CrossableObject.WATERFALL_ROPE_ON_TREE.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new InventoryRequirement("Rope", 1)),
    WATERFALL_DUNGEON_LEFT_DOOR_FIRST(new Coordinate(2565, 9881, 0), new Coordinate(2565, 9881, 0), new Coordinate(2565, 9882, 0), new String[] {"Large door"}, new String[]{"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    //WATERFALL_DUNGEON_LEFT_DOOR_FIRST_1(new Coordinate(2564, 9881, 0), new Coordinate(2564, 9881, 0), new Coordinate(2564, 9882, 0), new String[] {"Large door"}, new String[]{"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    WATERFALL_DUNGEON_LEFT_DOOR_SECOND(new Coordinate(2568, 9893, 0), new Coordinate(2568, 9893, 0), new Coordinate(2568, 9894, 0), new String[] {"Door"}, new String[]{"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true, new InventoryRequirement("Key", 298, 1)),
    WATERFALL_DUNGEON_LEFT_DOOR_THIRD(new Coordinate(2566, 9901, 0), new Coordinate(2566, 9901, 0), new Coordinate(2566, 9902, 0), new String[] {"Door"}, new String[]{"Open"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),
    WATERFALL_DUNGEON_RIGHT_DOOR(new Coordinate(2582, 9875, 0), new Coordinate(2581, 9875, 0), new Coordinate(2582, 9875, 0), new String[] {"Large door"}, new String[]{"Open"}, 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true),

    LUMBRIDGE_CELLAR_HOLE(CrossableObject.LUMBRIDGE_TO_CELLAR_HOLE, CrossableObject.LUMBRIDGE_TO_CELLAR_HOLE_1.getOutput(), CrossableObject.LUMBRIDGE_TO_CELLAR_HOLE.getOutput(), 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, false),
    LUMBRIDGE_CELLAR_HOLE_1(CrossableObject.LUMBRIDGE_TO_CELLAR_HOLE_1, CrossableObject.LUMBRIDGE_TO_CELLAR_HOLE.getOutput(), CrossableObject.LUMBRIDGE_TO_CELLAR_HOLE_1.getOutput(), 2, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, false),
    LUMBRIDGE_COOK_TUTOR_DOOR(OpenableObject.LUMBRIDGE_COOK_TUTOR_DOOR, OpenableObject.LUMBRIDGE_COOK_TUTOR_DOOR.getPosition().derive(0, 1),OpenableObject.LUMBRIDGE_COOK_TUTOR_DOOR.getPosition(), 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true),

    SHILO_INNER_GATE(CrossableObject.SHILO_INNER_GATE_OUT, CrossableObject.SHILO_INNER_GATE_OUT.getPosition(), CrossableObject.SHILO_INNER_GATE_OUT.getOutput(), 2, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, true),
    //SHILO_INNER_GATE_1(CrossableObject.SHILO_INNER_GATE_IN, CrossableObject.SHILO_INNER_GATE_OUT.getOutput(), CrossableObject.SHILO_INNER_GATE_IN.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    SHILO_METAL_GATE(CrossableObject.SHILO_METAL_GATE_OUT, CrossableObject.SHILO_METAL_GATE_OUT.getPosition(), CrossableObject.SHILO_METAL_GATE_OUT.getOutput(), 2, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, true, true),
    //SHILO_METAL_GATE_1(CrossableObject.SHILO_METAL_GATE_IN, CrossableObject.SHILO_METAL_GATE_OUT.getOutput(), CrossableObject.SHILO_METAL_GATE_IN.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    SHILO_BROKEN_CART(CrossableObject.SHILO_BROKEN_CART_OUT, CrossableObject.SHILO_BROKEN_CART_IN.getOutput(), CrossableObject.SHILO_BROKEN_CART_OUT.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    SHILO_BROKEN_CART_1(CrossableObject.SHILO_BROKEN_CART_IN, CrossableObject.SHILO_BROKEN_CART_OUT.getOutput(), CrossableObject.SHILO_BROKEN_CART_IN.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    SHILO_SLAYER_LADDER(CrossableObject.SHILO_SLAYER_LADDER_UP, CrossableObject.SHILO_SLAYER_LADDER_DOWN.getOutput(), CrossableObject.SHILO_SLAYER_LADDER_UP.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    SHILO_SLAYER_LADDER_1(CrossableObject.SHILO_SLAYER_LADDER_DOWN, CrossableObject.SHILO_SLAYER_LADDER_UP.getOutput(), CrossableObject.SHILO_SLAYER_LADDER_DOWN.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    ZANARIS_MYSTERIOUS_RUINS(CrossableObject.ZANARIS_MYSTERIOUS_RUINS, CrossableObject.COSMIC_PORTAL_ZANARIS_1.getOutput(), CrossableObject.ZANARIS_MYSTERIOUS_RUINS.getOutput(), 2, 0, false, new CombinedRequirement(false, new InventoryRequirement("Cosmic talisman", 1), new EquipmentRequirement("Cosmic tiara", 1))),
    PORTAL_ZANARIS_COSMIC_1(CrossableObject.COSMIC_PORTAL_ZANARIS_1, CrossableObject.ZANARIS_MYSTERIOUS_RUINS.getOutput(), CrossableObject.COSMIC_PORTAL_ZANARIS_1.getOutput(), 2, 0, false),
    PORTAL_ZANARIS_COSMIC_2(CrossableObject.COSMIC_PORTAL_ZANARIS_2, CrossableObject.ZANARIS_MYSTERIOUS_RUINS.getOutput(), CrossableObject.COSMIC_PORTAL_ZANARIS_2.getOutput(), 2, 0, false),
    PORTAL_ZANARIS_COSMIC_3(CrossableObject.COSMIC_PORTAL_ZANARIS_3, CrossableObject.ZANARIS_MYSTERIOUS_RUINS.getOutput(), CrossableObject.COSMIC_PORTAL_ZANARIS_3.getOutput(), 2, 0, false),
    PORTAL_ZANARIS_COSMIC_4(CrossableObject.COSMIC_PORTAL_ZANARIS_4, CrossableObject.ZANARIS_MYSTERIOUS_RUINS.getOutput(), CrossableObject.COSMIC_PORTAL_ZANARIS_4.getOutput(), 2, 0, false),
    SHILO_MYSTERIOUS_RUINS(CrossableObject.SHILO_MYSTERIOUS_RUINS, CrossableObject.NATURE_PORTAL_SHILO.getOutput(), CrossableObject.SHILO_MYSTERIOUS_RUINS.getOutput(), 2, 0, false, new CombinedRequirement(false, new InventoryRequirement("Nature talisman", 1), new EquipmentRequirement("Nature tiara", 1))),
    PORTAL_SHILO_NATURE(CrossableObject.NATURE_PORTAL_SHILO, CrossableObject.SHILO_MYSTERIOUS_RUINS.getOutput(), CrossableObject.NATURE_PORTAL_SHILO.getOutput(), 2, 0, false),

    //Spirit Trees
    SPIRIT_TREE_GE(TravelInterface.SPIRIT_TREE_GE, new Coordinate(3183, 3508, 0), TravelInterface.getOfType(TravelInterface.Type.SPIRIT_TREE), 5, 0, new QuestRequirement("Tree Gnome Village", Quest.Status.COMPLETE)),
    SPIRIT_TREE_GNOME_STRONGHOLD(TravelInterface.SPIRIT_TREE_GNOME_STRONGHOLD, new Coordinate(2461, 3444, 0), TravelInterface.getOfType(TravelInterface.Type.SPIRIT_TREE), 5, 0, new QuestRequirement("The Grand Tree", Quest.Status.COMPLETE)),
    SPIRIT_TREE_KHAZARD(TravelInterface.SPIRIT_TREE_KHAZARD, new Coordinate(2557, 3260, 0), TravelInterface.getOfType(TravelInterface.Type.SPIRIT_TREE), 5, 0, new QuestRequirement("Tree Gnome Village", Quest.Status.COMPLETE)),

    //Fairy Rings
    FAIRY_RING_CHASM_OF_FIRE(FairyRing.CHASM_OF_FIRE, new Coordinate(1454, 3658, 0), FairyRing.values(), 5, 0, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()),
    FAIRY_RING_MUSKIPPER_POINT(FairyRing.MUDSKIPPER_POINT, new Coordinate(2997, 3114, 0), FairyRing.values(), 5, 0, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()),
    FAIRY_RING_KANDARIN(FairyRing.KANDARIN, new Coordinate(2780, 3614, 0), FairyRing.values(), 5, 0, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()),
    FAIRY_RING_EDGEVILLE(FairyRing.EDGEVILLE, new Coordinate(3130, 3496, 0), FairyRing.values(), 5, 0, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()),
    FAIRY_RING_MOUNT_KARUULM(FairyRing.MOUNT_KARUULM, new Coordinate(1303, 3762, 0), FairyRing.values(), 5, 0, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()),
    FAIRY_RING_KELDAGRIM(FairyRing.KELDAGRIM_ENTRANCE, new Coordinate(2743, 3719, 0), FairyRing.values(), 5, 0, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()),
    FAIRY_RING_KHARIDIAN_DESERT(FairyRing.KHARIDIAN_DESERT, new Coordinate(3252, 3095, 0), FairyRing.values(), 5, 0, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()),
    FAIRY_RING_YANILLE(FairyRing.YANILLE, new Coordinate(2528, 3126, 0), FairyRing.values(), 5, 0, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()),
    FAIRY_RING_TOWER_OF_LIFE(FairyRing.TOWER_OF_LIFE, new Coordinate(2658, 3229, 0), FairyRing.values(), 5, 0, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()),
    FAIRY_RING_SHILO(FairyRing.SHILO_VILLAGE, new Coordinate(2801, 3002, 0), FairyRing.values(), 5, 0, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()),
    FAIRY_RING_CANIFIS(FairyRing.CANIFIS, new Coordinate(3448, 3470, 0), FairyRing.values(), 5, 0, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()),
    FAIRY_RING_ZANARIS(FairyRing.ZANARIS, new Coordinate(2412, 4435, 0), FairyRing.values(), 5, 0, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()),
    FAIRY_RING_FISHER_REALM(FairyRing.FISHER_REALM, new Coordinate(2650, 4729, 0), FairyRing.values(), 5, 0, new CombinedRequirement(true, new CombinedRequirement(true, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()), new CombinedRequirement(false, new VarpRequirement(5, 9), new QuestRequirement("Holy Grail", Quest.Status.COMPLETE)))),
    FAIRY_RING_HAUNTED_WOODS(FairyRing.HAUNTED_WOODS, new Coordinate(3596, 3495, 0), FairyRing.values(), 5, 0, new QuestRequirement("Fairytale II - Cure a Queen", Quest.Status.IN_PROGRESS), Requirements.DRAMEN_STAFF.getRequirements()),
    //TODO: Add other fairy rings we put in the enum.

    BURTHORPE_GATE_FIRST(OpenableObject.BURTHORPE_GATE_FIRST, new Coordinate(2825, 3555, 0), new Coordinate(2823, 3555, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BURTHORPE_GATE_FIRST_1(OpenableObject.BURTHORPE_GATE_FIRST_1, new Coordinate(2823, 3555, 0), new Coordinate(2825, 3555, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BURTHORPE_HOUSE_DOOR(OpenableObject.BURTHORPE_HOUSE_DOOR, new Coordinate(2823, 3555, 0), new Coordinate(2821, 3555, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BURTHORPE_HOUSE_DOOR_1(OpenableObject.BURTHORPE_HOUSE_DOOR_1, new Coordinate(2821, 3555, 0), new Coordinate(2823, 3555, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BURTHORPE_HOUSE_DOOR_PASS(OpenableObject.BURTHORPE_HOUSE_DOOR_PASS, new Coordinate(2820, 3556, 0), new Coordinate(2820, 3558, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BURTHORPE_HOUSE_DOOR_PASS_1(OpenableObject.BURTHORPE_HOUSE_DOOR_PASS_1, new Coordinate(2820, 3558, 0), new Coordinate(2820, 3556, 0), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BURTHORPE_STILE(CrossableObject.BURTHORPE_STILE, CrossableObject.BURTHORPE_STILE_1.getOutput(), CrossableObject.BURTHORPE_STILE.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BURTHORPE_STILE_1(CrossableObject.BURTHORPE_STILE_1, CrossableObject.BURTHORPE_STILE.getOutput(), CrossableObject.BURTHORPE_STILE_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    BURTHORPE_FIRST_ROCKS(CrossableObject.BURTHORPE_FIRST_ROCKS, CrossableObject.BURTHORPE_FIRST_ROCKS_1.getOutput(), CrossableObject.BURTHORPE_FIRST_ROCKS.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new EquipmentRequirement(new String[]{"Climbing boots"}, 1)),
    BURTHORPE_FIRST_ROCKS_1(CrossableObject.BURTHORPE_FIRST_ROCKS_1, CrossableObject.BURTHORPE_FIRST_ROCKS.getOutput(), CrossableObject.BURTHORPE_FIRST_ROCKS_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new EquipmentRequirement(new String[]{"Climbing boots"}, 1)),
    BURTHORPE_SECOND_ROCKS(CrossableObject.BURTHORPE_SECOND_ROCKS, CrossableObject.BURTHORPE_SECOND_ROCKS_1.getOutput(), CrossableObject.BURTHORPE_SECOND_ROCKS.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new EquipmentRequirement(new String[]{"Climbing boots"}, 1)),
    BURTHORPE_SECOND_ROCKS_1(CrossableObject.BURTHORPE_SECOND_ROCKS_1, CrossableObject.BURTHORPE_SECOND_ROCKS.getOutput(), CrossableObject.BURTHORPE_SECOND_ROCKS_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new EquipmentRequirement(new String[]{"Climbing boots"}, 1)),
    SECRET_DOOR_TO_TROLL_STRONGHOLD(CrossableObject.SECRET_DOOR_TO_TROLL_STRONGHOLD, CrossableObject.SECRET_DOOR_FROM_TROLL_STRONGHOLD.getOutput(), CrossableObject.SECRET_DOOR_TO_TROLL_STRONGHOLD.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new QuestRequirement("Troll Stronghold", Quest.Status.COMPLETE)),
    SECRET_DOOR_FROM_TROLL_STRONGHOLD(CrossableObject.SECRET_DOOR_FROM_TROLL_STRONGHOLD, CrossableObject.SECRET_DOOR_TO_TROLL_STRONGHOLD.getOutput(), CrossableObject.SECRET_DOOR_FROM_TROLL_STRONGHOLD.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    TROLL_STRONGHOLD_LOWER_STAIRS(CrossableObject.TROLL_STRONGHOLD_LOWER_STAIRS, CrossableObject.TROLL_STRONGHOLD_MIDDLE_STAIRS.getOutput(), CrossableObject.TROLL_STRONGHOLD_LOWER_STAIRS.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    TROLL_STRONGHOLD_MIDDLE_STAIRS(CrossableObject.TROLL_STRONGHOLD_MIDDLE_STAIRS, CrossableObject.TROLL_STRONGHOLD_LOWER_STAIRS.getOutput(), CrossableObject.TROLL_STRONGHOLD_MIDDLE_STAIRS.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    TROLL_STRONGHOLD_MIDDLE_PRISON_DOOR(OpenableObject.TROLL_STRONGHOLD_MIDDLE_PRISON_DOOR, new Coordinate(2847, 10107, 1), new Coordinate(2849, 10107, 1), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    TROLL_STRONGHOLD_MIDDLE_TROLL_SECTION_DOOR(OpenableObject.TROLL_STRONGHOLD_MIDDLE_TROLL_SECTION_DOOR, new Coordinate(2834, 10070, 1), new Coordinate(2832, 10070, 1), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    FREMENNIK_TO_TROLL_STRONGHOLD_ROCKS(CrossableObject.FREMENNIK_ROCKS_TO_TROLL_STRONGHOLD_LAST, CrossableObject.FREMENNIK_ROCKS_TO_TROLL_STRONGHOLD_LAST_1.getOutput(), CrossableObject.FREMENNIK_ROCKS_TO_TROLL_STRONGHOLD_LAST.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new EquipmentRequirement(new String[]{"Climbing boots"}, 1)),
    FREMENNIK_TO_TROLL_STRONGHOLD_ROCKS_1(CrossableObject.FREMENNIK_ROCKS_TO_TROLL_STRONGHOLD_LAST_1, CrossableObject.FREMENNIK_ROCKS_TO_TROLL_STRONGHOLD_LAST.getOutput(), CrossableObject.FREMENNIK_ROCKS_TO_TROLL_STRONGHOLD_LAST_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, new EquipmentRequirement(new String[]{"Climbing boots"}, 1)),

    STRONGHOLD_WAR_TO_FAMINE_LADDER(CrossableObject.STRONGHOLD_WAR_TO_FAMINE, new Coordinate(1903, 5222, 0), CrossableObject.STRONGHOLD_WAR_TO_FAMINE.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    STRONGHOLD_FAMINE_TO_WAR_LADDER(CrossableObject.STRONGHOLD_FAMINE_TO_WAR, CrossableObject.STRONGHOLD_WAR_TO_FAMINE.getOutput(), CrossableObject.STRONGHOLD_FAMINE_TO_WAR.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    STRONGHOLD_FAMINE_TO_PEST(CrossableObject.STRONGHOLD_FAMINE_TO_PESTILENCE, new Coordinate(2025, 5218, 0), CrossableObject.STRONGHOLD_FAMINE_TO_PESTILENCE.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    STRONGHOLD_PEST_TO_FAMINE(CrossableObject.STRONGHOLD_PESTILENCE_TO_FAMINE, CrossableObject.STRONGHOLD_FAMINE_TO_PESTILENCE.getOutput(), CrossableObject.STRONGHOLD_PESTILENCE_TO_FAMINE.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    STRONGHOLD_PEST_TO_DEATH(CrossableObject.STRONGHOLD_PESTILENCE_TO_DEATH, new Coordinate(2147, 5284, 0), CrossableObject.STRONGHOLD_PESTILENCE_TO_DEATH.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    STRONGHOLD_DEATH_TO_PEST(CrossableObject.STRONGHOLD_DEATH_TO_PESTILENCE, CrossableObject.STRONGHOLD_PESTILENCE_TO_DEATH.getOutput(), CrossableObject.STRONGHOLD_DEATH_TO_PESTILENCE.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    STRONGHOLD_DEATH_TO_OUT(CrossableObject.STRONGHOLD_DEATH_CHAIN_OUT, new Coordinate(2349, 5215, 0), CrossableObject.STRONGHOLD_DEATH_CHAIN_OUT.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    STRONGHOLD_WAR_PORTAL(CrossableObject.STRONGHOLD_WAR_PORTAL, new Coordinate(1863, 5239, 0), CrossableObject.STRONGHOLD_WAR_PORTAL.getOutput(), 2, 0, false, new VarbitRequirement(2309, 1)),
    STRONGHOLD_FAMINE_PORTAL(CrossableObject.STRONGHOLD_FAMINE_PORTAL, new Coordinate(2040, 5240, 0), CrossableObject.STRONGHOLD_FAMINE_PORTAL.getOutput(), 2, 0, false, new VarbitRequirement(2310, 1)),
    STRONGHOLD_PESTILENCE_PORTAL(CrossableObject.STRONGHOLD_PESTILENCE_PORTAL, new Coordinate(2120, 5257, 0), CrossableObject.STRONGHOLD_PESTILENCE_PORTAL.getOutput(), 2, 0, false, new VarbitRequirement(2311, 1)),
    STRONGHOLD_DEATH_PORTAL(CrossableObject.STRONGHOLD_DEATH_PORTAL, new Coordinate(2364, 5212, 0), CrossableObject.STRONGHOLD_DEATH_PORTAL.getOutput(), 2, 0, false, new VarbitRequirement(2312, 1)),

    BARBARIAN_VILLAGE_TO_STRONGHOLD(CrossableObject.BARBARIAN_VILLAGE_TO_STRONGHOLD, CrossableObject.STRONGHOLD_TOP_TO_BARB_VILLAGE.getOutput(), CrossableObject.BARBARIAN_VILLAGE_TO_STRONGHOLD.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    STRONGHOLD_TOP_TO_BARB_VILLAGE(CrossableObject.STRONGHOLD_TOP_TO_BARB_VILLAGE, CrossableObject.BARBARIAN_VILLAGE_TO_STRONGHOLD.getOutput(), CrossableObject.STRONGHOLD_TOP_TO_BARB_VILLAGE.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    SOUL_WARS_CAVE(CrossableObject.ISLE_OF_SOULS_CAVE, CrossableObject.ISLE_OF_SOULS_CAVE_1.getOutput(), CrossableObject.ISLE_OF_SOULS_CAVE.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    SOUL_WARS_CAVE_1(CrossableObject.ISLE_OF_SOULS_CAVE_1, CrossableObject.ISLE_OF_SOULS_CAVE.getOutput(), CrossableObject.ISLE_OF_SOULS_CAVE_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    SOUL_WARS_PORTAL(CrossableObject.SOUL_WARS_PORTAL, CrossableObject.SOUL_WARS_PORTAL_1.getOutput(), CrossableObject.SOUL_WARS_PORTAL.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    SOUL_WARS_PORTAL_1(CrossableObject.SOUL_WARS_PORTAL_1, CrossableObject.SOUL_WARS_PORTAL.getOutput(), CrossableObject.SOUL_WARS_PORTAL_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    KARAMJA_STEPPING_STONE(new Coordinate(2925, 2948, 0), new Coordinate(2925, 2947, 0), new Coordinate(2925, 2948, 0), new String[]{"Stepping stones"}, new String[]{"Cross"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, false, true),
    KARAMJA_STEPPING_STONE_1(new Coordinate(2925, 2949, 0), new Coordinate(2925, 2948, 0), new Coordinate(2925, 2949, 0), new String[]{"Stepping stones"}, new String[]{"Cross"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, true),
    KARAMJA_STEPPING_STONE_2(new Coordinate(2925, 2950, 0), new Coordinate(2925, 2949, 0), new Coordinate(2925, 2950, 0), new String[]{"Stepping stones"}, new String[]{"Cross"}, 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, false, true),
    KARAMJA_STEPPING_STONE_a(new Coordinate(2925, 2948, 0), new Coordinate(2925, 2949, 0), new Coordinate(2925, 2948, 0), new String[]{"Stepping stones"}, new String[]{"Cross"}, 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, false, true),
    KARAMJA_STEPPING_STONE_1a(new Coordinate(2925, 2949, 0), new Coordinate(2925, 2950, 0), new Coordinate(2925, 2949, 0), new String[]{"Stepping stones"}, new String[]{"Cross"}, 2, Region.CollisionFlags.BLOCKED_TILE, false, true),
    KARAMJA_STEPPING_STONE_2a(new Coordinate(2925, 2950, 0), new Coordinate(2925, 2951, 0), new Coordinate(2925, 2950, 0), new String[]{"Stepping stones"}, new String[]{"Cross"}, 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, false, true),

    HAM_STOREROOM_DOOR_1(CrossableObject.HAM_STOREROOM_LOCKED_DOOR_1_IN, CrossableObject.HAM_STOREROOM_LOCKED_DOOR_1_OUT.getOutput(), CrossableObject.HAM_STOREROOM_LOCKED_DOOR_1_IN.getOutput(), 10, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, false, true),
    HAM_STOREROOM_DOOR_1a(CrossableObject.HAM_STOREROOM_LOCKED_DOOR_1_OUT, CrossableObject.HAM_STOREROOM_LOCKED_DOOR_1_IN.getOutput(), CrossableObject.HAM_STOREROOM_LOCKED_DOOR_1_OUT.getOutput(), 10, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, false, true),

    HAM_STOREROOM_DOOR_2(CrossableObject.HAM_STOREROOM_LOCKED_DOOR_2_IN, CrossableObject.HAM_STOREROOM_LOCKED_DOOR_2_OUT.getOutput(), CrossableObject.HAM_STOREROOM_LOCKED_DOOR_2_IN.getOutput(), 10, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, false, true),
    HAM_STOREROOM_DOOR_2a(CrossableObject.HAM_STOREROOM_LOCKED_DOOR_2_OUT, CrossableObject.HAM_STOREROOM_LOCKED_DOOR_2_IN.getOutput(), CrossableObject.HAM_STOREROOM_LOCKED_DOOR_2_OUT.getOutput(), 10, Region.CollisionFlags.WEST_BOUNDARY_OBJECT, false, true),

    HAM_STOREROOM_DOOR_3(CrossableObject.HAM_STOREROOM_LOCKED_DOOR_3_IN, CrossableObject.HAM_STOREROOM_LOCKED_DOOR_3_OUT.getOutput(), CrossableObject.HAM_STOREROOM_LOCKED_DOOR_3_IN.getOutput(), 10, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, false, true),
    HAM_STOREROOM_DOOR_3a(CrossableObject.HAM_STOREROOM_LOCKED_DOOR_3_OUT, CrossableObject.HAM_STOREROOM_LOCKED_DOOR_3_IN.getOutput(), CrossableObject.HAM_STOREROOM_LOCKED_DOOR_3_OUT.getOutput(), 10, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, false, true),

    HAM_STOREROOM_DOOR_4(CrossableObject.HAM_STOREROOM_LOCKED_DOOR_4_IN, CrossableObject.HAM_STOREROOM_LOCKED_DOOR_4_OUT.getOutput(), CrossableObject.HAM_STOREROOM_LOCKED_DOOR_4_IN.getOutput(), 10, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, false, true),
    HAM_STOREROOM_DOOR_4a(CrossableObject.HAM_STOREROOM_LOCKED_DOOR_4_OUT, CrossableObject.HAM_STOREROOM_LOCKED_DOOR_4_IN.getOutput(), CrossableObject.HAM_STOREROOM_LOCKED_DOOR_4_OUT.getOutput(), 10, Region.CollisionFlags.EAST_BOUNDARY_OBJECT, false, true),

    HAM_STOREROOM_CRACK_1(CrossableObject.HAM_STOREROOM_CRACK_1, CrossableObject.HAM_STOREROOM_CRACK_1.getOutput(), CrossableObject.HAM_STOREROOM_CRACK_1.getPosition(), 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true, false),
    HAM_STOREROOM_CRACK_2(CrossableObject.HAM_STOREROOM_CRACK_2, CrossableObject.HAM_STOREROOM_CRACK_2.getPosition(), CrossableObject.HAM_STOREROOM_CRACK_2.getOutput(), 2, Region.CollisionFlags.NORTH_BOUNDARY_OBJECT, true, false),

    HAM_STOREROOM_LADDER(CrossableObject.HAM_STOREROOM_LADDER, CrossableObject.HAM_HIDEOUT_TRAPDOOR.getOutput(), CrossableObject.HAM_STOREROOM_LADDER.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    HAM_HIDEOUT_TRAPDOOR(CrossableObject.HAM_HIDEOUT_TRAPDOOR, CrossableObject.HAM_STOREROOM_LADDER.getOutput(), CrossableObject.HAM_HIDEOUT_TRAPDOOR.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    HAM_HIDEOUT_DOOR(CrossableObject.HAM_HIDEOUT_DOOR, new Coordinate(3157, 9641, 0), CrossableObject.HAM_HIDEOUT_DOOR.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, true),

    HAM_HIDEOUT_LADDER(CrossableObject.HAM_HIDEOUT_LADDER, CrossableObject.HAM_ENTRANCE_TRAPDOOR.getOutput(), CrossableObject.HAM_HIDEOUT_LADDER.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    HAM_ENTRANCE_TRAPDOOR(CrossableObject.HAM_ENTRANCE_TRAPDOOR, CrossableObject.HAM_HIDEOUT_LADDER.getOutput(), CrossableObject.HAM_ENTRANCE_TRAPDOOR.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    LIZARDMAN_SHAMAN_DWELLING_ENTRANCE(CrossableObject.LIZARDMAN_SHAMAN_ENTRANCE_DWELLING, new Coordinate(1292, 3659, 0), CrossableObject.LIZARDMAN_SHAMAN_ENTRANCE_DWELLING.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    BOAT_TO_UNGAEL(CrossableObject.BOAT_TO_UNGAEL, new Coordinate(2640, 3698, 0), CrossableObject.BOAT_TO_UNGAEL.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    //READDRESS IF THEY BLOCK THE TILES OR NOT...
    SHAMAN_MYSTICAL_BARRIER_1(CrossableObject.SHAMAN_MYSTICAL_BARRIER_1, new Coordinate(1292, 10091, 0), CrossableObject.SHAMAN_MYSTICAL_BARRIER_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    SHAMAN_MYSTICAL_BARRIER_2(CrossableObject.SHAMAN_MYSTICAL_BARRIER_2, new Coordinate(1296, 10096, 0), CrossableObject.SHAMAN_MYSTICAL_BARRIER_2.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    SHAMAN_MYSTICAL_BARRIER_3(CrossableObject.SHAMAN_MYSTICAL_BARRIER_3, new Coordinate(1307, 10096, 0), CrossableObject.SHAMAN_MYSTICAL_BARRIER_3.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    SHAMAN_MYSTICAL_BARRIER_4(CrossableObject.SHAMAN_MYSTICAL_BARRIER_4, new Coordinate(1316, 10096, 0), CrossableObject.SHAMAN_MYSTICAL_BARRIER_4.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, true),
    SHAMAN_MYSTICAL_BARRIER_5(CrossableObject.SHAMAN_MYSTICAL_BARRIER_5, new Coordinate(1324, 10096, 0), CrossableObject.SHAMAN_MYSTICAL_BARRIER_5.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, true),

    FARMING_GUILD_ENTRANCE(CrossableObject.FARMING_GUILD_ENTRANCE_DOOR_1, new Coordinate(1249, 3724, 0), CrossableObject.FARMING_GUILD_ENTRANCE_DOOR_1.getOutput(), 2, Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT, true, new SkillRequirement(Skill.FARMING, 45)),

    PORT_PHASMATYS_ENERGY_BARRIER(new Coordinate(3659, 3508, 0), new Coordinate(3659, 3509, 0), new Coordinate(3659, 3507, 0), new String[]{"Energy Barrier"}, new String[]{"Pay-toll(2-Ecto)"}, 2, Region.CollisionFlags.BLOCKED_TILE, true, new QuestRequirement(Quests.GHOSTS_AHOY.getQuestName(), Quest.Status.COMPLETE)),

    PORT_PHASMATYS_GANGPLANK_BILL(CrossableObject.PORT_PHASMATYS_GANGPLANK_BILL, CrossableObject.PORT_PHASMATYS_GANGPLANK_BILL_1.getOutput(), CrossableObject.PORT_PHASMATYS_GANGPLANK_BILL.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    PORT_PHASMATYS_GANGPLANK_BILL_1(CrossableObject.PORT_PHASMATYS_GANGPLANK_BILL_1, CrossableObject.PORT_PHASMATYS_GANGPLANK_BILL.getOutput(), CrossableObject.PORT_PHASMATYS_GANGPLANK_BILL_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    MOS_LE_HARMLESS_GANGPLANK_BILL(CrossableObject.MOS_LE_HARMLESS_GANGPLANK_BILL, CrossableObject.MOS_LE_HARMLESS_GANGPLANK_BILL_1.getOutput(), CrossableObject.MOS_LE_HARMLESS_GANGPLANK_BILL.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    MOS_LE_HARMLESS_GANGPLANK_BILL_1(CrossableObject.MOS_LE_HARMLESS_GANGPLANK_BILL_1, CrossableObject.MOS_LE_HARMLESS_GANGPLANK_BILL.getOutput(), CrossableObject.MOS_LE_HARMLESS_GANGPLANK_BILL_1.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),

    CAVE_HORROR_ENTRANCE(CrossableObject.CAVE_HORROR_ENTRANCE, CrossableObject.CAVE_HORROR_EXIT.getOutput(), CrossableObject.CAVE_HORROR_ENTRANCE.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false, Requirements.LIGHT_SOURCES.getRequirements(), Requirements.WITCHWOOD_ICON.getRequirements()),
    CAVE_HORROR_EXIT(CrossableObject.CAVE_HORROR_EXIT, CrossableObject.CAVE_HORROR_ENTRANCE.getOutput(), CrossableObject.CAVE_HORROR_EXIT.getOutput(), 2, Region.CollisionFlags.BLOCKED_TILE, false),
    ;

    private final Coordinate spot;
    private final Coordinate in;
    private Handleable handleable;
    private Coordinate out;
    private Handleable[] handleables;
    private String[] actions;
    private final int weight;
    private String[] names;
    private int maxDistance = -1;
    private final int flag;
    private boolean bidirectional;
    private boolean swapEdge;
    private final Requirement[] requirementList;

    GameObjectVertices(Coordinate spot, Coordinate in, Coordinate out, String dir, Requirement... requirementList) {
        this(spot, in, out, toArray("Door"), toArray("Open"), 1, setDirection(dir), true, requirementList);
    }

    GameObjectVertices(Coordinate spot, Coordinate in, Coordinate out, String dir, boolean bidirectional, Requirement... requirementList) {
        this(spot, in, out, toArray("Door"), toArray("Open"), 1, setDirection(dir), bidirectional, requirementList);
    }

    GameObjectVertices(Coordinate spot, Coordinate in, Coordinate out, String[] names, String dir, Requirement... requirementList) {
        this(spot, in, out, names, toArray("Open"), 1, setDirection(dir), true, requirementList);
    }

    GameObjectVertices(Coordinate spot, Coordinate in, Coordinate out, String name, String action, String dir, Requirement... requirementList) {
        this(spot, in, out, toArray(name), toArray(action), 1, setDirection(dir), true, requirementList);
    }

    GameObjectVertices(Coordinate spot, Coordinate in, Coordinate out, String name, String action, String dir, int weight, boolean bidirectional, Requirement... requirementList) {
        this(spot, in, out, toArray(name), toArray(action), weight, setDirection(dir), bidirectional, requirementList);
    }

    GameObjectVertices(Coordinate spot, Coordinate in, Coordinate out, String name, String action, Requirement... requirementList) {
        this(spot, in, out, toArray(name), toArray(action), 2, setDirection(null), false, requirementList);
    }

    GameObjectVertices(Coordinate spot, Coordinate in, Coordinate out, String[] names, String[] actions, Requirement... requirementList) {
        this(spot, in, out, names, actions, 2, setDirection(null), false, requirementList);
    }

    GameObjectVertices(Coordinate spot, Coordinate in, Coordinate out, String[] names, String[] actions, int weight, Requirement... requirementList) {
        this(spot, in, out, names, actions, weight, setDirection(null), false, requirementList);
    }

    private static String[] toArray(String string) {
        return new String[]{string};
    }

    private static int setDirection(String text) {
        if (text != null) {
            switch (text) {
                case "north": case "North": case "N": case "n":
                    return Region.CollisionFlags.NORTH_BOUNDARY_OBJECT;
                case "south": case "South": case "S": case "s":
                    return Region.CollisionFlags.SOUTH_BOUNDARY_OBJECT;
                case "east": case "East": case "E": case "e":
                    return Region.CollisionFlags.EAST_BOUNDARY_OBJECT;
                case "west": case "West": case "W": case "w":
                    return Region.CollisionFlags.WEST_BOUNDARY_OBJECT;
            }
        }
        return Region.CollisionFlags.BLOCKED_TILE;
    }

    GameObjectVertices(Coordinate spot, Coordinate in, Coordinate out, String[] names, String[] actions, int weight, int flag, boolean bidirectional, Requirement... requirementList) {
        this.spot = spot;
        this.in = in;
        this.out = out;
        this.names = names;
        this.actions = actions;
        this.weight = weight;
        this.flag = flag;
        this.bidirectional = bidirectional;
        this.requirementList = requirementList;
    }

    GameObjectVertices(Coordinate spot, Coordinate in, Coordinate out, String[] names, String[] actions, int weight, int flag, boolean bidirectional, boolean swapEdge, Requirement... requirementList) {
        this.spot = spot;
        this.in = in;
        this.out = out;
        this.names = names;
        this.actions = actions;
        this.weight = weight;
        this.flag = flag;
        this.bidirectional = bidirectional;
        this.requirementList = requirementList;
        this.swapEdge = swapEdge;
    }

    GameObjectVertices(Coordinate spot, Coordinate in, Coordinate out, String[] names, String[] actions, int weight, int maxDistance, int flag, boolean bidirectional, Requirement... requirementList) {
        this.spot = spot;
        this.in = in;
        this.out = out;
        this.names = names;
        this.actions = actions;
        this.weight = weight;
        this.maxDistance = maxDistance;
        this.flag = flag;
        this.bidirectional = bidirectional;
        this.requirementList = requirementList;
    }

    GameObjectVertices(Handleable handleable, Coordinate in, Handleable[] handleables, int weight, int flag, Requirement... requirementList) {
        this.handleable = handleable;
        this.in = in;
        this.handleables = handleables;
        this.spot = handleable.getPosition();
        this.weight = weight;
        this.flag = flag;
        this.requirementList = requirementList;
    }

    GameObjectVertices(Handleable handleable, Coordinate in, Coordinate out, int weight, int flag, boolean bidirectional, Requirement... requirementList) {
        this.handleable = handleable;
        if(handleable instanceof Teleport) {
            this.spot = ((Teleport) handleable).getIn();
        } else {
            this.spot = handleable.getPosition();
        }
        this.in = in;
        this.out = out;
        this.weight = weight;
        this.flag = flag;
        this.bidirectional = bidirectional;
        this.requirementList = requirementList;
    }

    GameObjectVertices(Handleable handleable, Coordinate in, Coordinate out, int weight, int flag, boolean bidirectional, boolean swapGameObjectEdge, Requirement... requirementList) {
        this.handleable = handleable;
        this.spot = handleable.getPosition();
        this.in = in;
        this.out = out;
        this.weight = weight;
        this.flag = flag;
        this.bidirectional = bidirectional;
        this.swapEdge = swapGameObjectEdge;
        this.requirementList = requirementList;
    }


    public static void addAll(WeightedGraph weightedGraph) {
        try {

            Arrays.stream(values()).forEach(val -> {
                LinkedHashMap<Integer, Vertex> graph = weightedGraph.getGraph();
                Vertex obj;

                if ((obj = graph.get(val.spot.hashCode())) == null) {
                    obj = new CoordinateVertex(val.spot, val.flag);
                }
                obj.setFlag(val.flag);
                obj.createEdgeList();
                weightedGraph.swapVertex(obj);

                boolean inSpotEqual = false;
                boolean outSpotEqual = false;
                if (val.in.equals(val.spot)) {
                    inSpotEqual = true;
                } else if (val.out != null && val.out.equals(val.spot)) {
                    outSpotEqual = true;
                }

                graph.putIfAbsent(val.in.hashCode(), new CoordinateVertex(val.in, val.flag));
                if (val.out != null) {
                    graph.putIfAbsent(val.out.hashCode(), new CoordinateVertex(val.out, val.flag));
                }

                Vertex source = graph.get(val.spot.hashCode());
                Vertex in = graph.get(val.in.hashCode());
                Vertex out = null;
                if (val.out != null) {
                    out = graph.get(val.out.hashCode());
                }

                if (val.handleables != null) {
                    Arrays.stream(val.handleables).forEach(handleable -> {
                        graph.putIfAbsent(handleable.getPosition().hashCode(), new CoordinateVertex(handleable.getPosition(), val.flag));
                        Vertex outer = graph.get(handleable.getPosition().hashCode());
                        Edge edge = new GameObjectEdge(source, outer, val.weight, val.requirementList, handleable);
                        source.addEdge(edge);
                        weightedGraph.addEdgesBack(source, null);
                    });
                } else if (inSpotEqual || outSpotEqual) { //Special Case with just two GameObjectEdges
                    if (inSpotEqual) {
                        Edge edge;
                        if (val.handleable != null) {
                            edge = new GameObjectEdge(source, out, val.weight, val.requirementList, val.handleable);
                        } else {
                            edge = new GameObjectEdge(source, out, val.weight, val.requirementList, val.names, val.actions, val.maxDistance, false);
                        }
                        source.addEdge(edge);
                        if (val.bidirectional) {
                            if (val.handleable != null) {
                                edge = new GameObjectEdge(out, source, val.weight, val.requirementList, val.handleable);
                            } else {
                                edge = new GameObjectEdge(out, source, val.weight, val.requirementList, val.names, val.actions, val.maxDistance, true);
                            }
                            out.addEdge(edge);
                        }
                    } else {
                        Edge edge;
                        if(val.swapEdge) {
                            if (val.handleable != null) {
                                edge = new GameObjectEdge(in, source, val.weight, val.requirementList, val.handleable);
                            } else {
                                edge = new GameObjectEdge(in, source, val.weight, val.requirementList, val.names, val.actions, val.maxDistance, true);
                            }
                            in.addEdge(edge);
                        } else {
                            if (val.handleable != null) {
                                edge = new GameObjectEdge(source, in, val.weight, val.requirementList, val.handleable);
                            } else {
                                edge = new GameObjectEdge(source, in, val.weight, val.requirementList, val.names, val.actions, val.maxDistance, false);
                            }
                            source.addEdge(edge);
                        }
                        if (val.bidirectional) {
                            if (val.handleable != null) {
                                edge = new GameObjectEdge(in, source, val.weight, val.requirementList, val.handleable);
                            } else {
                                edge = new GameObjectEdge(in, source, val.weight, val.requirementList, val.names, val.actions, val.maxDistance, true);
                            }
                            in.addEdge(edge);
                        }
                    }
                } else { //Normal Case
                    Edge edge;
                    if (val.handleable != null) {
                        edge = new GameObjectEdge(source, out, val.weight, val.requirementList, val.handleable);
                    } else {
                        edge = new GameObjectEdge(source, out, val.weight, val.requirementList, val.names, val.actions, val.maxDistance, false);
                    }
                    source.addEdge(edge);

                    edge = new Edge(in, source, val.weight, null);
                    in.addEdge(edge);

                    if (val.bidirectional) {
                        if (val.handleable != null) {
                            edge = new GameObjectEdge(source, in, val.weight, val.requirementList, val.handleable);
                        } else {
                            edge = new GameObjectEdge(source, in, val.weight, val.requirementList, val.names, val.actions, val.maxDistance, false);
                        }
                        source.addEdge(edge);

                        edge = new Edge(out, source, val.weight, null);
                        out.addEdge(edge);
                    }
                }
                //weightedGraph.addEdgesBack(obj, null);
            });
            for (StrongholdGate val : StrongholdGate.values()) {
                LinkedHashMap<Integer, Vertex> graph = weightedGraph.getGraph();
                Vertex obj;
                obj = new CoordinateVertex(val.getPosition(), Region.CollisionFlags.BLOCKED_TILE);
                obj.createEdgeList();
                weightedGraph.swapVertex(obj);
                obj.getEdges().clear();
                graph.putIfAbsent(val.getPosition().hashCode(), obj);
                graph.putIfAbsent(val.getOuter().hashCode(), new CoordinateVertex(val.getOuter(), Region.CollisionFlags.BLOCKED_TILE));
                graph.putIfAbsent(val.getInner().hashCode(), new CoordinateVertex(val.getInner(), Region.CollisionFlags.BLOCKED_TILE));

                Vertex outer = graph.get(val.getOuter().hashCode());
                Vertex inner = graph.get(val.getInner().hashCode());

                obj.addEdge(new GameObjectEdge(obj, outer, 5, null, val));
                inner.addEdge(new Edge(inner, obj, 5, null));

                outer.addEdge(new GameObjectEdge(outer, obj, 5, null, val));
                obj.addEdge(new Edge(obj, inner, 5, null));

                removeOldEdges(graph, inner, obj, weightedGraph, null);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Environment.getLogger().debug("Error when adding GameObjectVertices... Graph size is: " + weightedGraph.getGraph().size());
        }
    }

    private static void removeOldEdges(LinkedHashMap<Integer, Vertex> graph, Vertex input, Vertex obj, WeightedGraph weightedGraph, Requirement[] requirementList) {
        if(graph.containsKey(input.getCoordinate().hashCode())) {
            graph.get(input.getCoordinate().hashCode()).getEdges().removeIf(edge -> edge.getDestination() == null);
        }
        weightedGraph.addEdgesBack(obj, requirementList);

    }

    @Override
    public String toString() {
        return "[Spot: " + spot + " | In: " + in + " | Out: " + out + " | Name: " + Arrays.toString(names) + " | Actions: " + Arrays.toString(actions) + " | Weight: " + weight + "]";
    }

    public int getMaxDistance() {
        return maxDistance;
    }

    public Handleable getHandleable() {
        return handleable;
    }

    public Coordinate getSpot() {
        return spot;
    }
}
