package com.regal.utility.awesome_navigation.vertices;

import com.regal.utility.awesome_navigation.WeightedGraph;
import com.regal.utility.awesome_navigation.edges.Edge;
import com.regal.utility.awesome_navigation.edges.NpcEdge;
import com.regal.utility.awesome_navigation.handles.Handleable;
import com.regal.utility.awesome_navigation.handles.NpcInteract;
import com.regal.utility.awesome_navigation.handles.TravelInterface;
import com.regal.utility.awesome_navigation.requirements.*;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.local.Quest;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Region;
import com.runemate.game.api.hybrid.util.Regex;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

public enum NpcVertices {

    KARAMJA_PAY_FARE(new Coordinate(2956, 3145, 0), new Coordinate(2956, 3146, 0), new Coordinate(3032, 3217, 1), new String[]{"Customs officer"}, new String[]{"Pay-Fare"}, new Area.Rectangular(new Coordinate(2944, 3158, 0), new Coordinate(2959, 3146, 0)),
            new String[] {"Can I journey on this ship?", "Search away, I have nothing to hide.", "Ok."},
            2, 0, false, new Requirement[]{new InventoryRequirement("Coins", 30)}),
    PORT_SARIM_PAY_FARE(new Coordinate(3028, 3217, 0), new Coordinate(3029, 3217, 0), new Coordinate(2956, 3143, 1), new String[] {"Captain Tobias", "Seaman Lorris", "Seaman Thresnor"}, new String[] {"Pay-fare"}, new Area.Rectangular(new Coordinate(3021, 3238, 0), new Coordinate(3029, 3206, 0)),
            new String[] {"Yes please."}, 2, 0, false, new Requirement[]{new InventoryRequirement("Coins", 30)}),
    VEOS_KOUREND(new Coordinate(1825, 3691, 0), new Coordinate(1825, 3690, 0), new Coordinate(3055, 3242, 1), new String[]{"Veos"}, new String[]{"Port Sarim"}, new Area.Rectangular(new Coordinate(1824, 3690, 0), new Coordinate(1825, 3691, 0)),
            null, 2, Region.CollisionFlags.BLOCKED_TILE, false, new Requirement[]{new SubscriptionRequirement(true)}),
    VEOS_PORT_SARIM(new Coordinate(3054, 3245, 0), new Coordinate(3054, 3246, 0), new Coordinate(1824, 3695, 1), new String[]{"Veos"}, new String[]{"Port Piscarilius"}, new Area.Rectangular(new Coordinate(3054, 3245, 0), new Coordinate(3055, 3245, 0)),
            null, 2, Region.CollisionFlags.BLOCKED_TILE, false, new Requirement[]{new SubscriptionRequirement(true)}),
    CABIN_BOY_COLIN_RIMMINGTON(new Coordinate(2910, 3226, 0), new Coordinate(2911, 3226, 0), new Coordinate(2578, 2837, 1), new String[]{"Cabin Boy Colin"}, new String[]{"Travel"}, new Area.Circular(new Coordinate(2910, 3226, 0), 1),
            null, 2, 0, false, new Requirement[]{new QuestRequirement("The Corsair Curse", Quest.Status.IN_PROGRESS)}),
    CABIN_BOY_COLIN_CORSAIR(new Coordinate(2574, 2835, 1), new Coordinate(2578, 2837, 1), new Coordinate(2911, 3226, 0), new String[]{"Cabin Boy Colin"}, new String[]{"Travel"}, new Area.Circular(new Coordinate(2574, 2835, 1), 1),
            null, 2, 0, false, new Requirement[]{new QuestRequirement("The Corsair Curse", Quest.Status.IN_PROGRESS)}),
    ARDOUGNE_BARNABY(new Coordinate(2676, 3274, 0), new Coordinate(2676, 3275, 0), new Coordinate(2775, 3233, 1), new String[]{"Captain Barnaby"}, new String[]{"Brimhaven"},
            new Area.Polygonal(
                    new Coordinate(2667, 3265, 0),
                    new Coordinate(2672, 3265, 0),
                    new Coordinate(2676, 3269, 0),
                    new Coordinate(2676, 3274, 0),
                    new Coordinate(2683, 3274, 0),
                    new Coordinate(2683, 3271, 0),
                    new Coordinate(2685, 3271, 0),
                    new Coordinate(2685, 3274, 0),
                    new Coordinate(2690, 3274, 0),
                    new Coordinate(2690, 3277, 0),
                    new Coordinate(2667, 3277, 0)
            ),
            //393 - 0
            null, 20, Region.CollisionFlags.BLOCKED_TILE, false, new Requirement[]{new SubscriptionRequirement(true), new InventoryRequirement("Coins", 30)}),
    BRIMHAVEN_BARNABY(new Coordinate(2772, 3229, 0), new Coordinate(2772, 3230, 0), new Coordinate(2683, 3268, 1), new String[]{"Captain Barnaby"}, new String[]{"Ardougne"},
            new Area.Rectangular(new Coordinate(2769, 3236, 0), new Coordinate(2775, 3218, 0)),
            null, 20, Region.CollisionFlags.BLOCKED_TILE, false, new Requirement[]{new SubscriptionRequirement(true), new InventoryRequirement("Coins", 30)}),
    CAPTAIN_DALBUR(TravelInterface.GNOME_GLIDER_KAR_HEWO, new Coordinate(3284, 3210, 0), TravelInterface.getOfType(TravelInterface.Type.GNOME_GLIDER), new Area.Circular(new Coordinate(3284, 3212, 0), 1), null, 5, 0, new Requirement[]{new QuestRequirement("The Grand Tree", Quest.Status.COMPLETE)}),
    CAPTAIN_KLEMFOODLE(TravelInterface.GNOME_GLIDER_GANDIUS, new Coordinate(2969, 2973, 0), TravelInterface.getOfType(TravelInterface.Type.GNOME_GLIDER), new Area.Circular(new Coordinate(2970, 2973, 0), 1), null, 5, 0, new Requirement[]{new QuestRequirement("The Grand Tree", Quest.Status.COMPLETE)}),
    CAPTAIN_ERRDO(TravelInterface.GNOME_GLIDER_TA_QUIR_PRIW, new Coordinate(2465, 3501, 3), TravelInterface.getOfType(TravelInterface.Type.GNOME_GLIDER), new Area.Circular(new Coordinate(2464, 3501, 3), 1), null, 5, 0, new Requirement[]{new QuestRequirement("The Grand Tree", Quest.Status.COMPLETE)}),
    CAPTAIN_BLEEMADGE(TravelInterface.GNOME_GLIDER_SINDARPOS, new Coordinate(2848, 3498, 0), TravelInterface.getOfType(TravelInterface.Type.GNOME_GLIDER), new Area.Circular(new Coordinate(2847, 3499, 0), 1), null, 5, 0, new Requirement[]{new QuestRequirement("The Grand Tree", Quest.Status.COMPLETE)}),
    //CAPTAIN_DALBUR_SINDARPOS(new Coordinate(3283, 3212, 0), new Coordinate(3284, 3210, 0), new Coordinate(2848, 3498, 0), new String[]{"Captain Dalbur"}, new String[]{"Glider"}, TravelInterface.getOfType(TravelInterface.Type.GNOME_GLIDER), new Area.Circular(new Coordinate(3284, 3212, 0), 1), null, 5, 0, false, new Requirement[]{new QuestRequirement("Monkey Madness I", Quest.Status.COMPLETE)}),
    MOUNTAIN_GUIDE_KOUREND_INNER(NpcInteract.MOUNTAIN_GUIDE_KOUREND_INNER, new Coordinate(1274, 3559, 0), 2, 0, new Requirement[]{new VarbitRequirement(5421, 1)}),
    MOUNTAIN_GUIDE_KOUREND_OUTER(NpcInteract.MOUNTAIN_GUIDE_KOUREND_OUTER, new Coordinate(1402, 3536, 0), 2, 0, new Requirement[]{new VarbitRequirement(5421, 1)}),
    KAZGAR_TO_MINES_LUMBRIDGE(NpcInteract.KAZGAR_CELLAR_TO_MINES, new Coordinate(3229, 9610, 0), 2, 0, new Requirement[]{new CombinedRequirement(false, new EquipmentRequirement(new Pattern[]{Regex.getPatternForContainsString("Lit")}, 1), new InventoryRequirement(Regex.getPatternForContainsString("Lit"), 1))}),
    MISTAG_TO_CELLAR_LUMBRIDGE(NpcInteract.MISTAG_MINES_TO_CELLAR, new Coordinate(3320, 9615, 0), 2, 0, new Requirement[]{new CombinedRequirement(false, new EquipmentRequirement(new Pattern[]{Regex.getPatternForContainsString("Lit")}, 1), new InventoryRequirement(Regex.getPatternForContainsString("Lit"), 1))}),
    SHILO_MOSOL_REI(NpcInteract.MOSOL_REI, new Coordinate(2881, 2951, 0), 2, 0, new Requirement[] {new QuestRequirement("Shilo Village", Quest.Status.COMPLETE)}),

    ELKOY_MAZE_IN(NpcInteract.ELKOY_IN, new Coordinate(2506, 3191, 0), 10, 0, new Requirement[] {new QuestRequirement("Tree Gnome Village", Quest.Status.IN_PROGRESS)}),
    ELKOY_MAZE_OUT(NpcInteract.ELKOY_OUT, new Coordinate(2515, 3159, 0), 10, 0, new Requirement[] {new QuestRequirement("Tree Gnome Village", Quest.Status.IN_PROGRESS)}),

    MONK_ENTRANA_LEAVE(NpcInteract.MONK_ENTRANA_LEAVE, new Coordinate(2839, 3335, 0), 10, 0, null),
    BILL_TEACH_LEAVE_PHASMATYS(NpcInteract.BILL_TEACH_LEAVE_PHASMATYS, new Coordinate(3714, 3498, 1), 10, 0, new Requirement[]{new QuestRequirement(Quests.CABIN_FEVER.getQuestName(), Quest.Status.COMPLETE)}),
    BILL_TEACH_LEAVE_MOS_LE_HARMLESS(NpcInteract.BILL_TEACH_LEAVE_MOS_LE_HARMLESS, new Coordinate(3683, 2949, 1), 10, 0, new Requirement[]{new QuestRequirement(Quests.CABIN_FEVER.getQuestName(), Quest.Status.COMPLETE)}),
    ;
    //TODO: FIX BUG WHEN 30 COINS USED TO TRAVEL

    private final Coordinate spot;
    private Handleable handleable;
    private final Coordinate in;
    private Handleable[] handleables;
    private Coordinate out;
    private String[] names;
    private String[] actions;
    private TravelInterface travelInterface;
    private final Area npcArea;
    private final int weight;
    private final int flag;
    private boolean bidirectional;
    private final Requirement[] requirementList;
    private String[] chatOptions;

    NpcVertices(Coordinate spot, Coordinate in, Coordinate out, String[] names, String[] actions, Area npcArea, String[] chatOptions, int weight, int flag, boolean bidirectional, Requirement[] requirementList) {
        this.spot = spot;
        this.in = in;
        this.out = out;
        this.names = names;
        this.actions = actions;
        this.npcArea = npcArea;
        this.chatOptions = chatOptions;
        this.weight = weight;
        this.flag = flag;
        this.bidirectional = bidirectional;
        this.requirementList = requirementList;
    }

    NpcVertices(Coordinate spot, Coordinate in, Coordinate out, String[] names, String[] actions, TravelInterface travelInterface, Area npcArea, String[] chatOptions, int weight, int flag, boolean bidirectional, Requirement[] requirementList) {
        this.spot = spot;
        this.in = in;
        this.out = out;
        this.names = names;
        this.actions = actions;
        this.travelInterface = travelInterface;
        this.npcArea = npcArea;
        this.chatOptions = chatOptions;
        this.weight = weight;
        this.flag = flag;
        this.bidirectional = bidirectional;
        this.requirementList = requirementList;
    }

    NpcVertices(Handleable handleable, Coordinate in, Handleable[] handleables, Area npcArea, String[] chatOptions, int weight, int flag, Requirement[] requirementList) {
        this.handleable = handleable;
        this.spot = handleable.getPosition();
        this.in = in;
        this.handleables = handleables;
        this.npcArea = npcArea;
        this.chatOptions = chatOptions;
        this.weight = weight;
        this.flag = flag;
        this.requirementList = requirementList;
    }

    NpcVertices(NpcInteract handleable, Coordinate in, int weight, int flag, Requirement[] requirementList) {
        this.handleable = handleable;
        this.spot = handleable.getPosition();
        this.in = in;
        this.out = handleable.getOutput();
        this.weight = weight;
        this.flag = flag;
        this.requirementList = requirementList;
        this.npcArea = handleable.getArea();
    }

    public static void addAll(WeightedGraph weightedGraph) {
        try {
            //Remove if requirements fail somehow, or maybe just remove all first...

            Arrays.stream(values()).forEach(val -> {
                LinkedHashMap<Integer, Vertex> graph = weightedGraph.getGraph();
                CoordinateVertex obj = new CoordinateVertex(val.spot, val.flag);
                obj.createEdgeList();
                weightedGraph.swapVertex(obj);

                boolean inSpotEqual = false;
                boolean outSpotEqual = false;
                if (val.in.equals(val.spot)) {
                    inSpotEqual = true;
                } else if (val.out != null && val.out.equals(val.spot)) {
                    outSpotEqual = true;
                }

                graph.putIfAbsent(val.in.hashCode(), new CoordinateVertex(val.in, val.flag));
                if (val.out != null) {
                    graph.putIfAbsent(val.out.hashCode(), new CoordinateVertex(val.out, val.flag));
                }

                Vertex source = graph.get(val.spot.hashCode());
                Vertex in = graph.get(val.in.hashCode());
                Vertex out = null;
                if (val.out != null) {
                    out = graph.get(val.out.hashCode());
                }

                if (val.handleables != null) {
                    Arrays.stream(val.handleables).forEach(handleable -> {
                        graph.putIfAbsent(handleable.getPosition().hashCode(), new CoordinateVertex(handleable.getPosition(), val.flag));
                        Vertex outer = graph.get(handleable.getPosition().hashCode());
                        Edge edge = new NpcEdge(source, outer, val.weight, val.requirementList, handleable);
                        source.addEdge(edge);
                    });
                } else if (inSpotEqual || outSpotEqual) { //Special Case with just two NpcEdges
                    if (inSpotEqual) {
                        Edge edge;
                        if (val.handleable != null) {
                            edge = new NpcEdge(source, out, val.weight, val.requirementList, val.handleable);
                        } else {
                            edge = new NpcEdge(source, out, val.weight, val.requirementList, val.names, val.actions, val.npcArea, val.chatOptions, false);
                        }
                        source.addEdge(edge);
                        if (val.bidirectional) {
                            if (val.handleable != null) {
                                edge = new NpcEdge(out, source, val.weight, val.requirementList, val.handleable);
                            } else {
                                edge = new NpcEdge(out, source, val.weight, val.requirementList, val.names, val.actions, val.npcArea, val.chatOptions, true);
                            }
                            out.addEdge(edge);
                        }
                    } else {
                        Edge edge;
                        if (val.handleable != null) {
                            edge = new NpcEdge(source, in, val.weight, val.requirementList, val.handleable);
                        } else {
                            edge = new NpcEdge(source, in, val.weight, val.requirementList, val.names, val.actions, val.npcArea, val.chatOptions, false);
                        }
                        source.addEdge(edge);
                        if (val.bidirectional) {
                            if (val.handleable != null) {
                                edge = new NpcEdge(in, source, val.weight, val.requirementList, val.handleable);
                            } else {
                                edge = new NpcEdge(in, source, val.weight, val.requirementList, val.names, val.actions, val.npcArea, val.chatOptions, true);
                            }
                            in.addEdge(edge);
                        }
                    }
                } else { //Normal Case
                    Edge edge;
                    if (val.handleable != null) {
                        edge = new NpcEdge(source, out, val.weight, val.requirementList, val.handleable);
                    } else {
                        edge = new NpcEdge(source, out, val.weight, val.requirementList, val.names, val.actions, val.npcArea, val.chatOptions, false);
                    }
                    source.addEdge(edge);

                    edge = new Edge(in, source, val.weight, null);
                    in.addEdge(edge);

                    if (val.bidirectional) {
                        if (val.handleable != null) {
                            edge = new NpcEdge(source, in, val.weight, val.requirementList, val.handleable);
                        } else {
                            edge = new NpcEdge(source, in, val.weight, val.requirementList, val.names, val.actions, val.npcArea, val.chatOptions, false);
                        }
                        source.addEdge(edge);

                        edge = new Edge(out, source, val.weight, null);
                        out.addEdge(edge);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            Environment.getLogger().debug("Error when adding NpcObjectVertices... Graph size is: " + weightedGraph.getGraph().size());
        }
    }

    private static void removeOldEdges(LinkedHashMap<Integer, Vertex> graph, Vertex input, Vertex obj, WeightedGraph weightedGraph, Requirement[] requirementList) {
        if(graph.containsKey(input.getCoordinate().hashCode())) {
            graph.get(input.getCoordinate().hashCode()).getEdges().removeIf(edge -> edge.getDestination() == null);
        }
        weightedGraph.addEdgesBack(obj, requirementList);
    }

    @Override
    public String toString() {
        return "[Spot: " + spot + " | In: " + in + " | Out: " + out + " | Name: " + Arrays.toString(names) + " | Actions: " + Arrays.toString(actions) + " | Weight: " + weight + "]";
    }

    public Handleable getHandleable() {
        return handleable;
    }

    public Coordinate getSpot() {
        return (handleable != null) ? handleable.getPosition() : spot;
    }
}
