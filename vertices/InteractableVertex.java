package com.regal.utility.awesome_navigation.vertices;

import com.regal.utility.awesome_navigation.handles.TravelInterface;

public interface InteractableVertex extends Vertex {
    String[] getNames();
    String[] getInteractions();
    TravelInterface getTravelInterface();
}
