package com.regal.utility.awesome_navigation.vertices;

import com.regal.utility.awesome_navigation.edges.Edge;
import com.runemate.game.api.hybrid.location.Coordinate;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CoordinateVertex implements Vertex, Externalizable {

    private final static long serialVersionUID = 7998170313736905389L;
    public final static int VERTEX_ID = 1;
    private Coordinate coordinate;
    private int flag;
    private transient List<Edge> edges = new ArrayList<>();
    private transient Vertex from;
    private transient int totalCost = -1;

    public CoordinateVertex(Coordinate coordinate, int flag) {
        this.coordinate = coordinate;
        this.flag = flag;
    }

    public CoordinateVertex() {

    }

    @Override
    public void addEdge(Edge edge) {
        if(edges == null) {
            edges = new ArrayList<>();
        }
        if(edge != null) {
            Edge remove = edges.stream().filter(edge1 -> edge.getSource().getCoordinate().equals(edge1.getSource().getCoordinate()) &&
                    edge.getDestination() != null && edge1.getDestination() != null && edge.getDestination().getCoordinate().equals(edge1.getDestination().getCoordinate())).findFirst().orElse(null);
            if(remove == null) {
                edges.add(edge);
            }
            else if (remove.getClass().equals(Edge.class)) {
                edges.remove(remove);
                edges.add(edge);
            }
        }
    }

    public int getFlag() {
        return flag;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    @Override
    public int hashCode() {
        return coordinate.hashCode();
    }

    public Vertex getFrom() {
        return from;
    }

    @Override
    public void setFrom(Vertex from) {
        this.from = from;
    }

    public int getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(int totalCost) {
        this.totalCost = totalCost;
    }

    public boolean equals(Object vertex) {
        return vertex instanceof CoordinateVertex && ((CoordinateVertex) vertex).coordinate != null && ((CoordinateVertex) vertex).coordinate.equals(this.coordinate);
    }

    @Override
    public String toString() {
        if(from != null) {
            return "CoordinateVertex: [" + coordinate + "] [From: " + from.getCoordinate() + "] [Edges: " + edges.size() + "] " + edges.stream().map(Edge::toString).collect(Collectors.joining());
        } else {
            return "CoodinateVertex: [" + coordinate + "] [From: null] [Edges: " + edges.size() + "] " + edges.stream().map(Edge::toString).collect(Collectors.joining());
        }
    }

    public void addEdge(List<Edge> edges1) {
        if(edges1 != null) {
            List<Integer> edgesToRemove = new ArrayList<>();
            List<Edge> edgesToAdd = new ArrayList<>();
            edges1.forEach(edge -> {
                if (edge != null) {
                    Edge remove = edges.stream().filter(edge1 -> edge.getSource().getCoordinate().equals(edge1.getSource().getCoordinate()) &&
                            edge.getDestination() != null && edge1.getDestination() != null && edge.getDestination().getCoordinate().equals(edge1.getDestination().getCoordinate())).findFirst().orElse(null);
                    if(remove == null) {
                        edgesToAdd.add(edge);
                    }
                    else if (remove.getClass().equals(Edge.class)) {
                        edgesToRemove.add(edges.indexOf(remove));
                        edgesToAdd.add(edge);
                    }
                }
            });
            edgesToRemove.sort(Comparator.reverseOrder());
            for (int i = 0; i < edgesToRemove.size(); i++) {
                edges.remove(edgesToRemove.get(i).intValue());
            }
            edges.addAll(edgesToAdd);
        }
    }

    @Override
    public int getVertexID() {
        return VERTEX_ID;
    }

    public void createEdgeList() {
        if(edges == null) {
            edges = new ArrayList<>();
        }
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(coordinate.hashCode());
        out.writeInt(flag);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException {
        coordinate = new Coordinate(in.readInt());
        flag = in.readInt();
    }
}
