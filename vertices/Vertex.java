package com.regal.utility.awesome_navigation.vertices;

import com.regal.utility.awesome_navigation.edges.Edge;
import com.runemate.game.api.hybrid.location.Coordinate;

import java.io.Externalizable;
import java.util.List;

public interface Vertex extends Externalizable {

    void setFlag(int flag);
    void addEdge(Edge edge);
    int getFlag();
    Coordinate getCoordinate();
    List<Edge> getEdges();
    int hashCode();
    Vertex getFrom();
    void setFrom(Vertex from);
    int getTotalCost();
    void setTotalCost(int totalCost);
    boolean equals(Object other);
    void createEdgeList();
    void addEdge(List<Edge> edges);
    int getVertexID();
}
