package com.regal.utility.awesome_navigation;

import com.regal.utility.awesome_navigation.handles.Call;
import com.regal.utility.awesome_navigation.vertices.CoordinateVertex;
import com.regal.utility.awesome_navigation.vertices.Vertex;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.entities.details.Locatable;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.location.navigation.Landmark;
import com.runemate.game.api.hybrid.location.navigation.Path;
import com.runemate.game.api.hybrid.location.navigation.basic.BresenhamPath;
import com.runemate.game.api.hybrid.location.navigation.basic.CoordinatePath;
import com.runemate.game.api.hybrid.location.navigation.cognizant.RegionPath;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.util.calculations.Distance;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.AbstractBot;

import java.util.Comparator;
import java.util.List;

public class Pathing {

    private WeightedGraph graph;
    private Locatable prevThing;
    private List<Vertex> prevPath;
    private Landmark prevLandmark;
    private final AwesomePath awesomePath;
    private boolean useTeleports = true;

    public Pathing() {
        awesomePath = new AwesomePath();
    }

    public static void pathTo(Locatable destination) {
        if (destination == null) {
            Environment.getLogger().warn("Null destination entity in pathToEntity");
            return;
        }
        Area.Rectangular objArea = destination.getArea();
        if (objArea != null) {
            List<Coordinate> surrounding = objArea.getSurroundingCoordinates();
            if (surrounding != null && !surrounding.isEmpty()) {
                CoordinatePath path = RegionPath.buildTo(surrounding);
                if (path == null) {
                    Player player;
                    if ((player = Players.getLocal()) != null) {
                        surrounding.sort(Comparator.comparingDouble(coor -> coor.distanceTo(player)));
                        path = BresenhamPath.buildTo(surrounding.get(0));
                    }
                }
                if (path != null) {
                    path.step(true);
                }
            } else {
                Environment.getLogger().warn("Could not get coords surrounding " + objArea);
            }
        } else {
            Environment.getLogger().warn("Could not get rect area around " + destination);
        }
    }

    public static void pathTo(Locatable destination, Locatable backup) {
        Locatable toPathTo = destination;
        if (destination == null) {
            Environment.getLogger().warn("Null destination entity in pathToEntity");
            toPathTo = backup;
        }
        if (toPathTo == null) {
            Environment.getLogger().warn("Nothing to buildPath to in pathToEntity");
            return;
        }
        Area.Rectangular objArea = toPathTo.getArea();
        if (objArea != null) {
            List<Coordinate> surrounding = objArea.getSurroundingCoordinates();
            if (surrounding != null && !surrounding.isEmpty()) {
                CoordinatePath path = RegionPath.buildTo(surrounding);
                if (path == null) {
                    path = BresenhamPath.buildTo(surrounding.get(0));
                }
                if (path != null) {
                    Coordinate next = path.getNext();
                    if (path.step()) {
                        if (next != null && surrounding.contains(next)) {
                            Execution.delayUntil(() -> Distance.to(next) < 2, Call.playerMoving(), 1000, 3000);
                        } else {
                            Execution.delay(200, 900, 350);
                        }
                    }
                }
            } else {
                Environment.getLogger().warn("Could not get coords surrounding " + objArea);
            }
        } else {
            Environment.getLogger().warn("Could not get rect area around " + destination);
        }
    }

    public static boolean viewportTo(Locatable to) {
        Environment.getLogger().debug("Viewporting to " + to);
        BresenhamPath path = BresenhamPath.buildTo(to);
        if (path != null) {
            return path.step(Path.TraversalOption.PREFER_VIEWPORT);
        } else {
            Environment.getLogger().warn("Could not build bres buildPath to " + to);
        }
        return false;
    }

    public static boolean regionPathTo(Locatable thing) {
        Environment.getLogger().info("Region Path To: " + thing);
        if (thing != null) {
            RegionPath path = RegionPath.buildTo(thing);
            if (path != null) {
                Coordinate next = path.getNext();
                if (path.step()) {
                    if (next != null && Distance.between(thing, next) < 3) {
                        Execution.delayUntil(() -> Distance.to(next) < 2, Call.playerMoving(), 1000, 3000);
                    } else {
                        Execution.delay(200, 900, 350);
                    }
                }
                return true;
            } else {
                Environment.getLogger().warn("Could not build regionpath to " + thing);
            }
        } else {
            Environment.getLogger().warn("Null argument in pathTo");
        }
        return false;
    }

    public boolean webPathTo(Locatable thing) {
        long time = System.currentTimeMillis();
        Environment.getLogger().debug("Building AwesomeWebPath To: " + thing);
        if (thing != null) {
            Player player = Players.getLocal();
            Coordinate pos;
            if (player != null && (pos = player.getPosition()) != null && thing.getPosition() != null) {
                int index;
                boolean equal = false;
                if(thing instanceof Area.Polygonal && prevThing instanceof Area.Polygonal) {
                    equal = ((Area.Polygonal) thing).getVertices().equals(((Area.Polygonal)thing).getVertices());
                }
                if ((equal || thing.equals(prevThing)) && prevPath != null && prevPath.size() > 50 && (index = getCachedIndex(prevPath, pos)) > -1) {
                    for (int i = 0; i < index; i++) {
                        prevPath.remove(0);
                    }
                    Environment.getLogger().debug("Using Cached Path... (" + (System.currentTimeMillis() - time) + "ms.)");
                    awesomePath.path(prevPath);
                    return true;
                } else {
                    Environment.getLogger().debug("Couldn't use Cached Path...");
                    awesomePath.setUseTeleports(useTeleports);
                    graph = awesomePath.buildHighLevelPath(thing, pos);
                    Environment.getLogger().debug("Generated High Level Path... (" + (System.currentTimeMillis() - time) + "ms.)");
                    if(graph == null) {
                        Environment.getLogger().debug("Failed to load graph");
                        return false;
                    }
                    List<Vertex> path = awesomePath.buildPath(graph, thing, pos);
                    if (path != null && !path.isEmpty()) {
                        prevPath = path;
                        Environment.getLogger().debug("Generated New Path... (" + (System.currentTimeMillis() - time) + "ms.)");
                        awesomePath.path(path);
                        prevThing = thing;
                        return true;
                    } else {
                        Environment.getLogger().warn("Could not build webpath to " + thing);
                    }
                }
            } else {
                Environment.getLogger().warn("Null argument in webPathTo");
            }
        }
        return false;
    }

    public boolean webPathTo(Landmark landmark) {
        long time = System.currentTimeMillis();
        AbstractBot bot = Environment.getBot();
        if (landmark != null && bot != null) {
            Player player = Players.getLocal();
            Coordinate pos;
            if (player != null && (pos = player.getPosition()) != null) {
                int index;
                if (landmark.equals(prevLandmark) && prevPath != null && prevPath.size() > 50 && (index = getCachedIndex(prevPath, pos)) > -1) {
                    for (int i = 0; i < index; i++) {
                        prevPath.remove(0);
                    }
                    Environment.getLogger().debug("Using Cached Path... (" + (System.currentTimeMillis() - time) + "ms.)");
                    awesomePath.path(prevPath);
                    return true;
                } else {
                    Environment.getLogger().debug("Couldn't use Cached Path...");
                    awesomePath.setUseTeleports(useTeleports);
                    graph = awesomePath.buildHighLevelPath(landmark, pos);
                    Environment.getLogger().debug("Generated High Level Path... (" + (System.currentTimeMillis() - time) + "ms.)");
                    if(graph == null) {
                        Environment.getLogger().warn("High Level Graph was null to: " + landmark);
                        if(((Loggable) bot).getLogCount() > 100) {
                            bot.stop("Pathing is continually trying to reach same position 100x.");
                        }
                        return false;
                    }
                    List<Vertex> path = awesomePath.buildPath(graph, landmark, pos);
                    if (path != null && !path.isEmpty()) {
                        prevPath = path;
                        Environment.getLogger().debug("Generated New Path... (" + (System.currentTimeMillis() - time) + "ms.)");
                        awesomePath.path(path);
                        prevLandmark = landmark;
                        return true;
                    } else {
                        Environment.getLogger().warn("Could not build webpath to " + landmark);
                        if(((Loggable) bot).getLogCount() > 100) {
                            bot.stop("Pathing is continually trying to reach same position 100x.");
                        }
                    }
                }
            } else {
                Environment.getLogger().warn("Null argument in webPathTo");
            }
        }
        return false;
    }

    private int getCachedIndex(List<Vertex> prevPath, Coordinate pos) {
        Environment.getLogger().debug("Searching for Cached index to use...");
        Vertex res = prevPath.subList(0, 20).stream().filter(vertex -> vertex.getCoordinate().equals(pos)).findFirst().orElse(null);
        if(res == null) {
            List<Vertex> sublist;
            if((sublist = prevPath.subList(0, 20)).stream().allMatch(vertex -> vertex instanceof CoordinateVertex)) {
                res = sublist.stream().filter(vertex -> vertex.getCoordinate().distanceTo(pos, Distance.Algorithm.EUCLIDEAN) <= 1).findFirst().orElse(null);
            }
        }
        if(res != null) {
            return prevPath.indexOf(res);
        }
        return -1;
    }

    public void setUseTeleports(boolean useTeleports) {
        this.useTeleports = useTeleports;
    }

    public int getDistanceBetween(Coordinate start, Coordinate end) {
        awesomePath.setUseTeleports(useTeleports);
        WeightedGraph tmp = awesomePath.buildHighLevelPath(end, start);
        if(tmp == null) {
            return Integer.MAX_VALUE;
        }
        List<Vertex> path = awesomePath.buildPath(tmp, end, start);
        if (path != null && !path.isEmpty()) {
            return path.get(path.size() - 1).getTotalCost();
        }
        return Integer.MAX_VALUE;
    }
}
