package com.regal.utility.awesome_navigation;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.GameObjectDefinition;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.entities.definitions.NpcDefinition;
import com.runemate.game.api.hybrid.local.Rune;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.osrs.local.RunePouch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class RegalItem {

    public static String getSelectedItemName() {
        SpriteItem selected = Inventory.getSelectedItem();
        if (selected != null) {
            ItemDefinition tempDef = selected.getDefinition();
            if (tempDef != null) {
                return tempDef.getName();
            }
        }
        return null;
    }

    public static boolean hasInventoryAction(Item item, String action) {
        if (item == null || action == null) {
            return false;
        }
        ItemDefinition definition = item.getDefinition();
        if (definition != null) {
            List<String> actions = definition.getInventoryActions();
            if (actions != null && !actions.isEmpty()) {
                return actions.contains(action);
            }
        }
        return false;
    }

    public static String getSpriteItemName(SpriteItem spriteItem) {
        ItemDefinition itemDefinition;
        String itemName;
        if (spriteItem != null) {
            if ((itemDefinition = spriteItem.getDefinition()) != null) {
                if ((itemName = itemDefinition.getName()) != null) {
                    return itemName;
                } else {
                    Environment.getBot().getLogger().warn("Cannot retrieve name: Name of of " + itemDefinition + " is null.");
                }
            } else {
                Environment.getBot().getLogger().warn("Cannot retrieve name: Definition of " + spriteItem + " is null.");
            }
        } else {
            Environment.getBot().getLogger().warn("Cannot retrieve name: SpriteItem input is null");
        }
        return null;
    }

    public static String getNameOf(Object item) {
        if (item == null) {
            return null;
        }
        if(item instanceof Item) {
            ItemDefinition itemDef;
            if ((itemDef = ((Item) item).getDefinition()) != null) {
                return itemDef.getName();
            }
        } else if(item instanceof GameObject) {
            GameObjectDefinition def = ((GameObject) item).getDefinition();
            if (def != null) {
                return def.getName();
            }
        }
        return null;
    }

    public static String getFirstAction(SpriteItem item) {
        if(item == null) {
            return null;
        }
        ItemDefinition def;
        if ((def = item.getDefinition()) != null) {
            List<String> actions;
            if (!(actions = def.getInventoryActions()).isEmpty()) {
                return actions.get(0);
            }
        }
        return null;
    }

    public static String getFirstAction(LocatableEntity item) {
        List<String> actions = getActions(item);
        if(actions != null && !actions.isEmpty()) {
            return actions.get(0);
        }
        return "";
    }

    public static List<String> getActions(Object object) {
        if(object instanceof GameObject) {
            GameObjectDefinition def = ((GameObject) object).getDefinition();
            GameObjectDefinition localState;
            ArrayList<String> combActions = new ArrayList<>();
            if(def != null) {
                localState = def.getLocalState();
                if(localState != null) {
                    combActions.addAll(localState.getActions());
                }
                combActions.addAll(def.getActions());
            }
            return combActions;
        } else if(object instanceof Npc) {
            NpcDefinition def = ((Npc) object).getDefinition();
            NpcDefinition localState;
            ArrayList<String> combActions = new ArrayList<>();
            if (def != null) {
                localState = def.getLocalState();
                if (localState != null) {
                    combActions.addAll(localState.getActions());
                }
                combActions.addAll(def.getActions());
            }
            return combActions;
        } else if(object instanceof GroundItem) {
            ItemDefinition def = ((GroundItem) object).getDefinition();
            return def != null ? def.getGroundActions() : new ArrayList<>();
        } else if(object instanceof SpriteItem) {
            ItemDefinition def = ((SpriteItem) object).getDefinition();
            if(Objects.equals(((SpriteItem) object).getOrigin(), SpriteItem.Origin.EQUIPMENT)) {
                return def != null ? def.getWornActions() : new ArrayList<>();
            }
            return def != null ? def.getInventoryActions() : new ArrayList<>();
        }
        return new ArrayList<>();
    }

    public static int getQuantityOfInventory(String... names) {
        int quantity;
        return (quantity = getQuantityOfRunePouch(names)) > 0 ? quantity : Inventory.newQuery().names(names).results().stream().filter(Objects::nonNull).map(SpriteItem::getQuantity).reduce(0, Integer::sum);
    }

    public static int getQuantityOfInventory(int id, String... names) {
        int quantity;
        return (quantity = getQuantityOfRunePouch(names)) > 0 ? quantity : Inventory.newQuery().ids(id).names(names).results().stream().filter(Objects::nonNull).map(SpriteItem::getQuantity).reduce(0, Integer::sum);
    }

    private static int getQuantityOfRunePouch(String[] names) {
        List<String> runeNames;
        if(!(runeNames = Arrays.stream(names).filter(name -> name.contains(" rune")).map(s -> s.replace(" rune", "").toUpperCase()).collect(Collectors.toList())).isEmpty()) {
            int quantity;
            if((quantity = runeNames.stream().mapToInt(runeName -> RunePouch.getQuantity(Rune.valueOf(runeName))).sum()) > 0) {
                return quantity;
            }
        }
        return 0;
    }

    public static int getQuantityOfInventory(Pattern... names) {
        return Inventory.newQuery().names(names).results().stream().filter(Objects::nonNull).map(SpriteItem::getQuantity).reduce(0, Integer::sum);
    }

    public static int getQuantityOfEquipment(String... names) {
        return Equipment.newQuery().names(Regex.getPatternsForContainsStrings(names)).results().stream().filter(Objects::nonNull).map(SpriteItem::getQuantity).reduce(0, Integer::sum);
    }

    public static int getPotionFullness(SpriteItem spriteItem) {
        String name;
        if((name = RegalItem.getNameOf(spriteItem)) != null) {
            int index;
            int secondInd;
            if(((index = name.indexOf('(')) > -1 && (secondInd = name.indexOf(')')) > -1) || ((index = name.lastIndexOf("fungicide spray ")) > -1 && (secondInd = Pattern.compile("\\d+").matcher(name).end()) > -1)) {
                try {
                    return Integer.parseInt(name.substring(index + 1, secondInd));
                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return 0;
    }
}
